module StudentCourseHelper
  def calc_actual_fee_need_to_paid student_course
    (student_course.course.tuition / student_course.course.number_of_days) *  (student_course.course.number_of_days - student_course.days_late)
  end

  def calc_actual_fee_need_to_paid_transfer(student_course, actual_day_studied)
    (student_course.course.tuition / student_course.course.number_of_days) *  (student_course.course.number_of_days - actual_day_studied)
  end
end