task spec: ["auth:db:test:prepare"]

namespace :auth do

  namespace :db do |ns|
    task :create do
      Rake::Task["db:create"].invoke
    end

    task :reset do
      Rake::Task["db:reset"].invoke
    end

    task :drop do
      Rake::Task["db:drop"].invoke
    end

    task :migrate do
      Rake::Task["db:migrate"].invoke
    end

    task :setup do
      Rake::Task["db:setup"].invoke
    end

    task :rollback do
      Rake::Task["db:rollback"].invoke
    end

    task :seed do
      Rake::Task["db:seed"].invoke
    end

    task :version do
      Rake::Task["db:version"].invoke
    end

    namespace :schema do
      task :load do
        Rake::Task["db:schema:load"].invoke
      end

      task :dump do
        Rake::Task["db:schema:dump"].invoke
      end
    end

    namespace :test do
      task :prepare do
        Rake::Task["db:test:prepare"].invoke
      end
    end

    # append and prepend proper tasks to all the tasks defined here above
    ns.tasks.each do |task|
      task.enhance ["auth:set_custom_config"] do
        Rake::Task["auth:revert_to_original_config"].invoke
      end
    end
  end

  task :set_custom_config do
    # save current vars
    @original_config = {
        env_schema: ENV['SCHEMA'],
        config: Rails.application.config.dup
    }

    # set config variables for custom database
    ENV['SCHEMA'] = "db_auth/schema.rb"
    Rails.application.config.paths['db'] = ["db_auth"]
    Rails.application.config.paths['db/migrate'] = ["db_auth/migrate"]
    Rails.application.config.paths['db/seeds.rb'] = ["db_auth/seeds.rb"]
    Rails.application.config.paths['config/database'] = ["config/database_auth.yml"]
  end

  task :revert_to_original_config do
    # reset config variables to original values
    ENV['SCHEMA'] = @original_config[:env_schema]
    Rails.application.config = @original_config[:config]
  end


end

task all_domain: :environment do
  db_configs = DbConfig.all
  db_configs.each do |db_config|
    p db_config.configs
    # config = db_config.configs
    # ActiveRecord::Base.establish_connection(config[Rails.env])
    # p "database status: #{ActiveRecord::Base.connection}"
    # p ActiveRecord::Base.connection.raw_connection.conninfo_hash
    # p Test.name


  end
end

