task :reset_all do
  Rake::Task["db:drop"].invoke
  Rake::Task["db:create"].invoke
  Rake::Task["db:migrate"].invoke
end

task database_setup: :environment do
  db_configs = DbConfig.all
  db_configs.each do |db_config|
    config = db_config.configs
    p "kiểm tra database #{config[:development][:database]}"
    ActiveRecord::Base.clear_active_connections!
    ActiveRecord::Base.establish_connection(config[Rails.env])
    if database_exists?
      Rake::Task["db:migrate"].invoke
      p "Chạy migration cho database #{p ActiveRecord::Base.connection.current_database}"
    else
      Rake::Task["db:create"].invoke#tạo database
      Rake::Task["db:migrate"].invoke #tạo bảng, tạo cột
      Rake::Task["db:seed"].invoke#tạo databsae
      p "setup database #{p ActiveRecord::Base.connection.current_database}"
    end
  end

end



def get_db_config
  db = DbConfig.where(domain: request.domain)
  if db.count == 0
    #TODO: chạy xuất lỗi
    return nil
  end
  config = db.first.configs
  return config
end

def database_exists?
  ActiveRecord::Base.connection
rescue ActiveRecord::NoDatabaseError
  false
else
  true
end