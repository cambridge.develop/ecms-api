class AuthMailer < ApplicationMailer

  def change_password(user)
    p 'gửi mail'
    @user = user
    mail to: @user.email, subject: "Tài khoản #{@user.email} của bạn đã được thay đổi mật khẩu"
  end
  #
  # def request_account_confirmation(user)
  #   @user = user
  #   mail to: @user.email, subject: "Tài khoản #{@user.email} của bạn cần được kích hoạt"
  # end
  #
  # def account_confirmed(user)
  #   @user = user
  #   mail to: @user.email, subject: "Tài khoản #{@user.email} của bạn đã được kích hoạt"
  # end

  def reset_password user
    @user = user
    mail to: @user.email, subject: "Tài khoản #{@user.email} của bạn được yêu cầu khôi phục mật khẩu"
  end
end