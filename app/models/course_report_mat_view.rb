class CourseReportMatView < ActiveRecord::Base
  self.table_name = 'course_reports_matview'
  has_many :students, through: :student_courses
  belongs_to :teacher, class_name: 'Teacher', foreign_key: :teacher_id
  belongs_to :second_teacher, class_name: 'Teacher', foreign_key: :second_teacher_id, optional: true
  belongs_to :branch, class_name: 'Branch', foreign_key: :branch_id
  belongs_to :curriculum, class_name: 'Curriculum', foreign_key: :curriculum_id
  has_one :course_schedule
  has_many :student_courses, ->{where is_cancel: false }, class_name: 'StudentCourse'
  has_many :attendance_courses, ->{ order(created_at: :desc) }

  def readonly?
    true
  end

  def self.refresh
    ActiveRecord::Base.connection.execute('REFRESH MATERIALIZED VIEW course_reports_matview')
  end
end
