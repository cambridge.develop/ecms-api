class StudentCertificate < MainRecordBase
  belongs_to :certificate, class_name: 'Certificate', foreign_key: :certificate_id
  belongs_to :student, class_name: 'Student', foreign_key: :student_id
end
