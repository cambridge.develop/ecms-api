class Literacy < MainRecordBase
  validates :code, uniqueness: true, presence: true
  validates :name, presence: true
  has_many :schools, class_name: 'School', dependent: :restrict_with_error
  has_many :students, through: :schools, dependent: :restrict_with_error
end
