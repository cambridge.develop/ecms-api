class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  # after_save :logging_for_save


  private
    def logging_for_update
      if self.changed?
        self.changed.each do |attr|
          p attr
          p self.attributes[attr]
          p self.changed_attributes[attr]
        end
      end
    end

  def logging_for_save
    if self.changed?

    else

    end
  end

end
