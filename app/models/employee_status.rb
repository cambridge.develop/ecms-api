class EmployeeStatus < MainRecordBase
  validates :code, uniqueness: true, presence: true
  validates :name, presence: true
  has_many :employees, class_name: 'Employee'

  scope :active, ->{find_by(code: 'active')}

  def self.codes
      {
        new: 'New',
        active: 'Active',
        resign: 'Resign',
      }
  end

  def self.humanize_status key
      case key.underscore.to_sym
      when :active
        "Đang làm việc"
      when :resign
        "Đã thôi việc"
      else
        "none"
      end
  end

  def name_with_locale
    case self.code.to_sym
    when :active
      "Đang làm việc"
    when :resign
      "Đã thôi việc"
    else
      "none"
    end
  end

end