class ReceiptV2 < MainRecordBase
  self.table_name = 'receipts'
  belongs_to :student_course_v2, foreign_key: :student_course_id
  has_one :course_v2, class_name: 'CourseV2', through: :student_course_v2
  belongs_to :gift, class_name: 'Gift', foreign_key: :gift_id, optional: true
  has_one :student, class_name: 'Student', through: :student_course_v2
  belongs_to :creator, foreign_key: :creator_id, class_name: "User"
end
