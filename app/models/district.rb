class District < MainRecordBase
    has_many :wards, class_name: "ward", foreign_key: :ward_id, dependent: :restrict_with_error
    # has_many :hoc_sinhs, class_name: "HocSinh", foreign_key: :noi_sinh_huyen_id , dependent: :restrict_with_error

    belongs_to :province, class_name: "Province", foreign_key: :province_code

    validates :code, uniqueness: true, presence: true
    validates :name, presence: true
end
