class MainRecordBase < ApplicationRecord
  self.abstract_class = true

  connects_to database: { writing: :main, reading: :main }


  PHONE_FORMAT = /\A(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}\z/

  def self.not_found_message
    I18n.t("activerecord.errors.messages.record_not_found", model: self.model_name.human.downcase)
  end

  def self.invalid_message
    I18n.t("activerecord.messages.record_invalid", model: self.model_name.human)
  end

  def self.locked_message
    I18n.t("activerecord.messages.record_locked", model: self.model_name.human)
  end

  def self.lock_success_message
    I18n.t("activerecord.messages.lock_success", model: self.model_name.human.downcase)
  end

  def self.unlock_success_message
    I18n.t("activerecord.messages.unlock_success", model: self.model_name.human.downcase)
  end

  def self.create_success_message
    I18n.t("activerecord.messages.create_success", model: self.model_name.human.downcase)
  end

  def self.exist_record_reference_message(model1, model2)
    I18n.t("activerecord.errors.messages.record_reference", model1: model1.model_name.human, model2: model2.model_name.human.downcase)
  end

  def self.destroy_success_message param
    I18n.t("activerecord.messages.destroy_success", model: self.model_name.human.downcase, name: param)
  end

  def self.update_success_message param
    I18n.t("activerecord.messages.update_success", attribute: self.human_attribute_name(param).downcase)
  end

  def self.add_reference_success_message(model1, model2)
    I18n.t("activerecord.successes.messages.add_reference", model1: model1.model_name.human, model2: model2.model_name.human.downcase)
  end

  def self.destroy_reference_success_message(model1, model2)
    I18n.t("activerecord.successes.messages.destroy_reference", model1: model1.model_name.human, model2: model2.model_name.human.downcase)
  end


  def self.destroy_reference_error_message(model1, model2)
    I18n.t("activerecord.errors.messages.destroy_reference", model1: model1.model_name.human, model2: model2.model_name.human.downcase)
  end

  def self.not_found_model_message model
    I18n.t("activerecord.errors.messages.record_not_found", model: model.model_name.human.downcase)
  end

  def self.notify_course_end_message(name, date)
    I18n.t("activerecord.messages.notify_course_end", name: name, date: date)
  end

  def self.notify_course_ended_message(name, date)
    I18n.t("activerecord.messages.notify_course_end", name: name, date: date)
  end
end