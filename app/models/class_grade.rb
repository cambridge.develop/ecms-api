class ClassGrade < MainRecordBase
  belongs_to :branch, class_name: 'Branch', foreign_key: :branch_id
  belongs_to :curriculum, class_name: 'Curriculum'
  delegate :level, :to => :curriculum
  has_many :courses, -> { order(:course_no => :asc) }, class_name: 'Course', dependent: :restrict_with_error
  has_many :courses_opening, -> { where(course_status_id: CourseStatus.open.id).order(:course_no => :asc) }, class_name: 'Course', dependent: :restrict_with_error
  has_many :student_courses, class_name: 'StudentCourse', through: :courses
  validates :name, presence: true
  # validates :code, presence: true, uniqueness: true
  validates_numericality_of :number_of_courses, greater_than_or_equal_to: 0
  scope :not_closed, -> {where(('start_date > ? OR end_date >= ?'), Date.today, Date.today)}
  scope :closed, -> {where(('end_date < ?'), Date.today)}
  scope :att_not_closed, -> {where(is_end:false)}
  scope :att_closed, -> {where(is_end:true)}
  attribute :paid_amount

  def paid_amount
    sum = 0
    self.courses.each do |course|
      sum += course.tuition
    end
    return sum
  end

  def cancel
    result = true
    ActiveRecord::Base.transaction do
      self.is_end = true
      self.courses_opening.each do |course|
        course_cancel_result = course.cancel
        unless course_cancel_result
          result = false
          raise ActiveRecord::Rollback
          break
        end
      end
    end
    unless self.save
      result = false
    end
    result
  end


  private
  def self.class_grade_was_closed_message
    I18n.t("controllers.class_grade.class_grade_was_closed")
  end

  def self.close_successfully_message
    I18n.t("controllers.class_grade.close_successfully")
  end
  def self.some_course_not_finished_message
    I18n.t("controllers.class_grade.some_course_not_finished")
  end

  def self.class_grade_end_date_less_than_now_message
    I18n.t("controllers.class_grade.class_grade_end_date_less_than_now")
  end

  def self.delete_when_course_exist_message
    I18n.t("controllers.class_grade.delete_when_course_exist")
  end
end
