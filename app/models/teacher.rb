class Teacher < MainRecordBase
  belongs_to :teacher_status, class_name: 'TeacherStatus', foreign_key: :teacher_status_id, optional: true
  validates :teacher_status, presence: true
  belongs_to :user, class_name: 'User', foreign_key: :user_id, optional: true
  validates :user, presence: true
  has_many :teacher_courses, class_name: 'TeacherCourse', dependent: :restrict_with_error
  has_many :courses, through: :teacher_courses
  has_one :gender, class_name: 'Gender', through: :user
  has_many :class_grades, class_name: 'ClassGrade', through: :courses
  has_many :user_branches, through: :user
  has_many :branches, class_name: 'Branch', through: :user_branches
  before_create do
    last_teacher = Teacher.order(created_at: :desc).select(:code).first
    if last_teacher
      self.code = "GV-#{(last_teacher.code.split('-')[1].to_i + 1).to_s.rjust(6, '0')}"
    else
      self.code = "GV-#{'1'.rjust(6, '0')}"
    end
  end

  def lock
    result = true
    MainRecordBase.transaction do
      self.teacher_status_id = TeacherStatus.find_by_code("locked").id
      @user = self.user
      @user.lock_at = DateTime.now
      unless @user.save!
        result = false
        raise ActiveRecord::Rollback
      end
      unless self.save!
        result = false
      end
    end
    result
  end
end