class UserGroup < MainRecordBase
  belongs_to :user_type
  belongs_to :branch, class_name: "Branch", foreign_key: :branch_id
  has_many :permission_and_user_groups, class_name: 'PermissionAndUserGroup'
  has_many :permissions, :through => :permission_and_user_groups, dependent: :restrict_with_error
  has_many :user_and_user_groups, dependent: :restrict_with_error
  has_many :users, through: :user_and_user_groups
end