class Branch < MainRecordBase

  has_many :user_groups, class_name: 'UserGroup'
  # has_many :branch_approvers, class_name: 'BranchApprover'
  has_many :user_branches, class_name: 'UserBranch', dependent: :restrict_with_error
  belongs_to :manager, class_name: 'User', foreign_key: :manager_id, optional:true

  has_many :users, :through => :user_branches
  has_many :class_grades, class_name: 'ClassGrade'
  has_many :courses, through: :class_grades

  validates_presence_of :name, :address, :phone, :email
  validates :phone, length: {maximum: 15}
  validates :code, uniqueness: true, presence: true

  validates :email, uniqueness: true, if: :should_validate_email?
  validates_format_of :email, with: URI::MailTo::EMAIL_REGEXP

  validate :check_exist_manager

  def check_exist_manager
    if !User.where(id: manager_id).exists?
      errors.add(:manager, :not_exist)
    end
  end

  def should_validate_email?
    new_record? || email.present?
  end

end
