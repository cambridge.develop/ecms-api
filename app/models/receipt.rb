class Receipt < MainRecordBase
  belongs_to :student_course
  has_one :course, class_name: 'Course', through: :student_course
  has_one :student, class_name: 'Student', through: :student_course
  belongs_to :creator, foreign_key: :creator_id, class_name: "User"
  scope :not_cancel, -> {where(is_cancel: false)}

  validates :student_course_id,
            :paid_person_name,
            :actual_paid_amount,
            :paid_amount,
            :money_in_words,
            :creator_id,
            presence: true
  # validates :student_course_id, uniqueness: { scope: :is_paid_for_class }
  validates :paid_amount, numericality: { greater_than_or_equal_to: 0 }
  validates_numericality_of :discount_number, less_than_or_equal_to: :paid_amount, if: :validate_paid_amount
  validates_numericality_of :discount_percentage, less_than_or_equal_to: 100, greater_than_or_equal_to: 0
  validates_numericality_of :credit_used, greater_than_or_equal_to: 0, less_than_or_equal_to: :student_credit
  validates_numericality_of :actual_paid_amount
  validate :validate_receipt_data, if: :validate_paid_amount && :validate_credit_used, on: :create

  def student_credit
    self.student.credit
  end

  def validate_paid_amount
    self.errors[:paid_amount].blank?
  end

  def validate_credit_used
    self.errors[:credit_used].blank?
  end

  def validate_receipt_data
    unless self.is_paid_for_class
      # errors.add(:paid_amount, :invalid) unless (self.paid_amount == self.student_course.actual_tuition_need_to_paid)
      amount_to_pay = self.student_course.actual_tuition_need_to_paid - self.student_course.credit
      errors.add(:paid_amount, :invalid) if (self.paid_amount > amount_to_pay)
      discount_amount = self.discount_number + self.credit_used
      errors.add(:actual_paid_amount, :invalid) if (self.actual_paid_amount != (self.paid_amount - discount_amount))
    end
  end

  def cancel(no_refund_newest_receipt = false)
    result = false
    messages = [I18n.t('controllers.receipt.errors.cancel_receipt_fail')]
    receipt = self
    student_course = self.student_course
    if receipt.is_cancel
      return [false, [I18n.t('controllers.receipt.errors.cancel_receipt_fail')]]
    end
    newest_receipt = student_course.receipt_all.order(created_at: :desc).first
    ActiveRecord::Base.transaction do
      receipt.is_cancel = true
      is_newest_receipt = true
      if receipt != newest_receipt or no_refund_newest_receipt == false or receipt.is_paid_for_class
        redundant_money = (receipt.actual_paid_amount.abs + receipt.credit_used.abs)
        is_newest_receipt = false # không refund receipt mới nhất
      else
        redundant_money = receipt.credit_used.abs
      end
      is_receipt_of_course_first = false
      if receipt.is_paid_for_class
        # lấy thông tin lớp, khóa học đầu tiên
        class_grade = ClassGrade.where(id: student_course.course.class_grade_id).first
        first_course = class_grade.courses.where(course_no: 1).first unless class_grade.blank?
        student_course_of_first_course = StudentCourse.where(student_id: student_course.student.id, course_id: first_course.id, is_cancel: false).first
        # nếu tồn tại khóa học đầu tiên thực hiện xóa hóa đơn cũ, tiến hành tạo hóa đơn mới và cập nhật credit trong student_course
        unless student_course_of_first_course.blank?
          # Lấy thông tin của hóa đơn cho khóa học đầu tiên
          receipt_student_course_of_first_course = Receipt.where(student_course_id: student_course_of_first_course.id, is_cancel: false)
          is_receipt_of_course_first = true if receipt == receipt_student_course_of_first_course.first
          # nếu hóa đơn hủy là hóa đơn đầu tiên hoặc đã được bù tiền từ việc hủy khóa học khác thì không cập nhật, tiến hành hủy hóa đơn như bình thường
          unless receipt_student_course_of_first_course.blank?
            unless (is_receipt_of_course_first || receipt_student_course_of_first_course.where.not(actual_paid_amount: receipt_student_course_of_first_course.first.paid_amount).first.blank?)
              receipt_student_course_of_first_course.update(is_cancel: true)
              new_receipt_student_course_of_first_course = receipt_student_course_of_first_course.first.dup
              new_credit_of_first_course = student_course_of_first_course.credit - new_receipt_student_course_of_first_course.discount_number
              student_course_of_first_course.update(discount_amount: 0,credit: new_credit_of_first_course, is_add_when_paid: false)
              # update new receipt
              new_receipt_student_course_of_first_course.paid_amount = new_credit_of_first_course
              new_receipt_student_course_of_first_course.actual_paid_amount = new_credit_of_first_course - new_receipt_student_course_of_first_course.credit_used
              new_receipt_student_course_of_first_course.discount_number = 0
              new_receipt_student_course_of_first_course.discount_percentage = 0
              new_receipt_student_course_of_first_course.is_cancel = false
              # new_receipt_student_course_of_first_course.is_paid_for_class = false
              new_receipt_student_course_of_first_course.description_internal << '<Hóa đơn mới được tạo thay thế cho hóa đơn cũ khi hủy hóa đơn khóa học đóng theo lớp>'
              unless new_receipt_student_course_of_first_course.save
                return [result, messages << new_receipt_student_course_of_first_course.errors.full_messages]
              end
            end
          end
        end
      end
      student_result, _, _ = student_course.student.deposit_credit(redundant_money)
      unless student_result
        raise ActiveRecord::Rollback
      end

      student_course_result, _, _ = student_course.withdraw_credit(receipt.actual_paid_amount.abs + receipt.discount_number.abs + receipt.credit_used.abs)
      unless student_course_result
        raise ActiveRecord::Rollback
      end

      #cập nhật discount_amount
      new_discount_amount = student_course.discount_amount - receipt.discount_number.abs
      student_course.update!(discount_amount: new_discount_amount)

      if receipt.save
        result = true
        if is_newest_receipt
          return [result, [I18n.t('controllers.receipt.successes.cancel_receipt_success')]]
        else
          return [result, [I18n.t('controllers.receipt.successes.cancel_receipt_with_extra_money_success')]]
        end
      else
        return [result, messages << receipt.errors.full_messages]
      end
    end
    [result, messages]
  end

  def self.calc_actual_fee_need_to_paid student_course
    (student_course.course.tuition / student_course.course.number_of_days) * (student_course.course.number_of_days - student_course.days_late)
  end

  def self.calc_actual_fee_need_to_paid_transfer(student_course, actual_day_studied)
    (student_course.course.tuition / student_course.course.number_of_days) *  (student_course.course.number_of_days - actual_day_studied)
  end

  def self.create_with_class receipt_params, current_user
    result = false
    messages = []
    student = Student.find_by_id(receipt_params[:student_id])
    class_grade = ClassGrade.find_by_id(receipt_params[:class_grade_id])
    if student.blank?
      result = false
      messages << Student.not_found_message
      return [result, messages]
    end
    if class_grade.blank?
      result = false
      messages << ClassGrade.not_found_message
      return [result, messages]
    end
    credit_used = receipt_params[:credit_used].to_s.gsub(/\D/, '').to_i.abs
    discount_number = receipt_params[:discount_number].to_s.gsub(/\D/, '').to_i.abs
    actual_paid_amount = receipt_params[:actual_paid_amount].to_s.gsub(/\D/, '').to_i.abs
    days_late = receipt_params[:days_late].to_s.gsub(/\D/, '').to_i.abs
    class_paid_amount = receipt_params[:paid_amount].to_s.gsub(/\D/, '').to_i.abs

    course_first = class_grade.courses.where(course_no: 1).first
    days_late_amount = (course_first.tuition / course_first.number_of_days) * days_late

    # if (class_paid_amount != class_grade.paid_amount - days_late_amount) || actual_paid_amount != (class_paid_amount - discount_number - credit_used)
    # if actual_paid_amount != (class_paid_amount - discount_number - credit_used)
    #   result = false
    #   messages << I18n.t('activerecord.messages.error')
    #   return [result, messages]
    # end
    MainRecordBase.transaction do
      discount_course_no = 1
      class_grade.courses.includes(:course_status).each do |course|
        if course.course_status.code == 'cancel'
          next
        end
        # update student_course
        exist_student_course = StudentCourse.where(student_id: student.id, course_id: course.id, is_cancel: false).first
        if exist_student_course.present?
          if course.course_no == 1 # update days late for course ( only first )
            exist_student_course.update!(days_late: days_late)
            exist_student_course.update!(credit: exist_student_course.actual_tuition_need_to_paid, days_late: days_late, discount_amount: discount_number)
          else
            exist_student_course.update!(credit: exist_student_course.actual_tuition_need_to_paid)
          end
        else
          if course.course_no == 1 # update days late for course ( only first )
            new_credit = (course.tuition / course.number_of_days) * (course.number_of_days - days_late)
            StudentCourse.create!(student_id: student.id, course_id: course.id, credit: new_credit, days_late: days_late, is_add_when_paid: true, start_date: Date.today, discount_amount: discount_number)
          else
            new_credit = course.tuition
            StudentCourse.create!(student_id: student.id, course_id: course.id, credit: new_credit, is_add_when_paid: true, start_date: Date.today)
          end
        end
        # create receipt info
        student_course = course.student_courses.where(student_id: student.id).first
        receipt = Receipt.new receipt_params.except(:student_course_id, :class_grade_id, :days_late, :student_id)
        receipt.student_course_id = student_course.id
        receipt.is_paid_for_class = true
        receipt.creator_id = current_user.id
        # update paid_amount
        paid_amount = student_course.actual_tuition_need_to_paid
        receipt.paid_amount = paid_amount
        receipt.gift_id = nil unless course.course_no == 1
        # update discount_number
        if course.course_no != 1
          receipt.discount_number = 0
          receipt.discount_percentage = 0
        end
        # update credit_used
        if credit_used > paid_amount - receipt.discount_number
          receipt_credit_used = paid_amount - receipt.discount_number
        else
          receipt_credit_used = credit_used
        end
        credit_used = credit_used - receipt_credit_used
        receipt.credit_used = receipt_credit_used
        receipt.actual_paid_amount = paid_amount - receipt.discount_number - receipt_credit_used
        unless receipt.save
          result = false
          messages << receipt.errors.full_messages
          raise ActiveRecord::Rollback
          return [result, messages]
        end
        # update student with new credit if credit used
        new_credit_student = student.credit - receipt_credit_used
        unless student.update!(credit: new_credit_student)
          result = false
          messages << student.errors.full_messages
          raise ActiveRecord::Rollback
          return [result, messages]
        end
        class_paid_amount -= receipt.paid_amount
      end
      if class_paid_amount == 0
        result = true
        messages << Receipt.create_success_message
      else
        result = false
        messages << I18n.t('activerecord.messages.error')
        raise ActiveRecord::Rollback
        return [result, messages]
      end
    rescue ActiveRecord::RecordInvalid => invalid
      result = false
      messages << invalid.record.errors.full_messages
      raise ActiveRecord::Rollback
      return [result, messages]
    end
    [result, messages]
  end
end
