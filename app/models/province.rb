class Province < MainRecordBase
    # has_many :hoc_sinhs, class_name: "HocSinh", foreign_key: :noi_sinh_tinh_tp_id, dependent: :restrict_with_error
    has_many :districts, class_name: "District", foreign_key: :district_id, dependent: :restrict_with_error

    validates :code, uniqueness: true, presence: true
    validates :name, presence: true
end
