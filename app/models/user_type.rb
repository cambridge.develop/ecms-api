class UserType < MainRecordBase
  has_many :users, class_name: "User"

  scope :employee, ->{find_by(code: 'employee')}
  scope :teacher, ->{find_by(code: 'teacher')}
  scope :student, ->{find_by(code: 'student')}
  scope :par, ->{find_by(code: 'parent')}
  scope :admin, ->{find_by(code: 'admin')}
  scope :with_teacher_and_employee_types, ->{where("code = ? OR code = ?", self.codes[:employee], self.codes[:teacher])}

  def self.codes
    {
      employee: 'employee',
      teacher: 'teacher',
      student: 'student',
      parent: 'parent',
      admin: 'admin'
    }
  end

  def is_admin?
    self[:code] == 'admin'
  end

  def is_employee?
    self[:code] == 'employee'
  end
  
  def is_teacher?
    self[:code] == 'teacher'
  end
  
  def is_student?
    self[:code] == 'student'
  end

  def is_parent?
    self[:code] == 'parent'
  end

end