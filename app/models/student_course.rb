class StudentCourse < MainRecordBase
  attribute :is_paid
  attribute :is_paid_enough
  attribute :actual_tuition_need_to_paid, :boolean, default: :actual_tuition_need_to_paid
  attribute :debt, default: :debt
  has_one :receipt, ->{ order(created_at: :desc) }

  has_many :receipts, ->{where(is_cancel: false) }, class_name: 'Receipt', foreign_key: :student_course_id
  has_many :receipt_all, class_name: 'Receipt', foreign_key: :student_course_id

  belongs_to :course, class_name: 'Course', foreign_key: :course_id
  belongs_to :transfer_from_course, class_name: 'Course', foreign_key: :transfer_from_course_id, optional: true
  belongs_to :student, class_name: 'Student', foreign_key: :student_id

  def is_paid?
    self.receipt.present? or self.credit > 0
  end

  def is_paid_enough?
    actual_tuition_need_to_paid <= self.credit
  end

  def is_paid
    self.is_paid?
  end

  def is_paid_enough
    self.is_paid_enough?
  end
  def self.set_paid_status data_list
    data_list.each do |data|
      data.is_paid = data.is_paid?
      data.is_paid_enough = data.is_paid_enough?
    end
  end

  def actual_tuition_need_to_paid
    (self.course.tuition / self.course.number_of_days) * (self.course.number_of_days - self.days_late)
  end

  def actual_tuition_need_to_paid_for_learned learned_days = 0
    (self.course.tuition / self.course.number_of_days) * learned_days.to_i
  end

  def is_student_course_enough_credit credit_used
    self.credit >= credit_used
  end

  def debt
    debt = self.actual_tuition_need_to_paid - self.credit
    if debt > 0
      debt
    else
      0
    end
  end

  def deposit_credit(credit)
    self.with_lock do
      self.reload
      self.credit += credit.abs
      if self.save
        [true, self.credit, [I18n.t('controllers.student_course.successes.deposit_credit')]]
      else
        [false, self.credit, self.errors.full_messages]
      end
    end
  end

  def withdraw_credit(credit)
    self.with_lock do
      self.reload
      return [false, self.credit, [I18n.t('controllers.student_course.errors.withdraw_credit')]] if self.credit < credit
      self.credit -= credit.abs
      if self.save
        [true, self.credit, [I18n.t('controllers.student_course.successes.withdraw_credit')]]
      else
        [false, self.credit, self.errors.full_messages]
      end
    end
  end

  def cancel
    # receipts ở đây ko bao gồm receipt đã cancel
    result = true
    ActiveRecord::Base.transaction do
      self.is_cancel = true
      self.receipts.each do |receipt|
        receipt_result, _ = receipt.cancel(false)
        unless receipt_result
          result = false
          raise ActiveRecord::Rollback
          break
        end
      end
      unless self.save
        result = false
      end
    end
    result
  end

  def learned_days
    # Số ngày học sẽ tính trên các phiếu điểm danh đã tạo (bao gồm cả vắng và vắng không phép)
    learned_days = 0
    attendance_courses = self.course.attendance_courses
    attendance_courses.each do |attendance_course|
      attendance_list = attendance_course.attendance_list
      student_list = attendance_list.map{|item| item["student_id"]}
      learned_days += 1 if student_list.include?(self.student_id)
    end
    learned_days
  end

  def self.is_transferred_message
    I18n.t("activerecord.errors.models.student_course.is_transferred")
  end

  def self.is_cancelled_message
    I18n.t("activerecord.errors.models.student_course.is_cancelled")
  end

  def self.cancel_trial_success_message
    I18n.t("activerecord.errors.models.student_course.cancel_trial_success")
  end
end
