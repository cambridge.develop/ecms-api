class Holiday < MainRecordBase
    belongs_to :holiday_type, class_name: "HolidayType"
    belongs_to :branch, class_name: "Branch", optional: true

    def formatted_date_start
        self[:date_start]
    end

    def formatted_date_end
        self[:date_end]
    end
end
