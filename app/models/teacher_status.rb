class TeacherStatus < MainRecordBase
    validates :code, uniqueness: true, presence: true
    validates :name, presence: true
    has_many :teachers, class_name: 'Teacher'
    def self.codes
        {
          new: 'New',
          active: 'Active',
          locked: 'Locked',
          resign: 'Resign',
        }
    end

    def self.humanize_status key
        case key.underscore.to_sym
        when :active
            I18n.t('controllers.teacher_status.enum.active')
        when :resign
            I18n.t('controllers.teacher_status.enum.resign')
        when :locked
            I18n.t('controllers.teacher_status.enum.locked')
        else
          "none"
        end
    end

    def name_with_locale
        case self.code.to_sym
            when :active
                "Đang làm việc"
            when :resign
                "Đã thôi việc"
            else
                "none"
        end
    end

end
