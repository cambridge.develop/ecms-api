class PermissionAndUserGroup < MainRecordBase
  belongs_to :permission, foreign_key: :permission_id
  belongs_to :user_group, foreign_key: :user_group_id
end
