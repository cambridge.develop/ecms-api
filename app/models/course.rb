class Course < MainRecordBase
  has_many :students, through: :student_courses
  # has_many :teachers, through: :teacher_courses
  # validates :code, uniqueness: true
  # belongs_to :teacher, class_name: 'Teacher', foreign_key: :teacher_id
  # belongs_to :second_teacher, class_name: 'Teacher', foreign_key: :second_teacher_id, optional: true
  # belongs_to :branch, class_name: 'Branch', foreign_key: :branch_id
  belongs_to :class_grade, class_name: 'ClassGrade', foreign_key: :class_grade_id
  belongs_to :course_status, class_name: 'CourseStatus', foreign_key: :course_status_id
  # belongs_to :curriculum, class_name: 'Curriculum', foreign_key: :curriculum_id
  has_one :course_schedule, dependent: :destroy
  has_many :student_courses, ->{where(is_cancel: false)}, class_name: 'StudentCourse', dependent: :restrict_with_error
  has_many :student_courses_studying, ->{where(is_free_trial: false, is_cancel: false, is_transferred: false)}, class_name: 'StudentCourse', dependent: :restrict_with_error
  has_many :student_courses_free_trial, ->{where(is_free_trial: true, is_cancel: false, is_transferred: false)}, class_name: 'StudentCourse', dependent: :restrict_with_error
  has_many :student_courses_history, ->{where(is_cancel: true).or(where(is_transferred: true)) }, class_name: 'StudentCourse', dependent: :restrict_with_error

  has_many :teacher_courses, class_name: 'TeacherCourse', dependent: :restrict_with_error
  has_many :students, through: :student_courses, dependent: :restrict_with_error
  has_many :teachers, through: :teacher_courses, dependent: :restrict_with_error
  has_many :attendance_courses, ->{ order(created_at: :desc) }

  # has_one :course_schedule, dependent: :restrict_with_error

  # scope :not_closed, -> {where('(start_date > ? AND end_date > ?) OR (start_date <= ? AND ? <= end_date)', Date.today, Date.today, Date.today, Date.today)}
  # scope :closed, -> {where('start_date < ? AND end_date < ? ', Date.today, Date.today)}

  scope :not_closed, -> {where(('start_date > ? OR end_date >= ?'), Date.today, Date.today)}
  scope :closed, -> {where(('end_date < ?'), Date.today)}
  validates_presence_of :code, :name, :start_date, :end_date, :tuition, :number_of_days, :number_of_students
  validates_format_of :number_of_days, :number_of_students, with: /\A\d+\z/
  validates :tuition, :numericality =>{:greater_than => 0}
  validates :number_of_students, :numericality =>{:greater_than => 0}
  # validates :number_of_days, :numericality =>{:greater_than => 0}
  def cancel
    # student course ở đây ko bao gồm student course đã cancel
    result = true
    ActiveRecord::Base.transaction do
      self.course_status_id = CourseStatus.cancel.id
      self.student_courses_studying.each do |student_course|
        student_course_result = student_course.cancel
        unless student_course_result
          result = false
          raise ActiveRecord::Rollback
          break
        end
      end
      unless self.save
        result = false
      end
    end
    result
  end

  def self.status_not_open_message
    I18n.t("controllers.course.status_not_open")
  end

  def self.delete_when_student_course_exist_message
    I18n.t("controllers.course.delete_when_student_course_exist")
  end

end
