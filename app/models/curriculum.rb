class Curriculum < MainRecordBase
  # validates :code, uniqueness: true, presence: true
  validates :name, presence: true

  validates :number_of_courses, :numericality => {:greater_than_or_equal_to => 0}
  validates :days_per_course, :numericality => {:greater_than_or_equal_to => 0}
  validates :fee_per_course, :numericality => {:greater_than_or_equal_to => 0}
  belongs_to :level, class_name: 'Level', foreign_key: :level_id, optional: true
  validates :level, presence: true
  has_many :class_grades, class_name: 'ClassGrade'
  # has_many :courses, class_name: 'Course', dependent: :restrict_with_error
  # belongs_to :major, class_name: 'Major', foreign_key: :major_id
end
