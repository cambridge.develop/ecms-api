class User < MainRecordBase

  # include Devise::JWT::RevocationStrategies::Whitelist
  include Util

  module MyCustomStrategy
    # def self.jwt_revoked?(payload, user)
    #   # Does something to check whether the JWT token is revoked for given user
    # end
    #
    # def self.revoke_jwt(payload, user)
    #   # Does something to revoke the JWT token for given user
    # end
  end

  attr_accessor :selected_branch
  attr_accessor :current_password
  attribute :is_locked
  validates :password, :presence => true,
            :confirmation => true,
            :length => {:within => 6..40},
            :on => :create
  validates :password, :confirmation => true,
            :length => {:within => 6..40},
            :allow_blank => true,
            :on => :update
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable, :confirmable
  devise :database_authenticatable, :rememberable,
         :trackable,
         :jwt_authenticatable, jwt_revocation_strategy: self, authentication_keys: [:login]

  attr_accessor :login

  validates_format_of :username, with: /^[a-zA-Z0-9_\.]*$/, :multiline => true
  validate :validate_username
  validates :username, presence: :true, uniqueness: { case_sensitive: false }
  validates :email, uniqueness: true, if: :should_validate_email?
  validates_format_of :email, with: URI::MailTo::EMAIL_REGEXP, if: :should_validate_email?
  # validates :phone, uniqueness: true

  belongs_to :gender, class_name: 'Gender', foreign_key: :gender_id, optional: true
  validates :gender, presence: true
  belongs_to :user_type, class_name: 'UserType', foreign_key: :user_type_id
  # belongs_to :student, class_name: 'Student', optional: true
  # belongs_to :teacher, class_name: 'Teacher', optional: true

  has_many :user_branches, class_name: 'UserBranch', dependent: :restrict_with_error
  has_many :branches, through: :user_branches
  has_many :user_and_user_groups

  has_many :whitelisted_jwts, dependent: :destroy

  has_one :teacher
  has_one :student
  has_one :employee

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      if conditions[:username].nil?
        where(conditions).first
      else
        where(username: conditions[:username]).first
      end
    end
  end

  def login
    @login || self.username || self.email
  end

  def should_validate_email?
    !email.blank?
  end

  def validate_username
    if User.where(email: username).exists?
      errors.add(:username, :invalid)
    end
  end

  def is_locked?
    self.locked_at.present?
  end

  def is_locked
    self.is_locked?
  end
  def generate_user
    new_username = I18n.transliterate(self.full_name, locale: "vi").downcase
    new_username = generate_username(new_username)
    new_username = find_unique_username(new_username)
    random_pass = random_password
    self.username = new_username
    self.initial_password = random_pass
    self.password = random_pass
    self.must_change_password = true
  end

  def generate_reset_password_token
    self.reset_password_token = SecureRandom.hex(32)
    self.reset_password_sent_at = DateTime.now
  end

  def password_token_valid?
    (self.reset_password_sent_at + 6.hours) > Time.now.utc
  end

  def self.reset_password_token_invalid_message
    I18n.t("activerecord.errors.models.user.reset_password_token_invalid")
  end

  def self.jwt_revoked?(payload, user)
    if !user.whitelisted_jwts.exists?(payload.slice('jti', 'aud'))
      true
    else
      current_jti = WhitelistedJwt.find_by_jti(payload.slice('jti')['jti'])
      current_jti.exp = DateTime.now
      current_jti.save
      false
    end
  end

  def self.revoke_jwt(payload, user)
    jwt = user.whitelisted_jwts.find_by(payload.slice('jti', 'aud'))
    jwt.destroy! if jwt
  end

  def on_jwt_dispatch(_token, payload)
    whitelisted_jwts.create!(
        jti: payload['jti'],
        aud: payload['aud'],
        exp: Time.at(payload['exp'].to_i)
    )
  end
end
