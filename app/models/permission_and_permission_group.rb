class PermissionAndPermissionGroup < MainRecordBase
  belongs_to :permission, foreign_key: :permission_id
  belongs_to :permission_group, foreign_key: :permission_group_id
  validates :permission_group, :permission, presence: true
end
