class CourseSchedule < MainRecordBase
  belongs_to :course, dependent: :destroy
end
