class UserBranch < MainRecordBase
  belongs_to :user, foreign_key: :user_id
  belongs_to :branch, foreign_key: :branch_id
end
