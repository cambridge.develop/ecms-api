class ClassGradeV2 < MainRecordBase
  self.table_name = 'class_grades'
  belongs_to :branch, class_name: 'Branch', foreign_key: :branch_id
end
