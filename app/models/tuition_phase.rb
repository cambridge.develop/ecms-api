class TuitionPhase < MainRecordBase
  belongs_to :course, ->{where is_cancel: false }, class_name: "Course"
  belongs_to :branch, class_name: "Branch"
end