class HolidayType < MainRecordBase
    has_many :holidays, class_name: "Holiday"
end
