class PermissionGroup < MainRecordBase
  belongs_to :user_type

  has_many :permission_and_permission_groups, class_name: 'PermissionAndPermissionGroup', dependent: :destroy
  has_many :permissions, :through => :permission_and_permission_groups

  validates :name, uniqueness: true, presence: true
end
