class Objective < MainRecordBase
  validates :code, uniqueness: true, presence: true
  validates :name, presence: true
  has_many :student_objectives, class_name: 'StudentObjective'
  has_many :students, through: :student_objectives
end
