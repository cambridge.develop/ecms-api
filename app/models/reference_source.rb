class ReferenceSource < MainRecordBase
  validates :code, uniqueness: true, presence: true
  validates :name, presence: true
  has_many :student_reference_sources, class_name: 'StudentReferenceSource', dependent: :restrict_with_error
  has_many :students, through: :student_reference_sources
end
