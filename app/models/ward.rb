class Ward < MainRecordBase
    belongs_to :district, class_name: "District", foreign_key: :district_code
    
    validates :code, uniqueness: true, presence: true
    validates :name, presence: true
end
