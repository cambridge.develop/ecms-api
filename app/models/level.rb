class Level < MainRecordBase
  validates :code, uniqueness: true, presence: true
  validates :name, presence: true

  belongs_to :major, class_name: 'Major', foreign_key: :major_id, optional: true
  validates :major, presence: true
  has_many :curriculums, class_name: "Curriculum", dependent: :restrict_with_error
  has_many :class_grades, through: :curriculums

end
