class StudentObjective < MainRecordBase
  belongs_to :objective, class_name: 'Objective', foreign_key: :objective_id
  belongs_to :student, class_name: 'Student', foreign_key: :student_id
end
