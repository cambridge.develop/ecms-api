class Student < MainRecordBase
  extend ActiveModel::Translation
  belongs_to :user, class_name: 'User', foreign_key: :user_id, optional: true, dependent: :destroy
  has_many :student_parents, class_name: 'StudentParent', dependent: :destroy
  has_many :parents, through: :student_parents
  has_many :student_courses, ->{where is_cancel: false }
  has_many :courses, through: :student_courses
  has_many :class_grades, through: :courses
  belongs_to :school, class_name: 'School', foreign_key: :school_id, optional: true
  has_many :student_certificates, class_name: 'StudentCertificate'
  has_many :certificates, through: :student_certificates
  has_many :student_objectives, class_name: 'StudentObjective'
  has_many :objectives, through: :student_objectives
  has_many :student_potentials, class_name: 'StudentPotential'
  has_many :student_reference_sources, class_name: 'StudentReferenceSource'
  has_many :reference_sources, through: :student_reference_sources
  has_one :gender, class_name: 'Gender', through: :user
  has_many :deposit_receipts, class_name: 'DepositReceipt', dependent: :restrict_with_error
  scope :studying, -> {where(status: :studying)}


  enum status: { studying: 0, left: 1, deleted: 99 }, _suffix: true

  #validations
  validates_presence_of :birthday


  def set_is_not_finish_study_fee
    self.is_not_finish_study_fee = self.is_not_finish_study_fee?
  end

  before_create do
    # last_student = Student.order(created_at: :desc).select(:code).first
    last_student = Student.order(code: :desc).select(:code).first
    if last_student
      self.code = "HV-#{(last_student.code.split('-')[1].to_i + 1).to_s.rjust(6, '0')}"

    else
      self.code = "HV-#{'1'.rjust(6, '0')}"
    end
    self.status = "studying"
  end

  after_commit :update_user
  after_save :update_user

  def self.humanize_status key
    case key.to_sym
    when :studying
      I18n.t('controllers.student.enum.studying')
    when :left
      I18n.t('controllers.student.enum.left')
    when :deleted
      I18n.t('controllers.student.enum.deleted')
    else
      "none"
    end
  end

  def is_not_finish_study_fee?
    result = true
    self.student_courses.each do |student_course|
      result = false unless student_course.is_paid_enough?
    end
    result
  end

  def current_status
    if self.status == 'studying'
      { name: 'Đang học', color: '#307c26' }
    elsif self.status == 'left'
      { name: 'Đã nghỉ học', color: '#bc4f23' }
    elsif self.status == 'deleted'
      { name: 'Đã xóa', color: '#8e8a89' }
    else
      { name: 'Không xác định', color: '#000000' }
    end
  end

  def student_logs
    StudentLog.where(student_id: self.id)
  end

  def is_student_enough_credit(credit_used)
    self.credit >= credit_used
  end

  def deposit_credit(credit)
    self.with_lock do
      self.reload
      self.credit += credit.abs

      if self.save
        [true, self.credit, I18n.t('controllers.student.successes.deposit_credit')]
      else
        [false, self.credit, self.errors.full_messages]
      end
    end
  end

  def withdraw_credit(credit)
    self.with_lock do
      self.reload
      return [false, self.credit, I18n.t('controllers.student.errors.withdraw_credit')] if self.credit < credit
      self.credit -= credit.abs
      if self.save
        [true, self.credit, I18n.t('controllers.student.successes.withdraw_credit')]
      else
        [false, self.credit, self.errors.full_messages]
      end
    end
  end

  def self.deposit_must_to_greater_than_zero
    I18n.t("activerecord.errors.models.student.deposit_must_greater_than_zero")
  end

  def self.deposit_success student_name
    I18n.t("activerecord.successes.models.student.deposit_success", student_name: student_name)
  end

  # private
  def update_user
    if self.status.to_sym == :deleted
      if self.user.present?
        user.update!(locked_at: Time.now)
      end
    else
      if self.user.present?
        user.update!(locked_at: nil)
      end
    end
  end

end