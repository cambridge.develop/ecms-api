class CourseStatus < MainRecordBase
  has_many :courses, class_name: 'Course'
  validates :code, uniqueness: true, presence: true
  validates :name, presence: true

  scope :open, -> { find_by(code: 'open') }
  scope :end, -> { find_by(code: 'end') }
  scope :cancel, -> { find_by(code: 'cancel') }

  def name_with_locale
    case self.code.to_sym
      when :open
        "Đang mở"
      when :end
        "Đã kết thúc"
      when :cancel
        "Đã hủy"
      else
        "none"
    end
  end
end