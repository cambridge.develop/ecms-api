class AttendanceCourse < MainRecordBase
  belongs_to :course
  belongs_to :creator, foreign_key: :creator_id, class_name: "User"
  enum status: { present: 0, absent: 1, uabsent: 99}, _suffix: true

  validates   :course_id,
              :creator_id,
              :check_date,
              presence: true

  def self.humanize_status key
    case key.to_sym
    when :present
      "Present"
    when :absent
      "Authorised absent"
    when :uabsent
      "Unauthorised absent"
    else
      "none"
    end
  end

  def current_status status
    if status == 'present'
      {name: 'Present', color: '#307c26'}
    elsif self.status == 'absent'
      {name: 'Authorised absent', color: '#bc4f23'}
    elsif self.status == 'uabsent'
      {name: 'Unauthorised absent', color: '#8e8a89'}
    else
      {name: 'Undefined', color: '#000000'}
    end
  end
end
