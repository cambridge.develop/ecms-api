class Major < MainRecordBase
  validates :code, uniqueness: true, presence: true
  validates :name, presence: true

  has_many :levels, class_name: "Level", dependent: :restrict_with_error
end
