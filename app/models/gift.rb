class Gift < MainRecordBase
  validates :code, uniqueness: true, presence: true
  validates :name, presence: true
end
