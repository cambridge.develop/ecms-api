class StudentPotential < MainRecordBase
  belongs_to :student, class_name: 'Student', foreign_key: :student_id
  belongs_to :branch, class_name: 'Branch', foreign_key: :branch_id
  belongs_to :curriculum, class_name: 'Curriculum', foreign_key: :curriculum_id
  delegate :level, :to => :curriculum
  delegate :major, :to => :level

  validates_presence_of :name

  enum status: { waiting: 0, successful: 1, failed: 99 }, _suffix: true
  def self.humanize_status key
    case key.to_sym
    when :waiting
      I18n.t('controllers.student_potential.enum.waiting')
    when :successful
      I18n.t('controllers.student_potential.enum.successful')
    when :failed
      I18n.t('controllers.student_potential.enum.failed')
    else
      I18n.t('controllers.student_potential.enum.undefined')
    end
  end

  # Do not update this method
  def current_status
    if self.status == 'waiting'
      { name: I18n.t('controllers.student_potential.enum.waiting'), color: '#307c26', code: self.status}
    elsif self.status == 'successful'
      { name: I18n.t('controllers.student_potential.enum.successful'), color: '#bc4f23', code: self.status }
    elsif self.status == 'failed'
      { name: I18n.t('controllers.student_potential.enum.failed'), color: '#8e8a89', code: self.status }
    else
      { name: I18n.t('controllers.student_potential.enum.undefined'), color: '#000000', code: self.status }
    end
  end

  before_create do
    p "Before create----------------------------------------------------"
    self.status = "waiting"
  end
end
