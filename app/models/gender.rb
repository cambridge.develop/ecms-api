class Gender < MainRecordBase
    # has_many :hoc_sinhs, class_name: "HocSinh", foreign_key: :gioi_tinh_id, dependent: :restrict_with_error

    validates :code, uniqueness: true, presence: true
    validates :name, presence: true
end
