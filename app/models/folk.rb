class Folk < MainRecordBase
    # has_many :hoc_sinhs, class_name: "HocSinh", foreign_key: :hoc_sinh_id, dependent: :restrict_with_error

    validates :code, uniqueness: true, presence: true
    validates :name, presence: true

end
