class Refund < MainRecordBase
  belongs_to :creator, foreign_key: :creator_id, class_name: "User"
  belongs_to :student, class_name: 'Student', optional: true
  validates :student, presence: true
  validates :withdrawal_person_name, presence: true
  validates_numericality_of :withdrawal_amount, less_than_or_equal_to: :student_credit, greater_than: 0, if: :student

  def student_credit
    self.student.credit
  end
end
