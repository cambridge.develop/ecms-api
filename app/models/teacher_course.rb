class TeacherCourse < MainRecordBase
  belongs_to :course, class_name: 'Course', foreign_key: :course_id
  belongs_to :teacher, class_name: 'Teacher', foreign_key: :teacher_id
  has_one :course_schedule, through: :course
  has_one :class_grade, through: :course
end
