class StudentCourseV2 < MainRecordBase
  self.table_name = 'student_courses'
  has_many :receipt_v2s
  belongs_to :course_v2, class_name: 'CourseV2', foreign_key: :course_id
  belongs_to :student, class_name: 'Student', foreign_key: :student_id
end
