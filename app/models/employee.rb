class Employee < MainRecordBase
  belongs_to :user, class_name: 'User', foreign_key: :user_id, dependent: :destroy
  belongs_to :employee_status, class_name: 'EmployeeStatus', foreign_key: :employee_status_id
  has_one :gender, class_name: 'Gender', through: :user
  has_one :user_type, class_name: 'UserType', through: :user
  has_many :user_branches, class_name: 'UserBranch', through: :user, dependent: :restrict_with_error
  has_many :branches, class_name: 'Branch', through: :user_branches, dependent: :restrict_with_error

  before_create do
    last_employee = Employee.order(created_at: :desc).select(:code).first
    if last_employee
      self.code = "NV-#{(last_employee.code.split('-')[1].to_i + 1).to_s.rjust(6, '0')}"
    else
      self.code = "NV-#{'1'.rjust(6, '0')}"
    end
  end
end