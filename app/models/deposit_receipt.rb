class DepositReceipt < MainRecordBase

  belongs_to :creator, class_name: "User", foreign_key: :creator_id
  belongs_to :student, class_name: 'Student', foreign_key: :student_id
  belongs_to :branch, class_name: 'Branch', foreign_key: :branch_id
end
