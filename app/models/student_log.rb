class StudentLog
  include Mongoid::Document
  include Mongoid::Timestamps

  # enum action_type: { new: 0, change: 1, info: 2, account_status: 3}
  ACT_NEW = 0
  ACT_CHANGE = 1
  ACT_INFO = 2
  ACT_STATUS = 3

  field :student_id, type: String
  field :action_type, type: Integer
  #action_type = 0 new
  #action_type = 1 change
  #action_type = 2 info
  #action_type = 3 student status
  field :current_name, type: String
  field :content, type: String
  field :changed_time, type: DateTime
  field :changed_by, type: String

  index({ student_id: 1 }, {background: true })

  def student
    students = Student.where(id: self.student_id)
    return nil if students.blank?
    students.first
  end

  def action_type_text
    return 'Khởi tạo' if self.action_type == 0
    return 'Thay đổi' if self.action_type == 1
    return 'Thông tin' if self.action_type == 2
    return 'Trạng thái học viên' if self.action_type == 3
    return ''
  end

  DATATABLE_COLUMNS = %w[student_id action_type current_name content changed_time changed_by].freeze
  DATATABLE_DISPLAY_COLUMNS = %w[action_type content changed_time changed_by].freeze

  class << self
    def datatable_filter(search_value, search_columns)
      return all if search_value.blank?
      conditional = []
      search_columns.each do |key, value|
        conditional << {DATATABLE_COLUMNS[key.to_i] => /#{search_value}/}
      end
      result = self.any_of(conditional)
      result
    end

    def datatable_order(order_column_index, order_dir)
      order("#{StudentLog::DATATABLE_DISPLAY_COLUMNS[order_column_index]} #{order_dir}")
    end
  end


end