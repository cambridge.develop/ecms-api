class StudentReferenceSource < MainRecordBase
  belongs_to :reference_source, class_name: 'ReferenceSource', foreign_key: :reference_source_id
  belongs_to :student, class_name: 'Student', foreign_key: :student_id
end
