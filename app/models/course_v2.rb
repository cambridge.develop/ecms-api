class CourseV2 < MainRecordBase
    self.table_name = 'courses'
    has_many :students, through: :student_courses
    belongs_to :class_grade_v2, class_name: 'ClassGradeV2', foreign_key: :class_grade_id
    belongs_to :course_status, class_name: 'CourseStatus', foreign_key: :course_status_id
  end
  