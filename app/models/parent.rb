class Parent < MainRecordBase
  belongs_to :user, class_name: 'User', foreign_key: :user_id, optional: true
  validates :user, presence: true
  has_many :student_parents, class_name: 'StudentParent', dependent: :destroy
  has_many :students, through: :student_parents
	has_one :gender, class_name: 'Gender', through: :user

  # validates :birthday, presence: true
end