class UserAndUserGroup < MainRecordBase
  belongs_to :user, foreign_key: :user_id
  belongs_to :user_group, foreign_key: :user_group_id
end
