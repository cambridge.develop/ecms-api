class Permission < MainRecordBase
  has_many :permission_and_permission_groups, class_name: 'PermissionAndPermissionGroup'
  has_many :permission_and_user_groups, class_name: 'PermissionAndUserGroup'
  
  has_many :permission_groups, :through => :permission_and_permission_groups
  has_many :user_groups, :through => :permission_and_user_groups
end