class Policy < MainRecordBase
  validates_presence_of :name, :code
end
