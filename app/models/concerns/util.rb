module Util
  def calc_age(dob)
    now = Time.now.utc.to_date
    now.year - dob.year - ((now.month > dob.month || (now.month == dob.month && now.day >= dob.day)) ? 0 : 1)
  end

  def generate_username(fullname)
    ActiveSupport::Inflector.transliterate(fullname) # change ñ => n
        .downcase              # only lower case
        .strip                 # remove spaces around the string
        .gsub(/[^a-z]/, '_')   # any character that is not a letter or a number will be _
        .gsub(/\A_+/, '')      # remove underscores at the beginning
        .gsub(/_+\Z/, '')      # remove underscores at the end
        .gsub(/_+/, '_')       # maximum an underscore in a row
  end

  def find_unique_username(username)
    taken_usernames = User
                          .where("username LIKE ?", "#{username}%")
                          .pluck(:username)

    # username if it's free
    return username if ! taken_usernames.include?(username)

    count = 2
    while true
      # username_2, username_3...
      new_username = "#{username}_#{count}"
      return new_username if ! taken_usernames.include?(new_username)
      count += 1
    end
  end

  def random_password
    [*('A'..'Z')].sample(4).join + rand(1000...9999).to_s
  end
end
