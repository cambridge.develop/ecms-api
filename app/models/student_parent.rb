class StudentParent < MainRecordBase
  belongs_to :student, foreign_key: :student_id
  belongs_to :parent, foreign_key: :parent_id
end