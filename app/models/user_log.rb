class UserLog
  include Mongoid::Document
  include Mongoid::Timestamps

  ACT_NEW = 0
  ACT_UPDATE = 1
  ACT_SHOW = 2
  ACT_DELETE = 3
  #action_type = 0 new
  #action_type = 1 update
  #action_type = 2 show
  #action_type = 3 delete

  # enum action_type: { new: 0, update: 1, show: 2, delete: 3}

  field :user_id, type: String
  field :full_name, type: String
  field :action_type, type: Integer
  field :branch_id, type: Integer
  field :branch_name, type: String




  field :function, type: String
  field :action, type: String
  field :content, type: String
  field :changed_time, type: DateTime
  field :changed_by, type: String
  field :ip, type: String
  field :browser, type: String
  field :device, type: String
  field :platform, type: String


  index({ user_id: 1 }, {background: true })
  index({ branch_id: 1 }, {background: true })
  index({ action_type: 1 }, {background: true })

  def user
    users = User.where(id: self.user_id)
    return nil if users.blank?
    users.first
  end

  def branch
    branches = Branch.where(id: self.branch_id)
    return nil if branches.blank?
    branches.first
  end

  def action_type_text
    return 'Khởi tạo' if self.action_type == 0
    return 'Thay đổi' if self.action_type == 1
    return 'Xem' if self.action_type == 2
    return 'Xóa dữ liệu' if self.action_type == 3
    return ''
  end

  DATATABLE_COLUMNS = %w[user_id full_name action_type branch_id branch_name function action content changed_time changed_by].freeze
  DATATABLE_DISPLAY_COLUMNS = %w[full_name branch_id action_type content changed_time changed_by].freeze
  DATATABLE_DISPLAY_SYSTEM_LOGS_COLUMNS = %w[full_name branch_name action_type function action content changed_time changed_by ip browser device platform].freeze

  class << self
    def datatable_filter(search_value, search_columns)
      return all if search_value.blank?
      conditional = []
      search_columns.each do |key, value|
        conditional << {DATATABLE_COLUMNS[key.to_i] => /#{search_value}/}
      end
      result = self.any_of(conditional)
      result
    end

    def datatable_order(order_column_index, order_dir)
      order("#{UserLog::DATATABLE_DISPLAY_COLUMNS[order_column_index]} #{order_dir}")
    end

    def datatable_order_system_log(order_column_index, order_dir)
      order("#{UserLog::DATATABLE_DISPLAY_SYSTEM_LOGS_COLUMNS[order_column_index]} #{order_dir}")
    end
  end


end