class School < MainRecordBase
  validates :code, uniqueness: true, presence: true
  validates :name, presence: true
  belongs_to :literacy, class_name: 'Literacy', foreign_key: :literacy_id
  has_many :students, class_name: 'Student', dependent: :restrict_with_error
end
