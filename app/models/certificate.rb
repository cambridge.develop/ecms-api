class Certificate < MainRecordBase
  validates :code, uniqueness: true, presence: true
  validates :name, presence: true
  has_many :student_certificates, class_name: 'StudentCertificate', dependent: :restrict_with_error
  has_many :students, through: :student_certificates, dependent: :restrict_with_error
end
