class ApplicationController < ActionController::API
  include Pagy::Backend
  include ActionController::Cookies
  before_action :set_locale


  def render_resource(resource)
    if resource.errors.empty?
      render json: resource
    else
      validation_error(resource)
    end
  end


  def validation_error(resource)
    render json: {
        errors: [
            {
                status: '400',
                title: 'Bad Request',
                detail: resource.errors,
                code: '100'
            }
        ]
    }, status: :bad_request
  end

  def selected_branch
    begin
      @selected_branch = Branch.first
      # @selected_branch = Branch.find_by_id(cookies[:branch_id])
      # raise StandardError.new Branch.invalid_message if @selected_branch.blank?
      # return render json: { errors: "Tài khoản không còn được đăng nhập" }, status: 401 unless current_user
      # unless current_user.user_type.is_admin?
      #   if current_user.user_branches.where(branch_id: @selected_branch.id).blank?
      #     raise StandardError.new Branch.invalid_message
      #   end
      # end
    rescue => e
      render json: { errors: [e] }, status: 451
    end
  end

  private
    def set_locale
      I18n.locale = params[:locale] || I18n.default_locale
    end

    def check_user
      begin
        if current_user.is_locked?
          raise StandardError.new User.locked_message
        end
      rescue => e
        render json: { errors: [e] }, status: 401
      end
    end

end
