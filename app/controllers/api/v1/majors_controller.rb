class Api::V1::MajorsController < Api::V1::ApiBaseController
  before_action :set_major, only: [:show, :update, :destroy, :levels, :lock, :unlock]
  def index
    @q = Major.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      major_list = @q.result
      return_obj[:majors] = serialize_data major_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, major_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:majors] = serialize_data major_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data(@major), status: :ok
  end

  def create
    @major = Major.new major_params
    if @major.save
      render json: @major, status: :ok
    else
      render json: { errors: @major.errors.full_messages }, status: :bad_request
    end
  end

  def update
    @major.attributes = major_params
    return (render json: @major, status: :ok) if @major.save
    render json: {errors: @major.errors.full_messages}, status: :bad_request
  end

  def destroy
    return render json: { messages: [Major.destroy_success_message(@major.name)] }, status: :ok if @major.destroy
    render json: {errors: @major.errors.full_messages}, status: :bad_request
  end

  def levels

    @q = @major.levels.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      major_list = @q.result
      return_obj[:levels] = serialize_data major_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, major_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:levels] = serialize_data major_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def lock
    @major.is_locked = true
    return (render json: { messsages: [Major.lock_success_message] }, status: :ok) if @major.save
    render json: { errors: @major.errors.full_messages }, status: :bad_request
  end

  def unlock
    @major.is_locked = false
    return (render json: { messsages: [Major.unlock_success_message] }, status: :ok) if @major.save
    render json: { errors: @major.errors.full_messages }, status: :bad_request
  end

  private

  def set_major
    begin
      @major = Major.find_by_id(params[:id])
      return render json: { errors: [Major.not_found_message] }, status: :not_found if @major.blank?
    rescue => exception
      render json: { errors: [Major.not_found_message] }, status: :not_found
    end
  end

  def major_params
    params[:major].permit(:name, :code, :description)
  end

  def serialize_data data
    data.as_json(
        except: [:created_at, :updated_at]
    )
  end
end
