class Api::V1::PaymentV2::ReceiptsController < Api::V1::PaymentV2::PaymentBaseControllerV2
  def create
    receipts = Payment::ReceiptService.process_create receipt_params, current_user.id
    return_obj = {}
    return_obj[:receipts] = serialize_data receipts
    status = :ok if receipts.length > 0
    status = 400 if receipts.length == 0
    render json: return_obj, status: status
  end

  def cancel_by_receipt_no
    status = Payment::ReceiptService.process_cancel delete_receipt_params
    status_req = :ok if status == 1
    status_req = 400 if status == 0
    return_obj = {}
    return_obj[:status] = status
    render json: return_obj, status: status_req
  end

  private
  def receipt_params
    params[:receipt].permit(:student_id, :paid_person_name, :total_actual_paid_amount, :description_receipt, :is_bank, :description_internal, :gift_id, :credit_used , student_course_pay_info: [:id, :day_late, :discount_percentage])
  end

  def delete_receipt_params
    params[:receipt].permit(:receipt_no)
  end

  def serialize_data (data)
    data.as_json(
        include: [
            gift: {only: [:name]},
            creator: { except: [:created_at, :updated_at] },
            course_v2: {
                include: [
                    class_grade_v2: {
                        only: :name,
                        include: [
                            branch: {
                                only: [:name, :address, :phone, :email]
                            }
                        ]
                    }
                ],
                except: [:created_at, :updated_at]
            },
            student: {
                include: [
                    user: {
                        only: :full_name
                    }
                ],
                except: [:created_at, :updated_at]
            },
            student_course_v2: {
                except: [:created_at, :updated_at]
            }
        ]
    )
  end

end