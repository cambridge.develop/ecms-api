class Api::V1::PaymentV2::StudentCoursesController < Api::V1::PaymentV2::PaymentBaseControllerV2
  def index

  end

  def get_student_course
    res = {}
    res[:student_courses] = render_json StudentCourseService.GetStudentCourse(student_course_params[:student_id], student_course_params[:class_id])
    render json: res
  end

  private

  def student_course_params
    params[:student_course].permit(:student_id, :class_id)
  end

  def render_json(data)
    data.as_json()
  end
end