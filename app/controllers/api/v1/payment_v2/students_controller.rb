class Api::V1::PaymentV2::StudentsController < Api::V1::PaymentV2::PaymentBaseControllerV2
  before_action :set_student
  def class_grade
    classes = @student.class_grades.where(branch_id: @selected_branch.id).distinct
    @q = classes.includes(:courses).where.not(:courses => {:course_status_id => CourseStatus.cancel.id})
    current_page = params[:page] || 1
    @q = @q.ransack(params[:q])
    @q.sorts = ['created_at asc'] if @q.sorts.empty?
    p classes
    return_obj = {}
    if params.has_key?(:all)
      class_list = @q.result
      return_obj[:classes] = serialize_class_data class_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, class_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:classes] = serialize_class_data class_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def receipts
    course_ids = CourseV2.where(class_grade_id: receipt_params[:class_id]).pluck(:id)
    student_course_ids = StudentCourseV2.where(student_id: @student.id, course_id: course_ids, is_cancel: false).pluck(:id)
    receipts = ReceiptV2.where(student_course_id: student_course_ids).includes(:gift, :creator, :student, :student_course_v2, course_v2:[:class_grade_v2])
    return_obj = {}
    receipt_by_receipt_no = {}
    receipts.each do |r|
      key = r.receipt_no
      receipt_by_receipt_no[key] = [] if receipt_by_receipt_no[key].blank?
      receipt_by_receipt_no[key] << r
    end
    list_receipt = []
    receipt_by_receipt_no.each do |key, value|
      list_receipt << value
    end
    return_obj[:receipts] =  serialize_receipt_data list_receipt
    render json: return_obj
  end

  private

  def receipt_params
    params[:receipt].permit(:class_id)
  end

  def set_student
    begin
      @student = Student.find_by_id(params[:id])
      return render json: { errors: [Student.not_found_message] }, status: :not_found if @student.blank?
    rescue => exception
      render json: { errors: [Student.not_found_message] }, status: :not_found
    end
  end

  def serialize_class_data data
    data.as_json(
        include: [
            courses: {}
        ]
    )
  end

  def serialize_receipt_data (data)
    data.as_json(
        include: [
            gift: {only: [:name]},
            creator: { except: [:created_at, :updated_at] },
            course_v2: {
                include: [
                    class_grade_v2: {
                        only: :name,
                        include: [
                            branch: {
                                only: [:name, :address, :phone, :email]
                            }
                        ]
                    }
                ],
                except: [:created_at, :updated_at]
            },
            student: {
                include: [
                    user: {
                        only: :full_name
                    }
                ],
                except: [:created_at, :updated_at]
            },
            student_course_v2: {
                except: [:created_at, :updated_at]
            }
        ]
    )
  end

  def render_json(data)
    data.as_json()
  end
end