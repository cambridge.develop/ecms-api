class Api::V1::CoursesController < Api::V1::ApiBaseController
  before_action :set_course, only: [:show, :update, :destroy, :students, :student_courses, :student_courses_history, :student_courses_free_trial,:close, :cancel]

  def index
    @q = Course.joins(:class_grade).where(:course_status, :class_grades => { :branch_id => @selected_branch }).includes(:course_schedule, teachers: :user, :class_grade => [:curriculum => [:level]])
    course_status = params[:status]
    if course_status == '0'
      @q = @q.not_closed
    elsif course_status == '1'
      @q = @q.closed
    end
    @q = @q.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      course_list = @q.result(distinct: true)
      return_obj[:courses] = serialize_data_with_short_info course_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, course_list = pagy @q.result(distinct: true), items: params[:items], page: current_page
      return_obj[:courses] = serialize_data_with_short_info course_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end

    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data(@course), status: :ok
  end

  def create
    course = Course.new course_params
    course_schedule = CourseSchedule.new course_schedule_params
    class_grade = ClassGrade.find_by_id(course.class_grade_id)
    return render json: { errors: [ClassGrade.not_found_message] }, status: :not_found if class_grade.blank?
    course_no = class_grade.courses.size + 1
    course.course_no = course_no
    course.code = class_grade.code + course_no.to_s
    course.course_status_id = CourseStatus.open.id
    MainRecordBase.transaction do
      course.save!
      course_schedule.course_id = course.id
      course_schedule.save!
      render json: serialize_data(course), status: :ok
        # raise ActiveRecord::Rollback
    rescue ActiveRecord::RecordInvalid => invalid
      render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
      raise ActiveRecord::Rollback
    end

  end

  def update
    return render json: { errors: [Course.status_not_open_message] }, status: :bad_request if @course.course_status.code != 'open'
    @course.attributes = course_params.except(:class_grade_id, :tuition)
    p @course
    course_schedule = @course.course_schedule
    if course_schedule
      course_schedule.attributes = course_schedule_params.except(:course_id)
    else
      course_schedule = CourseSchedule.new course_schedule_params
      course_schedule.course_id = @course.id
    end

    MainRecordBase.transaction do
      @course.save!
      course_schedule.save!
      # TODO check reference (receipt)
      render json: serialize_data(@course), status: :ok
        # raise ActiveRecord::Rollback
    rescue ActiveRecord::RecordInvalid => invalid
      p invalid.record.errors
      render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
      raise ActiveRecord::Rollback
    end
  end

  def destroy
    return render json: { errors: [Course.status_not_open_message] }, status: :bad_request if @course.course_status.code != 'open'
    student_courses = @course.student_courses
    return render json: { errors: [Course.delete_when_student_course_exist_message] }, status: :bad_request unless student_courses.blank?

    return render json: { messages: [Course.destroy_success_message(@course.name)] }, status: :ok if @course.destroy
    render json: { errors: @course.errors.full_messages }, status: :bad_request
  end

  def close
    return render json: { errors: [Course.status_not_open_message] }, status: :bad_request if @course.course_status.code != 'open'
    #check số buổi điểm danh
    attendance_count = @course.attendance_courses.size
    return render json: { errors: [I18n.t("controllers.course.attendance_count_not_enough")] }, status: :bad_request if @course.number_of_days > attendance_count

    @course.course_status_id = CourseStatus.end.id
    return render json: { messages: [I18n.t("controllers.course.close_successfully")] }, status: :ok if @course.save
    render json: { errors: [I18n.t("controllers.course.close_unsuccessfully")] }, status: :bad_request

  end

  def cancel
    return render json: { errors: [Course.status_not_open_message] }, status: :bad_request if @course.course_status.code != 'open'
    return render json: { messages: [I18n.t("controllers.course.cancel_successfully")] }, status: :ok if @course.cancel
    render json: { errors: [I18n.t("controllers.course.cancel_unsuccessfully")] }, status: :bad_request
  end

  def students
    @q = @course.students.includes(:gender, :school, :user, parents: [:user]).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['code asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      student_list = @q.result
      return_obj[:students] = serialize_student_data student_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, student_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:students] = serialize_student_data student_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end

    render json: return_obj, status: :ok
  end

  def student_courses
    @q = @course.student_courses_studying.includes(:receipt, :course, :transfer_from_course, student: [:gender, :school, :user, parents: [:user]]).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['student_code asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      student_course_list = @q.result
      return_obj[:student_courses] = serialize_student_course_data student_course_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, student_course_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:student_courses] = serialize_student_course_data student_course_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end

    render json: return_obj, status: :ok
  end



  def student_courses_free_trial
    @q = @course.student_courses_free_trial.includes(:receipt, :course, :transfer_from_course, student: [:gender, :school, :user, parents: [:user]]).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['student_code asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      student_course_list = @q.result
      return_obj[:student_courses] = serialize_student_course_data student_course_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, student_course_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:student_courses] = serialize_student_course_data student_course_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end

    render json: return_obj, status: :ok
  end

  def student_courses_history
    @q = @course.student_courses_history.includes(:receipt, :course, :transfer_from_course, student: [:gender, :school, :user, parents: [:user]]).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['student_code asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      student_course_list = @q.result
      return_obj[:student_courses] = serialize_student_course_data student_course_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, student_course_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:student_courses] = serialize_student_course_data student_course_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end

    render json: return_obj, status: :ok
  end

  def copy_student_course
    target_course = Course.find_by_id(copy_student_course_params[:target_course_id])
    return render json: { errors: [Course.not_found_message] }, status: :bad_request unless target_course

    exist_students = target_course.student_courses.where(student_id: copy_student_course_params[:student_id_list])
    p exist_students
    if exist_students.size > 0
      return render json: { errors: [I18n.t("activerecord.errors.models.course.copy_student_exist")] }, status: :bad_request
    end

    students = Student.where(id: copy_student_course_params[:student_id_list])
    return render json: { errors: [Student.not_found_message] }, status: :bad_request unless students

    student_courses = []
    students.each do |student|
      student_courses << StudentCourse.new(course_id: target_course.id, student_id: student.id, start_date: copy_student_course_params[:start_date], days_late: copy_student_course_params[:days_late])
    end

    MainRecordBase.transaction do
      student_courses.each do |student_course|
        unless student_course.save
          render json: { errors: [@student_course.errors.full_messages] }, status: :bad_request
          raise ActiveRecord::Rollback
        end
      end
      render json: { messages: [I18n.t("activerecord.errors.models.course.copy_student_success")]}, status: :ok
    end

    # if student_courses.save
    #   render json: { messages: [I18n.t("activerecord.errors.models.course.copy_student_success")]}, status: :ok
    # else
    #   render json: { errors: [@student_course.errors.full_messages] }, status: :bad_request
    # end
  end

  private

  def set_course
    begin
      @course = Course.includes(:class_grade, :course_status, :course_schedule, teachers: [:user], students: [:user], student_courses: [:receipts, :course]).find_by_id(params[:id])
      return render json: { errors: [Course.not_found_message] }, status: :not_found if @course.blank?
    rescue => exception
      p exception
      return render json: { errors: [Course.not_found_message] }, status: :not_found
    end
  end


  def course_params
    params.require(:course).permit(:name,
                                   # :code,
                                   :room,
                                   :start_date,
                                   :opening_date,
                                   :end_date,
                                   :class_grade_id,
                                   :tuition,
                                   :number_of_days,
                                   :is_end,
                                   # :course_no,
                                   :number_of_students,
                                   :description)
  end

  def course_schedule_params
    params.require(:course_schedule).permit(:monday_start,
                                            :monday_end,
                                            :tuesday_start,
                                            :tuesday_end,
                                            :wednesday_start,
                                            :wednesday_end,
                                            :thursday_start,
                                            :thursday_end,
                                            :friday_start,
                                            :friday_end,
                                            :saturday_start,
                                            :saturday_end,
                                            :sunday_start,
                                            :sunday_end)
  end

  def copy_student_course_params
    params.require(:copy_student_form).permit(:target_course_id, :start_date, :days_late, student_id_list: [])
  end

  def serialize_data data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            class_grade: { except: %i[created_at updated_at] },
            course_schedule: { except: %i[created_at updated_at] },
            teachers: {
                only: %i[id user_id],
                include: [user: { only: %i[id email username full_name] }]
            },
            students: {
                only: %i[id user_id],
                include: [user: { only: %i[id email username full_name] }]
            },
            course_schedule: { except: %i[created_at updated_at] },
            course_status: {}
        ]
    )
  end

  def serialize_data_with_short_info data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            class_grade: {
                only: %i[id code name],
                include: [
                    curriculum: {
                        only: %i[id code name],
                        include: [
                            level: {
                                only: %i[id code name],
                            }
                        ]
                    }
                ]
            },
            teachers: {
                only: %i[id],
                include: [user: { only: %i[id username full_name] }]
            },
            course_schedule: { except: %i[created_at updated_at] },
            course_status: {}
        ]
    )
  end

  def serialize_student_data data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            gender: { only: %i[id name code] },
            user: { only: %i[id full_name username email locked_at phone] },
            school: { only: %i[id name code literacy_id] },
            parents: { include: [user: { only: %i[id full_name] }] }
        ]
    )
  end

  def serialize_student_course_data data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            transfer_from_course: {
                only: %i[name],
            },
            student: {
                except: %i[created_at updated_at],
                include: [
                    gender: { only: %i[id name code] },
                    user: { only: %i[id full_name username email locked_at phone] },
                    school: { only: %i[id name code literacy_id] },
                    parents: { include: [user: { only: %i[id full_name] }] }
                ]
            }

        ]
    )
  end

end