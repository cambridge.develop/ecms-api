class Api::V1::PermissionAndPermissionGroupsController < Api::V1::ApiBaseController

  # def create
  #   permission = Permission.find_by_id(permission_and_permission_group_params[:permission_id])
  #   permission_group = PermissionGroup.find_by_id(permission_and_permission_group_params[:permission_group_id])
  #   return render json: { errors: [Permission.not_found_message] }, status: :not_found if permission.blank?
  #   return render json: { errors: [PermissionGroup.not_found_message] }, status: :not_found if permission_group.blank?
  #
  #   exist_permission_and_permission_group = PermissionAndPermissionGroup.where(permission_id: permission.id, permission_group_id: permission_group.id)
  #   return render json: { errors: [PermissionAndPermissionGroup.exist_record_reference_message(Permission, PermissionGroup)] } if exist_permission_and_permission_group.blank? == false
  #
  #   permission_and_permission_group = PermissionAndPermissionGroup.new permission_and_permission_group_params
  #   return (render json: { messages: [PermissionAndPermissionGroup.add_reference_success_message(Permission, PermissionGroup)] }, status: :ok) if permission_and_permission_group.save
  #   return render json: { errors: [permission_and_permission_group.errors.full_messages]}, status: :bad_request
  # end

  def create
    MainRecordBase.transaction do
      permissions = Permission.where(id: permission_and_permission_group_params[:permission_id].uniq).pluck(:id)
      permission_group = PermissionGroup.where(id: permission_and_permission_group_params[:permission_group_id]).first
      permission_and_permission_groups =  permissions.map do |permission_id|
        PermissionAndPermissionGroup.new(permission_id: permission_id, permission_group_id: permission_group.id)
      end
      PermissionAndPermissionGroup.import permission_and_permission_groups
      return (render json: { messages: [PermissionAndPermissionGroup.add_reference_success_message(Permission, PermissionGroup)] }, status: :ok)
    rescue ActiveRecord::RecordInvalid => invalid
      render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
      raise ActiveRecord::Rollback
    end
  end

  # def destroy
  #   permission = Permission.find_by_id(permission_and_permission_group_params[:permission_id])
  #   permission_group = PermissionGroup.find_by_id(permission_and_permission_group_params[:permission_group_id])
  #   return render json: { errors: [Permission.not_found_message] }, status: :not_found if permission.blank?
  #   return render json: { errors: [PermissionGroup.not_found_message] }, status: :not_found if permission_group.blank?
  #
  #   exist_permission_and_permission_group = PermissionAndPermissionGroup.where(permission_id: permission.id, permission_group_id: permission_group.id)
  #   return render json: { errors: [PermissionAndPermissionGroup.not_found_message] }, status: :not_found if exist_permission_and_permission_group.blank?
  #
  #   return (render json: { messages: [PermissionAndPermissionGroup.destroy_reference_success_message(Permission, PermissionGroup)]}, status: :ok) if exist_permission_and_permission_group.destroy
  #   return render json: { errors: [exist_permission_and_permission_group.errors.full_messages]}, status: :bad_request
  # end

  def delete_permissions
    MainRecordBase.transaction do
      permissions = Permission.where(id: permission_and_permission_group_params[:permission_id].uniq)
      permission_group = PermissionGroup.where(id: permission_and_permission_group_params[:permission_group_id]).first
      PermissionAndPermissionGroup.where(permission_id: permissions.ids, permission_group_id: permission_group.id).delete_all
      return (render json: { messages: [PermissionAndPermissionGroup.destroy_reference_success_message(Permission, PermissionGroup)] }, status: :ok)
    rescue ActiveRecord::RecordInvalid => invalid
      render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
      raise ActiveRecord::Rollback
    end
  end
  private

  def permission_and_permission_group_params
    params[:permission_and_permission_group].permit(:permission_group_id, permission_id:[])
  end
end
