class Api::V1::ClassGradesController < Api::V1::ApiBaseController
  before_action :set_class_grade, except: [:index, :create, :close]
  before_action :initial_load_class_grade, only: [:close]

  def index
    @q = ClassGrade.where(branch_id: @selected_branch.id).includes(:curriculum => { :level => :major }).includes(:branch, courses: [:course_status, :course_schedule]).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['created_at desc'] if @q.sorts.empty?
    return_obj = {}

    if params.has_key?(:all)
      class_grade_list = @q.result
      return_obj[:class_grades] = serialize_data class_grade_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, class_grade_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:class_grades] = serialize_data class_grade_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end

    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data(@class_grade), status: :ok
  end

  def create
    @class_grade = ClassGrade.new class_grade_params
    @class_grade.branch_id = @selected_branch.id
    return (render json: { errors: [Curriculum.locked_message] }, status: :bad_request) if (@class_grade.curriculum.is_locked == true)
    curriculum = Curriculum.find_by_id(@class_grade.curriculum_id)
    if curriculum.present?
      level = curriculum.level
      classes_numbers = level.class_grades.where(branch:@selected_branch).size
      @class_grade.code = "#{@selected_branch.code}#{level.code}#{(classes_numbers + 1).to_s.rjust(4, '0')}"
    end
    if @class_grade.save
      render json: @class_grade, status: :ok
    else
      render json: { errors: @class_grade.errors.full_messages }, status: :bad_request
    end
  end

  def update
    if @class_grade.curriculum_id_changed?
      return (render json: { errors: [Curriculum.locked_message] }, status: :bad_request) if (@class_grade.curriculum.is_locked == true)
    end
    return render json: { errors: [ClassGrade.class_grade_was_closed_message] }, status: :bad_request if @class_grade.is_end == true
    @class_grade.attributes = class_grade_params
    return (render json: serialize_data(@class_grade), status: :ok) if @class_grade.save
    render json: { errors: @class_grade.errors.full_messages }, status: :bad_request
  end

  def destroy
    return render json: { errors: [ClassGrade.class_grade_was_closed_message] }, status: :bad_request if @class_grade.is_end == true
    courses_opening = @class_grade.courses_opening
    return render json: { errors: [ClassGrade.delete_when_course_exist_message] }, status: :bad_request unless courses_opening.blank?

    return render json: { messages: [ClassGrade.destroy_success_message(@class_grade.name)] }, status: :ok if @class_grade.destroy
    render json: { errors: @class_grade.errors.full_messages }, status: :bad_request
  end

  def close
    #check trạng thái hiện tại
    return render json: { errors: [ClassGrade.class_grade_was_closed_message] }, status: :bad_request if @class_grade.is_end == true
    #check ngày cuối cùng
    # return render json: { errors: [ClassGrade.class_grade_end_date_less_than_now_message] }, status: :bad_request  if @class_grade.end_date >= DateTime.now
    #check class grade có lớp nào đang chạy không?
    courses_opening = @class_grade.courses_opening
    p courses_opening
    return render json: { errors: [ClassGrade.some_course_not_finished_message] }, status: :bad_request unless courses_opening.blank?
    @class_grade.is_end = true
    if @class_grade.save
      return render json: { messages: [ClassGrade.close_successfully_message] }, status: :ok
    else
      render json: { errors: @class_grade.errors.full_messages }, status: :bad_request
    end
  end

  def cancel
    MainRecordBase.transaction do
      courses = @class_grade.courses.not_closed
      courses.each do |course|
        p course
        course.cancel
      end
      render json: serialize_course_data(courses)
      raise ActiveRecord::Rollback
    end
  end

  def courses
    @q = @class_grade.courses.includes(:course_schedule, :student_courses).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['course_no asc'] if @q.sorts.empty?
    return_obj = {}

    if params.has_key?(:all)
      course_list = @q.result
      return_obj[:courses] = serialize_course_data course_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, course_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:courses] = serialize_course_data course_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end

    render json: return_obj, status: :ok
  end

  private

  def set_class_grade
    begin
      @class_grade = ClassGrade.includes(:curriculum => { :level => :major }).includes(:branch, :courses => [:course_status, :course_schedule, :student_courses => [:student, :receipts, :receipt, :course]]).find_by_id(params[:id])
      return render json: { errors: [ClassGrade.not_found_message] }, status: :not_found if @class_grade.blank?
    rescue => exception
      return render json: { errors: [ClassGrade.not_found_message] }, status: :not_found
    end
  end

  def initial_load_class_grade
    begin
      @class_grade = ClassGrade.includes(:courses_opening).find_by_id(params[:class_grade_id])
      return render json: { errors: [ClassGrade.not_found_message] }, status: :not_found if @class_grade.blank?
    rescue => exception
      return render json: { errors: [ClassGrade.not_found_message] }, status: :not_found
    end
  end

  def class_grade_params
    params.require(:class_grade).permit(:name,
                                        :code,
                                        :curriculum_id,
                                        :start_date,
                                        :opening_date,
                                        :end_date,
                                        :is_end,
                                        :number_of_courses,
                                        :description)
  end

  def serialize_data data
    data.as_json(
        except: [:created_at, :updated_at],
        include: [
            curriculum: {
                only: %i[id name],
                include: [
                    level: {
                        only: %i[id name],
                        include: [
                            major: { only: %i[id name] }
                        ]
                    }
                ]
            },
            branch: { except: %i[created_at updated_at] },
            courses: {
                except: %i[created_at updated_at],
                include: [
                    course_status: {},
                    course_schedule: { except: %i[created_at updated_at] },
                ]
            }
        ]
    )
  end

  def serialize_course_data data
    data.as_json(
        except: [:created_at, :updated_at],
        include: [
            course_schedule: { except: %i[created_at updated_at] },
            course_status: {},
            student_courses: { except: %i[created_at updated_at] }

        ]
    )
  end
end
