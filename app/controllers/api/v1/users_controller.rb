class Api::V1::UsersController < Api::V1::ApiBaseController
  before_action :set_user, only: [:lock, :unlock]
  def index
    @q = User.where(:id => UserBranch.where(branch_id: @selected_branch).pluck(:user_id)).includes(:gender, :user_type).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['full_name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      user_list = @q.result
      return_obj[:users] = serialize_data user_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, user_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:users] = serialize_data user_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def update_password
    @user = current_user
    if @user.valid_password?(user_params[:current_password])
      if @user.update(user_params)
        bypass_sign_in(@user)
        render json: { messages: [User.update_success_message('password')] }, status: :ok
      else
        render json: { errors: [@user.errors.full_messages] }, status: :bad_request
      end
    else
      @user.errors.add(:current_password, :invalid)
      render json: { errors: [@user.errors.full_messages] }, status: :bad_request
    end
  end

  def lock
    @user.locked_at = Time.now
    return (render json: { messsages: [User.lock_success_message] }, status: :ok) if @user.save
    render json: { errors: @user.errors.full_messages }, status: :bad_request
  end

  def unlock
    @user.locked_at = nil
    return (render json: { messsages: [User.unlock_success_message] }, status: :ok) if @user.save
    render json: { errors: @user.errors.full_messages }, status: :bad_request
  end

  private

  def set_user
    begin
      @user = User.find_by_id(params[:id])
      return render json: { errors: [User.not_found_message] }, status: :not_found if @user.blank?
    rescue => exception
      return render json: { errors: [User.not_found_message] }, status: :not_found
    end
  end

  def serialize_data data
    data.as_json(
        # except: [:created_at, :updated_at],
        include: [
            gender: { only: [:name] },
            user_type: { only: [:name] }
        ]
    )
  end

  def user_params
    params.require(:user).permit(:password, :password_confirmation, :current_password)
  end
end
