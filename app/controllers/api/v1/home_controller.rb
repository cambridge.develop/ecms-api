class Api::V1::HomeController < Api::V1::ApiBaseController
  def global_search
    students = Student.includes(:gender, :user, :school).ransack(g: {
        "0": {
            m: "or",
            code_cont: params[:text_search],
            user_full_name_cont: params[:text_search],
            user_email_cont: params[:text_search],
            user_phone_cont: params[:text_search]
        }
    }).result.limit(10)

    parents = Parent.includes(:gender, :user).ransack(g: {
        "0": {
            m: "or",
            user_full_name_cont: params[:text_search],
            user_email_cont: params[:text_search],
            user_phone_cont: params[:text_search]
        }
    }).result.limit(10)

    class_grades = ClassGrade.includes(:courses, :branch).ransack(g: {
        "0": {
            m: "or",
            name_cont: params[:text_search],
            code_cont: params[:text_search],
        }
    }).result.limit(10)

    return_object = {
        students: {
            group_name: 'Học viên',
            data: convert_student_data(students)
        },
        parents: {
            group_name: 'Phụ huynh',
            data: convert_parent_data(parents)
        },
        class_grades: {
            group_name: 'Lớp học',
            data: convert_class_grade_data(class_grades)
        }
    }
    render json: return_object, status: :ok
  end

  private

  def convert_student_data data
    result = []
    data.each do |student|
      result << { title: student.user.full_name, code: student.code, phone: student.user.phone, birthday: student.birthday ,url: "/students/#{student.id}" }
    end
    result
  end

  def convert_parent_data data
    result = []
    data.each do |parent|
      result << { title: parent.user.full_name, phone: parent.user.phone, url: "/phu-huynh/#{parent.id}"}
    end
    result
  end

  def convert_class_grade_data data
    result = []
    data.each do |class_grade|
      result << { title: class_grade.name, code: class_grade.code, branch: class_grade.branch.name, start_date: class_grade.start_date, end_date: class_grade.end_date,url: "/lop/#{class_grade.id}"}
    end
    result
  end


  def serialize_student_data data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            gender: { only: %i[name code] },
            user: { only: %i[full_name username email phone] },
            school: { only: %i[id name code literacy_id] }
        ]
    )
  end

  def serialize_parent_data data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            gender: { only: %i[name code] },
            user: { only: %i[full_name username email phone] }
        ]
    )
  end

  def serialize_class_grade_data data
    data.as_json()
  end


end