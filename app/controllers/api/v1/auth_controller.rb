class Api::V1::AuthController < Api::V1::ApiBaseController
  skip_before_action :authenticate_user!, :selected_branch, :check_user
  def reset_password
    users = User.where(email: params[:email])
    
    return render json: { errors: User.not_found_message }, status: :bad_request if users.blank?
    @user = users.first
    @user.generate_reset_password_token
    if @user.save
      AuthMailer.reset_password(@user).deliver_later
      render  json: serialize_data(@user), status: :ok
    else
      # render status: :internal_server_error
      render json: { errors: @user.errors.full_messages }, status: :bad_request
    end
  end

  def new_password
    users = User.where(reset_password_token: params[:token])
    return render json: { errors: User.not_found_message }, status: :bad_request if users.blank?
    @user = users.first
    return render json: { errors: User.reset_password_token_invalid_message }, status: :bad_request if @user.password_token_valid? == false
    @user.password = new_password_params[:password]
    @user.reset_password_token = nil
    @user.reset_password_sent_at = nil
    if @user.save
      AuthMailer.change_password(@user).deliver_later
      render  json: serialize_data(@user), status: :ok
    else
      render json: { errors: @user.errors.full_messages }, status: :bad_request
    end
  end

  private
  def serialize_data data
    data.as_json(
        only: %i[id email username reset_password_token reset_password_sent_at]
    )
  end

  def new_password_params
    params[:user].permit(:password, :password_confirmation)
  end
end
