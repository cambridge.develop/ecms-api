class Api::V1::Authorization::PermissionsController < Api::V1::Authorization::AuthorizationBaseController
  def index
    @permissions = []
    # if(current_user.user_type.is_admin?)
    #   @permissions = Permission.where(":admin_type = ANY(user_types)", admin_type: UserType.admin.id)
    # else
    #   @permissions = Permission.joins(permission_and_user_groups: {user_group: :user_and_user_groups})
    #                             .where(user_groups: {branch_id: @selected_branch.id}, 
    #                                     user_and_user_groups: {user_id: current_user.id})
    # end
    @permissions = Permission.all
    render json: {permissions: @permissions}, status: :ok
  end
end