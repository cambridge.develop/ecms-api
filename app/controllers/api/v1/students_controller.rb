class Api::V1::StudentsController < Api::V1::ApiBaseController
  before_action :set_student, except: [:index, :create]

  def index
    # class_in_branch = ClassGrade.where(branch_id: @selected_branch).pluck(:id)
    # student_course_in_branch = StudentCourse.joins(:course).where(:courses => {:class_grade_id => class_in_branch}).pluck(:id)
    # @q = Student.joins(:student_courses).where(:student_courses => {:id => student_course_in_branch})
    @q = Student.includes(:school, :user, :gender, parents: :user).where.not(status: Student.statuses[:deleted]).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['status asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      return_obj[:students] = serialize_data @q.result
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, student_list = pagy @q.result, items: params[:items], page: current_page
      p @q.result
      return_obj[:students] = serialize_data student_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def statistics
    return_obj = {
        class_grade_count: @student.class_grades.size,
        course_count: @student.courses.size,
        student_certificate_count: @student.student_certificates.size
    }
    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data(@student), status: :ok
  end

  def create
    student = Student.new student_params
    user_student = User.new student_user_params

    #Create new Student
    user_student.generate_user
    user_student.user_type = UserType.student

    MainRecordBase.transaction do
      user_student.save!

      student.user_id = user_student.id
      student.save!

      # Create or add parent(s) for student
      registered_parents = parent_params.as_json["parents"] || []
      registered_parents.each do |registered_parent|
        #Create new Parent if not exist
        p registered_parent
        parent_id = registered_parent["id"]
        if parent_id.blank?
          user_parent = User.new(
              # email: registered_parent["email"],
              full_name: registered_parent["full_name"],
              gender_id: registered_parent["gender_id"],
              user_type_id: UserType.par.id)
          user_parent.generate_user
          user_parent.save!
          parent = Parent.new(address: student.address,
                              # address: registered_parent["address"],
                              # description: registered_parent["description"],
                              birthday: registered_parent["birthday"],
                              current_job: registered_parent["current_job"],
                              user_id: user_parent.id)
          parent.save!
        else
          parent = Parent.find_by_id(parent_id)
          if parent.blank?
            render json: { errors: [Parent.not_found_message] }, status: :not_found
            raise ActiveRecord::Rollback
          end
        end
        student_parent = StudentParent.new(student: student, parent: parent, relation: registered_parent["relation"])
        student_parent.save!
      end

      # Create for add student-certificate
      registered_certificates = student_certificates_params.as_json["certificates"] || []
      registered_certificates.each do |registered_certificate|
        certificate = Certificate.find_by_id(registered_certificate["certificate_id"])
        if certificate.blank?
          render json: { errors: [Certificate.not_found_message] }, status: :not_found
          raise ActiveRecord::Rollback
        end
        StudentCertificate.create!(student_id: student.id,
                                   certificate_id: certificate.id,
                                   issue_organization: registered_certificate["issue_organization"],
                                   issue_date: registered_certificate["issue_date"],
                                   expiration_date: registered_certificate["expiration_date"],
                                   license_number: registered_certificate["license_number"],
                                   point: registered_certificate["point"])
      end

      # Create for add student-objective
      registered_objectives = student_objectives_params[:objectives] || []
      registered_objectives.each do |registered_objective|
        objective = Objective.find_by_id(registered_objective)
        if objective.blank?
          return render json: { errors: [Objective.not_found_message] }, status: :not_found
          raise ActiveRecord::Rollback
        end
        StudentObjective.create!(student: student, objective: objective)
      end

      # Create for add student-reference source
      registered_reference_sources = student_reference_sources_params[:reference_sources] || []
      registered_reference_sources.each do |registered_reference_source|
        reference_source = ReferenceSource.find_by_id(registered_reference_source)
        if reference_source.blank?
          render json: { errors: [ReferenceSource.not_found_message] }, status: :not_found
          raise ActiveRecord::Rollback
        end
        StudentReferenceSource.create!(student: student, reference_source: reference_source)
      end

      # Create for add student-potential
      registered_student_potential = student_potentials_params.as_json["potentials"] || []
      registered_student_potential.each do |registered_student_potential|
        curriculum = Curriculum.find_by_id(registered_student_potential["curriculum_id"])
        if curriculum.blank?
          render json: { errors: [Curriculum.not_found_message] }, status: :not_found
          raise ActiveRecord::Rollback
        end
        StudentPotential.create!(student: student,
                                 branch: @selected_branch,
                                 curriculum: curriculum,
                                 name: registered_student_potential["name"],
                                 description: registered_student_potential["description"])
        # status: registered_student_potential["status"])
      end

      render json: serialize_student_data(student), status: :ok
        # raise ActiveRecord::Rollback
    rescue ActiveRecord::RecordInvalid => invalid
      render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
      raise ActiveRecord::Rollback
    end
  end

  def update
    begin
      user = @student.user
      MainRecordBase.transaction do
        @student.attributes = student_update_params.except(:user)
        user.attributes = student_update_params[:user]
        @student.save!
        user.save!

        # student_user_params = if user_params[:password] == ''
        #                         user_params.except(:password)
        #                       else
        #                         user_params
        #                       end
        # user.attributes = student_user_params
        # user.save!
        # @student.attributes = student_params
        # student_data_change = student.changes
        # text_data_change = detect_object_changed(Student, student_data_change)
        # StudentLog.create(student_id: student.id,
        #                   action_type: StudentLog::ACT_CHANGE,
        #                   current_name: user.full_name,
        #                   content: "Học viên #{user.full_name} được cập nhật trên hệ thống, #{text_data_change}",
        #                   changed_time: DateTime.now,
        #                   changed_by: current_user.email)
        #
        # user_log(UserLog::ACT_UPDATE,
        #          "Học viên",
        #          "Cập nhật",
        #          "Học viên #{user.full_name} được cập nhật trên hệ thống, #{text_data_change}")
        #
        # @student.save!
        render json: serialize_data(@student), status: :ok
      rescue ActiveRecord::RecordInvalid => invalid
        render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
        raise ActiveRecord::Rollback
      end
    end
  end

  # Chỉ đổi trạng thái chứ không xóa khỏi hệ thống
  def destroy
    MainRecordBase.transaction do
      @student.status = 99
      @student.save!
      render json: { student: serialize_data(@student), messages: [Student.destroy_success_message(@student.user.full_name)] }, status: :ok
    rescue ActiveRecord::RecordInvalid => invalid
      render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
      raise ActiveRecord::Rollback
    end
  end

  def student_courses
    student_courses = @student.student_courses.includes(:receipt, :transfer_from_course, :course => [:course_schedule, :teacher_courses => [:teacher => [:user]], :class_grade => [:branch, :courses]])
    @q = student_courses.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['status asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      return_obj[:student_courses] = serialize_student_courses @q.result
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, student_courses = pagy @q.result, items: params[:items], page: current_page
      return_obj[:student_courses] = serialize_student_courses student_courses
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def class_grades
    student_class_grades = @student.class_grades.includes(:branch, :curriculum, :courses)
    @q = student_class_grades.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['status asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      return_obj[:class_grades] = serialize_student_class_grades @q.result
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, student_class_grades = pagy @q.result, items: params[:items], page: current_page
      return_obj[:class_grades] = serialize_student_class_grades student_class_grades
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def student_certificates
    certificates = @student.student_certificates.includes(:certificate)
    @q = certificates.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['status asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      return_obj[:certificates] = serialize_student_certificates @q.result
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, certificate_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:certificates] = serialize_student_certificates certificate_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def student_reference_sources
    student_reference_sources = @student.student_reference_sources.includes(:reference_source)
    @q = student_reference_sources.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['status asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      return_obj[:student_reference_sources] = serialize_student_reference_sources @q.result
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, reference_source_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:student_reference_sources] = serialize_student_reference_sources reference_source_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def student_objectives
    objectives = @student.student_objectives.includes(:objective)
    @q = objectives.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['status asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      return_obj[:objectives] = serialize_student_objectives @q.result
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, objective_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:objectives] = serialize_student_objectives objective_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def student_potentials
    student_potentials = @student.student_potentials.includes(:curriculum, :branch)
    @q = student_potentials.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['created_date desc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      return_obj[:student_potentials] = serialize_student_potentials @q.result
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, student_potential_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:student_potentials] = serialize_student_potentials student_potential_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def student_logs
    student_logs = StudentLog.where(student_id: @student.id).order_by(changed_time: :desc)
    return_obj = {}
    if params.has_key?(:all)
      return_obj[:student_logs] = return_logs_data student_logs
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      current_page = params[:page] || 1
      total_page = ((student_logs.size) / 10.to_f).ceil
      student_logs = student_logs.limit(10).offset((current_page.to_i - 1) * 10)
      return_obj[:student_logs] = return_logs_data student_logs
      return_obj[:current_page] = current_page.to_i
      return_obj[:total_page] = total_page
    end

    render json: return_obj, status: :ok
  end

  #thay thế bằng student_parents
  def parents
    parents = @student.parents.includes(:gender)
    @q = parents.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['status asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      return_obj[:parents] = serialize_parents @q.result
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, parent_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:parents] = serialize_parents parent_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def student_parents
    parents = @student.student_parents.includes({ parent: [user: [:gender]] }, :student)
    @q = parents.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['status asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      return_obj[:student_parents] = serialize_student_parents @q.result
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, parent_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:student_parents] = serialize_student_parents parent_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def update_test_result
    @student.test_result = params[:test_result]
    return render json: { messages: [Student.update_success_message(@student.user.full_name)] }, status: :ok if @student.save
    render json: { errors: @student.errors.full_messages }, status: :bad_request
  end

  private

  def set_student
    begin
      @student = Student.includes(:user, :gender, :school, parents: :user).find_by_id(params[:id])
      return render json: { errors: [Student.not_found_message] }, status: :not_found if @student.blank?
    rescue => exception
      return render json: { errors: [Student.not_found_message] }, status: :not_found
    end
  end

  def student_params
    params[:student].permit(:code,
                            :description,
                            # :literacies_id,
                            :school_id,
                            :class_name,
                            :birthday,
                            # :current_job,
                            :status,
                            :address,
                            :test_result
    )
  end

  def student_certificates_params
    params[:student].permit(:certificates => [:certificate_id, :issue_organization, :issue_date, :expiration_date, :license_number, :point])
  end

  def student_objectives_params
    params[:student].permit(objectives: [])
  end

  def student_reference_sources_params
    params[:student].permit(reference_sources: [])
  end

  def student_potentials_params
    params[:student].permit(:potentials => [:curriculum_id, :name, :description])
  end

  def student_user_params
    params[:student].permit(:email,
                            :full_name,
                            :phone,
                            :gender_id
    )
  end

  def parent_params
    params[:parent].permit(parents: %i[id address description birthday current_job email full_name phone gender_id relation])
  end

  def user_params
    params[:student].permit(:email,
                            :full_name,
                            :phone,
                            :gender_id,
                            :username,
                            :password
    )
  end

  def student_update_params
    params[:student].permit(:address,
                            :description,
                            :birthday,
                            :school_id,
                            :test_result,
                            :class_name,
                            user: %i[full_name phone gender_id]
    )
  end


  def serialize_data data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            gender: { only: %i[id name code] },
            user: { only: %i[id full_name username email locked_at phone] },
            school: { only: %i[id name code literacy_id] },
            parents: { include: [user: { only: %i[id full_name] }] }
        ]
    )
  end

  def serialize_data_for_show data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            gender: { only: %i[id name code] },
            user: { only: %i[id full_name username email] },
            school: { only: %i[id name code literacy_id] },
            parents: { include: [user: { only: %i[id full_name] }] }
        ]
    )
  end

  def serialize_student_data data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            gender: { only: %i[id name code] },
            user: { only: %i[id full_name username email initial_password] },
            school: { only: %i[id name code] },
            parents: { except: %i[created_at updated_at], include: [user: { only: %i[id full_name email initial_password] }] },
            student_objectives: { except: %i[created_at updated_at], include: [objective: { only: %i[id name code] }] },
            student_certificates: { except: %i[created_at updated_at], include: [certificate: { only: %i[id name code] }] },
            student_reference_sources: { except: %i[created_at updated_at], include: [reference_source: { only: %i[id name code] }] },
            student_potentials: { except: %i[created_at updated_at], include: [curriculum: { only: %i[id name code] }, branch: { only: %i[id name code] }] }
        ]
    )
  end

  def serialize_student_certificates data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            certificate: { except: %i[created_at updated_at] }
        ]
    )
  end

  def serialize_student_objectives data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            objective: { except: %i[created_at updated_at] }
        ]
    )
  end

  def serialize_student_potentials data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            curriculum: { only: %i[id name code] },
            branch: { only: %i[id name code] }
        ]
    )
  end

  def serialize_student_class_grades data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            branch: { only: %i[id name] },
            curriculum: { only: %i[id name] },
        ]
    )
  end

  def serialize_parents data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            gender: { only: %i[id name code] }
        ]
    )
  end

  def serialize_student_parents data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            parent: {
                except: %i[created_at updated_at],
                include: [
                    user: {
                        only: %i[id email full_name username phone must_change_password initial_password locked_at],
                        include: [
                            gender: { only: %i[id name code] }
                        ]
                    }
                ]
            }
        ]
    )
  end

  def serialize_student_reference_sources data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            reference_source: { except: %i[created_at updated_at] }
        ]
    )
  end

  def serialize_student_courses data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            course: { except: %i[created_at updated_at],
                      include: [
                          course_schedule: { except: %i[created_at updated_at] },
                          teacher_courses: {
                              except: %i[created_at updated_at],
                              include: [
                                  teacher: {
                                      except: %i[created_at updated_at],
                                      include: [user: { only: %i[id full_name] }]
                                  },
                              ]
                          },
                          class_grade: {
                              include: [branch: { only: %i[id name] }]
                          }
                      ] },
            transfer_from_course: { except: %i[created_at updated_at] }
        ]
    )
  end

  def return_logs_data student_logs
    return_data = []
    student_logs.each do |log|
      return_data << { action_type: log.action_type_text, content: log.content, changed_time: log.changed_time.strftime("%d/%m/%Y %H:%M:%S"), changed_by: log.changed_by }
    end
    return_data
  end

end