class Api::V1::UserAndUserGroupsController < Api::V1::ApiBaseController

  # def create
  #   user = User.find_by_id(permission_and_user_group_params[:user_id])
  #   user_group = UserGroup.find_by_id(permission_and_user_group_params[:user_group_id])
  #
  #   return render json: { errors: [User.not_found_message] }, status: :not_found if user.blank?
  #   return render json: { errors: [UserGroup.not_found_message] }, status: :not_found if user_group.blank?
  #
  #   exist_user_and_user_group = UserAndUserGroup.where(permission_id: permission.id, permission_group_id: user_group.id)
  #   return render json: { errors: [UserAndUserGroup.exist_record_reference_message(User, UserGroup)] } if exist_user_and_user_group.blank? == false
  #
  #   user_and_user_group = UserAndUserGroup.new user_and_user_group_params
  #   return (render json: { messages: [UserAndUserGroup.add_reference_success_message(UserAndUserGroup, UserGroup)] }, status: :ok) if user_and_user_group.save
  #   return render json: { errors: [user_and_user_group.errors.full_messages]}, status: :bad_request
  # end

  def create
    MainRecordBase.transaction do
      users = User.where(id: user_and_user_group_params[:user_id].uniq).pluck(:id)
      user_group = UserGroup.where(id: user_and_user_group_params[:user_group_id]).first
      user_and_user_groups =  users.map do |user_id|
        UserAndUserGroup.new(user_id: user_id, user_group_id: user_group.id)
      end
      UserAndUserGroup.import user_and_user_groups
      return (render json: { messages: [UserAndUserGroup.add_reference_success_message(User, UserGroup)] }, status: :ok)
    rescue ActiveRecord::RecordInvalid => invalid
      render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
      raise ActiveRecord::Rollback
    end
  end

  def delete_users
    MainRecordBase.transaction do
      users = User.where(id: user_and_user_group_params[:user_id].uniq)
      user_group = UserGroup.where(id: user_and_user_group_params[:user_group_id]).first
      UserAndUserGroup.where(user_id: users.ids, user_group_id: user_group.id).delete_all
      return (render json: { messages: [UserAndUserGroup.destroy_reference_success_message(User, UserGroup)] }, status: :ok)
    rescue ActiveRecord::RecordInvalid => invalid
      render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
      raise ActiveRecord::Rollback
    end
  end

  def destroy
    user = User.find_by_id(permission_and_user_group_params[:user_id])
    user_group = UserGroup.find_by_id(permission_and_user_group_params[:user_group_id])

    return render json: { errors: [User.not_found_message] }, status: :not_found if user.blank?
    return render json: { errors: [UserGroup.not_found_message] }, status: :not_found if user_group.blank?

    exist_user_and_user_group = UserAndUserGroup.where(permission_id: permission.id, permission_group_id: user_group.id)
    return render json: { errors: [UserAndUserGroup.not_found_message] } if exist_user_and_user_group.blank?

    return (render json: { messages: [UserAndUserGroup.destroy_reference_success_message(UserAndUserGroup, UserGroup)] }, status: :ok) if exist_user_and_user_group.destroy
    return render json: { errors: [exist_user_and_user_group.errors.full_messages]}, status: :bad_request
  end

  private

  def user_and_user_group_params
    params[:user_and_user_group].permit( :user_group_id, user_id: [])
  end
end
