class Api::V1::Report::Debt::DetailsController < Api::V1::Report::Debt::DebtBaseController
  def class_grades
    result_obj = {}
    courses = []
    class_in_branch = ClassGrade.find_by_id(debt_detail_params[:class_grade_id])
    return render json: { errors: [ClassGrade.not_found_message] }, status: :not_found if class_in_branch.blank?
    result_obj[:id] = class_in_branch.id
    result_obj[:name] = class_in_branch.name
    course_in_class = class_in_branch.courses
    course_in_class.each do |course|
      student_courses = course.student_courses.includes(:course, :student => [:user])
      course_debt = 0
      students = []
      student_courses.each do |item|
        if item.debt > 0
          student_obj = {}
          student_obj[:name] = item.student.user.full_name
          student_obj[:code] = item.student.code
          student_obj[:phone] = item.student.user.phone
          student_obj[:debt_amount] = item.debt
          course_debt += item.debt
          students << student_obj
        end
      end
      course_obj = {}
      course_obj[:name] = course.name
      course_obj[:code] = course.code
      course_obj[:course_no] = course.course_no
      course_obj[:branch] = class_in_branch.branch
      course_obj[:class_grade] = class_in_branch
      course_obj[:curriculum] = class_in_branch.curriculum
      course_obj[:students] = students
      course_obj[:total_debt] = course_debt
      courses << course_obj if course_debt > 0
    end
    result_obj[:courses] = courses
    total_debt = courses.inject(0) {|total, item| total + item[:total_debt]}
    result_obj[:total_debt] = total_debt
    render json: result_obj, status: :ok
  end

  def search_student
    @q = Student.all
    @q = @q.includes(:school, :user, :gender, :parents, :student_courses).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['created_at desc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      student_list = @q.result
      return_obj[:students] = serialize_data student_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, student_list = pagy @q.result, items: params[:items], page: current_page
      # student_list.each do |s|
      #   s.is_not_finish_study_fee = s.is_not_finish_study_fee?
      # end
      return_obj[:students] = serialize_data student_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def students
    student = Student.find_by_id(params[:student][:id])
    result_obj = {}
    result_obj[:id] = student.id
    result_obj[:name] = student.user.full_name
    result_obj[:code] = student.code
    class_grades = []
    classes = student.class_grades.includes(:curriculum => [:level => [:major]], :courses => [:student_courses])
    classes.each do |class_grade|
      class_grade_obj = {}
      courses = []
      class_grade_obj[:id] = class_grade.id
      class_grade_obj[:name] = class_grade.name
      class_grade_obj[:level] = class_grade.curriculum.level.name
      class_grade_obj[:major] = class_grade.curriculum.level.major.name
      class_grade_obj[:curriculum] = class_grade.curriculum.name
      course_in_class = class_grade.courses
      course_in_class.each do |course|
        student_course = course.student_courses.includes(:course).find_by_student_id(student.id)
        p student_course
        if !student_course.blank?
          course_debt = 0
          if student_course.debt > 0
            course_debt += student_course.debt
          end
          course_obj = {}
          course_obj[:id] = course.id
          course_obj[:name] = course.name
          course_obj[:course_no] = course.course_no
          course_obj[:debt_amount] = course_debt
          courses << course_obj if course_debt > 0
        end
      end
      class_grade_obj[:courses] = courses
      total_debt = courses.inject(0) {|total, item| total + item[:debt_amount]}
      class_grade_obj[:total_debt] = total_debt
      class_grades << class_grade_obj if courses.size > 0
    end
    result_obj[:class_grades] = class_grades
    total_debt = class_grades.inject(0) {|total, item| total + item[:total_debt]}
    result_obj[:total_debt] = total_debt
    render json: result_obj, status: :ok
  end

  private
  def serialize_data data
    data.as_json(
        include: [
            gender: { except: [:created_at, :updated_at] },
            user: { except: [:created_at, :updated_at] }
        ]
    )
  end

  def debt_detail_params
    params.require(:debt).permit(:class_grade_id)
  end

end
