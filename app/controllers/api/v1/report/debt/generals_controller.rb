class Api::V1::Report::Debt::GeneralsController < Api::V1::Report::Debt::DebtBaseController
  def class_grades
    begin
      # Báo cáo nợ theo chuyên ngành
      result_obj = {}
      branch_list = Branch.where(id: debt_generals_params[:branch_id])
      branches = []
      branch_list.each do |branch|
        branch_result_obj = {}
        total_debt = 0
        class_grades = []
        class_in_branch = ClassGrade.includes(:curriculum => [:level => [:major]], :courses => [:student_courses]).where(branch_id: branch.id)
        class_in_branch.each do |class_grade|
          class_grade_obj = {}
          courses = []
          class_grade_obj[:id] = class_grade.id
          class_grade_obj[:name] = class_grade.name
          class_grade_obj[:code] = class_grade.code
          class_grade_obj[:start_date] = class_grade.start_date
          class_grade_obj[:end_date] = class_grade.end_date
          class_grade_obj[:level] = class_grade.curriculum.level
          class_grade_obj[:major] = class_grade.curriculum.level.major
          class_grade_obj[:curriculum] = class_grade.curriculum
          course_in_class = class_grade.courses
          course_in_class.each do |course|
            student_courses = course.student_courses.includes(:course)
            course_debt = 0
            debt_number = 0
            student_courses.each do |item|
              if item.debt > 0
                course_debt += item.debt
                debt_number += 1
              end
            end
            course_obj = {}
            course_obj[:id] = course.id
            course_obj[:name] = course.name
            course_obj[:number_of_students] = course.student_courses.size
            course_obj[:debt_amount] = course_debt
            course_obj[:debt_number] = debt_number
            courses << course_obj if course_debt > 0
          end
          class_grade_obj[:courses] = courses
          total_debt = courses.inject(0) {|total, item| total + item[:debt_amount]}
          class_grade_obj[:total_debt] = total_debt
          class_grades << class_grade_obj if courses.size > 0
        end
        branch_result_obj[:phone] = branch.phone
        branch_result_obj[:email] = branch.email
        branch_result_obj[:address] = branch.address
        branch_result_obj[:total_debt] = class_grades.inject(0) {|total, item| total + item[:total_debt]}
        branch_result_obj[:class_grades] = class_grades
        branch_result_obj[:id] = branch.id
        branch_result_obj[:name] = branch.name
        branches << branch_result_obj
      end
      result_obj[:total_debt] = branches.inject(0) {|total, item| total + item[:total_debt]}
      result_obj[:branches] = branches
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: {messages: [invalid_date_format_message]}, status: :bad_request
    end
  end

  private

  def debt_generals_params
    params.require(:debt).permit(branch_id: [])
  end

end
