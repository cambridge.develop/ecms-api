class Api::V1::Report::IncomeAndDebt::BranchesController < Api::V1::Report::IncomeAndDebt::IncomeAndDebtBaseController
  def class_grades
    begin
      date_from = Date.parse(params[:from])
      date_to = Date.parse(params[:to])
      unless date_to >= date_from
        return render json: {errors: [ClassGrade.invalid_date_range_message]},
                      status: :bad_request
      end
      result_obj = {}
      class_list = []
      class_in_branch = ClassGrade.where(branch_id: @selected_branch).where("start_date <= ? AND ? <= end_date", date_from, date_to).includes(:courses, :branch, :curriculum)
      class_in_branch.each do |class_item|
        student_courses = StudentCourse.joins(:course).where(:courses => {:class_grade_id => class_item.id}).includes(:course)
        receipts = Receipt.joins(:student_course).where(:student_courses => {:id => student_courses})
                       .where("receipts.created_at >= ? and receipts.created_at <= ?", date_from.beginning_of_day, date_to.end_of_day)
                       .where(is_cancel: false).uniq
                       .pluck(:id, :actual_paid_amount, :paid_amount)
        debt = 0
        student_courses.each do |student_course|
          if student_course.debt > 0 && student_course.is_cancel == false
            debt += student_course.debt
          end
        end
        class_obj = {
            id: class_item.id,
            name: class_item.name,
            code: class_item.code,
            start_date: class_item.start_date,
            end_date: class_item.end_date,
            curriculum: class_item.curriculum,
            total_income: receipts.inject(0) { |total, receipt| total + receipt[1] },
            total_debt: debt,
            # total_discount: ""
        }
        class_list << class_obj
      end
      branch_obj = {}
      branch_obj[:name] = @selected_branch.name
      branch_obj[:phone] = @selected_branch.phone
      branch_obj[:address] = @selected_branch.address
      branch_obj[:email] = @selected_branch.email
      branch_obj[:logo_base64] = @selected_branch.logo_base64
      branch_obj[:class_grades] = class_list
      result_obj[:branch] = branch_obj
      render json: result_obj, status: :ok
    end
  end
end