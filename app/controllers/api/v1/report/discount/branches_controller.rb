class Api::V1::Report::Discount::BranchesController < Api::V1::Report::Discount::DiscountBaseController
  def students
    begin
      date_from = Date.parse(params[:from])
      date_to = Date.parse(params[:to])
      unless date_to >= date_from
        return render json: { errors: [ClassGrade.invalid_date_range_message] },
                      status: :bad_request
      end
      result_obj = {}
      class_list = []
      class_in_branch = ClassGrade.where(branch_id: @selected_branch).includes(:courses, :branch, :curriculum)
      return render json: { errors: [ClassGrade.not_found_message] }, status: :not_found if class_in_branch.blank?
      class_in_branch.each do |class_grade|
        student_courses = StudentCourse.joins(:course).where(:courses => {:class_grade_id => class_grade.id})
        receipts = Receipt.where(student_course_id: student_courses.ids)
                          .where("receipts.created_at >= ? and receipts.created_at <= ?", date_from.beginning_of_day, date_to.end_of_day)
                          .where.not(discount_number: 0)
                          .where(is_cancel: false).includes(:creator, :student_course => [:receipt, :course, :student => [:user]]).uniq
        class_obj = {
            name: class_grade.name,
            branch: class_grade.branch,
            receipts: receipt_data(receipts)
        }
        class_list << class_obj if receipts.size > 0
      end
      # result_obj[:class_grades] = class_list
      branch_obj = {}
      branch_obj[:name] = @selected_branch.name
      branch_obj[:phone] = @selected_branch.phone
      branch_obj[:address] = @selected_branch.address
      branch_obj[:email] = @selected_branch.email
      branch_obj[:logo_base64] = @selected_branch.logo_base64
      branch_obj[:class_grades] = class_list
      result_obj[:branch] = branch_obj
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: { errors: [invalid_date_format_message] }, status: :bad_request
    end
  end

  private

  def receipt_data data
      data.as_json(
          only: %i[discount_number discount_percentage description_receipt created_at],
          include: [
              creator: { only: %i[name full_name]},
              student_course: {
                  only: %i[actual_tuition_need_to_paid],
                  include: [course: { only: %i[name course_no] }, student: { only: %i[name code], include: [user: {only: %i[name full_name]}] } ]
              }
                   ]
      )
  end
end