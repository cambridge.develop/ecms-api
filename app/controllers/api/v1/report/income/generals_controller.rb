class Api::V1::Report::Income::GeneralsController < Api::V1::Report::Income::IncomeBaseController
  def day
    begin
      # Báo cáo doanh thu theo trung tâm theo ngày
      date_from = day_params[:from]
      date_to = day_params[:to]
      date_from = date_from != "" ? Date.parse(date_from) : Date.today
      date_to =  date_to != "" ? Date.parse(date_to) : Date.today
      branch_list = Branch.where(id: day_params[:branch_list])
      return render json: {messages: [invalid_date_range_message]}, status: :bad_request unless date_from <= date_to
      result_obj = {}
      branches = []
      branch_list.each do |branch|
        class_in_branch = ClassGrade.where(branch_id: branch.id).pluck(:id)
        student_course = StudentCourse.joins(:course).where(:courses => {:class_grade_id => class_in_branch})
        branch_result_obj = {}
        incomes = []
        (date_from..date_to).each do |day|
          income = {}
          receipts = Receipt.joins(:student_course).where(:student_courses => {:id => student_course})
                         .where("receipts.created_at >= ? and receipts.created_at <= ?", day.beginning_of_day, day.end_of_day).uniq
                         .pluck(:id, :actual_paid_amount, :paid_amount)
          income[:date] = day.to_s
          income[:total_actual_paid_amount] = receipts.inject(0){|total, e| total + e[1]}
          income[:total_paid_amount] = receipts.inject(0){|total, e| total + e[2]}
          incomes << income
        end
        branch_result_obj[:id] = branch.id
        branch_result_obj[:name] = branch.name
        branch_result_obj[:incomes] = incomes
        branches << branch_result_obj
      end
      result_obj[:branches] = branches
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: {messages: [invalid_date_format_message]}, status: :bad_request
    end
  end

  def month
    begin
      # Báo cáo doanh thu theo trung tâm theo tháng
      date_from = day_params[:from] || ""
      date_to = day_params[:to] || ""
      date_from = Date.parse(date_from)
      date_to = Date.parse(date_to)
      branch_list = Branch.where(id: day_params[:branch_list])
      return render json: {messages: [invalid_date_range_message]}, status: :bad_request unless date_from <= date_to
      result_obj = {}
      branches = []
      branch_list.each do |branch|
        class_in_branch = ClassGrade.where(branch_id: branch.id).pluck(:id)
        student_course = StudentCourse.joins(:course).where(:courses => {:class_grade_id => class_in_branch})
        branch_result_obj = {}
        incomes = []
        month_list = (date_from..date_to).to_a.map(&:beginning_of_month).uniq
        month_list.each do |day|
          income = {}
          receipts = Receipt.joins(:student_course).where(:student_courses => {:id => student_course})
                         .where("receipts.created_at >= ? and receipts.created_at <= ?", day.beginning_of_month.beginning_of_day, day.end_of_month.end_of_day).uniq
                         .pluck(:id, :actual_paid_amount, :paid_amount)
          income[:month] = "#{Date::MONTHNAMES[day.month]} (#{day.year})"
          income[:total_actual_paid_amount] = receipts.inject(0){|total, e| total + e[1]}
          income[:total_paid_amount] = receipts.inject(0){|total, e| total + e[2]}
          incomes << income
        end
        branch_result_obj[:id] = branch.id
        branch_result_obj[:name] = branch.name
        branch_result_obj[:incomes] = incomes
        branches << branch_result_obj
      end
      result_obj[:branches] = branches
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: {messages: [invalid_date_format_message]}, status: :bad_request
    end
  end

  def curriculums
    begin
      # Báo cáo doanh thu theo trung tâm theo ngày
      date_from = day_params[:from] || ""
      date_to = day_params[:to] || ""
      date_from = Date.parse(date_from)
      date_to = Date.parse(date_to)
      branch_list = Branch.where(id: day_params[:branch_list])
      return render json: {messages: [invalid_date_range_message]}, status: :bad_request unless date_to > date_from
      result_obj = {}
      branches = []
      branch_list.each do |branch|
        branch_result_obj = {}
        curriculums =  []
        class_in_branch = ClassGrade.where(branch_id: branch.id).includes(:curriculum)
        class_in_branch.each do |class_grade|
          student_course = StudentCourse.joins(:course).where(:courses => {:class_grade_id => class_grade.id})
          curriculum = {}
          receipts = Receipt.joins(:student_course).where(:student_courses => {:id => student_course})
                         .where("receipts.created_at >= ? and receipts.created_at <= ?", date_from.beginning_of_day, date_to.end_of_day).uniq
                         .pluck(:id, :actual_paid_amount, :paid_amount)
          curriculum[:id] = class_grade.curriculum.id
          curriculum[:name] = class_grade.curriculum.name
          curriculum[:total_actual_paid_amount] = receipts.inject(0){|total, e| total + e[1]}
          curriculum[:total_paid_amount] = receipts.inject(0){|total, e| total + e[2]}
          curriculums << curriculum unless curriculum[:total_actual_paid_amount] == 0 && curriculum[:total_paid_amount] == 0
        end
        branch_result_obj[:id] = branch.id
        branch_result_obj[:name] = branch.name
        branch_result_obj[:curriculums] = curriculums
        branches << branch_result_obj
      end
      result_obj[:branches] = branches
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: {messages: [invalid_date_format_message]}, status: :bad_request
    end
  end

  def class_grades
    begin
      date_from = Date.parse(income_generals_params[:from])
      date_to = Date.parse(income_generals_params[:to])
      unless date_to >= date_from
        return render json: { errors: [ClassGrade.invalid_date_range_message] },
                      status: :bad_request
      end

      result_obj = {}
      branch_list = []
      income_generals_params[:branch_id].uniq.each do |branch_id|
        branch = Branch.find_by_id(branch_id)
        class_in_branch = ClassGrade.where(branch_id: branch_id).includes(:courses, :branch, :curriculum)
        class_grades =  []
        class_in_branch.each do |class_grade|
          student_courses = StudentCourse.joins(:course).where(:courses => {:class_grade_id => class_grade.id})
          receipts = Receipt.joins(:student_course).where(:student_courses => {:id => student_courses})
                         .where("receipts.created_at >= ? and receipts.created_at <= ?", date_from.beginning_of_day, date_to.end_of_day)
                         .where(is_cancel: false).uniq
                         .pluck(:id, :actual_paid_amount, :paid_amount)
          class_grade_obj = {}
          class_grade_obj[:id] = class_grade.id
          class_grade_obj[:name] = class_grade.name
          class_grade_obj[:code] = class_grade.code
          class_grade_obj[:start_date] = class_grade.start_date
          class_grade_obj[:end_date] = class_grade.end_date
          class_grade_obj[:curriculum] = class_grade.curriculum
          class_grade_obj[:total_actual_paid_amount] = receipts.inject(0){|total, receipt| total + receipt[1]}
          class_grade_obj[:total_paid_amount] = receipts.inject(0){|total, receipt| total + receipt[2]}
          class_grades << class_grade_obj unless receipts.blank?
        end
        # class_grade_obj = {}
        # class_grade_obj[:class_grades] = class_grades
        branch_obj = {}
        branch_obj[:name] = branch.name
        branch_obj[:phone] = branch.phone
        branch_obj[:email] = branch.email
        branch_obj[:address] = branch.address
        branch_obj[:class_grades] = class_grades
        branch_list << branch_obj
      end
      result_obj[:branches] = branch_list
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: { errors: [invalid_date_format_message] }, status: :bad_request
    end
  end

  private

  def income_generals_params
    params.require(:income).permit(:from, :to, branch_id: [])
  end

end
