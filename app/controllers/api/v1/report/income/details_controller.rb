class Api::V1::Report::Income::DetailsController < ApplicationController
  def class_grades
    begin
      date_from = Date.parse(income_detail_params[:from])
      date_to = Date.parse(income_detail_params[:to])
      unless date_to >= date_from
        return render json: { errors: [ClassGrade.invalid_date_range_message] },
                      status: :bad_request
      end
      return render json: { errors: [ClassGrade.not_found_message] }, status: :not_found if income_detail_params[:class_grade_id].blank?
      result_obj = {}
      course_list = []
      class_in_branch = ClassGrade.find_by_id(income_detail_params[:class_grade_id])
      return render json: { errors: [ClassGrade.not_found_message] }, status: :not_found if class_in_branch.blank?
      courses = class_in_branch.courses
      courses.each do |course|
      student_courses = StudentCourse.joins(:course).where(:courses => {:class_grade_id => class_in_branch.id, :id => course.id})
      receipt_list =  []
      receipts = Receipt.where(student_course_id: student_courses.ids)
                     .where("receipts.created_at >= ? and receipts.created_at <= ?", date_from.beginning_of_day, date_to.end_of_day)
                     .where(is_cancel: false).includes(:creator, :student_course => [:course, :student => [:user]]).uniq
      receipts.each do |receipt|
        student_course = receipt.student_course
        receipt_obj = {}
        receipt_obj[:created_at] = receipt.created_at
        receipt_obj[:creator] = receipt.creator.full_name
        receipt_obj[:credit_used] = receipt.credit_used
        receipt_obj[:actual_paid_amount] = receipt.actual_paid_amount
        receipt_obj[:paid_amount] = receipt.paid_amount
        receipt_obj[:description_receipt] = receipt.description_receipt
        receipt_obj[:description_internal] = receipt.description_internal
        student_obj = {}
        student_obj[:code] = student_course.student.code
        student_obj[:name] = student_course.student.user.full_name
        receipt_obj[:student] = student_obj
        receipt_list << receipt_obj
      end
      course_obj = {}
      course_obj[:receipts] = receipt_list
      course_obj[:name] = course.name
      course_obj[:code] = course.code
      course_obj[:course_no] = course.course_no
      course_obj[:branch] = class_in_branch.branch
      course_obj[:class_grade] = class_in_branch
      course_obj[:curriculum] = class_in_branch.curriculum
      course_list << course_obj
    end
      result_obj[:courses] = course_list
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: { errors: [invalid_date_format_message] }, status: :bad_request
    end
  end

  private

  def income_detail_params
    params.require(:income).permit(:from, :to, :class_grade_id)
  end

end
