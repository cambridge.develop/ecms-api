class Api::V1::Report::Income::BranchesController < Api::V1::Report::Income::IncomeBaseController
  before_action :students_in_branch, except: :curriculums
  before_action :students_in_branch, except: :class_grades
  def day
    begin
      # Báo cáo doanh thu theo chi nhánh theo ngày
      date_from = day_params[:from]
      date_to = day_params[:to]
      date_from = date_from != "" ? Date.parse(date_from) : Date.today
      date_to =  date_to != "" ? Date.parse(date_to) : Date.today
      result_obj = {}
      incomes = []
      unless date_from <= date_to
        return render json: {messages: [invalid_date_range_message]}, status: :bad_request
      end
      (date_from..date_to).each do |day|
        income = {}
        receipts = Receipt.joins(:student_course).where(:student_courses => {:id => @student_course})
                       .where("receipts.created_at >= ? and receipts.created_at <= ?", day.beginning_of_day, day.end_of_day).uniq
                       .pluck(:id, :actual_paid_amount, :paid_amount)
        income[:date] = day.to_s
        income[:total_actual_paid_amount] = receipts.inject(0){|total, e| total + e[1]}
        income[:total_paid_amount] = receipts.inject(0){|total, e| total + e[2]}
        incomes << income
      end
      result_obj[:incomes] = incomes
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: {messages: [invalid_date_format_message]}, status: :bad_request
    end
  end

  def month
    # Báo cáo doanh thu theo chi nhánh theo tháng
    begin
      date_from = day_params[:from]
      date_to = day_params[:to]
      date_from = date_from != "" ? Date.parse(date_from) : Date.today
      date_to =  date_to != "" ? Date.parse(date_to) : Date.today
      result_obj = {}
      incomes = []
      unless date_from <= date_to
        return render json: {messages: [invalid_date_range_message]}, status: :bad_request
      end
      month_list = (date_from..date_to).to_a.map(&:beginning_of_month).uniq
      month_list.each do |day|
        income = {}
        receipts = Receipt.joins(:student_course).where(:student_courses => {:id => @student_course})
                       .where("receipts.created_at >= ? and receipts.created_at <= ?", day.beginning_of_month.beginning_of_day, day.end_of_month.end_of_day).uniq
                       .pluck(:id, :actual_paid_amount, :paid_amount)
        # income[:month] = "#{Date::MONTHNAMES[day.month]} (#{day.year})"
        income[:month] = "#{day.month}/#{day.year}"
        income[:total_actual_paid_amount] = receipts.inject(0){|total, e| total + e[1]}
        income[:total_paid_amount] = receipts.inject(0){|total, e| total + e[2]}
        incomes << income
      end
      result_obj[:incomes] = incomes
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: {messages: [invalid_month_format_message]}, status: :bad_request
    end
  end

  def curriculums
    begin
      # Báo cáo doanh thu theo chi nhánh theo giáo trình
      date_from = day_params[:from] || ""
      date_to = day_params[:to] || ""
      date_from = Date.parse(date_from)
      date_to = Date.parse(date_to)
      result_obj = {}
      unless date_to > date_from
        return render json: {messages: [invalid_date_range_message]}, status: :bad_request
      end
      curriculums =  []
      class_in_branch = ClassGrade.where(branch_id: @selected_branch).includes(:curriculum)
      class_in_branch.each do |class_grade|
        student_course = StudentCourse.joins(:course).where(:courses => {:class_grade_id => class_grade.id})
        curriculum = {}
        receipts = Receipt.joins(:student_course).where(:student_courses => {:id => student_course})
                       .where("receipts.created_at >= ? and receipts.created_at <= ?", date_from.beginning_of_day, date_to.end_of_day).uniq
                       .pluck(:id, :actual_paid_amount, :paid_amount)
        curriculum[:id] = class_grade.curriculum.id
        curriculum[:name] = class_grade.curriculum.name
        curriculum[:total_actual_paid_amount] = receipts.inject(0){|total, e| total + e[1]}
        curriculum[:total_paid_amount] = receipts.inject(0){|total, e| total + e[2]}
        unless curriculum[:total_actual_paid_amount] == 0 && curriculum[:total_paid_amount] == 0
          curriculums << curriculum
        end
      end
      result_obj[:curriculums] = curriculums
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: {messages: [invalid_date_format_message]}, status: :bad_request
    end
  end

  def class_grades
    begin
      date_from = Date.parse(income_branch_params[:from])
      date_to = Date.parse(income_branch_params[:to])
      unless date_to >= date_from
        return render json: { errors: [invalid_date_range_message] },
                      status: :bad_request
      end

      result_obj = {}
      class_grades =  []
      class_in_branch = ClassGrade.where(branch_id: @selected_branch).includes(:courses, :branch, :curriculum)
      class_in_branch.each do |class_grade|
        student_courses = StudentCourse.joins(:course).where(:courses => {:class_grade_id => class_grade.id})
        receipts = Receipt.joins(:student_course).where(:student_courses => {:id => student_courses})
                       .where("receipts.created_at >= ? and receipts.created_at <= ?", date_from.beginning_of_day, date_to.end_of_day)
                       .where(is_cancel: false).uniq
                       .pluck(:id, :actual_paid_amount, :paid_amount)
        class_grade_obj = {}
        class_grade_obj[:id] = class_grade.id
        class_grade_obj[:name] = class_grade.name
        class_grade_obj[:code] = class_grade.code
        class_grade_obj[:start_date] = class_grade.start_date
        class_grade_obj[:end_date] = class_grade.end_date
        class_grade_obj[:branch] = @selected_branch
        class_grade_obj[:curriculum] = class_grade.curriculum
        class_grade_obj[:total_actual_paid_amount] = receipts.inject(0){|total, receipt| total + receipt[1]}
        class_grade_obj[:total_paid_amount] = receipts.inject(0){|total, receipt| total + receipt[2]}
        class_grades << class_grade_obj unless receipts.blank?
      end
      branch_obj = {}
      branch_obj[:name] = @selected_branch.name
      branch_obj[:phone] = @selected_branch.phone
      branch_obj[:address] = @selected_branch.address
      branch_obj[:email] = @selected_branch.email
      branch_obj[:logo_base64] = @selected_branch.logo_base64
      branch_obj[:class_grades] = class_grades
      result_obj[:branch] = branch_obj
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: { errors: [invalid_date_format_message] }, status: :bad_request
    end
  end

  private

  def income_branch_params
    params.require(:income).permit(:from, :to)
  end

end
