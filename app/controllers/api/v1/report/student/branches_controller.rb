class Api::V1::Report::Student::BranchesController < Api::V1::Report::Student::StudentBaseController
  before_action :selected_branch, :students_in_branch
  def day
    begin
      # Báo cáo học viên theo chi nhánh theo ngày
      date_from = day_params[:from] || ""
      date_to = day_params[:to] || ""
      date_from = Date.parse(date_from)
      date_to = Date.parse(date_to)
      return render json: {messages: [invalid_date_range_message]}, status: :bad_request unless date_to > date_from
      result_obj = {}
      students = []
      (date_from..date_to).each do |day|
        student_obj = {}
        student_list = Student.includes(:user).joins(:student_courses).where(:student_courses => {:id => @student_course})
                           .where("student_courses.start_date >= ? and student_courses.start_date <=?", day, day).uniq
        student_obj[:date] = day.to_s
        student_obj[:student_list] = serialize_student_data student_list
        students << student_obj
      end
      result_obj[:student_report] = students
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: {messages: [invalid_date_format_message]}, status: :bad_request
    end
  end

  def month
    # Báo cáo học viên theo chi nhánh theo tháng
    begin
      date_from = day_params[:from] || ""
      date_to = day_params[:to] || ""
      date_from = Date.parse(date_from)
      date_to = Date.parse(date_to)
      result_obj = {}
      students = []
      return render json: {messages: [invalid_date_range_message]}, status: :bad_request unless date_to > date_from
      month_list = (date_from..date_to).to_a.map(&:beginning_of_month).uniq
      month_list.each do |day|
        student_obj = {}
        student_list = Student.includes(:user).joins(:student_courses).where(:student_courses => {:id => @student_course})
                           .where("student_courses.start_date >= ? and student_courses.start_date <=?", day.beginning_of_month, day.end_of_month).uniq
        student_obj[:month] = "#{Date::MONTHNAMES[day.month]} (#{day.year})"
        student_obj[:student_list] = student_list
        students << student_obj
      end
      result_obj[:student_report] = students
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: {messages: [invalid_month_format_message]}, status: :bad_request
    end
  end
end
