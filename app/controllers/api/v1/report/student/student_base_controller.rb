class Api::V1::Report::Student::StudentBaseController < Api::V1::Report::ReportBaseController
  protected
  def serialize_student_data data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            gender: { only: %i[id name code] },
            user: { only: %i[id full_name username email initial_password] },
        ]
    )
  end
end
