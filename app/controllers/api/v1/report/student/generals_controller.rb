class Api::V1::Report::Student::GeneralsController < Api::V1::Report::Student::StudentBaseController
  def day
    begin
      # Báo cáo học viên theo trung tâm theo ngày
      date_from = day_params[:from] || ""
      date_to = day_params[:to] || ""
      date_from = Date.parse(date_from)
      date_to = Date.parse(date_to)
      branch_list = Branch.where(id: day_params[:branch_list])
      return render json: {messages: [invalid_date_range_message]}, status: :bad_request unless date_to > date_from
      result_obj = {}
      branches = []
      branch_list.each do |branch|
        class_in_branch = ClassGrade.where(branch_id: branch.id).pluck(:id)
        student_course = StudentCourse.joins(:course).where(:courses => {:class_grade_id => class_in_branch})
        branch_result_obj = {}
        students = []
        (date_from..date_to).each do |day|
          student_obj = {}
          student_list = Student.includes(:user, :gender).joins(:student_courses).where(:student_courses => {:id => student_course})
                             .where("student_courses.start_date >= ? and student_courses.start_date <=?", day, day).uniq
          student_obj[:date] = day.to_s
          student_obj[:student_list] = serialize_student_data student_list
          students << student_obj
        end
        branch_result_obj[:id] = branch.id
        branch_result_obj[:name] = branch.name
        branch_result_obj[:student_report] = students
        branches << branch_result_obj
      end
      result_obj[:branches] = branches
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: {messages: [invalid_date_format_message]}, status: :bad_request
    end
  end

  def month
    begin
      # Báo cáo học viên theo trung tâm theo tháng
      date_from = day_params[:from] || ""
      date_to = day_params[:to] || ""
      date_from = Date.parse(date_from)
      date_to = Date.parse(date_to)
      branch_list = Branch.where(id: day_params[:branch_list])
      return render json: {messages: [invalid_date_range_message]}, status: :bad_request unless date_to > date_from
      result_obj = {}
      branches = []
      branch_list.each do |branch|
        class_in_branch = ClassGrade.where(branch_id: branch.id).pluck(:id)
        student_course = StudentCourse.joins(:course).where(:courses => {:class_grade_id => class_in_branch})
        branch_result_obj = {}
        students = []
        month_list = (date_from..date_to).to_a.map(&:beginning_of_month).uniq
        month_list.each do |day|
          student_obj = {}
          student_list = Student.includes(:user, :gender).joins(:student_courses).where(:student_courses => {:id => student_course})
                             .where("student_courses.start_date >= ? and student_courses.start_date <=?", day, day).uniq
          student_obj[:month] = "#{Date::MONTHNAMES[day.month]} (#{day.year})"
          student_obj[:student_list] = serialize_student_data student_list
          students << student_obj
        end
        branch_result_obj[:id] = branch.id
        branch_result_obj[:name] = branch.name
        branch_result_obj[:student_report] = students
        branches << branch_result_obj
      end
      result_obj[:branches] = branches
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: {messages: [invalid_date_format_message]}, status: :bad_request
    end
  end
end
