class Api::V1::Report::Trial::DetailsController < Api::V1::Report::Trial::TrialBaseController
  def class_grades
    result_obj = {}
    courses = []
    class_in_branch = ClassGrade.find_by_id(trial_detail_params[:class_grade_id])
    return render json: { errors: [ClassGrade.not_found_message] }, status: :not_found if class_in_branch.blank?
    result_obj[:id] = class_in_branch.id
    result_obj[:name] = class_in_branch.name
    course_in_class = class_in_branch.courses
    course_in_class.each do |course|
      student_courses = course.student_courses.includes(:course, :student => [:user]).where(is_free_trial: true)
      students = []
      trial_amount = student_courses.size
      student_courses.each do |item|
        student_obj = {}
        student_obj[:name] = item.student.user.full_name
        student_obj[:code] = item.student.code
        student_obj[:phone] = item.student.user.phone
        students << student_obj
      end
      course_obj = {}
      course_obj[:name] = course.name
      course_obj[:code] = course.code
      course_obj[:course_no] = course.course_no
      course_obj[:branch] = class_in_branch.branch
      course_obj[:class_grade] = class_in_branch
      course_obj[:curriculum] = class_in_branch.curriculum
      course_obj[:students] = students
      course_obj[:total_trial] = trial_amount
      courses << course_obj if trial_amount > 0
    end
    result_obj[:courses] = courses
    total_trial = courses.inject(0) {|total, item| total + item[:total_trial]}
    result_obj[:total_trial] = total_trial
    render json: result_obj, status: :ok
  end

  private

  def trial_detail_params
    params.require(:trial).permit(:class_grade_id)
  end

end
