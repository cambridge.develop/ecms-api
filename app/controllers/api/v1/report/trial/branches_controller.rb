class Api::V1::Report::Trial::BranchesController < Api::V1::Report::Trial::TrialBaseController
  def class_grades
    begin
      # Báo cáo học thử theo chuyên ngành
      result_obj = {}
      class_grades = []

      # p major_list
      class_in_branch = ClassGrade.includes(:curriculum => [:level => [:major]], :courses => [:student_courses]).where(branch_id: @selected_branch)
      class_in_branch.each do |class_grade|
        class_grade_obj = {}
        courses = []
        class_grade_obj[:id] = class_grade.id
        class_grade_obj[:name] = class_grade.name
        class_grade_obj[:code] = class_grade.code
        class_grade_obj[:level] = class_grade.curriculum.level
        class_grade_obj[:major] = class_grade.curriculum.level.major
        class_grade_obj[:curriculum] = class_grade.curriculum
        course_in_class = class_grade.courses
        course_in_class.each do |course|
          student_courses = course.student_courses.includes(:course).where(is_free_trial: true)
          trial_amount = student_courses.size
          course_obj = {}
          course_obj[:id] = course.id
          course_obj[:name] = course.name
          course_obj[:number_of_students] = course.student_courses.size
          course_obj[:trial_amount] = trial_amount
          courses << course_obj if trial_amount > 0
        end
        class_grade_obj[:courses] = courses
        total_trial = courses.inject(0) {|total, item| total + item[:trial_amount]}
        class_grade_obj[:total_trial] = total_trial
        class_grades << class_grade_obj if courses.size > 0
      end
      branch_obj = {}
      p @selected_branch
      branch_obj[:name] = @selected_branch.name
      branch_obj[:phone] = @selected_branch.phone
      branch_obj[:address] = @selected_branch.address
      branch_obj[:email] = @selected_branch.email
      branch_obj[:logo_base64] = @selected_branch.logo_base64
      branch_obj[:class_grades] = class_grades
      branch_obj[:total_trial] = class_grades.inject(0) {|total, item| total + item[:total_trial]}
      result_obj[:branch] = branch_obj
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: {messages: [invalid_date_format_message]}, status: :bad_request
    end
  end
end
