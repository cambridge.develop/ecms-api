class Api::V1::Report::Trial::TrialBaseController < Api::V1::Report::ReportBaseController
  protected
  def students_in_branch
    class_in_branch = ClassGrade.where(branch_id: @selected_branch).pluck(:id)
    @student_course = StudentCourse.joins(:course).where(:courses => {:class_grade_id => class_in_branch})
  end

  def trial_params
    params[:trial].permit(:major_list => [], :branch_list => [])
  end
end
