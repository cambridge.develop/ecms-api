class Api::V1::Report::ReportBaseController < Api::V1::ApiBaseController
  # before_action :authenticate_user!
  # before_action :check_user
  # before_action :selected_branch

  protected
  def day_params
    params[:day].permit(:from, :to, :branch_list => [])
  end

  def month_params
    params[:month].permit(:from, :to)
  end

  def invalid_date_range_message
    I18n.t("controllers.statistics.student.invalid_date_range")
  end
  def invalid_month_range_message
    I18n.t("controllers.statistics.student.invalid_month_range")
  end
  def invalid_date_format_message
    I18n.t("controllers.statistics.student.date_format")
  end
  def invalid_month_format_message
    I18n.t("controllers.statistics.student.month_format")
  end

end
