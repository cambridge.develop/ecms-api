class Api::V1::SelectedBranchesController < Api::V1::ApiBaseController
  skip_before_action :selected_branch
  def index
    @q = Branch.joins(:user_branches).where(:user_branches => {user_id: current_user.id}).includes(:manager).ransack(params[:q])
    @q = Branch.includes(:manager).ransack(params[:q]) if current_user.user_type.is_admin?
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      branch_list = @q.result
      return_obj[:branches] = serialize_data branch_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, branch_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:branches] = serialize_data branch_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  private

  def serialize_data data
    data.as_json(
        include: :manager,
        except: [:created_at, :updated_at]
    )
  end
end
