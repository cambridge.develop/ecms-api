class Api::V1::ApiBaseController < ApplicationController
  include Pundit
  before_action :authenticate_user!
  before_action :check_user
  before_action :selected_branch
  before_action :check_permission
  rescue_from Pundit::NotAuthorizedError, with: :_user_not_authorized

  def check_permission
    current_user.selected_branch = @selected_branch
    authorize(self.class.name.gsub("Controller", "").split("::").map{|e| e.underscore.to_sym})
  end

  
  private
  def _user_not_authorized
    render json: { errors: ["Không có quyền truy cập"]}, status: :forbidden
  end

  # def _update_session_exp_time
  #
  # end
end
