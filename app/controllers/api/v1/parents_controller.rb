class Api::V1::ParentsController < Api::V1::ApiBaseController
  before_action :set_parent, only: [:show, :update, :destroy, :students, :student_parents, :delete]
  def index
    @q = Parent.where.not(is_deleted: true).includes(:gender, :user).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['address asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      parent_list = @q.result
      return_obj[:parents] = serialize_data parent_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, parent_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:parents] = serialize_data parent_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data(@parent), status: :ok
  end

  def create
    MainRecordBase.transaction do
      user = User.new user_params
      user.generate_user
      user.user_type = UserType.par
      user.save!

      parent = Parent.new parent_params
      parent.user_id = user.id
      parent.save!

      render json: serialize_data(parent), status: :ok
      # raise ActiveRecord::Rollback
    rescue ActiveRecord::RecordInvalid => invalid
      render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
      raise ActiveRecord::Rollback
    end
  end

  def update
    parent_user_params = if user_params[:password] == ''
                           user_params.except(:password)
                         else
                           user_params
                         end
    user = @parent.user
    user.attributes = parent_user_params
    @parent.attributes = parent_params
    MainRecordBase.transaction do
      user.save!
      @parent.save!
      render json: serialize_data(@parent), status: :ok
      # raise ActiveRecord::Rollback
    rescue ActiveRecord::RecordInvalid => invalid
      render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
      raise ActiveRecord::Rollback
    end
  end

  def destroy
    return render json: { messages: [Parent.destroy_success_message(@parent.user.full_name)] }, status: :ok if @parent.destroy
    render json: {errors: @parent.errors.full_messages}, status: :bad_request
  end

  def delete
    @parent.is_deleted = true
    return render json: { messages: [Parent.destroy_success_message(@parent.user.full_name)] }, status: :ok if @parent.save
    render json: { errors: @parent.errors.full_messages }, status: :bad_request
  end

  def students
    @q = @parent.students.includes(:user, :gender, :student_parent).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      student_list = @q.result
      return_obj[:students] = serialize_data_with_user student_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, student_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:students] = serialize_data_with_user student_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def student_parents
    @q = @parent.student_parents.includes({ student: [user: [:gender], school: []]}, :parent).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      student_list = @q.result
      return_obj[:student_parents] = serialize_data_with_user student_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, student_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:student_parents] = serialize_data_with_user student_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def update_relation
    student_parent = StudentParent.find_by_id(params[:id])
    return render json: { errors: [StudentParent.not_found_message] }, status: :not_found if student_parent.blank?
    student_parent.relation = params[:relation]
    return render json: { messages: [StudentParent.update_success_message(student_parent.relation)] }, status: :ok if student_parent.save
    render json: { errors: student_parent.errors.full_messages }, status: :bad_request
  end
  private

  def set_parent
    begin
      @parent = Parent.find_by_id(params[:id])
      return render json: { errors: [Parent.not_found_message] }, status: :not_found if @parent.blank?
    rescue => exception
      render json: { errors: [Parent.not_found_message] }, status: :not_found
    end
  end

  def parent_params
    params[:parent].permit( :address,
                            :description,
                            :birthday,
                            :current_job,
    )
  end

  def user_params
    params[:user].permit(:email,
                         :full_name,
                         :phone,
                         :gender_id,
                         # :username,
                         :password
    )
  end

  def serialize_data data
    data.as_json(
      except: %i[created_at updated_at],
      include: [
        user: { except: %i[created_at updated_at] },
        gender: { only: %i[id code name] }
      ]
    )
  end

  def serialize_data_with_user data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
                    student: {
                        except: %i[created_at updated_at],
                        include: [
                                    user: {
                                        only: %i[id email full_name username phone must_change_password initial_password locked_at],
                                        include: [
                                                  gender: { only: %i[id name code] }
                                              ]
                                    },
                                    school: { only: %i[name] }
                                ]
                    }
                ]
    )
  end
end