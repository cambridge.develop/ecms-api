class Api::V1::Payment::RefundsController < Api::V1::Payment::PaymentBaseController
  before_action :set_refund, only: [:show]
  def index
    class_in_branch = ClassGrade.where(branch_id: @selected_branch).pluck(:id)
    student_course_in_branch = StudentCourse.joins(:course).where(:courses => {:class_grade_id => class_in_branch}).pluck(:id)
    student_in_branch = Student.joins(:student_courses).where(:student_courses => {:id => student_course_in_branch})
    @q = Refund.joins(:student).where(:students => {:id => student_in_branch}).includes(:creator, :student).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['created_at desc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      refund_list = @q.result
      return_obj[:refunds] = serialize_data refund_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, refund_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:refunds] = serialize_data refund_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data(@refund), status: :ok
  end

  def create
    @refund = Refund.new refund_params
    @refund.creator_id = current_user.id
    MainRecordBase.transaction do
      if @refund.save
        student = Student.find_by_id(refund_params[:student_id])
        new_credit = @refund.student.credit - @refund.withdrawal_amount
        student.credit = new_credit
        if student.save
          @refund.reload_student
          render json: serialize_data(@refund), status: :ok
        else
          render json: { errors: [student.errors.full_messages] }, status: :bad_request
          raise ActiveRecord::Rollback
        end
      else
        render json: { errors: [@refund.errors.full_messages] }, status: :bad_request
        raise ActiveRecord::Rollback
      end
    end
  end

  private


  def set_refund
    begin
      @refund = Refund.find_by_id(params[:id])
      return render json: { errors: [Refund.not_found_message] }, status: :not_found if @refund.blank?
    rescue => exception
      render json: { errors: [Refund.not_found_message] }, status: :not_found
    end
  end

  def serialize_data data
    data.as_json(
        include: [
            creator: { except: [:created_at, :updated_at] },
            student: { except: [:created_at, :updated_at] }
        ]
    )
  end

  def refund_params
    params[:refund].permit( :withdrawal_person_name,
                            :withdrawal_amount,
                            :description,
                            :student_id)
  end
end
