class Api::V1::Payment::ReceiptsController < Api::V1::Payment::PaymentBaseController
  before_action :set_receipt, only: [:show, :cancel]
  def index
    class_in_branch = ClassGrade.where(branch_id: @selected_branch).pluck(:id)
    student_course = StudentCourse.includes(:receipt).joins(:course).where(:courses => {:class_grade_id => class_in_branch})
    @q = Receipt.joins(:student_course).where(:student_courses => {:id => student_course})
             .includes(:creator)
             .includes(  :student_course => :receipt, :student => :user, :course => {:class_grade => :branch}).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['created_at desc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      receipt_list = @q.result
      return_obj[:receipts] = serialize_data receipt_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, receipt_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:receipts] = serialize_data receipt_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def create_with_course
    student_course = StudentCourse.find_by_id(receipt_params[:student_course_id])
    return (render json: { errors: [StudentCourse.not_found_message] }, status: :not_found) if student_course.blank?
    return (render json: { errors: [I18n.t('controllers.receipt.errors.course_is_cancel')] }, status: :bad_request) if student_course.is_cancel
    return (render json: { errors: [I18n.t('controllers.receipt.errors.student_is_deleted')] }, status: :bad_request) if student_course.student.status == Student.statuses[:deleted]

    #tính course debt dựa trên receipt trước khi tạo receipt mới
    _, _, student_course_debt, _ = calc_student_course_money(student_course.receipts, student_course)
    return (render json: { errors: [I18n.t('controllers.receipt.errors.charged')] }, status: :bad_request) if student_course_debt == 0
    #thực hiện chuẩn bị data liên quan đến
    actual_paid_amount = receipt_params[:actual_paid_amount].to_s.gsub(/\D/, '').to_i.abs
    discount_number = receipt_params[:discount_number].to_s.gsub(/\D/, '').to_i.abs
    credit_used = receipt_params[:credit_used].to_s.gsub(/\D/, '').to_i.abs

    # unless student_course.student.is_student_enough_credit(credit_used)
    #   return (render json: { errors: [I18n.t('controllers.receipt.errors.credit_is_not_enough')] }, status: :bad_request)
    # end

    #tính tiền dư nếu hoá đơn này được tạo thành công
    redundant_money = student_course_debt - (actual_paid_amount + discount_number + credit_used)

    receipt_params_updated = receipt_params.except(:class_grade_id, :student_id, :days_late)
    receipt_params_updated[:actual_paid_amount] = actual_paid_amount
    receipt_params_updated[:discount_number] = discount_number
    receipt_params_updated[:credit_used] = credit_used

    MainRecordBase.transaction do
      @receipt = Receipt.new(receipt_params_updated)
      @receipt.student_course_id = student_course&.id
      @receipt.creator_id = current_user.id

      if @receipt.save
        student_course.lock!
        #nếu tiền thối là số dương
        if redundant_money >= 0
          #nạp tiền vào khoá học, không tiền hoàn lại
          student_course_result, student_course_credit, student_course_messages = student_course.deposit_credit(actual_paid_amount + discount_number + credit_used)
          unless student_course_result
            render json: { errors: [student_course_messages] }, status: :bad_request
            raise ActiveRecord::Rollback
            return
          end

          result, _, messages = student_course.student.withdraw_credit(credit_used.abs)
          unless result
            render json: { errors: [messages]}, status: :bad_request
            raise ActiveRecord::Rollback
            return
          end

        else
          #nạp tiền vào khoá học với mức nợ, hoàn tiền với số redundant_money
          student_course_result, student_course_credit, student_course_messages = student_course.deposit_credit(student_course_debt)
          unless student_course_result
            render errors: { errors: [I18n.t('controllers.receipt.errors.system_error')] }, status: :bad_request
            raise ActiveRecord::Rollback
            return
          end
          result, current_credit, messages = student_course.student.deposit_credit(redundant_money.abs - credit_used.abs)
          unless result
            render json: { errors: [messages]}, status: :bad_request
            raise ActiveRecord::Rollback
            return
          end
        end
        #cập nhật discount_amount
        new_discount_amount = student_course.discount_amount + discount_number
        student_course.update(discount_amount: new_discount_amount)
        @receipt.reload_student_course
        @receipt.student_course.is_paid = student_course.is_paid?
        @receipt.student_course.is_paid_enough = student_course.is_paid_enough?
        render json: serialize_data(@receipt), status: :ok
      else
        render json: { errors: [@receipt.errors.full_messages] }, status: :bad_request
      end
    end
  end

  def create_with_class
    result, messages = Receipt.create_with_class(receipt_params, current_user)
    student = Student.find(receipt_params[:student_id])
    paid_class = ClassGrade.includes(:courses, :branch).order('courses.course_no')
                          .includes(:courses => {:student_courses => {:receipts => :creator}}).order('receipts.created_at DESC')
                          .where(id: receipt_params[:class_grade_id])
                          .where(:receipts => {:is_paid_for_class => true }, :student_courses => {student_id: student.id}).first
    returned_json = paid_class.as_json(
                      include: [
                        :branch,
                        courses: {
                          include: [
                            student_courses: {
                              include: [
                                receipts: {
                                  include: :creator
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    )
    if result
      render json: returned_json, status: :ok
    else
      render json: { errors: messages }, status: :bad_request
    end
  end

  def show
    render json: serialize_data(@receipt), status: :ok
  end

  # HUỶ HOÁ ĐƠN
  # nếu hoá đơn huỷ là hoá đơn gần nhất thì chỉ đơn thuần là đổi trạng thái
  # nếu hoá đơn huỷ không là hoá đơn gần nhất, phí đóng và credit sẽ được chuyển vào credit
  def cancel
    result, messages = @receipt.cancel(true) #no refund newest receipt
    return (render json: { messages: [messages] }, status: :ok) if result
    render json: {errors: [messages.join(', ')] }, status: :bad_request
  end

  def cancel_with_class
    MainRecordBase.transaction do
      class_grade = ClassGrade.find_by_id(receipt_params[:class_grade_id])
      student = Student.find_by_id(receipt_params[:student_id])
      return (render json: { error: ClassGrade.not_found_message }, status: :not_found) if class_grade.blank?
      return (render json: { error: Student.not_found_message }, status: :not_found) if student.blank?
      class_grade.courses.includes(:student_courses).each do |course|
        course.student_courses.includes(:receipts).where(student_id: student.id).each do |student_course|
          student_course.receipts.each do |receipt|
            if receipt.is_paid_for_class
              result, messages = receipt.cancel(true)
              unless result
                raise ActiveRecord::Rollback
                render json: {errors: [messages.join(', ')] }, status: :bad_request
                return
              end
            end
          end
          student_course.update(is_cancel: true) if student_course.is_add_when_paid == true
        end
      end
      return (render json: { messages: [I18n.t('controllers.receipt.successes.cancel_receipt_with_extra_money_success')] }, status: :ok)
    end
  end
  private

  def set_receipt
    begin
      @receipt = Receipt.find_by_id(params[:id])
      return render json: { errors: [Receipt.not_found_message] }, status: :not_found if @receipt.blank?
    rescue => exception
      render json: { errors: [Receipt.not_found_message] }, status: :not_found
    end
  end

  def calc_student_course_money(receipts, student_course)
    study_fee_need_to_paid = Receipt.calc_actual_fee_need_to_paid(student_course)
    total_actual_money_paid = 0
    total_discount_money = 0
    total_credit_used = 0
    student_course_debt = 0
    receipts.each do |item|
      next if item.is_cancel
      total_actual_money_paid += item.actual_paid_amount
      total_discount_money += item.discount_number
      total_credit_used += item.credit_used
    end
    #tiền còn thiếu sau khi tính tổng hoá đơn
    student_course_debt = study_fee_need_to_paid - (total_actual_money_paid + total_discount_money + total_credit_used) if (study_fee_need_to_paid - (total_actual_money_paid + total_discount_money + total_credit_used)) > 0
    [total_actual_money_paid, total_discount_money, student_course_debt, total_credit_used]
  end

  def is_student_enough_credit(credit_to_use, student)
    student.credit >= credit_to_use
  end

  def serialize_data data
    data.as_json(
        include: [
            creator: { except: [:created_at, :updated_at] },
            course: { 
              include: [
                class_grade: {
                  only: :name,
                  include: [
                    branch: {
                      only: [:name, :address, :phone, :email]
                    }
                  ]
                }
              ],
              except: [:created_at, :updated_at] 
            },
            student: {
              include: [
                user: {
                  only: :full_name
                }
              ],
              except: [:created_at, :updated_at] 
            },
            student_course: { 
              except: [:created_at, :updated_at] 
            }
        ]
    )
  end

  def receipt_params
    params[:receipt].permit(:paid_person_name,
                            :actual_paid_amount,
                            :money_in_words,
                            :description_receipt,
                            :description_internal,
                            :discount_number,
                            :discount_percentage,
                            :credit_used,
                            :gift,
                            :gift_id,
                            :student_course_id,
                            :class_grade_id,
                            :student_id,
                            :days_late,
                            :paid_amount)
  end
end
