class Api::V1::Payment::StudentCoursesController < Api::V1::Payment::PaymentBaseController
  before_action :set_student_course, only: [:show, :receipts]
  def index
    class_in_branch = ClassGrade.where(branch_id: @selected_branch.id).pluck(:id)
    @q = StudentCourse.where(is_cancel: false).joins(:course).where(:courses => {:class_grade_id => class_in_branch, :course_status_id => CourseStatus.open.id})
    @q = @q.includes( :student, :receipt).includes(:course => :class_grade).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['start_date asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      student_course_list = @q.result
      # StudentCourse.set_paid_status student_course_list
      return_obj[:student_courses] = serialize_data student_course_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, student_course_list = pagy @q.result, items: params[:items], page: current_page
      # StudentCourse.set_paid_status student_course_list
      return_obj[:student_courses] = serialize_data student_course_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    @student_course.is_paid = @student_course.is_paid?
    @student_course.is_paid_enough = @student_course.is_paid_enough?
    render json: serialize_data(@student_course), status: :ok
  end

  def receipts
    @q = @student_course.receipts.includes(:creator, :course, :student, :student_course).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      receipt_list = @q.result
      return_obj[:receipts] = serialize_data_with_creator receipt_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, receipt_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:receipts] = serialize_data_with_creator receipt_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  private
  def set_student_course
    begin
      @student_course = StudentCourse.find_by_id(params[:id])
      return render json: { errors: [StudentCourse.not_found_message] }, status: :not_found if @student_course.blank?
    rescue => exception
      render json: { errors: [StudentCourse.not_found_message] }, status: :not_found
    end
  end
  def serialize_data data
    data.as_json(
        # except: [:created_at, :updated_at],
        include: [
            course: { 
              except: [:created_at, :updated_at],
              include: [class_grade: {only: :name}]
            },
            student: { except: [:created_at, :updated_at] }
        ]
    )
  end
  def serialize_data_with_creator data
    data.as_json(
        # except: [:created_at, :updated_at],
        include: [
            course: { except: [:created_at, :updated_at] },
            student: { except: [:created_at, :updated_at] },
            creator: { except: [:created_at, :updated_at] },
            student_course: { except: [:created_at, :updated_at] }
        ]
    )
  end
end
