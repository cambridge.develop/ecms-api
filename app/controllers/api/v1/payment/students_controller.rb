class Api::V1::Payment::StudentsController < Api::V1::Payment::PaymentBaseController
  before_action :set_student, only: [:courses, :classes, :paid_classes, :credit, :deposit, :deposit_receipts]
  def index
    class_in_branch = ClassGrade.where(branch_id: @selected_branch).pluck(:id)
    # student_course_in_branch = StudentCourse.joins(:course).where(:courses => {:class_grade_id => class_in_branch}).pluck(:id)
    # @q = Student.joins(:student_courses).where(:student_courses => {:id => student_course_in_branch})
    @q = Student.all
    @q = @q.includes(:school, :user, :gender, :parents, :student_courses).where.not(status: Student.statuses[:deleted]).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['created_at desc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      student_list = @q.result
      return_obj[:students] = serialize_data student_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, student_list = pagy @q.result, items: params[:items], page: current_page
      # student_list.each do |s|
      #   s.is_not_finish_study_fee = s.is_not_finish_study_fee?
      # end
      return_obj[:students] = serialize_data student_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def credit
    render json: {credit: @student.credit}
  end

  def courses
    @q = @student.courses.where.not(course_status_id: CourseStatus.cancel.id).includes(:class_grade, :course_schedule).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      course_list = @q.result
      return_obj[:courses] = serialize_data_with_class course_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, course_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:courses] = serialize_data_with_class course_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def classes
    classes_joined = @student.class_grades.joins(:student_courses).where.not(:student_courses => {:credit => 0.0}).distinct.pluck(:id)
    classes = ClassGrade.where.not(id: classes_joined).where(branch_id: @selected_branch.id)
    @q = classes.includes(:courses).where.not(:courses => {:course_status_id => CourseStatus.cancel.id})
    current_page = params[:page] || 1
    @q = @q.ransack(params[:q])
    @q.sorts = ['created_at asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      class_list = @q.result
      return_obj[:classes] = serialize_class_data class_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, class_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:classes] = serialize_class_data class_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def paid_classes
    @q = ClassGrade.includes(:courses, :branch).order('courses.course_no')
                  .includes(:courses => {:student_courses => {:receipts => :creator}}).order('receipts.created_at DESC')
                  .where(:receipts => {:is_paid_for_class => true }, :student_courses => {student_id: @student.id})
                  
                    # .references(:courses => {:student_courses => :receipts})
    current_page = params[:page] || 1
    @q = @q.ransack(params[:q])
    # @q.sorts = ['created_at asc'] if @q.sorts.empty?
    return_obj = {}
    class_list = []
    if params.has_key?(:all)
      class_list = @q.result(distinct: true)
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, class_list = pagy @q.result(distinct: true), page: current_page
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    return_obj[:classes] = class_list.as_json(
      include: [
        :branch,
        courses: {
          include: [
            student_courses: {
              include: [
                receipts: {
                  include: :creator
                }
              ]
            }
          ]
        }
      ]
    )

    render json: return_obj, status: :ok
  end

  # params: student_id, 
  def deposit
    amount =  deposit_params[:amount]
    if amount < 0
      return render json: {errors: [Student.deposit_must_to_greater_than_zero]}, status: :bad_request
    end

    MainRecordBase.transaction do
      result, current_credit, messages = @student.deposit_credit(amount)
      deposit_receipt = DepositReceipt.new deposit_params
      deposit_receipt.creator_id = current_user.id
      deposit_receipt.student_id = @student.id
      deposit_receipt.branch_id = @selected_branch.id

      if result
        if deposit_receipt.save
          render json: {
            deposit_receipt: deposit_receipt.as_json(include:  [
                                                      :creator, 
                                                      :branch, 
                                                      student: {include: :user}
                                                    ])
            }
        else
          raise ActiveRecord::Rollback
          render errors: { errors: deposit_receipt.errors.full_messages }, status: :bad_request
        end
        
      else
        raise ActiveRecord::Rollback
        render errors: { errors: @student.errors.full_messages }, status: :bad_request
      end
    end
    
  end

  def deposit_receipts
    @q = @student.deposit_receipts.includes(:creator, :branch).includes(:student => :user)
    @q = @q.ransack(params[:q])
    @q.sorts = ['created_at desc'] if @q.sorts.empty?
    current_page = params[:page] || 1
    pagy, deposit_receipts = pagy @q.result(distinct: true), page: current_page
    return_obj = {}
    return_obj[:deposit_receipts] = deposit_receipts.as_json(
      include: [
        :creator, :branch, student: {include: [:user]}
      ]
    )
    return_obj[:current_page] = pagy.page
    return_obj[:total_page] = pagy.pages

    render json: return_obj, status: :ok
  end

  private

  def set_student
    begin
      @student = Student.find_by_id(params[:id])
      return render json: { errors: [Student.not_found_message] }, status: :not_found if @student.blank?
    rescue => exception
      render json: { errors: [Student.not_found_message] }, status: :not_found
    end
  end

  def serialize_data data
    data.as_json(
        include: [
            gender: { except: [:created_at, :updated_at] },
            user: { except: [:created_at, :updated_at] }
        ]
    )
  end

  def serialize_class_data data
    data.as_json(
        include: [
            courses: {}
        ]
    )
  end

  def serialize_data_with_class data
    data.as_json(
        except: [:created_at, :updated_at],
        include: [
            class_grade: { except: [:created_at, :updated_at] },
            course_schedule: { except: [:created_at, :updated_at] }
        ]
    )
  end

  def deposit_params
    params[:deposit].permit(:deposit_person_name, :amount, :internal_description, :receipt_description)
  end

end
