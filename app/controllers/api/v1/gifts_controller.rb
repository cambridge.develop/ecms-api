class Api::V1::GiftsController < Api::V1::ApiBaseController
  before_action :set_gift, only: [:show, :update, :destroy, :lock, :unlock]
  def index
    @q = Gift.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      gift_list = @q.result
      return_obj[:gifts] = serialize_data gift_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, gift_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:gifts] = serialize_data gift_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data(@gift), status: :ok
  end

  def create
    @gift = Gift.new gift_params
    if @gift.save
      render json: @gift, status: :ok
    else
      render json: { errors: @gift.errors.full_messages }, status: :bad_request
    end
  end

  def update
    @gift.attributes = gift_params
    return (render json: @gift, status: :ok) if @gift.save
    render json: {errors: @gift.errors.full_messages}, status: :bad_request
  end

  def destroy
    return render json: { messages: [Gift.destroy_success_message(@gift.name)] }, status: :ok if @gift.destroy
    render json: {errors: @gift.errors.full_messages}, status: :bad_request
  end


  def lock
    @gift.is_locked = true
    return (render json: { messsages: [Gift.lock_success_message] }, status: :ok) if @gift.save
    render json: { errors: @gift.errors.full_messages }, status: :bad_request
  end

  def unlock
    @gift.is_locked = false
    return (render json: { messsages: [Gift.unlock_success_message] }, status: :ok) if @gift.save
    render json: { errors: @gift.errors.full_messages }, status: :bad_request
  end

  private

  def set_gift
    begin
      @gift = Gift.find_by_id(params[:id])
      return render json: { errors: [Gift.not_found_message] }, status: :not_found if @gift.blank?
    rescue => exception
      render json: { errors: [Gift.not_found_message] }, status: :not_found
    end
  end

  def gift_params
    params[:gift].permit(:name, :code, :description)
  end

  def serialize_data data
    data.as_json(
        except: [:created_at, :updated_at]
    )
  end
end
