class Api::V1::StudentCoursesController < Api::V1::ApiBaseController
  before_action :set_student_course, except: [:index, :create]

  def index
    # StudentCourse.includes(:course, :student)
    class_in_branch = ClassGrade.where(branch_id: @selected_branch).pluck(:id)
    @q = StudentCourse.joins(:course).where(:courses => { :class_grade_id => class_in_branch })
    @q = @q.includes(:course, :student, :receipt, :transfer_from_course).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['start_date asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      student_list = @q.result
      return_obj[:student_courses] = serialize_data student_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, student_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:student_courses] = serialize_data student_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data(@student_course), status: :ok
  end

  def create
    student = Student.find_by_id(student_couse_params[:student_id])
    course = Course.find_by_id(student_couse_params[:course_id])
    return render json: { errors: [Course.status_not_open_message] }, status: :bad_request if course.course_status.code != 'open'

    exist_student_course = StudentCourse.where(student_id: student_couse_params[:student_id], course_id: student_couse_params[:course_id], is_cancel: false)
    student_course = StudentCourse.new student_couse_params.except(:learned_days, :stopped_date)
    if student.blank? == false && course.blank? == false && exist_student_course.blank?
      return_json = serialize_data student_course
      return (render json: return_json, status: :ok) if student_course.save
      render json: { errors: [student_course.errors.full_messages] }
    else
      return (render json: { errors: [StudentCourse.exist_record_reference_message(Student, Course)] }, status: :bad_request) if exist_student_course.blank? == false
      return (render json: { errors: [StudentCourse.not_found_model_message(Student)] }, status: :not_found) if student.blank? == false
      return (render json: { errors: [StudentCourse.not_found_model_message(Course)] }, status: :not_found) if course.blank? == false
    end
  end

  def update
    return render json: { errors: [Course.status_not_open_message] }, status: :bad_request if @student_course.course.course_status.code != 'open'

    days_late = student_course_update_params[:days_late]
    attendance_count = AttendanceCourse.where(course_id: @student_course.course_id).size
    total_days = @student_course.course.number_of_days

    # thực hiện kiểm tra lượt điểm danh, nếu ngày vào trễ và ngày đã điểm danh có lón hơn tổng số buổi hay không
    if (days_late.to_i + attendance_count.to_i) >= total_days.to_i
      return render json: { errors: [I18n.t("activerecord.errors.models.student_course.days_late_over_total_days")] }, status: :bad_request
    end

    @student_course.days_late = days_late
    @student_course.start_date = student_course_update_params[:start_date]

    #TODO thực hiện tính đoán thời điểm


    return render json: serialize_data(@student_course), status: :ok  if @student_course.save
    render json: { errors: [@student_course.errors.full_messages] }, status: :bad_request
  end

  def destroy
    return render json: { errors: [Course.status_not_open_message] }, status: :bad_request if @student_course.course.course_status.code != 'open'
    return render json: { messages: [StudentCourse.destroy_reference_success_message(Student, Course)] }, status: :ok if @student_course.cancel
    render json: { errors: [@student_course.errors.full_messages] }, status: :bad_request
  end

  def transfer
    # Params: :course_id, :student_id, :start_date, :days_late, :stopped_date
    # Kiểm tra tính hợp lệ của student_course
    return render json: { errors: [Course.status_not_open_message] }, status: :bad_request if @student_course.course.course_status.code != 'open'
    return render json: { errors: [StudentCourse.is_transferred_message] }, status: :not_found if @student_course.is_transferred
    return render json: { errors: [StudentCourse.is_cancelled_message] }, status: :not_found if @student_course.is_cancel
    new_student_course = StudentCourse.new student_couse_params.except(:stopped_date, :student_id, :learned_days)
    # TODO(1) Kiểm tra các input params
    @student = Student.find_by_id(@student_course.student_id)
    return render json: { errors: [Student.not_found_message] }, status: :not_found if @student.blank?
    new_student_course.student_id = @student.id
    new_course = Course.find_by_id(student_couse_params[:course_id])
    return render json: { errors: [Course.not_found_message] }, status: :not_found if new_course.blank?
    return render json: { errors: [Course.status_not_open_message] }, status: :bad_request if new_course.course_status.code != 'open'
    # TODO(2) Kiểm tra tình trạng học hiện tại
    # Trường hợp đã đóng trọn lớp, sẽ tồn tại student_course trong các khóa còn lại

    next_courses = @student_course.course.class_grade.courses.where("course_no > ?", @student_course.course.course_no)

    # list = []
    # list << @student_course
    MainRecordBase.transaction do
      # TODO(3) Kiểm tra tình trạng đóng tiền
      # Trường hợp đã đóng tiền, rút tất cả credits của student_courses hiện tại
      # về student credits và note lại trên các receipt vào internal_description là đã chuyển lớp
      process_credit @student_course
      # Rút tất cả credits về student credits trường hợp đã đóng trọn lớp
      next_courses.each do |course|
        student_course = course.student_courses.find_by_student_id(@student.id)
        if !student_course.blank?
          # list << student_course
          process_credit student_course
        end
      end
      # TODO(4) Trừ lại số tiền cho các buổi đã học vào credits của student
      learned_days = student_couse_params[:learned_days] || @student_course.learned_days
      p learned_days
      must_pay_credits = @student_course.actual_tuition_need_to_paid_for_learned learned_days
      @student.withdraw_credit(must_pay_credits)

      # TODO(5) Đăng ký học viên vào khóa học mới và tiến hành sử dụng credit
      credit_need_to_paid = new_student_course.actual_tuition_need_to_paid
      if credit_need_to_paid <= @student.credit then
        new_student_course.credit = credit_need_to_paid
        @student.withdraw_credit credit_need_to_paid
      else
        new_student_course.credit = @student.credit
        @student.withdraw_credit @student.credit
      end
      new_student_course.transfer_from_course_id = @student_course.course.id

      new_student_course.save!
      # list << new_student_course
      # render json: serialize_data(list), status: :ok
      render json: serialize_data(new_student_course), status: :ok
        # raise ActiveRecord::Rollback
    rescue ActiveRecord::RecordInvalid => invalid
      render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
      raise ActiveRecord::Rollback
      return
    end
  end

  def cancel_trial
    return render json: { errors: [Course.status_not_open_message] }, status: :bad_request if @student_course.course.course_status.code != 'open'
    return render json: { messages: [I18n.t("activerecord.errors.models.student_course.cancel_trial_success")] }, status: :ok if @student_course.destroy
    render json: { errors: [@student_course.errors.full_messages] }, status: :bad_request
  end

  def convert_trial_to_official
    return render json: { errors: [Course.status_not_open_message] }, status: :bad_request if @student_course.course.course_status.code != 'open'
    @student_course.is_free_trial = false
    return render json: { messages: [I18n.t("activerecord.errors.models.student_course.convert_trial_to_official_success")] }, status: :ok if @student_course.save
    render json: { errors: [@student_course.errors.full_messages] }, status: :bad_request
  end

  private

  def set_student_course
    begin
      @student_course = StudentCourse.includes(:course, :student).find_by_id(params[:id])
      return render json: { errors: [StudentCourse.not_found_message] }, status: :not_found if @student_course.blank?
    rescue => exception
      p exception
      return render json: { errors: [StudentCourse.not_found_message] }, status: :not_found
    end
  end

  def process_credit student_course
    # Chuyển credits từ student_course vào ngược lại student credits và cập nhật lại các receipts của student_course
    if student_course.is_paid?
      credit = student_course.credit
      student_course.withdraw_credit(credit)
      @student.deposit_credit(credit)
      receipts = student_course.receipts
      receipts.each do |receipt|
        receipt.description_internal << "<Đã chuyển sang lớp khác>"
        receipt.save!
      end
    end
    # Cập nhật lại student_course hiện tại
    student_course.stopped_date = student_couse_params[:stopped_date]
    student_course.is_transferred = true
    student_course.is_cancel = true
    student_course.save!
  end

  def student_couse_params
    params[:student_course].permit(:course_id, :student_id, :learned_days, :start_date, :days_late, :stopped_date, :is_free_trial)
  end

  def student_course_update_params
    params[:student_course].permit(:start_date, :days_late)
  end

  def serialize_data data
    data.as_json(
        except: [:created_at, :updated_at],
        include: [
            course: { except: [:created_at, :updated_at] },
            student: { except: [:created_at, :updated_at] },
            transfer_from_course: { except: [:created_at, :updated_at] }
        ]
    )
  end
end
