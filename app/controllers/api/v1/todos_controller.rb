class Api::V1::TodosController < Api::V1::ApiBaseController
  def courses
    courses = Course.joins(:class_grade).where(:class_grades => {:branch_id => @selected_branch}).where(course_status_id: CourseStatus.open.id).includes(:class_grade, :course_status)
    @q = courses.where('courses.end_date < ?', Date.today).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      course_list = @q.result
      return_obj[:courses] = serialize_data_with_class course_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, course_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:courses] = serialize_data_with_class course_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  private

  def serialize_data_with_class data
    data.as_json(include: [
                             class_grade: {only: %i[name]},
                             course_status: {only: %i[name code], methods: [:name_with_locale]},
                         ])
  end
end
