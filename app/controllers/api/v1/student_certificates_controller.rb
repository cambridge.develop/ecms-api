class Api::V1::StudentCertificatesController < Api::V1::ApiBaseController
  before_action :set_student_certificate, only: [:update, :destroy, :show]
  def create
    student = Student.find_by_id(student_certificate_params[:student_id])
    p student
    certificate = Certificate.find_by_id(student_certificate_params[:certificate_id])
    p certificate
    return render json: { errors: [Student.not_found_message] }, status: :not_found if student.blank?
    return render json: { errors: [Certificate.not_found_message] }, status: :not_found if certificate.blank?
    exist_student_certificate = StudentCertificate.where(student_id: student.id, certificate_id: certificate.id)
    return (render json: { errors: [StudentCertificate.exist_record_reference_message(Certificate, Student)] }, status: :not_acceptable) if exist_student_certificate.blank? == false

    student_certificate = StudentCertificate.new student_certificate_params
    return (render json: serialize_data(student_certificate), status: :ok) if student_certificate.save
    # return (render json: { messages: [StudentCertificate.add_reference_success_message(Certificate,Student)]}, status: :ok) if student_certificate.save
    render json: { errors: [student_certificate.errors.full_messages]}
  end

  def show
    return (render json: serialize_data(@student_certificate), status: :ok)
  end

  def destroy
    return (render json: { messages: [StudentCertificate.destroy_reference_success_message(Certificate,Student)]}, status: :ok) if @student_certificate.destroy
    render json: { errors: [@student_certificate.errors.full_messages]}
  end

  def update
    @student_certificate.attributes = student_certificate_params
    return (render json: serialize_data(@student_certificate), status: :ok) if @student_certificate.save
    render json: { errors: [@student_certificate.errors.full_messages]}
  end

  private

  def set_student_certificate
    begin
      @student_certificate = StudentCertificate.includes(:certificate).find_by_id(params[:id])
      return render json: { errors: [StudentCertificate.not_found_message] },status: :not_found if @student_certificate.blank?
    rescue => exception
      p exception
      return render json: { errors: [StudentCertificate.not_found_message] },status: :not_found
    end
  end

  def student_certificate_params
    params[:student_certificate].permit(:student_id, :certificate_id, :issue_organization, :issue_date, :expiration_date, :license_number, :point)
  end

  def serialize_data data
    data.as_json(
      except: %i[created_at updated_at],
      include: [
        certificate: {except: %i[created_at updated at]}
      ]
    )
  end
end
