class Api::V1::Attendance::CoursesController < Api::V1::Attendance::AttendanceBaseController
  before_action :set_course, except: [:index]
  def index
    class_in_branch = ClassGrade.where(branch_id: @selected_branch.id).pluck(:id)
    @q = Course.where(:class_grade_id => class_in_branch).includes(:course_schedule, :teacher_courses => [:teacher => [:user]])
    course_status = params[:status]
    if course_status == '0'
      @q = @q.not_closed
    elsif course_status == '1'
      @q = @q.closed
    end
    @q = @q.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['created_at desc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      course_list = @q.result
      return_obj[:courses] = serialize_data course_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, course_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:courses] = serialize_data course_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data_course_show(@course), status: :ok
  end

  def student_courses
    p @course
    p @course.attendance_courses
    @q = @course.student_courses.includes(:course, :receipt, :student => [:user, :gender]).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      student_course_list = @q.result
      return_obj[:student_courses] = serialize_student_course_data student_course_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, student_course_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:student_courses] = serialize_student_course_data student_course_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def teachers
    @q = @course.teachers.includes(:gender, :user).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      teacher_list = @q.result
      return_obj[:teachers] = serialize_teacher_data teacher_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, teacher_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:teachers] = serialize_teacher_data teacher_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def attendance_courses
    @q = @course.attendance_courses.includes(:creator).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['check_date desc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      attendance_course_list = @q.result
      return_obj[:attendance_courses] = serialize_attendance_course_data attendance_course_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, attendance_course_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:attendance_courses] = serialize_attendance_course_data attendance_course_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  private

  def set_course
    begin
      @course = Course.includes(:teacher_courses => [teacher: [:gender, :teacher_status]]).find_by_id(params[:id])
      return render json: { errors: [Course.not_found_message] }, status: :not_found if @course.blank?
    rescue => exception
      render json: { errors: [Course.not_found_message] }, status: :not_found
    end
  end

  def serialize_data data
    data.as_json(
      except: [:created_at, :updated_at],
      include: [
        teacher_courses: {
          except: [:created_at, :updated_at],
          teacher: {
            except: [:created_at, :updated_at],
            include: [
              gender: { except: [:created_at, :updated_at] },
              user: { except: [:created_at, :updated_at] },
              teacher_status: { except: [:created_at, :updated_at] }
            ]
          }
        }
      ]
    )
  end

  def serialize_data_course_show data
    data.as_json(
        except: [:created_at, :updated_at],
        include: [
            class_grade: {
                only: [:name]
            },
            teacher_courses: {
                except: [:created_at, :updated_at],
                include: [
                    teacher: {
                        except: [:created_at, :updated_at],
                        include: [
                            gender: { except: [:created_at, :updated_at] },
                            user: { except: [:created_at, :updated_at] },
                            teacher_status: { except: [:created_at, :updated_at] }
                        ]
                    }
                ]
            }
        ]
    )
  end

  def serialize_student_course_data data
    data.as_json(
      except: %i[created_at updated_at],
      include: [
        student: {
            except: %i[created_at updated_at],
          include: [
            gender: { only: %i[id name code] },
            user: { only: %i[id username full_name email phone] }
          ]
        }
      ]
    )
  end

  def serialize_teacher_data data
    data.as_json(
      except: [:created_at, :updated_at],
      include: [
        gender: { only: %i[id name code] },
        user: { only: %i[id username full_name email] }
      ]
    )
  end

  def serialize_attendance_course_data data
    data.as_json(
      except: [:created_at, :updated_at],
      include: [
        creator: { only: %i[id username full_name email] }
      ]
    )
  end
end
