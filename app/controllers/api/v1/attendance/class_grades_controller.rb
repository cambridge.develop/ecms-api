
class Api::V1::Attendance::ClassGradesController < Api::V1::Attendance::AttendanceBaseController
  before_action :set_class_grade, only: [:courses]
  def index
    class_in_branch = ClassGrade.att_not_closed.where(branch_id: @selected_branch.id).includes(:branch,:curriculum, :courses => [:teacher_courses => [:teacher => [:user]]])
    # class_status = params[:status]
    # if class_status == '0'
    #   # class_in_branch = class_in_branch.not_closed
    #   class_in_branch = class_in_branch.att_not_closed
    # elsif class_status == '1'
    #   class_in_branch = class_in_branch.att_closed
    #   # class_in_branch = class_in_branch.closed
    # end
    @q = class_in_branch.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['created_at desc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      class_grade_list = @q.result
      return_obj[:class_grades] = serialize_data class_grade_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, class_grade_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:class_grades] = serialize_data class_grade_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def courses
    @q = @class_grade.courses.includes(:class_grade, :course_schedule).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['created_at asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      course_list = @q.result
      return_obj[:courses] = serialize_data_with_class course_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, course_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:courses] = serialize_data_with_class course_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  private

  def set_class_grade
    begin
      @class_grade = ClassGrade.find_by_id(params[:id])
      return render json: { errors: [ClassGrade.not_found_message] }, status: :not_found if @class_grade.blank?
    rescue => exception
      render json: { errors: [ClassGrade.not_found_message] }, status: :not_found
    end
  end

  def serialize_data data
    data.as_json(
      except: [:created_at, :updated_at],
      include: [
        curriculum: { only: [:id, :name, :updated_at] },
        branch: { only: [:id, :name, :updated_at] },
        courses: {
          teacher_courses: {
            except: [:created_at, :updated_at],
            teacher: {
              except: [:created_at, :updated_at],
              include: [
                gender: { except: [:created_at, :updated_at] },
                user: { except: [:created_at, :updated_at] },
                teacher_status: { except: [:created_at, :updated_at] }
              ]
            }
          }
        }
      ]
    )
  end

  def serialize_data_with_class data
    data.as_json(
        except: [:created_at, :updated_at],
        include: [
            class_grade: { except: [:created_at, :updated_at] },
            course_schedule: { except: [:created_at, :updated_at] }
        ]
    )
  end
end
