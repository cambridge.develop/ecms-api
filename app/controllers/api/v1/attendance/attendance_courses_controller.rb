class Api::V1::Attendance::AttendanceCoursesController < Api::V1::Attendance::AttendanceBaseController
  before_action :set_attendance_course, except: [:index, :create]
  def index
    class_in_branch = ClassGrade.where(branch_id: @selected_branch.id).pluck(:id)
    course_in_branch = Course.where(:class_grade_id => class_in_branch).pluck(:id)
    attendance_courses = AttendanceCourse.includes(:creator, :course => [:class_grade => [:curriculum, :branch], :teacher_courses => [:teacher => :user]]).where(course_id: course_in_branch).order(:created_at => :desc)
    @q = attendance_courses.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['check_date asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      attendance_course_lisst = @q.result
      return_obj[:attendance_courses] = serialize_data attendance_course_lisst
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, attendance_course_lisst = pagy @q.result, items: params[:items], page: current_page
      return_obj[:attendance_courses] = serialize_data attendance_course_lisst
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data(@attendance_course), status: :ok
  end

  def create
    attendance_course = AttendanceCourse.new attendance_course_params
    attendance_course.creator_id = current_user.id
    # Check students in attendance whether exist or not
    attendance_course.attendance_list.each do |item|
      student = Student.find_by_id(item["student_id"])
      return render json: { errors: [Student.not_found_message] }, status: :not_found if student.blank?
      # Check students course whether exist or not
      student_course = StudentCourse.where(student_id: student.id, course_id: attendance_course.course_id)
      return render json: { errors: [StudentCourse.not_found_message] }, status: :not_found if student_course.blank?
    end
    MainRecordBase.transaction do
      attendance_course.save!
      attendance_course = AttendanceCourse.includes(:creator, :course => [:class_grade => [:curriculum, :branch], :teacher_courses => [:teacher => :user]]).where(id: attendance_course.id)
      render json: serialize_data(attendance_course), status: :ok
    rescue ActiveRecord::RecordInvalid => invalid
      render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
      raise ActiveRecord::Rollback
    end
  end

  def update
    # Cannot update course_id and check_date after created attendance course
    update_attendance_list = attendance_course_params[:attendance_list]
    # Check students in attendance whether exist or not
    update_student_list = []
    update_attendance_list.each do |item|
      update_student_list << item["student_id"]
      student = Student.find_by_id(item["student_id"])
      return render json: { errors: [Student.not_found_message] }, status: :not_found if student.blank?
      # Check students course whether exist or not
      student_course = StudentCourse.where(student_id: student.id, course_id: @attendance_course.course_id)
      return render json: { errors: [StudentCourse.not_found_message] }, status: :not_found if student_course.blank?
    end
    current_student_list = []
    @attendance_course.attendance_list.each do |item|
        current_student_list << item["student_id"]
    end
    return render json: { errors: [I18n.t("activerecord.errors.models.attendance_course.student_list_changed")] }, status: :bad_request unless current_student_list.sort == update_student_list.sort
    @attendance_course.attendance_list = update_attendance_list
    MainRecordBase.transaction do
      @attendance_course.save!
      render json: serialize_data(@attendance_course), status: :ok
    rescue ActiveRecord::RecordInvalid => invalid
      render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
      raise ActiveRecord::Rollback
    end
  end

  def destroy
    return (render json: { messages: [AttendanceCourse.destroy_reference_success_message(AttendanceCourse,Course)]}, status: :ok) if @attendance_course.destroy
    render json: { errors: [@attendance_course.errors.full_messages]}
  end

  def students
    begin
      attendance_list = @attendance_course.attendance_list
      student_ids = []
      attendance_list.each do |item|
        student_ids << item["student_id"]
      end
      students = Student.where(id: student_ids).includes(:gender, :user)
      @q = students.ransack(params[:q])
      current_page = params[:page] || 1
      @q.sorts = ['check_date desc'] if @q.sorts.empty?
      return_obj = {}
      if params.has_key?(:all)
        student_list = @q.result
        return_obj[:students] = serialize_student_data student_list
        return_obj[:current_page] = 0
        return_obj[:total_page] = 0
      else
        pagy, student_list = pagy @q.result, items: params[:items], page: current_page
        return_obj[:students] = serialize_student_data student_list
        return_obj[:current_page] = pagy.page
        return_obj[:total_page] = pagy.pages
      end
      render json: return_obj, status: :ok
    end
  end

  private

  def set_attendance_course
    begin
      @attendance_course = AttendanceCourse.includes(:creator, :course => [:course_schedule, :class_grade => [:curriculum, :branch], :teacher_courses => [:teacher => :user]]).find_by_id(params[:id])
      return render json: { errors: [AttendanceCourse.not_found_message] }, status: :not_found if @attendance_course.blank?
    rescue => exception
      render json: { errors: [AttendanceCourse.not_found_message] }, status: :not_found
    end
  end

  def attendance_course_params
    params[:attendance_course].permit(:course_id,
                                      :check_date,
                                      :attendance_list => [:student_id, :status] || [])
  end

  def student_attendance_params
    params[:student_attendance].permit(:course_id, :student_id)
  end

  def serialize_data data
    data.as_json(
      except: %i[created_at updated_at],
      include: [
        creator: { only: %i[id username full_name email] },
        course: {
          except: %i[created_at updated_at],
          include: [
            course_schedule:{except: %i[created_at updated_at]},
            class_grade:{
              except: %i[created_at updated_at],
              include: [
                branch: {only: %i[id name code]},
                curriculum: {only: %i[id name code]}
              ]
            },
            teacher_courses: {
              except: %i[created_at updated_at],
              include: [
                teacher: {except: %i[created_at updated_at], include: [user: {only: %i[id username full_name email]}]}
              ]
            }
          ]
        }
      ]
    )
  end

  def serialize_student_data data
    data.as_json(
      except: %i[created_at updated_at],
      include: [
        user: {only: %i[id username full_name]},
        gender: {only: %i[id name code]}
      ]
    )
  end

end
