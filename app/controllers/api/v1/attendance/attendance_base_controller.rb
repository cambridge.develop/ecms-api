class Api::V1::Attendance::AttendanceBaseController < ApplicationController
  before_action :authenticate_user!
  before_action :selected_branch
  before_action :check_user
end