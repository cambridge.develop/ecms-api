class Api::V1::CurriculumsController < Api::V1::ApiBaseController
  before_action :set_curriculum, only: [:show, :update, :destroy, :classes, :lock, :unlock]
  def index
    @q = Curriculum.includes(level: [:major]).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['created_at desc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      curriculum_list = @q.result
      return_obj[:curriculums] = serialize_data_with_level_and_major curriculum_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, curriculum_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:curriculums] = serialize_data_with_level_and_major curriculum_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end

    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data_with_level_and_major(@curriculum), status: :ok
  end

  def create
    @curriculum = Curriculum.new curriculum_params
    return (render json: { errors: [Level.locked_message] }, status: :bad_request) if (@curriculum.level.is_locked == true)
    level = Level.find_by_id(@curriculum.level_id)
    if level.present?
      curriculum_numbers = level.curriculums.size
      @curriculum.code = "#{level.code}#{(curriculum_numbers + 1).to_s.rjust(3, '0')}"
    end
    if @curriculum.save
      render json: serialize_data_with_level_and_major(@curriculum), status: :ok
    else
      render json: { errors: @curriculum.errors.full_messages }, status: :bad_request
    end
  end

  def update
    @curriculum.attributes = curriculum_params
    if @curriculum.level_id_changed?
      return (render json: { errors: [Level.locked_message] }, status: :bad_request) if (@curriculum.level.is_locked == true)
    end
    return (render json: serialize_data_with_level_and_major(@curriculum), status: :ok) if @curriculum.save
    render json: { errors: @curriculum.errors.full_messages }, status: :bad_request
  end

  def destroy
    return render json: { messages: [Curriculum.destroy_success_message(@curriculum.name)] }, status: :ok if @curriculum.destroy
    render json: { errors: @curriculum.errors.full_messages }, status: :bad_request
  end

  def classes
    @q = @curriculum.class_grades.where(branch_id:@selected_branch).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      class_list = @q.result
      return_obj[:classes] = serialize_data class_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, class_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:classes] = serialize_data class_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def lock
    @curriculum.is_locked = true
    return (render json: { messsages: [Curriculum.lock_success_message] }, status: :ok) if @curriculum.save
    render json: { errors: @curriculum.errors.full_messages }, status: :bad_request
  end

  def unlock
    @curriculum.is_locked = false
    return (render json: { messsages: [Curriculum.unlock_success_message] }, status: :ok) if @curriculum.save
    render json: { errors: @curriculum.errors.full_messages }, status: :bad_request
  end
  private

  def set_curriculum
    begin
      @curriculum = Curriculum.find_by_id(params[:id])
      return render json: { errors: [Curriculum.not_found_message] }, status: :not_found if @curriculum.blank?
    rescue => exception
      render json: { errors: [Curriculum.not_found_message] }, status: :not_found
    end
  end

  def curriculum_params
    params[:curriculum].permit(:name,
                               # :code,
                               :number_of_courses,
                               :days_per_course,
                               :fee_per_course,
                               :branch_id,
                               :level_id,
                               :description)
  end

  def serialize_data data
    data.as_json(
        except: [:created_at, :updated_at]
    )
  end

  def serialize_data_with_level_and_major data
    data.as_json(
        except: %i[created_at updated_at],
        include: [{
                      level: {
                          only: %i[id code name],
                          include: [{

                                        major: {
                                            only: %i[id code name]
                                        }
                                    }]
                      }
                  }]
    )
  end
end
