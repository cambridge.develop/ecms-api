class Api::V1::StudentObjectivesController < Api::V1::ApiBaseController
  before_action :set_student_objective, only: [:destroy]

  def create
    student = Student.find_by_id(student_objective_params[:student_id])
    objective = Objective.find_by_id(student_objective_params[:objective_id])

    return render json: { errors: [Student.not_found_message] }, status: :not_found if student.blank?
    return render json: { errors: [Objective.not_found_message] }, status: :not_found if objective.blank?

    exist_student_objective = StudentObjective.where(student_id: student.id, objective_id: objective.id)
    return render json: { errors: [StudentObjective.exist_record_reference_message(Objective, Student)] }, status: :not_found if exist_student_objective.blank? == false


    student_objective = StudentObjective.new student_objective_params
    return (render json: serialize_data(student_objective), status: :ok) if student_objective.save
    return render json: { errors: [student_objective.errors.full_messages]}
  end

  def destroy
    # student = Student.find_by_id(student_objective_params[:student_id])
    # objective = Objective.find_by_id(student_objective_params[:objective_id])
    # return render json: { errors: [Student.not_found_message] }, status: :not_found if student.blank?
    # return render json: { errors: [Objective.not_found_message] }, status: :not_found if objective.blank?
    #
    # exist_student_objective = StudentObjective.where(student_id: student.id, objective_id: objective.id)
    # return render json: { errors: [StudentObjective.not_found_message] }, status: :not_found if exist_student_objective.blank?
    #
    # return (render json: { messages: [StudentObjective.destroy_reference_success_message(Objective, Student)]}, status: :ok) if exist_student_objective.save
    # return render json: { errors: [exist_student_objective.errors.full_messages]}
    #
    return (render json: { messages: [StudentObjective.destroy_reference_success_message(Objective,Student)]}, status: :ok) if @student_objective.destroy
    render json: { errors: [@student_objective.errors.full_messages]}
  end

  private

  def set_student_objective
    begin
      @student_objective = StudentObjective.includes(:objective).find_by_id(params[:id])
      return render json: { errors: [StudentObjective.not_found_message] },status: :not_found if @student_objective.blank?
    rescue => exception
      p exception
      return render json: { errors: [StudentObjective.not_found_message] },status: :not_found
    end
  end

  def student_objective_params
    params[:student_objective].permit(:student_id, :objective_id)
  end

  def serialize_data data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            objective: {except: %i[created_at updated at]}
        ]
    )
  end
end
