class Api::V1::TeachersController < Api::V1::ApiBaseController
  before_action :set_teacher, only: [:show, :update, :destroy, :courses, :classes, :teacher_courses, :statistics]
  def index
    # @q = Teacher.includes(:teacher_status, :user, :gender, :user_branches, :branches)
    @q = Teacher.joins(:user).where(:users => {:id => UserBranch.where(branch_id: @selected_branch).pluck(:user_id)})
    @q = @q.includes(:teacher_status, :user, :gender, :user_branches, :branches).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['created_at desc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      teacher_list = @q.result
      return_obj[:teachers] = serialize_data_with_gender_user_and_teacher_status teacher_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, teacher_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:teachers] = serialize_data_with_gender_user_and_teacher_status teacher_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data_with_gender_user_and_teacher_status(@teacher), status: :ok
  end

  def create
    @teacher = Teacher.new teacher_params.except(:branch_id)
    MainRecordBase.transaction do
      user = User.new user_params
      user.user_type = UserType.teacher
      user.save!
      @teacher.user_id = user.id
      @teacher.save!
      if current_user.user_type.is_admin?
        if teacher_params[:branch_id]
          teacher_params[:branch_id].uniq.each do |branch_id|
            branch = Branch.find_by_id(branch_id)
            if branch
              UserBranch.create!(user_id: user.id, branch_id: branch.id)
            end
          end
        end
      elsif current_user.user_type.is_employee?
        UserBranch.create!(user_id: user.id, branch_id: @selected_branch.id)
      end
      render json: serialize_data_with_gender_user_and_teacher_status(@teacher), status: :ok
      # raise ActiveRecord::Rollback
    rescue ActiveRecord::RecordInvalid => invalid
      render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
      raise ActiveRecord::Rollback
    end
  end

  # def create
  #   @teacher = Teacher.new teacher_params.except(:branch_id)
  #   MainRecordBase.transaction do
  #     user = User.new(user_params)
  #     user.user_type = UserType.teacher
  #     if user.save
  #       @teacher.user_id = user.id
  #       if @teacher.save!
  #         if current_user.user_type.is_admin?
  #           if teacher_params[:branch_id]
  #             teacher_params[:branch_id].uniq.each do |branch_id|
  #               branch = Branch.find_by_id(branch_id)
  #               if branch
  #                 UserBranch.create!(user_id: user.id, branch_id: branch.id)
  #               end
  #             end
  #           end
  #         elsif current_user.user_type.is_employee?
  #           # p @selected_branch
  #           UserBranch.create!(user_id: user.id, branch_id: @selected_branch.id)
  #         end
  #         render json: serialize_data_with_gender_user_and_teacher_status(@teacher), status: :ok
  #       else
  #         render json: { errors: @teacher.errors.full_messages }, status: :bad_request
  #         raise ActiveRecord::Rollback
  #       end
  #     else
  #       render json: { errors: user.errors.full_messages }, status: :bad_request
  #       raise ActiveRecord::Rollback
  #     end
  #   rescue ActiveRecord::RecordInvalid => invalid
  #     render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
  #     raise ActiveRecord::Rollback
  #   end
  # end

  def update
    ActiveRecord::Base.transaction do
      user = @teacher.user
      current_user_params = if user_params[:password] == ''
                              user_params.except(:password)
                             else
                                user_params
                             end
      user.update! current_user_params
      @teacher.update! teacher_params.except(:branch_id)
      if current_user.user_type.is_admin?
        user.user_branches.delete_all
        if teacher_params[:branch_id]
          teacher_params[:branch_id].uniq.each do |branch_id|
            branch = Branch.find_by_id(branch_id)
            if branch
              UserBranch.create!(user_id: user.id, branch_id: branch.id)
            end
          end
        end
      end
      render json: serialize_data_with_gender_user_and_teacher_status(@teacher), status: :ok
    rescue ActiveRecord::RecordInvalid => invalid
      render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
      raise ActiveRecord::Rollback
    end
  end

  def lock
    @teacher.lock
    return (render json: serialize_data_with_gender_user_and_teacher_status(@teacher), status: :ok) if @teacher.lock
    render json: { errors: @teacher.errors.full_messages }, status: :bad_request
  end

  def destroy
    return render json: { messages: [Teacher.destroy_success_message(@teacher.user.full_name)] }, status: :ok if @teacher.destroy
    render json: {errors: @teacher.errors.full_messages}, status: :bad_request
  end

  def courses
    @q = @teacher.courses.includes(:class_grade, :course_schedule).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      course_list = @q.result
      return_obj[:courses] = serialize_data_with_class course_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, course_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:courses] = serialize_data_with_class course_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def teacher_courses
    @q = @teacher.teacher_courses.includes(:course, :course_schedule, :class_grade).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      teacher_course_list = @q.result
      return_obj[:teacher_courses] = serialize_teacher_course_data teacher_course_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, teacher_course_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:teacher_courses] = serialize_teacher_course_data teacher_course_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def classes
    @q = @teacher.class_grades.includes(:branch, :curriculum, :courses).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      class_list = @q.result
      return_obj[:classes] = serialize_class_data class_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, class_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:classes] = serialize_class_data class_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def statistics
    return_obj = {
        class_grade_count: @teacher.class_grades.size,
        course_count: @teacher.courses.size
    }
    render json: return_obj, status: :ok
  end
  private

  def set_teacher
    begin
      @teacher = Teacher.find_by_id(params[:id])
      return render json: { errors: [Teacher.not_found_message] }, status: :not_found if @teacher.blank?
    rescue => exception
      render json: { errors: [Teacher.not_found_message] }, status: :not_found
    end
  end

  def teacher_params
    params.require(:teacher).permit(:specialized, :birthday, :teacher_status_id, branch_id: [])
  end

  def user_params
    params[:user].permit(:email,
                        :full_name,
                        :phone,
                        :gender_id,
                        :username,
                        :password
                        )
  end

  def serialize_data_with_gender_user_and_teacher_status data
    data.as_json(
      except: [:updated_at],
      include: [
              gender: { except: [:created_at, :updated_at] },
              user: { except: [:created_at, :updated_at] },
              teacher_status: { except: [:created_at, :updated_at], methods: [:name_with_locale] },
              branches: { only: %i(id) }
                ]
    )
  end

  def serialize_data_with_class data
    data.as_json(
        except: [:created_at, :updated_at],
        include: [
            class_grade: { except: [:created_at, :updated_at] },
            course_schedule: { except: [:created_at, :updated_at] },
        ]
    )
  end

  def serialize_data data
    data.as_json(
        except: [:created_at, :updated_at]
    )
  end

  def serialize_class_data data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            branch: { only: %i[id name] },
            curriculum: { only: %i[id name] },
        ]
    )
  end

  def serialize_teacher_course_data data
    data.as_json(
        except: [:created_at, :updated_at],
        include: [
            course_schedule: { except: [:created_at, :updated_at] },
            course: { except: [:created_at, :updated_at] },
            class_grade: { only: %i(name) },
        ]
    )
  end
end