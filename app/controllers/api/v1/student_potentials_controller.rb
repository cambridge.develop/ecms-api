class Api::V1::StudentPotentialsController < Api::V1::ApiBaseController
  before_action :set_student_potential, only: [:update, :destroy]

  def index
    @q = StudentPotential.where(branch_id: @selected_branch.id).includes( :branch, curriculum: [level: [:major]], student: [:gender, :user]).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['created_at desc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      student_potential_list = @q.result
      return_obj[:student_potentials] = serialize_data student_potential_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, student_potential_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:student_potentials] = serialize_data student_potential_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def create
    student = Student.find_by_id(student_potential_params[:student_id])
    # branch = Branch.find_by_id(student_potential_params[:branch_id])
    curriculum = Curriculum.find_by_id(student_potential_params[:curriculum_id])

    return render json: { errors: [Student.not_found_message] }, status: :not_found if student.blank?
    # return render json: { errors: [Branch.not_found_message] }, status: :not_found if branch.blank?
    return render json: { errors: [Curriculum.not_found_message] }, status: :not_found if curriculum.blank?

    exist_student_potential = StudentPotential.where(student_id: student.id, branch_id: @selected_branch.id, curriculum_id: curriculum.id)
    return render json: { errors: [StudentPotential.exist_record_reference_message(StudentPotential, Branch)] }, status: :not_found if exist_student_potential.blank? == false

    student_potential = StudentPotential.new student_potential_params
    student_potential.branch_id = @selected_branch.id
    return (render json: { errors: [Curriculum.locked_message] }, status: :bad_request) if (student_potential.curriculum.is_locked == true)
    return (render json: serialize_data(student_potential), status: :ok) if student_potential.save
    return render json: { errors: [student_potential.errors.full_messages]}
  end

  def update
    @student_potential.attributes = student_update_potential_params
    if @student_potential.curriculum_id_changed?
      return (render json: { errors: [Curriculum.locked_message] }, status: :bad_request) if (@student_potential.curriculum.is_locked == true)
    end
    return (render json: serialize_data(@student_potential), status: :ok) if @student_potential.save
    render json: { errors: @student_potential.errors.full_messages }, status: :bad_request
  end

  def destroy
    return (render json: { messages: [StudentPotential.destroy_reference_success_message(StudentPotential,Student)]}, status: :ok) if @student_potential.destroy
    render json: { errors: [@student_potential.errors.full_messages]}
  end

  private

  def set_student_potential
    begin
      @student_potential = StudentPotential.includes(:branch, :student => [:user], :curriculum => [:level => [:major]]).find_by_id(params[:id])
      return render json: { errors: [StudentPotential.not_found_message] },status: :not_found if @student_potential.blank?
    rescue => exception
      p exception
      return render json: { errors: [StudentPotential.not_found_message] },status: :not_found
    end
  end

  def student_potential_params
    params[:student_potential].permit(:student_id, :curriculum_id, :name, :description)
  end

  def student_update_potential_params
    params[:student_potential].permit(:status, :name, :description, :curriculum_id)
  end

  def serialize_data data
    data.as_json(
      methods: :current_status,
      except: %i[updated_at],
      include: [
        curriculum: {
          only: %i[id name code],
          include: [
            level: {
              only: %i[id name code],
              include: [ major: { only: %i[id name code] } ]
            }
          ]
        },
        branch: { only: %i[id name] },
        student: {
            except: %i[created_at updated_at],
            include: [
              gender: { only: %i[id name code] },
              user: { only: %i[id full_name username email] },
            ]
        }
      ]
    )
  end
end

