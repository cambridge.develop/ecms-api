class Api::V1::TeacherCoursesController < Api::V1::ApiBaseController

  def create
    teacher = Teacher.find_by_id(teacher_course_params[:teacher_id])
    course = Course.find_by_id(teacher_course_params[:course_id])
    exist_teacher_course = TeacherCourse.where(teacher_id: teacher.id, course_id: course.id)

    if teacher.blank? == false && course.blank? == false && exist_teacher_course.blank?
      teacher_course = TeacherCourse.new teacher_course_params
      return (render json: { messages: ["Tạo mối liên kết giữa giáo viên và khóa học thành công"]}, status: :ok) if teacher_course.save
      render json: { errors: ['Tạo mối liên kết giữa giáo viên và khóa học không thành công']}
    else
      render json: { errors: ['Không tìm thấy dữ liệu, xin hãy tải lại trang và thử lại!']}, status: :bad_request
    end
  end

  def destroy
    teacher = Teacher.find_by_id(teacher_course_params[:teacher_id])
    course = Course.find_by_id(teacher_course_params[:course_id])
    exist_teacher_course = TeacherCourse.where(teacher_id: teacher.id, course_id: course.id)

    if teacher.blank? == false && course.blank? == false && exist_teacher_course.blank? == false
      return (render json: { messages: ["Xóa mối liên kết giữa giáo viên và khóa học thành công"]}, status: :ok) if exist_teacher_course.destroy
      render json: { errors: ['Xóa mối liên kết giữa giáo viên và khóa học không thành công']}
    else
      render json: { errors: ['Không tìm thấy dữ liệu, xin hãy tải lại trang và thử lại!']}, status: :bad_request
    end
  end

  private

  def teacher_course_params
    params[:teacher_course].permit(:teacher_id, :course_id)
  end
end
