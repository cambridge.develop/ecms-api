class Api::V1::EmployeesController < Api::V1::ApiBaseController
  before_action :set_employee, only: [:show, :destroy, :update]
  def index
    @q = Employee.includes(:user, :gender, :employee_status, :user_branches, :branches)
    @q = @q.joins(:user).where(:users => {:id => UserBranch.where(branch_id: @selected_branch.id).pluck(:user_id)})
    @q = @q.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['created_at desc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      employee_list = @q.result
      return_obj[:employees] = serialize_data employee_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, employee_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:employees] = serialize_data employee_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end

    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data(@employee), status: :ok
  end

  def update
    user = @employee.user
    employee_user_params = if user_params[:password] == ''
                            user_params.except(:password)
                          else
                            user_params
                          end
    MainRecordBase.transaction do
      if user.update employee_user_params.except(:confirm_password)
        if @employee.update employee_params.except(:branch_id)
          if current_user.user_type.is_admin?
            user.user_branches.delete_all
            if employee_params[:branch_id]
              employee_params[:branch_id].uniq.each do |branch_id|
                branch = Branch.find_by_id(branch_id)
                if branch
                  UserBranch.create!(user_id: user.id, branch_id: branch.id)
                end
              end
            end
          elsif current_user.user_type.is_employee?
            # do nothing
          end
          render json: serialize_data(@employee), status: :ok
        else
          render json: { errors: @employee.errors.full_messages }, status: :bad_request
          raise ActiveRecord::Rollback
        end
      else
        render json: { errors: user.errors.full_messages }, status: :bad_request
        raise ActiveRecord::Rollback
      end
    rescue ActiveRecord::RecordInvalid => invalid
      render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
      raise ActiveRecord::Rollback
    end
  end

  def create
    @employee = Employee.new employee_params.except(:branch_id)
    MainRecordBase.transaction do
      user = User.new(user_params.except(:confirm_password))
      user.user_type = UserType.employee
      if user.save
        @employee.user_id = user.id
        employee_status = EmployeeStatus.where(name: EmployeeStatus.codes[:active]).first
        @employee.employee_status_id = employee_status.id
        if @employee.save
          if current_user.user_type.is_admin?
            if employee_params[:branch_id]
              employee_params[:branch_id].uniq.each do |branch_id|
                branch = Branch.find_by_id(branch_id)
                if branch
                  UserBranch.create!(user_id: user.id, branch_id: branch.id)
                end
              end
            end
          elsif current_user.user_type.is_employee?
            UserBranch.create!(user_id: user.id, branch_id:@selected_branch)
          end
          render json: serialize_data(@employee), status: :ok
        else
          render json: { errors: @employee.errors.full_messages }, status: :bad_request
          raise ActiveRecord::Rollback
        end
      else
        render json: { errors: user.errors.full_messages }, status: :bad_request
        raise ActiveRecord::Rollback
      end
    rescue ActiveRecord::RecordInvalid => invalid
      render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
      raise ActiveRecord::Rollback
    end
  end

  # def destroy
  #   return render json: { messages: [Employee.destroy_success_message(@employee.user.full_name)] }, status: :ok if @employee.destroy
  #   render json: {errors: @employee.errors.full_messages}, status: :bad_request
  # end

  private

  def set_employee
    begin
      @employee = Employee.includes(:user, :user_type, :gender, :employee_status).find_by_id(params[:id])
      return render json: { errors: [Employee.not_found_message] }, status: :not_found if @employee.blank?
    rescue => exception
      return render json: { errors: [Employee.not_found_message] }, status: :not_found
    end
  end

  def employee_params
    params.require(:employee).permit(:birthday, :employee_status_id, :is_manager, branch_id: [])
  end

  def user_params
    params[:user].permit( :email,
                          :username,
                          :full_name,
                          :phone,
                          :password,
                          :confirm_password,
                          :gender_id,
    )
  end

  def serialize_data data
    data.as_json(
      only: %i(id birthday is_manager employee_status_id code),
      include: [
              gender: { only: %i(name) },
              employee_status: { only: %i(name description), methods: [:name_with_locale] },
              user: { only: %i(id email username full_name phone gender_id is_locked) },
              branches: { only: %i(id) },
                ],
    )
  end
end
