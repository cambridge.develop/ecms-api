class Api::V1::SelectDatasController < Api::V1::ApiBaseController

  def majors
    majors = Major.all
    render json: majors.as_json(only: %i(id name is_locked)), status: :ok
  end

  def curriculums
    curriculums = Curriculum.all
    render json: curriculums.as_json(only: %i(id name level_id is_locked)), status: :ok
  end

  def levels
    levels = Level.all
    render json: levels.as_json(only: %i(id name major_id is_locked)), status: :ok
  end

  def classes
    classes = ClassGrade.where(branch_id: @selected_branch)
    render json: serialize_data(classes), status: :ok
  end

  def class_generals
    classes = ClassGrade.all
    render json: serialize_data(classes), status: :ok
  end

  def courses
    courses = Course.joins(:class_grade).where(:class_grades => {:branch_id => @selected_branch})
    render json: serialize_data(courses), status: :ok
  end

  def managers
    managers = User.where(user_type: UserType.employee, locked_at: nil).joins(:employee).where(:employees => {:is_manager => true})
    render json: serialize_manager_data(managers), status: :ok
  end

  def teachers
    teachers = Teacher.joins(:user).where(:users => {:locked_at => nil, :id => UserBranch.where(branch_id: @selected_branch).pluck(:user_id)}).includes(:user)
    render json: serialize_data_with_user(teachers), status: :ok
  end

  def user_types
    user_types = UserType.all
    render json: serialize_data(user_types), status: :ok
  end

  def student_potential_statuses
    returned_data = StudentPotential.statuses.map do |key, value|
      { key: key, value: value, name: StudentPotential.humanize_status(key) }
    end
    render json: returned_data, status: :ok
  end

  def students
    q = Student.includes(:user).ransack(params[:q])
    _, students = pagy q.result
    render json: students.as_json( only: %i[id code],
                                    include: [
                                      user: { only: %i[full_name id] }
                                    ]
                                  )
  end

  def genders
    genders = Gender.all
    render json: serialize_data(genders), status: :ok
  end
  
  def literacies
    literacies = Literacy.all
    render json: serialize_data(literacies), status: :ok
  end

  def holiday_types
    holiday_types = HolidayType.all
    render json: serialize_data(holiday_types), status: :ok
  end

  def branches
    branches = Branch.joins(:user_branches).where(:user_branches => {user_id: current_user.id}).where(is_locked: false)
    branches = Branch.where(is_locked: false) if current_user.user_type.is_admin?
    render json: serialize_data(branches), status: :ok
  end

  def employee_statuses
    employee_statuses = EmployeeStatus.all
    render json: employee_statuses.as_json(only: %i(id name description), methods: [:name_with_locale]), status: :ok
  end

  def teacher_statuses
    teacher_statuses = TeacherStatus.all
    render json: teacher_statuses.as_json(only: %i(id name description), methods: [:name_with_locale]), status: :ok
  end

  def course_statuses
    course_statuses = CourseStatus.all
    render json: course_statuses.as_json(only: %i(id name description), methods: [:name_with_locale]), status: :ok
  end

  def gifts
    gifts = Gift.all
    render json: gifts.as_json(only: %i(id name is_locked)), status: :ok
  end

  private

  def serialize_data data
    data.as_json(
            only: %i(id name)
    )
  end

  def serialize_manager_data data
    data.as_json(
            only: %i(id full_name),
    )
  end

  def serialize_data_with_user data
    data.as_json(
        only: %i(id code),
        include: [
            user: { only: %i(full_name) }
                ]
        )
  end

end
