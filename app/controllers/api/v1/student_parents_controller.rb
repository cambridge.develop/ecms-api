class Api::V1::StudentParentsController < Api::V1::ApiBaseController
  include Util
  before_action :set_student_parent, only: [:destroy]

  def create
    student = Student.find_by_id(student_parent_params[:student_id])
    return render json: {errors: [Student.not_found_message]}, status: :not_found if student.blank?

    if student_parent_params[:parent_id].blank?
      begin
        MainRecordBase.transaction do
          user = User.new user_params
          user.generate_user
          user.user_type = UserType.par
          user.save!

          parent = Parent.new parent_params
          parent.user_id = user.id
          parent.save!
          student_parent = StudentParent.new(student: student, parent: parent, relation: student_parent_params[:relation])
          student_parent.save!
          render json: serialize_data(student_parent), status: :ok
          # raise ActiveRecord::Rollback
        rescue ActiveRecord::RecordInvalid => invalid
          render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
          raise ActiveRecord::Rollback
        end
      end
    else
      parent = Parent.find_by_id(student_parent_params[:parent_id])
      return render json: {errors: [Parent.not_found_message]}, status: :not_found if parent.blank?
      exist_student_parent = StudentParent.where(student_id: student.id, parent_id: parent.id)
      return (render json: {errors: [StudentParent.exist_record_reference_message(Parent, Student)]} , status: :bad_request ) if exist_student_parent.blank? == false

      student_parent = StudentParent.new student_parent_params.except(:id)
      return (render json: serialize_data(student_parent), status: :ok) if student_parent.save
      # return (render json: { messages: [StudentParent.add_reference_success_message(Parent,Student)]}, status: :ok) if student_parent.save
      render json: { errors: [student_parent.errors.full_messages]}
    end
  end

  def update
    student_parent = StudentParent.find_by_id(student_parent_params[:id])
    return render json: {errors: [StudentParent.not_found_message]}, status: :not_found if student_parent.blank?
    student_parent.relation = student_parent_params[:relation]
    parent = student_parent.parent
    user = student_parent.parent.user
    parent.attributes = parent_params
    user.attributes = user_params

    begin
      MainRecordBase.transaction do
        user.save!
        parent.save!
        student_parent.save!
        render json: serialize_data(student_parent), status: :ok
      end
    rescue ActiveRecord::RecordInvalid => invalid
      render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
      raise ActiveRecord::Rollback
      return
    end
  end

  def show
    return render json: serialize_data(@student_parent), status: :ok
  end

  def destroy
    return (render json: { messages: [StudentParent.destroy_reference_success_message(Parent,Student)]}, status: :ok) if @student_parent.destroy
    render json: { errors: [@student_parent.errors.full_messages]}
end

  private

  def set_student_parent
    begin
      @student_parent = StudentParent.includes(:parent => [:user, :gender]).find_by_id(params[:id])
      return render json: {errors: [StudentParent.not_found_message]}, status: :not_found if @student_parent.blank?
    rescue => exception
      p exception
      return render json: {errors: [StudentParent.not_found_message]}, status: :not_found
    end
  end

  def student_parent_params
    params[:student_parent].permit(:id, :student_id, :parent_id, :relation)
  end

  def parent_params
    params[:parent].permit( :address,
                            :description,
                            :birthday,
                            :current_job,
                            )
  end

  def user_params
    params[:user].permit(:email,
                         :full_name,
                         :phone,
                         :gender_id,
    )
  end

  def serialize_data data
    data.as_json(
      except: %i[created_at updated_at],
      include: [
        parent: {
          except: %i[created_at updated_at],
          include: [
            gender:{only: %i[id code name]},
            user: {only: %i[id full_name username initial_password]}
          ]
        }
      ]
    )
  end

end
