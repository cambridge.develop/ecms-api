class Api::V1::Master::CertificatesController < Api::V1::Master::MasterBaseController
  before_action :set_certificate, only: [:update, :destroy, :show, :students]
  def index
    @q = Certificate.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      certificate_list = @q.result
      return_obj[:certificates] = serialize_data certificate_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, certificate_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:certificates] = serialize_data certificate_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data(@certificate), status: :ok
  end

  def create
    @certificate = Certificate.new certificate_params
    if @certificate.save
      render json: @certificate, status: :ok
    else
      render json: { errors: @certificate.errors.full_messages }, status: :bad_request
    end
  end

  def update
    @certificate.attributes = certificate_params
    return (render json: @certificate, status: :ok) if @certificate.save
    render json: {errors: @certificate.errors.full_messages}, status: :bad_request
  end

  def destroy
    return render json: { messages: [Certificate.destroy_success_message(@certificate.name)] }, status: :ok if @certificate.destroy
    render json: {errors: @certificate.errors.full_messages}, status: :bad_request
  end

  def students
    @q = @certificate.students.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      students_list = @q.result
      return_obj[:students] = serialize_data students_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, students_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:students] = serialize_data students_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  private

  def set_certificate
    begin
      @certificate = Certificate.find_by_id(params[:id])
      return render json: { errors: [Certificate.not_found_message] }, status: :not_found if @certificate.blank?
    rescue
      render json: { errors: [Certificate.not_found_message] }, status: :not_found
    end
  end

  def certificate_params
    params[:certificate].permit(:name, :code, :description)
  end

  def serialize_data data
    data.as_json(
        except: [:created_at, :updated_at]
    )
  end
end
