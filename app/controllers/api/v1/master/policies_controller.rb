class Api::V1::Master::PoliciesController < Api::V1::Master::MasterBaseController
  before_action :set_policy, only: [:update, :show, :destroy]
  def index
    @q = Policy.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      policy_list = @q.result
      return_obj[:policies] = serialize_data policy_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, policy_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:policies] = serialize_data policy_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data(@policy), status: :ok
  end

  def create
    @policy = Policy.new policy_params
    if @policy.save
      render json: @policy, status: :ok
    else
      render json: { errors: @policy.errors.full_messages }, status: :bad_request
    end
  end

  def update
    @policy.attributes = policy_params
    return (render json: @policy, status: :ok) if @policy.save
    render json: {errors: @policy.errors.full_messages}, status: :bad_request
  end

  def destroy
    return render json: { messages: [Policy.destroy_success_message(@policy.name)] }, status: :ok if @policy.destroy
    render json: {errors: @policy.errors.full_messages}, status: :bad_request
  end

  private
  def set_policy
    begin
      @policy = Policy.find_by_id(params[:id])
      return render json: { error: [Policy.not_found_message] }, status: :not_found if @policy.blank?
    rescue => exception
      render json: { errors: [Policy.not_found_message] }, status: :not_found
    end
  end

  def policy_params
    params[:policy].permit(:name, :code, :description, :percent_discount, :amount_discounts)
  end

  def serialize_data data
    data.as_json(
        except: %i[created_at updated_at]
    )
  end

end
