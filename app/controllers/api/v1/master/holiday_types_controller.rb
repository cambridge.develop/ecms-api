class Api::V1::Master::HolidayTypesController < Api::V1::Master::MasterBaseController
  def index
    @q = HolidayType.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      holiday_type_list = @q.result
      return_obj[:holiday_types] = serialize_data holiday_type_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, holiday_type_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:holiday_types] = serialize_data holiday_type_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    holiday_type = HolidayType.find_by_id(params[:id])
    if holiday_type.blank?
      render json: { errors: [HolidayType.not_found_message] }, status: :not_found
    else
      render json: serialize_data(holiday_type), status: :ok
    end
  end

  def create
    holiday_type = HolidayType.new holiday_type_params
    if holiday_type.save
      render json: holiday_type, status: :ok
    else
      render json: { errors: holiday_type.errors.full_messages }, status: :bad_request
    end
  end

  def update
    holiday_type = HolidayType.find_by_id(params[:id])
    if holiday_type.blank?
      render json: { errors: [HolidayType.not_found_message] }, status: :not_found
    else
      holiday_type.attributes = holiday_type_params
      return (render json: holiday_type, status: :ok) if holiday_type.save
      render json: {errors: holiday_type.errors.full_messages}, status: :bad_request
    end
  end

  def destroy
    holiday_type = HolidayType.find_by_id(params[:id])
    if holiday_type.blank?
      render json: { errors: [HolidayType.not_found_message] }, status: :not_found
    else
      return render json: { messages: [HolidayType.destroy_success_message(holiday_type.name)] }, status: :ok if holiday_type.destroy
      render json: {errors: holiday_type.errors.full_messages}, status: :bad_request
    end
  end

  private

  def holiday_type_params
    params[:holiday_type].permit(:name, :code)
  end

  def serialize_data data
    data.as_json(
        except: [:created_at, :updated_at]
    )
  end
end
