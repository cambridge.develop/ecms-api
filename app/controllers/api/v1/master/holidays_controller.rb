class Api::V1::Master::HolidaysController < Api::V1::Master::MasterBaseController
  include Pundit
  before_action :selected_branch
  before_action :check_permission

  def index
    if policy(self.class.name.gsub("Controller", "").split("::").map{|e| e.underscore.to_sym}).holiday_admin?
      @q = Holiday.includes(:holiday_type, :branch).ransack(params[:q])
    else
      @q = Holiday.includes(:holiday_type, :branch).where( branch_id: @selected_branch.id).or(Holiday.includes(:holiday_type, :branch).where( is_apply_all: true)).ransack(params[:q])
    end

    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      holiday_list = @q.result
      return_obj[:holidays] = serialize_data holiday_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, holiday_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:holidays] = serialize_data holiday_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    holiday = Holiday.find_by_id(params[:id])
    if holiday.blank?
      render json: { errors: [Holiday.not_found_message] }, status: :not_found
    else
      render json: serialize_data(holiday), status: :ok
    end
  end

  def create
    holiday = Holiday.new holiday_params
    if holiday.is_apply_all
      authorize(self.class.name.gsub("Controller", "").split("::").map{|e| e.underscore.to_sym}, "holiday_admin?")
      holiday.branch_id = nil
    end
    if holiday.save
      render json: holiday, status: :ok
    else
      render json: { errors: holiday.errors.full_messages }, status: :bad_request
    end
  end

  def update
    holiday = Holiday.find_by_id(params[:id])


    if holiday.blank?
      render json: { errors: [Holiday.not_found_message] }, status: :not_found
    else
      holiday.attributes = holiday_params
      if holiday.is_apply_all
        authorize(self.class.name.gsub("Controller", "").split("::").map{|e| e.underscore.to_sym}, "holiday_admin?")
        holiday.branch_id = nil
      end

      return (render json: serialize_data(holiday), status: :ok) if holiday.save
      render json: { errors: holiday.errors.full_messages }, status: :bad_request
    end
  end

  def destroy
    holiday = Holiday.find_by_id(params[:id])
    authorize(self.class.name.gsub("Controller", "").split("::").map{|e| e.underscore.to_sym}, "holiday_admin?") if holiday.is_apply_all
    if holiday.blank?
      render json: { errors: [Holiday.not_found_message] }, status: :not_found
    else
      return render json: { messages: [Holiday.destroy_success_message(holiday.name)] }, status: :ok if holiday.destroy
      render json: { errors: holiday.errors.full_messages }, status: :bad_request
    end
  end

  private

  def holiday_params
    params[:holiday].permit(:name, :date_start, :date_end, :holiday_type_id, :description, :is_apply_all, :branch_id)
  end

  def serialize_data data
    data.as_json(except: [:created_at, :updated_at],
                 include: [
                     holiday_type: {
                         only: [:name, :id]
                     },
                     branch: {
                         only: [:name, :id]
                     }
                 ]

    )
  end
end
