class Api::V1::Master::ReferenceSourcesController < Api::V1::Master::MasterBaseController
  before_action :set_reference_source, only: [:show, :update, :destroy, :students]
  def index
    @q = ReferenceSource.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      reference_source_list = @q.result
      return_obj[:reference_sources] = serialize_data reference_source_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, reference_source_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:reference_sources] = serialize_data reference_source_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data(@reference_source), status: :ok
  end

  def create
    @reference_source = ReferenceSource.new reference_source_params
    if @reference_source.save
      render json: @reference_source, status: :ok
    else
      render json: { errors: @reference_source.errors.full_messages }, status: :bad_request
    end
  end

  def update
    @reference_source.attributes = reference_source_params
    return (render json: @reference_source, status: :ok) if @reference_source.save
    render json: {errors: @reference_source.errors.full_messages}, status: :bad_request
  end

  def destroy
    return render json: { messages: [ReferenceSource.destroy_success_message(@reference_source.name)] }, status: :ok if @reference_source.destroy
    render json: {errors: @reference_source.errors.full_messages}, status: :bad_request
  end

  def students
    @q = @reference_source.students.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      students_list = @q.result
      return_obj[:students] = serialize_data students_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, students_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:students] = serialize_data students_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  private
    def set_reference_source
      begin
        @reference_source = ReferenceSource.find_by_id(params[:id])
        return render json: { errors: [ReferenceSource.not_found_message] }, status: :not_found if @reference_source.blank?
      rescue => exception
        render json: { errors: [ReferenceSource.not_found_message] }, status: :not_found
      end
    end

    def reference_source_params
      params[:reference_source].permit(:name, :code, :description)
    end

    def serialize_data data
      data.as_json(
          except: [:created_at, :updated_at]
      )
    end
end
