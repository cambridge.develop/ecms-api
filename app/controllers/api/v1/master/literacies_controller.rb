class Api::V1::Master::LiteraciesController < Api::V1::Master::MasterBaseController
  before_action :set_literacy, only: [:destroy, :show, :update,:students, :schools]
  def index
    @q = Literacy.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      literacy_list = @q.result
      return_obj[:literacies] = serialize_data literacy_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, literacy_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:literacies] = serialize_data literacy_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data(@literacy), status: :ok
  end

  def create
    @literacy = Literacy.new literacy_params
    if @literacy.save
      render json: @literacy, status: :ok
    else
      render json: { errors: @literacy.errors.full_messages }, status: :bad_request
    end
  end

  def update
    @literacy.attributes = literacy_params
    return (render json: @literacy, status: :ok) if @literacy.save
    render json: {errors: @literacy.errors.full_messages}, status: :bad_request
  end

  def destroy
    return render json: { messages: [Literacy.destroy_success_message(@literacy.name)] }, status: :ok if @literacy.destroy
    render json: {errors: @literacy.errors.full_messages}, status: :bad_request
  end

  def students
    @q = @literacy.students.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      students_list = @q.result
      return_obj[:students] = serialize_data students_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, students_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:students] = serialize_data students_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def schools
    @q = @literacy.schools.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      schools_list = @q.result
      return_obj[:schools] = serialize_data schools_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, schools_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:schools] = serialize_data schools_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  private
    def set_literacy
      begin
        @literacy = Literacy.find_by_id(params[:id])
        return render json: { errors: [Literacy.not_found_message] }, status: :not_found if @literacy.blank?
      rescue
        render json: { errors: [Literacy.not_found_message] }, status: :not_found
      end
    end

    def literacy_params
      params[:literacy].permit(:name, :code, :description)
    end

    def serialize_data data
      data.as_json(
          except: [:created_at, :updated_at]
      )
    end
end
