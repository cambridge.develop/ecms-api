class Api::V1::Master::GendersController < Api::V1::Master::MasterBaseController
  def index
    @q = Gender.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      gender_list = @q.result
      return_obj[:genders] = serialize_data gender_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, gender_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:genders] = serialize_data gender_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    gender = Gender.find_by_id(params[:id])
    if gender.blank?
      render json: { errors: [Gender.not_found_message] }, status: :not_found
    else
      render json: serialize_data(gender), status: :ok
    end
  end

  def create
    gender = Gender.new gender_params
    if gender.save
      render json: gender, status: :ok
    else
      render json: { errors: gender.errors.full_messages }, status: :bad_request
    end
  end

  def update
    gender = Gender.find_by_id(params[:id])
    if gender.blank?
      render json: { errors: [Gender.not_found_message] }, status: :not_found
    else
      gender.attributes = gender_params
      return (render json: gender, status: :ok) if gender.save
      render json: {errors: gender.errors.full_messages}, status: :bad_request
    end
  end

  def destroy
    gender = Gender.find_by_id(params[:id])
    if gender.blank?
      render json: { errors: [Gender.not_found_message] }, status: :not_found
    else
      return render json: { messages: [Gender.destroy_success_message(gender.name)] }, status: :ok if gender.destroy
      render json: {errors: gender.errors.full_messages}, status: :bad_request
    end
  end

  private

  def gender_params
    params[:gender].permit(:name, :code, :description)
  end

  def serialize_data data
    data.as_json(
        except: [:created_at, :updated_at]
    )
  end
end
  