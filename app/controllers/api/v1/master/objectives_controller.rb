class Api::V1::Master::ObjectivesController < Api::V1::Master::MasterBaseController
  before_action :set_objective, only: [:update, :show, :destroy, :students]
  def index
    @q = Objective.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      objective_list = @q.result
      return_obj[:objectives] = serialize_data objective_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, objective_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:objectives] = serialize_data objective_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data(@objective), status: :ok
  end

  def create
    @objective = Objective.new objective_params
    if @objective.save
      render json: @objective, status: :ok
    else
      render json: { errors: @objective.errors.full_messages }, status: :bad_request
    end
  end

  def update
    @objective.attributes = objective_params
    return (render json: @objective, status: :ok) if @objective.save
    render json: {errors: @objective.errors.full_messages}, status: :bad_request
  end

  def destroy
    return render json: { messages: [Objective.destroy_success_message(@objective.name)] }, status: :ok if @objective.destroy
    render json: {errors: @objective.errors.full_messages}, status: :bad_request
  end

  def students
    @q = @objective.students.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      students_list = @q.result
      return_obj[:students] = serialize_data students_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, students_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:students] = serialize_data students_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  private
    def set_objective
      begin
        @objective = Objective.find_by_id(params[:id])
        return render json: { error: [Objective.not_found_message] }, status: :not_found if @objective.blank?
      rescue => exception
        render json: { errors: [Objective.not_found_message] }, status: :not_found
      end
    end

    def objective_params
      params[:objective].permit(:name, :code, :description)
    end

    def serialize_data data
      data.as_json(
          except: [:created_at, :updated_at]
      )
    end
end
