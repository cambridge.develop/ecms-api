class Api::V1::Master::SchoolsController < Api::V1::Master::MasterBaseController
  before_action :set_school, only: [:show, :destroy, :update, :students]
  def index
    @q = School.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      school_list = @q.result
      return_obj[:schools] = serialize_data school_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, school_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:schools] = serialize_data school_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data(@school), status: :ok
  end

  def create
    @school = School.new school_params
    if @school.save
      render json: @school, status: :ok
    else
      render json: { errors: @school.errors.full_messages }, status: :bad_request
    end
  end

  def update
    @school.attributes = school_params
    return (render json: @school, status: :ok) if @school.save
    render json: {errors: @school.errors.full_messages}, status: :bad_request
  end

  def destroy
    return render json: { messages: [School.destroy_success_message(@school.name)] }, status: :ok if @school.destroy
    render json: {errors: @school.errors.full_messages}, status: :bad_request
  end

  def students
    @q = @school.students.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      students_list = @q.result
      return_obj[:students] = serialize_data students_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, students_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:students] = serialize_data students_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  private
    def set_school
      begin
        @school = School.find_by_id(params[:id])
        return render json: { errors: [School.not_found_message] }, status: :not_found if @school.blank?
      rescue => exception
        return render json: { errors: [School.not_found_message] }, status: :not_found
      end
    end

    def school_params
      params[:school].permit(:name, :code, :literacy_id, :description)
    end

    def serialize_data data
      data.as_json(
          except: [:created_at, :updated_at]
      )
    end
end
