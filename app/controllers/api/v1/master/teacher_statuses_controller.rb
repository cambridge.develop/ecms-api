class Api::V1::Master::TeacherStatusesController < Api::V1::Master::MasterBaseController
  before_action :set_teacher_status, only: [:show, :update, :destroy, :teachers]
  def index
    @q = TeacherStatus.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      teacher_status_list = @q.result
      return_obj[:teacher_statuses] = serialize_data teacher_status_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, teacher_status_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:teacher_statuses] = serialize_data teacher_status_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data(@teacher_status), status: :ok
  end

  def create
    @teacher_status = TeacherStatus.new teacher_status_params
    if @teacher_status.save
      render json: @teacher_status, status: :ok
    else
      render json: { errors: @teacher_status.errors.full_messages }, status: :bad_request
    end
  end

  def update
    @teacher_status.attributes = teacher_status_params
    return (render json: @teacher_status, status: :ok) if @teacher_status.save
    render json: {errors: @teacher_status.errors.full_messages}, status: :bad_request
  end

  def destroy
    return render json: { messages: [TeacherStatus.destroy_success_message(@teacher_status.name)] }, status: :ok if @teacher_status.destroy
    render json: {errors: @teacher_status.errors.full_messages}, status: :bad_request
  end

  def teachers
      @q = @teacher_status.teachers.ransack(params[:q])
      current_page = params[:page] || 1
      @q.sorts = ['name asc'] if @q.sorts.empty?
      return_obj = {}
      if params.has_key?(:all)
        teachers_list = @q.result
        return_obj[:teachers] = serialize_data teachers_list
        return_obj[:current_page] = 0
        return_obj[:total_page] = 0
      else
        pagy, teachers_list = pagy @q.result, items: params[:items], page: current_page
        return_obj[:teachers] = serialize_data teachers_list
        return_obj[:current_page] = pagy.page
        return_obj[:total_page] = pagy.pages
      end
      render json: return_obj, status: :ok
  end

  private
    def set_teacher_status
      begin
        @teacher_status = TeacherStatus.find_by_id(params[:id])
        return render json: { errors:[TeacherStatus.not_found_message] }, status: :not_found if @teacher_status.blank?
      rescue => exception
        return render json: { errors:[TeacherStatus.not_found_message] }, status: :not_found
      end
    end

    def teacher_status_params
      params[:teacher_status].permit(:name, :code, :description)
    end

    def serialize_data data
      data.as_json(except: [:created_at, :updated_at])
    end
end
