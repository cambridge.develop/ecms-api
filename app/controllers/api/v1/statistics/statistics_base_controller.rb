class Api::V1::Statistics::StatisticsBaseController < Api::V1::ApiBaseController
  before_action :authenticate_user!
  before_action :check_user
end
