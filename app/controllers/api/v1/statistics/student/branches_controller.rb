class Api::V1::Statistics::Student::BranchesController < Api::V1::Statistics::Student::StudentBaseController
  before_action :students_in_branch

  def day
    # Thống kê học sinh mới phát sinh trong khoảng thời gian ngày X - ngày Y
    begin
      date_from = day_params[:from] || ""
      date_to = day_params[:to] || ""
      date_from = Date.parse(date_from).beginning_of_day
      date_to = Date.parse(date_to).end_of_day
      return render json: {errors: [invalid_date_range_message]}, status: :bad_request unless date_to > date_from
      students = Student.joins(:student_courses).where(:student_courses => {:id => @student_course})
        .where("students.created_at >= ? and students.created_at <= ?", date_from, date_to).uniq
      result_obj = {}
      result_obj[:from] = date_from
      result_obj[:to] = date_to
      result_obj[:number_of_new_students] = students.size
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: {errors: [invalid_date_format_message]}, status: :bad_request
    end
  end

  def month
    # Thống kê học sinh mới phát sinh trong khoảng tháng X - tháng Y
    begin
      month_from = month_params[:month_from].to_i || ""
      month_to = month_params[:month_to].to_i || ""
      year_from = month_params[:year_from].to_i || ""
      year_to = month_params[:year_to].to_i || ""
      date_from = DateTime.now.change(month: month_from, year: year_from).at_beginning_of_month
      date_to = DateTime.now.change(month: month_to, year: year_to).at_end_of_month
      return render json: {errors: [invalid_month_range_message]}, status: :bad_request if (date_from > date_to)
      students = Student.joins(:student_courses).where(:student_courses => {:id => @student_course})
                     .where("students.created_at >= ? and students.created_at <= ?", date_from, date_to).uniq
      result_obj = {}
      result_obj[:from] = date_from
      result_obj[:to] = date_to
      result_obj[:number_of_new_students] = students.size
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: {errors: [invalid_month_format_message]}, status: :bad_request
    end
  end

  def last_period
    # Thống kê học sinh mới phát sinh trong khoảng tháng X - tháng Y năm ngoái
    begin
      month_from = month_params[:from].to_i || ""
      month_to = month_params[:to].to_i || ""
      date_from = 1.year.ago.change(month: month_from).at_beginning_of_month
      date_to = 1.year.ago.change(month: month_to).at_end_of_month
      return render json: {errors: [invalid_month_format_message]}, status: :bad_request unless date_to > date_from
      students = Student.joins(:student_courses).where(:student_courses => {:id => @student_course})
                     .where("students.created_at >= ? and students.created_at <= ?", date_from, date_to).uniq
      result_obj = {}
      result_obj[:from] = date_from
      result_obj[:to] = date_to
      result_obj[:number_of_new_students] = students.size
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: {errors: [invalid_month_format_message]}, status: :bad_request
    end
  end

  def literacies
    # Thống kê học sinh theo trình độ học vấn
    literacy_list = Literacy.all
    result_object = {}
    literacies = []
    literacy_list.each do |literacy|
      literacy_students = literacy.students.studying.joins(:student_courses).where(:student_courses => {:id => @student_course}).uniq.size
      literacy_object = {}
      literacy_object[:id] = literacy.id
      literacy_object[:code] = literacy.code
      literacy_object[:name] = literacy.name
      literacy_object[:number_of_students] = literacy_students
      literacies << literacy_object
    end
    result_object[:literacies] = literacies
    render json: result_object, status: :ok
  end

  def objectives
    # Thống kê học sinh theo mục đích học tập
    objective_list = Objective.all
    result_object = {}
    objectives = []
    objective_list.each do |objective|
      objective_students = objective.students.studying.joins(:student_courses).where(:student_courses => {:id => @student_course}).uniq.size
      objective_object = {}
      objective_object[:id] = objective.id
      objective_object[:code] = objective.code
      objective_object[:name] = objective.name
      objective_object[:number_of_students] = objective_students
      objectives << objective_object
    end
    result_object[:objectives] = objectives
    render json: result_object, status: :ok
  end

  def reference_sources
    # Thống kê học sinh theo nguồn tiếp cận
    reference_source_list = ReferenceSource.all
    result_object = {}
    reference_sources = []
    reference_source_list.each do |reference_source|
      reference_source_students = reference_source.students.studying.joins(:student_courses).where(:student_courses => {:id => @student_course}).uniq.size
      reference_source_object = {}
      reference_source_object[:id] = reference_source.id
      reference_source_object[:code] = reference_source.code
      reference_source_object[:name] = reference_source.name
      reference_source_object[:number_of_students] = reference_source_students
      reference_sources << reference_source_object
    end
    result_object[:reference_sources] = reference_sources
    render json: result_object, status: :ok
  end


  # params: month_start, month_end
  def number_of_new_students_in_month_range
    month_start = params[:month_start].presence || 12.month.ago
    month_end = params[:month_end].presence || Date.today
    month_start = month_start.to_date.beginning_of_month.to_date
    month_end = month_end.to_date.end_of_month
    students = Student.joins(:student_courses => {:course => :class_grade})
                .where(created_at: month_start..month_end)
                .where(class_grades: {branch_id: @selected_branch.id})
                .select("date_part('year', students.created_at) AS nyear", "date_part('month', students.created_at) AS nmonth", "COUNT(DISTINCT(students.id))")
                .order("nyear, nmonth")
                .group("nyear, nmonth").load()

    chart_data = {}
    chart_data[:month_start] = month_start
    chart_data[:month_end] = month_end
    chart_data[:data] = students.map do |e|
      {text: "#{e.nmonth.to_i}/#{e.nyear.to_i}", count: e.count}
    end

    render json: chart_data
  end

  private
  def students_in_branch
    class_in_branch = ClassGrade.where(branch_id: @selected_branch).pluck(:id)
    @student_course = StudentCourse.joins(:course).where(:courses => {:class_grade_id => class_in_branch})
  end
end
