class Api::V1::Statistics::Student::GeneralsController < Api::V1::Statistics::Student::StudentBaseController

  def day
  # Thống kê học sinh mới phát sinh trong khoảng thời gian ngày X - ngày Y
    begin
      date_from = day_params[:from] || ""
      date_to = day_params[:to] || ""
      date_from = Date.parse(date_from).beginning_of_day
      date_to = Date.parse(date_to).end_of_day
      return render json: {errors: [invalid_date_range_message]}, status: :bad_request unless date_to > date_from
      students = Student.where("students.created_at >= ? and students.created_at <= ?", date_from, date_to)
      result_obj = {}
      result_obj[:from] = date_from
      result_obj[:to] = date_to
      result_obj[:number_of_new_students] = students.size
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: {errors: [invalid_date_format_message]}, status: :bad_request
    end
end

  def month
    # Thống kê học sinh mới phát sinh trong khoảng tháng X - tháng Y
    begin
      month_from = month_params[:month_from].to_i || ""
      month_to = month_params[:month_to].to_i || ""
      year_from = month_params[:year_from].to_i || ""
      year_to = month_params[:year_to].to_i || ""
      date_from = DateTime.now.change(month: month_from, year: year_from).at_beginning_of_month
      date_to = DateTime.now.change(month: month_to, year: year_to).at_end_of_month
      return render json: {errors: [invalid_month_range_message]}, status: :bad_request if (date_from > date_to)
      students = Student.where("students.created_at >= ? and students.created_at <= ?", date_from, date_to)
      result_obj = {}
      result_obj[:from] = date_from
      result_obj[:to] = date_to
      result_obj[:number_of_new_students] = students.size
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: {errors: [invalid_month_format_message]}, status: :bad_request
    end
  end

  def last_period
    # Thống kê học sinh mới phát sinh trong khoảng tháng X - tháng Y năm ngoái
    begin
      month_from = month_params[:from].to_i || ""
      month_to = month_params[:to].to_i || ""
      date_from = 1.year.ago.change(month: month_from).at_beginning_of_month
      date_to = 1.year.ago.change(month: month_to).at_end_of_month
      return render json: {errors: [invalid_month_format_message]}, status: :bad_request unless date_to > date_from
      students = Student.where("students.created_at >= ? and students.created_at <= ?", date_from, date_to)
      result_obj = {}
      result_obj[:from] = date_from
      result_obj[:to] = date_to
      result_obj[:number_of_new_students] = students.size
      render json: result_obj, status: :ok
    rescue ArgumentError
      render json: {errors: [invalid_month_format_message]}, status: :bad_request
    end
  end

  def literacies
    # Thống kê học sinh theo trình độ học vấn
    literacy_list = Literacy.all
    result_object = {}
    literacies = []
    literacy_list.each do |literacy|
      literacy_students = literacy.students.studying.size
      literacy_object = {}
      literacy_object[:id] = literacy.id
      literacy_object[:code] = literacy.code
      literacy_object[:name] = literacy.name
      literacy_object[:number_of_students] = literacy_students
      literacies << literacy_object
    end
    result_object[:literacies] = literacies
    render json: result_object, status: :ok
  end

  def objectives
    # Thống kê học sinh theo mục đích học tập
    objective_list = Objective.all
    result_object = {}
    objectives = []
    objective_list.each do |objective|
      objective_students = objective.students.studying.size
      objective_object = {}
      objective_object[:id] = objective.id
      objective_object[:code] = objective.code
      objective_object[:name] = objective.name
      objective_object[:number_of_students] = objective_students
      objectives << objective_object
    end
    result_object[:objectives] = objectives
    render json: result_object, status: :ok
  end

  def reference_sources
    # Thống kê học sinh theo nguồn tiếp cận
    reference_source_list = ReferenceSource.all
    result_object = {}
    reference_sources = []
    reference_source_list.each do |reference_source|
      reference_source_students = reference_source.students.studying.size
      reference_source_object = {}
      reference_source_object[:id] = reference_source.id
      reference_source_object[:code] = reference_source.code
      reference_source_object[:name] = reference_source.name
      reference_source_object[:number_of_students] = reference_source_students
      reference_sources << reference_source_object
    end
    result_object[:reference_sources] = reference_sources
    render json: result_object, status: :ok
  end

  private

end
