class Api::V1::Statistics::Student::StudentBaseController < Api::V1::Statistics::StatisticsBaseController
  protected
  def day_params
    params[:day].permit(:from, :to)
  end

  def month_params
    params[:month].permit(:month_from, :month_to, :year_from, :year_to)
  end

  def invalid_date_range_message
    I18n.t("controllers.statistics.student.invalid_date_range")
  end
  def invalid_month_range_message
    I18n.t("controllers.statistics.student.invalid_month_range")
  end
  def invalid_date_format_message
    I18n.t("controllers.statistics.student.date_format")
  end
  def invalid_month_format_message
    I18n.t("controllers.statistics.student.month_format")
  end
end
