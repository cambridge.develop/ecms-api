class Api::V1::StudentReferenceSourcesController < Api::V1::ApiBaseController
  def create
    student = Student.find_by_id(student_reference_source_params[:student_id])
    reference_source = ReferenceSource.find_by_id(student_reference_source_params[:reference_source_id])
    return render json: { errors: [Student.not_found_message] }, status: :not_found if student.blank?
    return render json: { errors: [ReferenceSource.not_found_message] }, status: :not_found if reference_source.blank?
    exist_student_reference_source = StudentReferenceSource.where(student_id: student.id, reference_source_id: reference_source.id)
    return render json: { errors: [StudentReferenceSource.exist_record_reference_message(ReferenceSource, Student)] } if exist_student_reference_source.blank? == false

    student_reference_source = StudentReferenceSource.new student_reference_source_params
    return (render json: serialize_data(student_reference_source), status: :ok) if student_reference_source.save
    render json: { errors: [student_reference_source.errors.full_messages]}
  end

  def destroy
    # student = Student.find_by_id(student_reference_source_params[:student_id])
    # reference_source = reference_source.find_by_id(parstudent_reference_source_paramsams[:reference_source_id])
    # return render json: { errors: [Student.not_found_message] }, status: :not_found if student.blank?
    # return render json: { errors: [ReferenceSource.not_found_message] }, status: :not_found if reference_source.blank?
    student_reference_source = StudentReferenceSource.find_by_id(params[:id])
    return (render json: { errors: [StudentReferenceSource.not_found_message] }, status: :not_found) if student_reference_source.blank?

    return (render json: { messages: [StudentReferenceSource.destroy_reference_success_message(ReferenceSource, Student)]}, status: :ok) if student_reference_source.destroy
    render json: { errors: [student_reference_source.errors.full_messages]}
  end

  private

  def student_reference_source_params
    params[:student_reference_source].permit(:student_id, :reference_source_id)
  end

  def serialize_data data
    data.as_json(
        except: %i[created_at updated_at],
        include: [
            reference_source: {except: %i[created_at updated at]}
        ]
    )
  end
end
