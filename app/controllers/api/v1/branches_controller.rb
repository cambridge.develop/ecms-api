class Api::V1::BranchesController < Api::V1::ApiBaseController
  before_action :set_branch, only: [ :update, :destroy, :show, :set_default_branch, :users, :courses, :unadded_users, :lock, :unlock]
  def index
    @q = Branch.joins(:user_branches).where(:user_branches => {user_id: current_user.id}).includes(:manager).ransack(params[:q])
    @q = Branch.includes(:manager).ransack(params[:q]) if current_user.user_type.is_admin?
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      branch_list = @q.result
      return_obj[:branches] = serialize_data branch_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, branch_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:branches] = serialize_data branch_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data(@branch), status: :ok
  end

  def create
    @branch = Branch.new branch_params
    if @branch.save
      render json: @branch, status: :ok
    else
      render json: { errors: @branch.errors.full_messages}, status: :bad_request
    end
  end

  def update
    @branch.attributes = branch_params
    return (render json: (serialize_data@branch), status: :ok) if @branch.save
    render json: {errors: @branch.errors.full_messages}, status: :bad_request
  end

  def destroy
    return render json: { messages: [Branch.destroy_success_message(@branch.name)] }, status: :ok if @branch.destroy
    render json: {errors: @branch.errors.full_messages}, status: :bad_request
  end

  def unadded_users
    user_in_branch = @branch.users.where(user_type_id: UserType.employee.id)
    @q = User.joins(:employee)
              .where.not(id: user_in_branch.ids)
              .where(employees: { employee_status_id: EmployeeStatus.active.id })
              .where(user_type_id: UserType.employee.id)
              .includes(:gender).includes(:employee => :employee_status).ransack(params[:q])

    current_page = params[:page] || 1
    @q.sorts = ['full_name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      user_list = @q.result
      return_obj[:users] = serialize_user_data user_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, user_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:users] = serialize_user_data user_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def users
    @q = @branch.users.includes(:gender).includes(:employee => :employee_status).where(user_type_id: UserType.employee.id).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['full_name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      user_list = @q.result
      return_obj[:users] = serialize_user_data user_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, user_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:users] = serialize_user_data user_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end
  #
  # def courses
  #   courses= @branch.courses
  #   course_status = params[:status] || 0
  #   case course_status
  #   when 1
  #       courses = courses.not_closed
  #   when 2
  #       courses = courses.closed
  #   end
  #
  #   @q = courses.ransack(params[:q])
  #   current_page = params[:page] || 1
  #   @q.sorts = ['name asc'] if @q.sorts.empty?
  #   return_obj = {}
  #   if params.has_key?(:all)
  #     course_list = @q.result
  #     return_obj[:courses] = serialize_data course_list
  #     return_obj[:current_page] = 0
  #     return_obj[:total_page] = 0
  #   else
  #     pagy, course_list = pagy @q.result, items: params[:items], page: current_page
  #     return_obj[:courses] = serialize_data course_list
  #     return_obj[:current_page] = pagy.page
  #     return_obj[:total_page] = pagy.pages
  #   end
  #   render json: return_obj, status: :ok
  # end

  def add_user_to_branch
    branch = Branch.where(id: params[:id])
    user = User.where(id: user_params[:id])
    return render json: { errors: [Branch.not_found_message] }, status: :not_found if branch.blank?
    return render json: { errors: [User.not_found_message] }, status: :not_found if user.blank?

    exist_user_branch = UserBranch.where(branch_id: branch.first.id, user_id: user.first.id)
    return render json: { errors: [UserBranch.exist_record_reference_message(User, Branch)] } if exist_user_branch.blank? == false

    @user_branch = UserBranch.new(branch_id: branch.first.id, user_id: user.first.id, is_approver: false)
    return (render json: { messages: [UserBranch.add_reference_success_message(User, Branch)]}, status: :ok) if @user_branch.save
    return render json: { errors: [@user_branch.errors.full_messages]}, status: :bad_request
  end

  def remove_user_in_branch
    branch = Branch.where(id: params[:id])
    user = User.where(id: user_params[:id])
    return render json: { errors: [Branch.not_found_message] }, status: :not_found if branch.blank?
    return render json: { errors: [User.not_found_message] }, status: :not_found if user.blank?

    exist_user_branch = UserBranch.where(branch_id: branch.first.id, user_id: user.first.id)
    return (render json: { messages: [UserBranch.destroy_reference_success_message(User, Branch)]}, status: :ok) if exist_user_branch.destroy_all
    render json: { errors: [exist_user_branch.errors.full_messages]}
  end

  def lock
    @branch.is_locked = true
    return (render json: { messsages: [Branch.lock_success_message] }, status: :ok) if @branch.save
    render json: { errors: @branch.errors.full_messages }, status: :bad_request
  end

  def unlock
    @branch.is_locked = false
    return (render json: { messsages: [Branch.unlock_success_message] }, status: :ok) if @branch.save
    render json: { errors: @branch.errors.full_messages }, status: :bad_request
  end

  private

  def branch_params
    params[:branch].permit(:name,
                           :code,
                           :address,
                           :phone,
                           :email,
                           :description,
                           :fax,
                           :manager_id,
                           :id,
                           :logo_name,
                           :logo_base64
    )
  end

  def user_params
    params[:user].permit(:id)
  end

  def serialize_data data
    data.as_json(
        include: :manager,
        except: [:created_at, :updated_at]
    )
  end

  def serialize_user_data data
    data.as_json(
        # except: [:created_at, :updated_at],
        include: [
            employee: {
              only: [:code],
              include: [
                employee_status: {
                  methods: [:name_with_locale]
                }
              ]
            },
            gender: { only: [:name] }
        ]
    )
  end

  def set_branch
    begin
      @branch = Branch.find_by_id(params[:id])
      render json: { errors: [Branch.not_found_message] }, status: :not_found if @branch.blank?
    rescue => exception
      render json: { errors: [Branch.not_found_message] }, status: :not_found
    end
  end
end
