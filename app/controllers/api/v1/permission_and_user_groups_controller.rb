class Api::V1::PermissionAndUserGroupsController < Api::V1::ApiBaseController

  def create
    MainRecordBase.transaction do
      permissions = Permission.where(id: permission_and_user_group_params[:permission_id]).pluck(:id)
      user_group = UserGroup.find_by_id(permission_and_user_group_params[:user_group_id])
      permission_groups = PermissionGroup.where(id: permission_and_user_group_params[:permission_group_id])
      permission_groups.each do |permission_group|
        permissions += permission_group.permissions.pluck(:id)
      end
      added_permissions = Permission.joins(:permission_and_user_groups)
                              .where(:permission_and_user_groups => {:user_group_id => user_group.id})
                              .pluck( :id)
      permission_and_user_groups = (permissions - added_permissions).uniq.map do |permission_id|
        PermissionAndUserGroup.new(permission_id: permission_id, user_group_id: user_group.id)
      end
      PermissionAndUserGroup.import permission_and_user_groups
      return (render json: { messages: [PermissionAndPermissionGroup.add_reference_success_message(Permission, UserGroup)] }, status: :ok)
    rescue ActiveRecord::RecordInvalid => invalid
      render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
      raise ActiveRecord::Rollback
    end
  end

  def delete_permissions
    MainRecordBase.transaction do
      permissions = Permission.where(id: permission_and_user_group_params[:permission_id])
      user_group = UserGroup.find_by_id(permission_and_user_group_params[:user_group_id])
      PermissionAndUserGroup.where(permission_id: permissions.ids, user_group_id: user_group.id).delete_all
      return (render json: { messages: [PermissionAndPermissionGroup.destroy_reference_success_message(Permission, UserGroup)] }, status: :ok)
    rescue ActiveRecord::RecordInvalid => invalid
      render json: { errors: invalid.record.errors.full_messages }, status: :bad_request
      raise ActiveRecord::Rollback
    end
  end

  private

  def permission_and_user_group_params
    params[:permission_and_user_group].permit(:user_group_id, permission_id: [], permission_group_id: [])
  end
end
