class Api::V1::UserGroupsController < Api::V1::ApiBaseController
  before_action :set_user_group, only: [:show, :update, :destroy, :users, :permissions, :not_in_group_permissions, :not_in_group_permission_groups, :not_in_group_users]
  def index
    @q = UserGroup.includes(:branch, :user_type)
    if current_user.user_type.is_employee?
      @q = @q.where(branch_id: @selected_branch.id)
    end
    @q = @q.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      user_group_list = @q.result
      return_obj[:user_groups] = serialize_data user_group_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, user_group_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:user_groups] = serialize_data user_group_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data(@user_group), status: :ok
  end

  def create
    @user_group = UserGroup.new user_group_params
    # if current_user.user_type.is_employee?
      @user_group.branch_id = @selected_branch.id
    # end
    if @user_group.save
      render json: @user_group, status: :ok
    else
      render json: { errors: @user_group.errors.full_messages }, status: :bad_request
    end
  end

  def update
    @user_group.attributes = user_group_params.except(:branch_id, :user_type_id)
    return (render json: serialize_data(@user_group), status: :ok) if @user_group.save
    render json: {errors: @user_group.errors.full_messages}, status: :bad_request
  end

  def destroy
    return render json: { messages: [UserGroup.destroy_success_message(@user_group.name)] }, status: :ok if @user_group.destroy
    render json: {errors: @user_group.errors.full_messages}, status: :bad_request
  end

  def permissions
    @q = @user_group.permissions.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      permission_list = @q.result
      return_obj[:permissions] = serialize_permission_data permission_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, permission_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:permissions] = serialize_permission_data permission_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def not_in_group_permissions
    @q = Permission.where.not(id: @user_group.permissions.pluck(:id)).where("? = ANY(user_types)", @user_group.user_type_id).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      permission_list = @q.result
      return_obj[:permissions] = serialize_permission_data permission_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, permission_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:permissions] = serialize_permission_data permission_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def not_in_group_permission_groups
    @q = PermissionGroup.where(user_type_id: @user_group.user_type_id).includes(:permissions).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      permission_list = @q.result
      return_obj[:permission_groups] = serialize_permission_group_data permission_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, permission_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:permission_groups] = serialize_permission_group_data permission_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end
  def not_in_group_users
    added_users = @user_group.users
    p added_users
    unadd_users = User.where(:id => UserBranch.where(branch_id: @user_group.branch_id).pluck(:user_id), user_type_id: @user_group.user_type_id).where.not(:id => added_users.pluck(:id))

    @q = unadd_users.includes(:user_type, :gender).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      users_list = @q.result
      return_obj[:users] = serialize_user_data users_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, users_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:user_groups] = serialize_user_data users_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def users
    @q = @user_group.users.includes(:user_type, :gender)
    @q = @q.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      users_list = @q.result
      return_obj[:users] = serialize_user_data users_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, users_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:user_groups] = serialize_user_data users_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end
  private
  def set_user_group
    begin
      @user_group = UserGroup.find_by_id(params[:id])
      render json: { errors: [UserGroup.not_found_message] }, status: :not_found if @user_group.blank?
    rescue => exception
      render json: { errors: [UserGroup.not_found_message] }, status: :not_found
    end
  end

  def user_group_params
    params[:user_group].permit(:name, :branch_id, :description, :user_type_id, :code)
  end

  def serialize_data data
    data.as_json(
      except: [:created_at, :updated_at],
      include: [
              branch: { except: [:created_at, :updated_at] },
              user_type: { except: [:created_at, :updated_at] }
                ]
    )
  end

  def serialize_user_data data
    data.as_json(
        except: [:created_at, :updated_at],
        include: [
                 user_type: { only: %i(name) },
                 gender: { only: %i(name) }
                 ]
    )
  end

  def serialize_permission_data data
    data.as_json(
        except: [:created_at, :updated_at]
    )
  end

  def serialize_permission_group_data data
    data.as_json(
        except: [:created_at, :updated_at],
        include: [
            permissions: {}
                ]
    )
  end

end
