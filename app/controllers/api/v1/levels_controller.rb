class Api::V1::LevelsController < Api::V1::ApiBaseController
  before_action :set_level, only: [:show, :update, :destroy, :curriculums, :lock, :unlock]
  def index
    @q = Level.includes(:major).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['created_at desc'] if @q.sorts.empty?
    return_obj = {}

    pagy, level_list = pagy @q.result, items: params[:items], page: current_page
    return_obj[:levels] = serialize_data_with_major level_list
    return_obj[:current_page] = pagy.page
    return_obj[:total_page] = pagy.pages

    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data_with_major(@level), status: :ok
  end

  def create
    @level = Level.new level_params
    return (render json: { errors: [Major.locked_message] }, status: :bad_request) if (@level.major.is_locked == true)
    if @level.save
      render json: serialize_data_with_major(@level), status: :ok
    else
      render json: { errors: @level.errors.full_messages }, status: :bad_request
    end
  end

  def update
    @level.attributes = level_params
    if @level.major_id_changed?
      return (render json: { errors: [Major.locked_message] }, status: :bad_request) if (@level.major.is_locked == true)
    end
    return (render json: serialize_data_with_major(@level), status: :ok) if @level.save
    render json: { errors: @level.errors.full_messages }, status: :bad_request
  end

  def destroy
    return render json: { messages: [Level.destroy_success_message(@level.name)] }, status: :ok if @level.destroy
    render json: { errors: @level.errors.full_messages }, status: :bad_request
  end

  def curriculums
    @q = @level.curriculums.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      level_list = @q.result
      return_obj[:curriculums] = serialize_data level_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, level_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:curriculums] = serialize_data level_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def lock
    @level.is_locked = true
    return (render json: { messsages: [Level.lock_success_message] }, status: :ok) if @level.save
    render json: { errors: @level.errors.full_messages }, status: :bad_request
  end

  def unlock
    @level.is_locked = false
    return (render json: { messsages: [Level.unlock_success_message] }, status: :ok) if @level.save
    render json: { errors: @level.errors.full_messages }, status: :bad_request
  end
  private

  def set_level
    begin
      @level = Level.find_by_id(params[:id])
      return render json: { errors: [Level.not_found_message] }, status: :not_found if @level.blank?
    rescue => exception
      render json: { errors: [Level.not_found_message] }, status: :not_found
    end
  end

  def level_params
    params[:level].permit(:name, :code, :major_id, :description)
  end

  def serialize_data data
    data.as_json(
        except: [:created_at, :updated_at]
    )
  end

  def serialize_data_with_major data
    data.as_json(
        except: %i[created_at updated_at],
        include: [{ major: { except: %i[created_at updated_at] } }]
    )
  end
  
end
