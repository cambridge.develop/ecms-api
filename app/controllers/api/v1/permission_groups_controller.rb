class Api::V1::PermissionGroupsController < Api::V1::ApiBaseController
  before_action :set_permission_group, only: [:show, :destroy, :update, :permissions, :not_in_group_permissions]
  def index
    @q = PermissionGroup.includes(:user_type).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['created_at asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      permission_group_list = @q.result
      return_obj[:permission_groups] = serialize_data permission_group_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, permission_group_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:permission_groups] = serialize_data permission_group_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def show
    render json: serialize_data(@permission_group), status: :ok
  end

  def create
    @permission_group = PermissionGroup.new permission_group_params
    if @permission_group.save
      render json: @permission_group, status: :ok
    else
      render json: { errors: @permission_group.errors.full_messages }, status: :bad_request
    end
  end

  def update
    @permission_group.attributes = permission_group_params.except(:user_type_id)
    return (render json: serialize_data(@permission_group), status: :ok) if @permission_group.save
    render json: {errors: @permission_group.errors.full_messages}, status: :bad_request
  end

  def destroy
    return render json: { messages: [PermissionGroup.destroy_success_message(@permission_group.name)] }, status: :ok if @permission_group.destroy
    render json: {errors: @permission_group.errors.full_messages}, status: :bad_request
  end

  def permissions
    @q = @permission_group.permissions.ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      permission_list = @q.result
      return_obj[:permissions] = serialize_permission_data permission_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, permission_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:permissions] = serialize_permission_data permission_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  def not_in_group_permissions
    p Permission.where.not(id: @permission_group.permissions.pluck(:id))
    @q = Permission.where.not(id: @permission_group.permissions.pluck(:id)).where("? = ANY(user_types)", @permission_group.user_type_id).ransack(params[:q])
    current_page = params[:page] || 1
    @q.sorts = ['name asc'] if @q.sorts.empty?
    return_obj = {}
    if params.has_key?(:all)
      permission_list = @q.result
      return_obj[:permissions] = serialize_permission_data permission_list
      return_obj[:current_page] = 0
      return_obj[:total_page] = 0
    else
      pagy, permission_list = pagy @q.result, items: params[:items], page: current_page
      return_obj[:permissions] = serialize_permission_data permission_list
      return_obj[:current_page] = pagy.page
      return_obj[:total_page] = pagy.pages
    end
    render json: return_obj, status: :ok
  end

  private
  def set_permission_group
    begin
      @permission_group = PermissionGroup.find_by_id(params[:id])
      return render json: { errors: [PermissionGroup.not_found_message] }, status: :not_found if @permission_group.blank?
    rescue => exception
      return render json: { errors: [PermissionGroup.not_found_message] }, status: :not_found
    end
  end

  def permission_group_params
    params[:permission_group].permit(:name, :description, :user_type_id)
  end

  def serialize_data data
    data.as_json(
      except: [:created_at, :updated_at],
      include: [
              user_type: { except: [:created_at, :updated_at] }
                ]
    )
  end
  def serialize_permission_data data
    data.as_json(
        except: [:created_at, :updated_at]
    )
  end

end
