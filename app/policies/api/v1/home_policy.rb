class Api::V1::HomePolicy < ApplicationPolicy

  def global_search?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name)
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
