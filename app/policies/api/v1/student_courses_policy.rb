class Api::V1::StudentCoursesPolicy < ApplicationPolicy

  def initialize(user, record)
    super(user, record)
    @PERMISSION_NAMESPACE = "Classes::Courses::StudentCourses"
  end

  def transfer?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name)
  end
  
  def cancel_trial?
    allowed?('destroy_free_trial')
  end
  
  def convert_trial_to_official?
    allowed?('transfer_free_trial')
  end
  

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
