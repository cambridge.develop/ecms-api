class Api::V1::UserGroupsPolicy < ApplicationPolicy
  
  def show?
    permissions? || users?
  end

  def permissions?
    allowed_by_permission?('UserGroups::Permissions#index')
  end

  def not_in_group_permissions?
    allowed_by_permission?('UserGroups::Permissions#create')
  end

  # concern
  def not_in_group_permission_groups?
    true
  end

  def not_in_group_users?
    allowed_by_permission?('UserGroups::Users#create')
  end

  def users?
    allowed_by_permission?('UserGroups::Users#index')
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
