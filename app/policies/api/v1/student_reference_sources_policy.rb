class Api::V1::StudentReferenceSourcesPolicy < ApplicationPolicy
  
  def initialize(user, record)
    super(user, record)
    @PERMISSION_NAMESPACE = "Students::ReferenceSources"
  end
  
  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
