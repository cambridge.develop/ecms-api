class Api::V1::CurriculumsPolicy < ApplicationPolicy

  def lock?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name)
    true
  end

  def unlock?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name)
    true
  end

  # concern
  def classes?s
    true
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
