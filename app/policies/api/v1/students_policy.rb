class Api::V1::StudentsPolicy < ApplicationPolicy

  def index?
    allowed_by_permission?('Students#index') || allowed_by_permission?('Classes::Courses::StudentCourses#create')
  end

  # concern
  def statistics?
    true
  end

  def student_courses?
    allowed_by_permission?('Students::Courses#index')
  end

  def class_grades?
    allowed_by_permission?('Students::Classes#index')
  end

  def student_certificates?
    allowed_by_permission?('Students::Certificates#index')
  end

  def student_reference_sources?
    show?
  end

  def student_objectives?
    show?
  end

  # concern
  def student_potentials?
    true
  end

  def student_logs?
    allowed_by_permission?('Students::Logs#index')
  end

  # concern
  def parents?
    true
  end

  def student_parents?
    allowed_by_permission?('Students::Parents#index')
  end

  def update_test_result?
    allowed_by_permission?('Students::TestResults#update')
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
