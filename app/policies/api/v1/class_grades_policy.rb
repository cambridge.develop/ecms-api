class Api::V1::ClassGradesPolicy < ApplicationPolicy
  
  def initialize(user, record)
    super(user, record)
    @PERMISSION_NAMESPACE = "Classes"
  end

  
  def close?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name)
  end

  def cancel?
    true
  end

  def courses?
    show?
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
