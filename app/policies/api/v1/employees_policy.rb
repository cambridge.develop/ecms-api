class Api::V1::EmployeesPolicy < ApplicationPolicy


  #concern
  def courses?
    true
  end

  #concern
  def teacher_courses?
    true
  end

  #concern
  def classes?
    true
  end

  #concern
  def statistics?
    true
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
