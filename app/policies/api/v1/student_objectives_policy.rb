class Api::V1::StudentObjectivesPolicy < ApplicationPolicy
  
  def initialize(user, record)
    super(user, record)
    @PERMISSION_NAMESPACE = "Students::Objectives"
  end
  
  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
