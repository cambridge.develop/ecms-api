class Api::V1::PermissionAndPermissionGroupsPolicy < ApplicationPolicy
  
  
  def initialize(user, record)
    super(user, record)
    @PERMISSION_NAMESPACE = "PermissionGroups::Permissions"
  end

  def delete_permissions?
    allowed?('destroy')
  end


  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
