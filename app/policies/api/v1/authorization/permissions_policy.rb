class Api::V1::Authorization::PermissionsPolicy < ApplicationPolicy
  def index?
    true
  end
  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
