class Api::V1::TodosPolicy < ApplicationPolicy
  # concern
  def courses?
    true
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
