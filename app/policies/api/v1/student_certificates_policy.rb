class Api::V1::StudentCertificatesPolicy < ApplicationPolicy
  

  def initialize(user, record)
    super(user, record)
    @PERMISSION_NAMESPACE = "Students::Certificates"
  end


  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
