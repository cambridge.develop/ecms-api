class Api::V1::Payment::StudentCoursesPolicy < ApplicationPolicy
  def index?
    allowed_by_permission?('Payment::Courses#index')
  end

  # concern
  def receipts?
    true
  end
  
  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
