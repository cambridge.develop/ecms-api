class Api::V1::Payment::ReceiptsPolicy < ApplicationPolicy
  
  def index?
    allowed_by_permission?('Payment::Courses::Receipts#index')
  end

  def create_with_course?
    allowed_by_permission?('Payment::Courses::Receipts#create')
  end

  def create_with_class?
    allowed_by_permission?('Payment::Classes::Receipts#create')
  end

  def cancel?
    allowed_by_permission?('Payment::Courses::Receipts#destroy')
  end

  def cancel_with_class?
    allowed_by_permission?('Payment::Classes::Receipts#destroy')
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
