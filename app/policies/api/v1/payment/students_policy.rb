class Api::V1::Payment::StudentsPolicy < ApplicationPolicy
  def index?
    allowed_by_permission?('Payment::Courses#index') || allowed_by_permission?('Payment::Classes#index') || deposit?
  end

  def credit?
    allowed_by_permission?('Payment::Courses::Receipts#create') ||
      allowed_by_permission?('Payment::Classes::Receipts#create') ||
      allowed_by_permission?('Payment::Courses::Receipts#destroy') ||
      allowed_by_permission?('Payment::Classes::Receipts#destroy')
  end

  def deposit?    
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name)
  end

  # concern
  def courses?
    true
  end

  def classes?
    allowed_by_permission?('Payment::Classes#index')
  end

  def paid_classes?
    allowed_by_permission?('Payment::Classes::Receipts#index')
  end

  def deposit_receipts?
    deposit?
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
