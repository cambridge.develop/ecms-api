class Api::V1::TeachersPolicy < ApplicationPolicy

  # concern
  def lock?
    true
  end

  # concern
  def courses?
    true
  end

  def teacher_courses?
    show?
  end

  def classes?
    show?
  end

  # concern
  def statistics?
    true
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
