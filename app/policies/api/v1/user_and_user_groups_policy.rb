class Api::V1::UserAndUserGroupsPolicy < ApplicationPolicy
  
  def initialize(user, record)
    super(user, record)
    @PERMISSION_NAMESPACE = "UserGroups::Users"
  end

  def delete_users?
    allowed?('destroy')
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
