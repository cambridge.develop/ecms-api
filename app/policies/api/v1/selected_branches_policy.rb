class Api::V1::SelectedBranchesPolicy < ApplicationPolicy
  def index?
    true
  end
  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
