class Api::V1::BranchesPolicy < ApplicationPolicy
  # concern
  def unadded_users?
    add_user_to_branch?
  end

  def add_user_to_branch?
    allowed_by_permission?('Branches::Employees#create')
  end

  def users?
    show?
  end

  def remove_user_in_branch?
    allowed_by_permission?('Branches::Employees#destroy')
  end

  def lock?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name)
  end

  def unlock?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name)
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
