class Api::V1::PaymentV2::StudentsPolicy < ApplicationPolicy
  def class_grade?
    true
  end

  def receipts?
    true
  end
end