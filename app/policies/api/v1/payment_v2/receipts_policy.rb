class Api::V1::PaymentV2::ReceiptsPolicy < ApplicationPolicy
  def create
    true
  end

  def cancel_by_receipt_no?
    true
  end
end