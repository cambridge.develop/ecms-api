class Api::V1::PaymentV2::StudentCoursesPolicy < ApplicationPolicy
  def get_student_course?
    true
  end
end