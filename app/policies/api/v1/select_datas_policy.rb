class Api::V1::SelectDatasPolicy < ApplicationPolicy
  def majors?
    true
  end

  def curriculums?
    true
  end

  def levels?
    true
  end

  def classes?
    true
  end

  def class_generals?
    true
  end

  def courses?
    true
  end

  def managers?
    true
  end

  def teachers?
    true
  end

  def user_types?
    true
  end

  def student_potential_statuses?
    true
  end

  def students?
    true
  end

  def genders?
    true
  end

  def literacies?
    true
  end

  def holiday_types?
    true
  end

  def branches?
    true
  end

  def employee_statuses?
    true
  end

  def teacher_statuses?
    true
  end

  def course_statuses?
    true
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
