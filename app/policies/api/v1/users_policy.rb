class Api::V1::UsersPolicy < ApplicationPolicy
  
  def initialize(user, record)
    super(user, record)
    @PERMISSION_NAMESPACE = "Accounts"
  end

  # concern -> current user change password
  def update_password?
    true
  end

  def lock?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name)
  end

  def unlock?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name)
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
