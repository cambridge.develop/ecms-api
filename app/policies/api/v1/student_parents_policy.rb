class Api::V1::StudentParentsPolicy < ApplicationPolicy
  
  def initialize(user, record)
    super(user, record)
    @PERMISSION_NAMESPACE = "Students::Parents"
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
