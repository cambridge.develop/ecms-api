class Api::V1::GiftsPolicy < ApplicationPolicy

  def lock?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name)
  end

  def unlock?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name)
  end


  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
