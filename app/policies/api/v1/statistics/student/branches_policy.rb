class Api::V1::Statistics::Student::BranchesPolicy < ApplicationPolicy
  
  def number_of_new_students_in_month_range?
    allowed_by_permission?('Statistic::Student::Branches#number_of_new_students_in_month_range')
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
