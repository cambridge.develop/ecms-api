class Api::V1::CoursesPolicy < ApplicationPolicy
  def initialize(user, record)
    super(user, record)
    @PERMISSION_NAMESPACE = "Classes::Courses"
  end

  def close?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name)
  end

  def cancel?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name)
  end

  def copy_student_course?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name)
  end

  # concern
  def students?
    true
  end

  def student_courses?
    show?
  end

  def student_courses_free_trial?
    show?
  end

  def student_courses_history?
    show?
  end


  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
