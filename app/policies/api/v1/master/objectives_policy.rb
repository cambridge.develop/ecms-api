class Api::V1::Master::ObjectivesPolicy < ApplicationPolicy
  
  
  def initialize(user, record)
    super(user, record)
    @PERMISSION_NAMESPACE = "Objectives"
  end

  # concern
  def students?
    true
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
