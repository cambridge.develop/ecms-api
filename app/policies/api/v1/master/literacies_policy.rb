class Api::V1::Master::LiteraciesPolicy < ApplicationPolicy
  
  def initialize(user, record)
    super(user, record)
    @PERMISSION_NAMESPACE = "Literacies"
  end
  # concern
  def students?
    true
  end
  # concern
  def schools?
    true
  end


  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
