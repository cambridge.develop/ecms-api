class Api::V1::Master::CertificatesPolicy < ApplicationPolicy

  def initialize(user, record)
    super(user, record)
    @PERMISSION_NAMESPACE = "Certificates"
  end

  # concern
  def students?
    true
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
