class Api::V1::Master::HolidaysPolicy < ApplicationPolicy

  def holiday_admin?
    action_name =  "holiday_admin"
    allowed?(action_name)
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
