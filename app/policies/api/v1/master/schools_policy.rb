class Api::V1::Master::SchoolsPolicy < ApplicationPolicy

  def initialize(user, record)
    super(user, record)
    @PERMISSION_NAMESPACE = "Schools"
  end

  # concern
  def students?
    true
  end
  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
