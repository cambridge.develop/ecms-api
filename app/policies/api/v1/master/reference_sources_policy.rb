class Api::V1::Master::ReferenceSourcesPolicy < ApplicationPolicy
  
  def initialize(user, record)
    super(user, record)
    @PERMISSION_NAMESPACE = "ReferenceSources"
  end

  # concern
  def students?
    true
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
