class Api::V1::StudentPotentialsPolicy < ApplicationPolicy
  
  def initialize(user, record)
    super(user, record)
    @PERMISSION_NAMESPACE = "Students::Potentials"
  end

  def index?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name) ||  allowed_by_permission?("Potentials##{action_name}")
  end

  def create?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name) || allowed_by_permission?("Potentials##{action_name}")
  end

  def update?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name) || allowed_by_permission?("Potentials##{action_name}")
  end

  def destroy?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name) || allowed_by_permission?("Potentials##{action_name}")
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
