class Api::V1::ParentsPolicy < ApplicationPolicy

  def index?
    allowed_by_permission?('Students::Parents#create') || allowed_by_permission?("Parents#index")
  end

  def delete?
    allowed?('destroy')
  end

  # concern
  def students?
    true
  end

  def student_parents?
    show?
  end

  def update_relation?
    allowed_by_permission?('Parents:Students#update')
  end


  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
