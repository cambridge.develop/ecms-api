class Api::V1::PermissionGroupsPolicy < ApplicationPolicy
  
  def show?
    allowed_by_permission?('PermissionGroups::Permissions#index')
  end

  def permissions?
    show?
  end
  
  def not_in_group_permissions?
    allowed_by_permission?('PermissionGroups::Permissions#create')
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
