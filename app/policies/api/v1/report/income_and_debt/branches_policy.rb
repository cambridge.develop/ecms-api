class Api::V1::Report::IncomeAndDebt::BranchesPolicy < ApplicationPolicy
  def class_grades?
    allowed_by_permission?('Report::IncomeAndDebt::Branches#class_grades')
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end