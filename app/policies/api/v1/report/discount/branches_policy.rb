class Api::V1::Report::Discount::BranchesPolicy < ApplicationPolicy
  def students?
    allowed_by_permission?('Report::Discount::Branches#students')
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end