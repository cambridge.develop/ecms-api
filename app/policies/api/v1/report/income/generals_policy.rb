class Api::V1::Report::Income::GeneralsPolicy < ApplicationPolicy
  def class_grades?
    allowed_by_permission?('Report::Income::Generals#class_grades')
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end