class Api::V1::Report::Income::DetailsPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end

    def class_grades?
      allowed_by_permission?('Report::Income::Details#class_grades')
    end
  end
end