class Api::V1::Report::Income::BranchesPolicy < ApplicationPolicy
  def class_grades?
    allowed_by_permission?('Report::Income::Branches#class_grades')
    p allowed_by_permission?('Report::Income::Branches#class_grades')
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end