class Api::V1::Report::Debt::GeneralsPolicy < ApplicationPolicy
  def class_grades?
    p allowed_by_permission?('Report::Debt::Generals#class_grades')
    allowed_by_permission?('Report::Debt::Generals#class_grades')
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
