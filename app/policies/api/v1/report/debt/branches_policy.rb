class Api::V1::Report::Debt::BranchesPolicy < ApplicationPolicy
  def class_grades?
    p allowed_by_permission?('Report::Debt::Branches#class_grades')
    allowed_by_permission?('Report::Debt::Branches#class_grades')
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
