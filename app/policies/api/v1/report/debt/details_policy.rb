class Api::V1::Report::Debt::DetailsPolicy < ApplicationPolicy
  def class_grades?
    p allowed_by_permission?('Report::Debt::Details#class_grades')
    allowed_by_permission?('Report::Debt::Details#class_grades')
  end

  def search_student?
    allowed_by_permission?('Report::Debt::Details#search_student')
  end

  def students?
    allowed_by_permission?('Report::Debt::Details#students')
  end



  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
