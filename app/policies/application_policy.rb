class ApplicationPolicy
  attr_reader :user, :record
  
  
  def initialize(user, record)
    @PERMISSION_NAMESPACE = ""
    @user = user
    @record = record
    if @user.selected_branch
      @permission_codes = Permission.joins(permission_and_user_groups: {user_group: :user_and_user_groups})
                                    .where(user_groups: {branch_id: @user.selected_branch.id}, 
                                            user_and_user_groups: {user_id: @user.id})
                                    .pluck(:code).uniq
    else
      @permission_codes = []
    end
  end
  
  def index?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name)
  end

  def show?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name)
  end

  def create?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name)
  end

  def update?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name)
  end

  def destroy?
    action_name =  __method__.to_s.remove("?")
    allowed?(action_name)
  end

  # TODO sua function phu hop voi permission code
  def allowed?(action_name)
    class_name = @PERMISSION_NAMESPACE.empty? ? self.class.name.remove("Policy").remove("Api::V1::") : @PERMISSION_NAMESPACE 
    requested_permission = "#{class_name}##{action_name}"
    @user.user_type.is_admin? || @permission_codes.include?(requested_permission)
  end

  def allowed_by_permission?(permission)
    @user.user_type.is_admin? || @permission_codes.include?(permission)
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope.all
    end
  end
end
