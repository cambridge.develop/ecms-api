class Payment::ReceiptService < Payment::PaymentServiceBase
  def self.process_create(receipt_params, creator_id)
    list_student_course_pay_info = []
    receipt_params[:student_course_pay_info].each do |e|
      std = StudentCoursePayInfo.new(e[:id], e[:day_late], e[:discount_percentage])
      list_student_course_pay_info << std if std.is_valid
    end
    is_bank = receipt_params[:is_bank]
    is_bank = false if is_bank.blank?
    receipt_base_info = ReceiptBaseInfo.new(
        receipt_params[:paid_person_name], receipt_params[:description_receipt], receipt_params[:description_internal], creator_id, receipt_params[:gift_id], is_bank, gen_receipt_no
    )
    receipts, student_course_updated_list = gen_receipts(receipt_params[:total_actual_paid_amount], receipt_params[:credit_used], list_student_course_pay_info, receipt_base_info)
    return receipts if commit(receipts, student_course_updated_list)
    return []
  end

  def self.process_cancel(receipt_params)
    logger = Logger.new(STDOUT)
    receipts = ReceiptV2.where(receipt_no: receipt_params[:receipt_no]).includes(:student_course_v2, :creator, :student)
    MainRecordBase.transaction do
      if !receipts.update(is_cancel: true)
        raise ActiveRecord::Rollback
        return 0
      end
      receipts.each do |r|
        ok = StudentCourseV2.where(id: r.student_course_id).update_counters('discount_amount' => -r.discount_number, 'credit' => -r.paid_amount)
        if !ok
          raise ActiveRecord::Rollback
          return 0
        end
        ok = Student.where(id: r.student.id).update_counters('credit' => r.actual_paid_amount + r.credit_used)
        if !ok
          raise ActiveRecord::Rollback
          return 0
        end
      end
    rescue ActiveRecord::RecordInvalid => invalid
      raise ActiveRecord::Rollback
      return 0
    end
    return 1
  end

  private

  def self.gen_receipt_no
    return Time.now.to_i
  end

  def self.gen_receipts(total_actual_paid_amount, credit_used, list_student_course_pay_info, receipt_base_info)
    receipts = []
    student_course_updated_list = []
    list_student_course_pay_info.each do |e|
      need_paid = e.need_paid
      if need_paid <= 0
        next
      end
      actual_paid = 0
      if credit_used.to_i < need_paid
        actual_paid = credit_used.to_i + total_actual_paid_amount.to_i
        r_credit_used = credit_used.to_i
        if actual_paid > need_paid
          total_actual_paid_amount = actual_paid - need_paid
          actual_paid = need_paid - r_credit_used
        else
          actual_paid = total_actual_paid_amount.to_i
          total_actual_paid_amount = 0
        end
        credit_used = 0
      else
        r_credit_used = need_paid
        credit_used -= need_paid
      end

      receipt = ReceiptV2.new(
          student_course_id: e.id,
          paid_person_name: receipt_base_info.paid_person_name,
          actual_paid_amount: actual_paid,
          description_receipt: receipt_base_info.description_receipt,
          description_internal: receipt_base_info.description_internal,
          creator_id: receipt_base_info.creator_id,
          discount_percentage: e.discount_percentage,
          discount_number: e.discount_number,
          paid_amount: actual_paid + r_credit_used + e.discount_number,
          credit_used: r_credit_used,
          is_bank: receipt_base_info.is_bank,
          receipt_no: receipt_base_info.receipt_no
      )
      receipt.gift_id = receipt_base_info.gift_id if receipts.length == 0

      student_course_updated = {}
      student_course_updated[:id] = e.id
      student_course_updated[:credit] = actual_paid + r_credit_used + e.discount_number
      student_course_updated[:discount_amount] = e.discount_number
      student_course_updated_list << student_course_updated
      receipts << receipt
      if total_actual_paid_amount == 0
        return receipts, student_course_updated_list
      end
    end
    return receipts, student_course_updated_list
  end

  def self.commit (receipts, student_course_updated_list)
    logger = Logger.new(STDOUT)
    MainRecordBase.transaction do
      receipts.each do |r|
        unless r.save
          logger.error r.errors.full_messages.to_s
          raise ActiveRecord::Rollback
          return false
        end
        if r.credit_used > 0
          ok = Student.where(id: r.student_course_v2.student.id).update_counters('credit' => -r.credit_used)
          if !ok
            raise ActiveRecord::Rollback
            return false
          end
        end
      end
      student_course_updated_list.each do |s|
        ok = StudentCourseV2.where(id: s[:id]).update_counters('credit' => s[:credit], 'discount_amount' => s[:discount_amount])
        if !ok
          raise ActiveRecord::Rollback
          return false
        end
      end
    rescue ActiveRecord::RecordInvalid => invalid
      raise ActiveRecord::Rollback
      return false
    end
    return true
  end
end

class StudentCoursePayInfo
  attr_accessor :id, :day_late, :discount_percentage, :tuition, :number_of_days, :credit

  def initialize(id, day_late, discount_percentage)
    @id = id
    @day_late = day_late
    @discount_percentage = discount_percentage
  end

  def is_valid
    logger = Logger.new(STDOUT)
    student_course = StudentCourseV2.find_by_id(self.id)

    if student_course.blank?
      logger.error "not found student course with id:  " + id unless id.blank?
      logger.error "id is null" if id.blank?
      return false
    end

    self.number_of_days = student_course.course_v2.number_of_days
    self.tuition = student_course.course_v2.tuition
    self.credit = student_course.credit
    self.day_late = student_course.days_late

    if student_course.course_v2.number_of_days < self.day_late
      logger.error "day late must equals or less than: " + student_course.days_late.to_s
      return false
    end
    return true
  end

  def need_paid
    return (self.tuition - self.credit) -
        ((self.tuition / self.number_of_days) * self.day_late) -
        ((self.tuition - self.credit) * (discount_percentage.to_i) / 100)
  end

  def discount_number
    return ((self.tuition / self.number_of_days) * self.day_late) +
        ((self.tuition - self.credit) * (discount_percentage.to_i) / 100)
  end
end

class ReceiptBaseInfo
  attr_accessor :paid_person_name, :description_receipt, :description_internal, :creator_id, :gift_id, :is_bank, :receipt_no

  def initialize(paid_person_name, description_receipt, description_internal, creator_id, gift_id, is_bank, receipt_no)
    @paid_person_name = paid_person_name
    @description_receipt = description_receipt
    @description_internal = description_internal
    @creator_id = creator_id
    @gift_id = gift_id
    @is_bank = is_bank
    @receipt_no = receipt_no
  end

  def is_valid
    logger = Logger.new(STDOUT)
    gift = Gift.find_by_id(self.gift_id)

    if gift.blank?
      logger.error "not found gift with id:  " + gift_id unless gift_id.blank?
      logger.error "gift_id is null" if gift_id.blank?
      return false
    end

    if paid_person_name.length == 0
      logger.error "invalid paid person name"
      return false
    end

    return true
  end
end