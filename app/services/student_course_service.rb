class StudentCourseService < BaseService
  def self.GetStudentCourse(student_id, class_id)
    course_ids = CourseV2.where(class_grade_id: class_id).pluck(:id)
    return StudentCourseV2.where(student_id: student_id, course_id: course_ids, is_cancel: false)
  end
end