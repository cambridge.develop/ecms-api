

require File.expand_path('../seed_data/master_data', __FILE__)
require File.expand_path('../seed_data/phuong_xa_data', __FILE__)
require File.expand_path('../seed_data/permissions_data', __FILE__)

include MasterData
include PhuongXaData
include PermissionData

MasterData.generate_dan_toc
MasterData.generate_tinh
MasterData.generate_quan_huyen
MasterData.generate_user_type
PhuongXaData.generate_phuong_xa
MasterData.generate_gioi_tinh
MasterData.generate_holiday_type
PermissionData.generate

employee_user_type = UserType.employee
teacher_user_type = UserType.teacher
student_user_type = UserType.student
genders = Gender.all.load

EMPLOYEE_NUMBER = 50
TEACHER_NUMBER = 20
STUDENT_NUMBER = 30

def generate_master_data
  # generate majors
  majors = []
  majors << Major.create(name: "Anh Văn", code: "AV", description: "Chuyên ngành tiếng Anh")
  majors << Major.create(name: "Tiếng Pháp", code: "TP", description: "Chuyên ngành tiếng Pháp")
  majors << Major.create(name: "Tiếng Nhật", code: "TN", description: "Chuyên ngành tiếng Nhật")

  #create teacher_status
  teacher_statuses = []
  teacher_statuses << TeacherStatus.create(code: 'active', name: 'Active', description: 'Đang công tác')
  teacher_statuses << TeacherStatus.create(code: 'resign', name: 'Resign', description: 'Đã nghỉ')

  #Employee status
  employee_statuses = []
  employee_statuses << EmployeeStatus.create(code: 'active', name: 'Active', description: 'Đang công tác')
  employee_statuses << EmployeeStatus.create(code: 'resign', name: 'Resign', description: 'Đã nghỉ')

  certificates = []
  certificates << Certificate.new(name: 'Toiec', code: 'toiec', description: 'Đây là chứng chỉ Toiec')
  certificates << Certificate.new(name: 'Ielts', code: 'ielts', description: 'Đây là chứng chỉ Ielts')
  certificates << Certificate.new(name: 'ABC', code: 'abc', description: 'Đây là chứng chỉ abc')
  Certificate.import certificates

  objectives = []
  objectives << Objective.new(name: 'Du học', code: 'du_hoc', description: 'Đi du học')
  objectives << Objective.new(name: 'Cải thiện kết quả học tập', code: 'hoc_tap', description: 'Cải thiện kết quả học tập')
  objectives << Objective.new(name: 'Đi làm', code: 'Đi làm', description: 'Đi làm')
  Objective.import objectives

  reference_sources = []
  reference_sources << ReferenceSource.new(name: 'Internet', code: 'net', description: 'Nguồn Internet')
  reference_sources << ReferenceSource.new(name: 'Báo chí', code: 'news', description: 'Nguồn Báo chí')
  reference_sources << ReferenceSource.new(name: 'Quảng cáo', code: 'adss', description: 'Nguồn Quảng cáo')
  ReferenceSource.import reference_sources


  literacies = []
  literacies << Literacy.new(name: 'Tiểu học', code: 'TH', description: 'Trình độ tiểu học')
  literacies << Literacy.new(name: 'Trung học cơ sở', code: 'THCS', description: 'Trình độ trung học')
  literacies << Literacy.new(name: 'Trung học phổ thông', code: 'THPT', description: 'Trình độ trung học phổ thông')
  Literacy.import literacies

  course_statuses = []
  course_statuses << CourseStatus.new(name: "Opening", code: "open", description: "Trạng thái khóa học đang được mở")
  course_statuses << CourseStatus.new(name: "Closed", code: "end", description: "Trạng thái khóa học đã kết thúc")
  course_statuses << CourseStatus.new(name: "Canceled", code: "cancel", description: "Trạng thái khóa học đã bị hủy")
  CourseStatus.import course_statuses

  gifts = []
  gifts << Gift.create(name: "Balo Cambridge", code: "balo", description: "Balo Cambridge")
  gifts << Gift.create(name: "Áo thun", code: "aothun", description: "Áo thun")
  gifts << Gift.create(name: "Sách tô màu", code: "sach01", description: "Sách tô màu")

  return majors, teacher_statuses, employee_statuses, certificates, objectives, reference_sources, literacies, course_statuses, gifts
end

def generate_levels_curriculums
  #create demo level and curriculum datas
  chuyen_nganh_AV = Major.find_by_code("AV")
  levels = []
  levels << Level.create(name: "FCE", code: "FCE", major_id: chuyen_nganh_AV.id, description: "Cấp độ FCE ")
  levels << Level.create(name: "Flyer", code: "FLY", major_id: chuyen_nganh_AV.id, description: "Cấp độ Flyer ")
  levels << Level.create(name: "Starter", code: "STA", major_id: chuyen_nganh_AV.id, description: "Cấp độ Starter ")
  levels << Level.create(name: "Mover", code: "MOV", major_id: chuyen_nganh_AV.id, description: "Cấp độ Mover ")
  levels << Level.create(name: "PET", code: "PET", major_id: chuyen_nganh_AV.id, description: "Cấp độ PET ")
  levels << Level.create(name: "KET", code: "KET", major_id: chuyen_nganh_AV.id, description: "Cấp độ KET ")

  curriculums = []
  levels.each_with_index do |level, index|
    curriculums << Curriculum.new(name: "Giáo trình #{level.name} #{index}",
                                  code: "#{level.code}#{index}",
                                  level_id: level.id,
                                  number_of_courses: (1..4).to_a.sample,
                                  days_per_course: [4, 8, 12, 16].sample,
                                  fee_per_course: [400000, 600000, 800000, 1000000, 1200000].sample,
                                  description: "Giáo trình #{level.name} #{index}")

  end
  Curriculum.import curriculums
  return levels, curriculums
end



def generate_employees branches, employee_statuses, genders, employee_user_type

  puts "Creating employee..."
  #create demo employee with branch
  user_employees = []
  (1..EMPLOYEE_NUMBER).each do |i|
    user_employees << User.new(username: "employee_#{i}", email: "employee_#{i}@123.com", password: "qweasd", full_name: "#{Faker::Name.name}", gender: genders.sample, user_type: employee_user_type)
  end
  User.import user_employees
  puts "#{user_employees.count{|e| e.id != nil}} user employees have generated "


  employees = []
  user_employees.each_with_index do |user, index|
    code = "NV-#{index.to_s.rjust(6, '0')}"
    employees << Employee.new(code: code, user_id: user.id, employee_status_id: employee_statuses.sample.id, birthday: '11/11/2011')
  end
  Employee.import employees
  puts "#{employees.count{|e| e.id != nil}} employees have generated "


  branch_managers = {}
  puts "Assigning employee to branches"
  user_branches = []
  user_employees.each do |user|
    branch = branches.sample
    user_branches << UserBranch.new(user: user, branch: branch)
    branch_managers[branch.id] = user.id
  end
  UserBranch.import user_branches
  puts "#{user_branches.count{|u| u.id != nil}} user_branches for employee"

  # assign user to branch as a manager
  branch_managers.each do |branch_id, user_id|
    Branch.find(branch_id).update(manager_id: user_id)
  end

end

def generate_students genders, student_user_type

  puts "Creating student"
  # Student
  users = []
  (1..STUDENT_NUMBER).each do |i|
    users << User.new(username: "HV_#{i}", email: "HV_#{i}@123.com", password: "qweasd", full_name: "#{Faker::Name.name}", gender: genders.sample, user_type: student_user_type)
  end
  User.import users

  students = []
  users.each_with_index do |user, index|
    students << Student.new(code: "HV-#{(index+1).to_s.rjust(6, '0')}", user_id: user.id, birthday: Faker::Date.birthday, phone: Faker::PhoneNumber.cell_phone, status: Student.statuses.to_a.sample[0].to_sym, created_at: Faker::Date.between(from: 24.month.ago, to: Date.today),class_name: Faker::Name.name)
  end
  Student.import students
  puts "#{students.count{|u| u.id != nil}} students have generated"
  return users, students
end

def generate_classes_and_courses branches = [], curriculums = []

  puts "Creating classes..."
  class_grades = []
  branches.each do |branch|
    curriculums.each do |curriculum|
      class_grade_no = 1
      (1..5).each do |i|
        start_date = Faker::Date.between(from: (i - 1).months.ago - 3.months, to: (i - 1).months.ago - 2.months)
        end_date = Faker::Date.between(from: start_date + 10.months, to: start_date + 9.months)
        class_grades << ClassGrade.new(code: "#{branch.name.underscore }_#{curriculum.code}_#{class_grade_no}",
                                       name: " #{curriculum.code} #{class_grade_no}",
                                       # room: "#{i}",
                                       # course_no: class_grade_no,
                                       number_of_students: Random.rand(40),
                                       number_of_courses: Random.rand(6),
                                       start_date: start_date,
                                       end_date: end_date,
                                       opening_date: start_date,
                                       is_end: (end_date <= Time.now),
                                       curriculum_id: curriculums.sample.id,
                                       # teacher_id: teachers.sample.id,
                                       # second_teacher_id: ([true, false].sample ? teachers.sample.id : nil),
                                       branch_id: branch.id
        # tuition: [300000, 600000, 800000, 1200000].sample,
        # number_of_days: [8,16].sample
        )
        class_grade_no += 1
      end
    end
  end
  ClassGrade.import class_grades
  puts "#{class_grades.count{|u| u.id != nil}} classes have been generated"


  puts "Creating Course..."
  courses = []

  class_grades.each do |class_grade|
    course_no = 1
    rand_no_of_courses = rand(1..5)
    start_date = class_grade.start_date
    end_date = start_date + rand(1..2).months
    # p course_duration
    (1..rand_no_of_courses).each do |i|
      courses << Course.new(code: "#{class_grade.code}_#{course_no}",
                            name: "#{class_grade.name} #{course_no}",
                            class_grade: class_grade,
                            room: "#{i}",
                            course_no: course_no,
                            number_of_students: Random.rand(10..40),
                            start_date: start_date,
                            end_date: end_date,
                            number_of_days: [8,16].sample,
                            tuition: [300000, 600000, 800000, 1200000].sample,
                            )
      course_no += 1
      start_date = end_date
      end_date = start_date + rand(1..2).months
    end
  end

  Course.import courses, validate: false
  puts "#{courses.count{|u| u.id != nil}} courses have been generated"

  return class_grades, courses
end

def generate_teachers genders, teacher_user_type, teacher_statuses, branches

  puts "Creating teachers..."
  #create demo teacher with branch
  users = []
  (1..TEACHER_NUMBER).each do |i|
    users << User.new(username: "teacher_#{i}", email: "teacher_#{i}@123.com", password: "qweasd", full_name: "#{Faker::Name.name}", gender: genders.sample, user_type: teacher_user_type)
  end
  User.import users

  teachers = []
  users.each do |user|
    teachers << Teacher.new(user_id: user.id, teacher_status: teacher_statuses.sample)
  end
  Teacher.import teachers
  puts "#{teachers.count{|e| e.id != nil}} teachers have generated "


  user_branches = []
  teachers.each do |teacher|
    branches.each do |branch|
      if [true, true, false, false].sample
        next
      end

      user_branches << UserBranch.new(user: teacher.user, branch: branch)
    end
  end
  UserBranch.import user_branches
  puts "#{user_branches.count{|u| u.id != nil}} user_branches for teacher"

  return users, teachers
end


def generate_assign_teacher_and_student_to_course courses , students, branches
  puts "Assign teachers to courses"
  teacher_courses = []

  branches.each do |branch|
    teachers = Teacher.joins(:user => :user_branches).where("user_branches.branch_id = ?",  branch.id)
    class_grades = branch.class_grades.all
    class_grades.each do |class_grade|
      courses = class_grade.courses
      courses.each do |course|
        rand_number_of_teachers = rand(1..3)
        (1..rand_number_of_teachers).each do
          teacher_courses << TeacherCourse.new(course: course, teacher: teachers.sample)
        end
      end
    end
  end

  TeacherCourse.import teacher_courses, validate: false
  puts "#{teacher_courses.count{|u| u.id != nil}} teacher_courses"

  puts "Student joins course"
  student_courses = []
  students.each do |student|
    rand_number_of_courses = rand(1..4)
    (1..rand_number_of_courses).each do
      course = courses.sample
      student_courses << StudentCourse.new(student: student, course: course, start_date: Faker::Date.between(from: course.class_grade.start_date, to: course.class_grade.end_date))
    end
  end
  StudentCourse.import student_courses
  puts "#{student_courses.count{|u| u.id != nil}} student_course"

  return teacher_courses, student_courses
end

def generate_permissions_policy
  puts "Generate permissions"
  PermissionData.generate_assign_group
end

def generate_student_certificate certificates, students
  student_certificates = []
  students.each do |student|
    cert = certificates.sample
    student_certificates << StudentCertificate.new(student_id: student.id, certificate_id: cert.id, issue_organization: 'ABC', issue_date: DateTime.now, expiration_date: DateTime.now, license_number: rand(100000), point: rand(100))
  end
  StudentCertificate.import student_certificates
  student_certificates
end


def generate_student_objectives objectives, students
  student_objectives = []
  students.each do |student|
    objective = objectives.sample
    student_objectives << StudentObjective.new(student_id: student.id, objective_id: objective.id)
  end
  StudentObjective.import student_objectives
  student_objectives
end


def generate_student_references reference_sources, students
  student_reference_sources = []
  students.each do |student|
    reference_source = reference_sources.sample
    student_reference_sources << StudentReferenceSource.new(student_id: student.id, reference_source_id: reference_source.id)
  end
  StudentReferenceSource.import student_reference_sources
  student_reference_sources
end


def generate_schools literacies
  schools = []
  literacies.each do |literacy|
    index = 0
    10.times do
      schools << School.new(name: "#{literacy.name} #{Faker::Name.name}", code: "#{literacy.code}_#{index}", literacy_id: literacy.id)
      index+=1
    end

  end
  School.import schools
  schools
end

def generate_policies
  policies = []
  policies << Policy.new(name: "Chính sách 1", code: "cs001", percent_discount: 50, description: "Chính sách 1 giảm giá 50%")
  policies << Policy.new(name: "Chính sách 2", code: "cs002", amount_discount: 50000, description: "Chính sách 2 giảm giá 50,000")
  policies << Policy.new(name: "Chính sách 3", code: "cs003", percent_discount: 10, description: "Chính sách 3 giảm giá 10%")

  Policy.import policies
  policies
end

#create demo account
admin_type = UserType.find_by_code('admin')

admin = User.create(email: 'test1@gmail.com', full_name: Faker::Name.name, username: 'test1', password: '123123A@', confirmed_at: DateTime.now, gender: Gender.first, user_type: UserType.admin)

#create branch
require File.expand_path('../seed_data/branch', __FILE__)
include BranchData
branches = BranchData.generate
puts "#{branches.count{|e| e.id != nil} } branches generated"

majors, teacher_statuses, employee_statuses, certificates, objectives, reference_sources, literacies, course_statuses = generate_master_data()

levels, curriculums = generate_levels_curriculums

student_users, students = generate_students genders, student_user_type

class_grades, courses = generate_classes_and_courses branches, curriculums

users, teachers = generate_teachers genders, teacher_user_type, teacher_statuses, branches

teacher_courses, student_courses = generate_assign_teacher_and_student_to_course courses, students, branches


generate_employees branches, employee_statuses, genders, employee_user_type


generate_permissions_policy

student_certificates = generate_student_certificate certificates, students

student_objectives = generate_student_objectives objectives, students

student_references = generate_student_references reference_sources, students

schools = generate_schools literacies

policies = generate_policies

p "Student: #{Student.all.count}"
p "Teacher: #{Teacher.all.count}"
p "Class: #{ClassGrade.all.count}"
p "Course: #{Course.all.count}"
p "TeacherCourse: #{TeacherCourse.all.count}"
p "StudentCourse: #{StudentCourse.all.count}"

# classes[]
# branches = Branch.all
# branches.each do |i, branch|
#   classes << ClassGrade.new(name: "class #{branch}" + i,
#                             code: "#{branch.name.underscore}"
#   )
# end



student_potentials = []
Student.limit(200).each do |student|
  curriculum = curriculums.sample
  branch = branches.sample
  student_potentials << StudentPotential.new(student_id: student.id,branch_id: branch.id, curriculum_id: curriculum.id, name: "#{branch.name}_#{curriculum.name}",status: StudentPotential.statuses.to_a.sample[0].to_sym)
end
StudentPotential.import student_potentials





Course.update(course_status_id: CourseStatus.where(code: 'open').first.id)