module PermissionData
    def generate
        admin_user_type =  UserType.admin
        employee_user_type =  UserType.employee
        teacher_user_type =  UserType.teacher

        # Major
        Permission.create(name: "[Quản trị] Danh sách ngành", code: "Majors#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Tạo ngành", code: "Majors#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Cập nhật ngành", code: "Majors#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Xóa ngành", code: "Majors#destroy" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Khóa ngành", code: "Majors#lock" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Mở khóa ngành", code: "Majors#unlock" , user_types: [admin_user_type.id, employee_user_type.id])

        # Level
        Permission.create(name: "[Quản trị] Danh sách cấp độ", code: "Levels#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Tạo cấp độ", code: "Levels#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Cập nhật cấp độ", code: "Levels#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Xóa cấp độ", code: "Levels#destroy" , user_types: [admin_user_type.id, employee_user_type.id])

        # Curriculum
        Permission.create(name: "[Quản trị] Danh sách giáo trình", code: "Curriculums#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Tạo giáo trình", code: "Curriculums#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Cập nhật giáo trình", code: "Curriculums#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Xóa giáo trình", code: "Curriculums#destroy" , user_types: [admin_user_type.id, employee_user_type.id])

        # Master Holidays
        Permission.create(name: "[Quản trị] Danh sách ngày nghỉ", code: "Master::Holidays#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Tạo ngày nghỉ", code: "Master::Holidays#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Cập nhật ngày nghỉ", code: "Master::Holidays#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Xóa ngày nghỉ", code: "Master::Holidays#destroy" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Quản trị ngày nghỉ trung tâm", code: "Master::Holidays#holiday_admin" , user_types: [admin_user_type.id, employee_user_type.id])

        # # Branch
        Permission.create(name: "[Quản trị] Danh sách chi nhánh", code: "Branches#index" , user_types: [admin_user_type.id])
        Permission.create(name: "[Quản trị] Tạo chi nhánh", code: "Branches#create" , user_types: [admin_user_type.id])
        Permission.create(name: "[Quản trị] Cập nhật chi nhánh", code: "Branches#update" , user_types: [admin_user_type.id])
        Permission.create(name: "[Quản trị] Thông tin chi tiết chi nhánh", code: "Branches#show" , user_types: [admin_user_type.id])
        Permission.create(name: "[Quản trị] Xóa chi nhánh", code: "Branches#destroy" , user_types: [admin_user_type.id])

        Permission.create(name: "[Quản trị] Khóa chi nhánh", code: "Branches#lock" , user_types: [admin_user_type.id])
        Permission.create(name: "[Quản trị] Mở khóa chi nhánh", code: "Branches#unlock" , user_types: [admin_user_type.id])

        Permission.create(name: "[Quản trị] Thêm nhân viên vào chi nhánh", code: "Branches::Employees#create" , user_types: [admin_user_type.id])
        Permission.create(name: "[Quản trị] Xóa nhân viên khỏi chi nhánh", code: "Branches::Employees#destroy" , user_types: [admin_user_type.id])

        # Accounts

        Permission.create(name: "[Quản trị] Danh sách tài khoản", code: "Accounts#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Khóa tài khoản", code: "Accounts#lock" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Mở khóa tài khoản", code: "Accounts#unlock" , user_types: [admin_user_type.id, employee_user_type.id])

        # Objective
        Permission.create(name: "[Quản trị] Danh sách mục tiêu", code: "Objectives#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Tạo mục tiêu", code: "Objectives#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Cập nhật mục tiêu", code: "Objectives#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Xóa mục tiêu", code: "Objectives#destroy" , user_types: [admin_user_type.id, employee_user_type.id])

        # Literacies
        Permission.create(name: "[Quản trị] Danh sách trình độ học vấn", code: "Literacies#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Tạo trình độ học vấn", code: "Literacies#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Cập nhật trình độ học vấn", code: "Literacies#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Xóa trình độ học vấn", code: "Literacies#destroy" , user_types: [admin_user_type.id, employee_user_type.id])

        # Schools
        Permission.create(name: "[Quản trị] Danh sách trường học", code: "Schools#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Tạo trường học", code: "Schools#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Cập nhật trường học", code: "Schools#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Xóa trường học", code: "Schools#destroy" , user_types: [admin_user_type.id, employee_user_type.id])

        # Certificates
        Permission.create(name: "[Quản trị] Danh sách chứng chỉ", code: "Certificates#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Tạo chứng chỉ", code: "Certificates#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Cập nhật chứng chỉ", code: "Certificates#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Xóa chứng chỉ", code: "Certificates#destroy" , user_types: [admin_user_type.id, employee_user_type.id])

        # ReferenceSources
        Permission.create(name: "[Quản trị] Danh sách nguồn tiếp cận", code: "ReferenceSources#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Tạo nguồn tiếp cận", code: "ReferenceSources#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Cập nhật nguồn tiếp cận", code: "ReferenceSources#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Xóa nguồn tiếp cận", code: "ReferenceSources#destroy" , user_types: [admin_user_type.id, employee_user_type.id])

        # PermissionGroups
        Permission.create(name: "[Quản trị] Danh sách nhóm quyền", code: "PermissionGroups#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Tạo nhóm quyền", code: "PermissionGroups#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Cập nhật nhóm quyền", code: "PermissionGroups#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Xóa nhóm quyền", code: "PermissionGroups#destroy" , user_types: [admin_user_type.id, employee_user_type.id])
        
        Permission.create(name: "[Quản trị] Danh sách quyền trong nhóm", code: "PermissionGroups::Permissions#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Thêm quyền vào nhóm", code: "PermissionGroups::Permissions#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Xóa quyền khỏi nhóm", code: "PermissionGroups::Permissions#destroy" , user_types: [admin_user_type.id, employee_user_type.id])

        # UserGroups
        Permission.create(name: "[Quản trị] Danh sách nhóm tài khoản", code: "UserGroups#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Tạo nhóm tài khoản", code: "UserGroups#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Cập nhật nhóm tài khoản", code: "UserGroups#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Xóa nhóm tài khoản", code: "UserGroups#destroy" , user_types: [admin_user_type.id, employee_user_type.id])
        #  UserGroups::Users
        Permission.create(name: "[Quản trị] Danh sách tài khoản trong nhóm tài khoản", code: "UserGroups::Users#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Thêm tài khoản vào nhóm tài khoản", code: "UserGroups::Users#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Xóa tài khoản khỏi nhóm tài khoản", code: "UserGroups::Users#destroy" , user_types: [admin_user_type.id, employee_user_type.id])
        #  UserGroups::Permissions
        Permission.create(name: "[Quản trị] Danh sách quyền trong nhóm tài khoản", code: "UserGroups::Permissions#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Thêm quyền vào nhóm tài khoản", code: "UserGroups::Permissions#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Xóa quyền khỏi nhóm tài khoản", code: "UserGroups::Permissions#destroy" , user_types: [admin_user_type.id, employee_user_type.id])

        # ----------------------
        # Students
        Permission.create(name: '[Học viên] Danh sách học viên', code: "Students#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Học viên] Tạo học viên', code: "Students#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Học viên] Cập nhật học viên', code: "Students#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Học viên] Thông tin học viên', code: "Students#show" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Học viên] Xóa học viên', code: "Students#destroy" , user_types: [admin_user_type.id, employee_user_type.id])

        Permission.create(name: '[Học viên] Thêm mục tiêu', code: "Students::Objectives#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Học viên] Xóa mục tiêu', code: "Students::Objectives#destroy" , user_types: [admin_user_type.id, employee_user_type.id])

        Permission.create(name: '[Học viên] Thêm nguồn tiếp cận', code: "Students::ReferenceSources#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Học viên] Xóa nguồn tiếp cận', code: "Students::ReferenceSources#destroy" , user_types: [admin_user_type.id, employee_user_type.id])

        Permission.create(name: '[Học viên] Danh sách nhu cầu tiềm năng', code: "Students::Potentials#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Học viên] Thêm nhu cầu tiềm năng', code: "Students::Potentials#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Học viên] Cập nhật nhu cầu tiềm năng', code: "Students::Potentials#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Học viên] Xóa nhu cầu tiềm năng', code: "Students::Potentials#destroy" , user_types: [admin_user_type.id, employee_user_type.id])

        Permission.create(name: '[Học viên] Danh sách chứng chỉ', code: "Students::Certificates#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Học viên] Thêm chứng chỉ', code: "Students::Certificates#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Học viên] Cập nhật chứng chỉ', code: "Students::Certificates#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Học viên] Xóa chứng chỉ', code: "Students::Certificates#destroy" , user_types: [admin_user_type.id, employee_user_type.id])

        Permission.create(name: '[Học viên] Danh sách khóa học', code: "Students::Courses#index" , user_types: [admin_user_type.id, employee_user_type.id])

        Permission.create(name: '[Học viên] Danh sách lớp học', code: "Students::Classes#index" , user_types: [admin_user_type.id, employee_user_type.id])

        Permission.create(name: '[Học viên] Danh sách phụ huynh', code: "Students::Parents#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Học viên] Tạo phụ huynh', code: "Students::Parents#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Học viên] Cập nhật phụ huynh', code: "Students::Parents#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Học viên] Xóa phụ huynh', code: "Students::Parents#destroy" , user_types: [admin_user_type.id, employee_user_type.id])

        Permission.create(name: '[Học viên] Lịch sử chỉnh sửa học viên', code: "Students::Logs#index" , user_types: [admin_user_type.id, employee_user_type.id])

        Permission.create(name: '[Học viên] Cập nhật kết quả thi đầu vào', code: "Students::TestResults#update" , user_types: [admin_user_type.id, employee_user_type.id])

        # Employees
        Permission.create(name: '[Nhân viên] Danh sách nhân viên', code: "Employees#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Nhân viên] Tạo nhân viên', code: "Employees#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Nhân viên] Cập nhật thông tin nhân viên', code: "Employees#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Nhân viên] Khóa nhân viên', code: "Employees#lock" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Nhân viên] Mở khóa nhân viên', code: "Employees#unlock" , user_types: [admin_user_type.id, employee_user_type.id])

        # Teachers
        Permission.create(name: '[Giáo viên] Danh sách giáo viên', code: "Teachers#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Giáo viên] Thông tin giáo viên', code: "Teachers#show" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Giáo viên] Tạo giáo viên', code: "Teachers#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Giáo viên] Cập nhật thông tin giáo viên', code: "Teachers#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Giáo viên] Khóa giáo viên', code: "Teachers#lock" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Giáo viên] Mở khóa giáo viên', code: "Teachers#unlock" , user_types: [admin_user_type.id, employee_user_type.id])

        # Parents
        Permission.create(name: '[Phụ huynh] Danh sách phụ huynh', code: "Parents#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Phụ huynh] Thông tin phụ huynh', code: "Parents#show" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Phụ huynh] Tạo phụ huynh', code: "Parents#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Phụ huynh] Cập nhật thông tin phụ huynh', code: "Parents#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Phụ huynh] Xóa phụ huynh', code: "Parents#destroy" , user_types: [admin_user_type.id, employee_user_type.id])

        Permission.create(name: '[Phụ huynh] Xóa học viên khỏi phụ huynh', code: "Parents:Students#destroy" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Phụ huynh] Cập nhật mối quan hệ học viên và phụ huynh', code: "Parents:Students#update" , user_types: [admin_user_type.id, employee_user_type.id])

        # Classes
        Permission.create(name: '[Lớp học] Danh sách lớp', code: "Classes#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Lớp học] Thông tin lớp', code: "Classes#show" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Lớp học] Tạo lớp', code: "Classes#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Lớp học] Cập nhật thông tin lớp', code: "Classes#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Lớp học] Xóa lớp', code: "Classes#destroy" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Lớp học] Đóng lớp', code: "Classes#close" , user_types: [admin_user_type.id, employee_user_type.id])

            Permission.create(name: '[Lớp học] Thêm khóa học vào lớp', code: "Classes::Courses#create" , user_types: [admin_user_type.id, employee_user_type.id])
            Permission.create(name: '[Lớp học] Cập nhật khóa học', code: "Classes::Courses#update" , user_types: [admin_user_type.id, employee_user_type.id])
            Permission.create(name: '[Lớp học] Đóng khóa học', code: "Classes::Courses#close" , user_types: [admin_user_type.id, employee_user_type.id])
            Permission.create(name: '[Lớp học] Hủy khóa học', code: "Classes::Courses#cancel" , user_types: [admin_user_type.id, employee_user_type.id])
            Permission.create(name: '[Lớp học] Xóa khóa học', code: "Classes::Courses#destroy" , user_types: [admin_user_type.id, employee_user_type.id])
            Permission.create(name: '[Lớp học] Thông tin khóa học', code: "Classes::Courses#show" , user_types: [admin_user_type.id, employee_user_type.id])

                Permission.create(name: '[Lớp học] Thêm học viên vào khóa học', code: "Classes::Courses::StudentCourses#create" , user_types: [admin_user_type.id, employee_user_type.id])
                Permission.create(name: '[Lớp học] Chuyển khóa học cho học viên', code: "Classes::Courses::StudentCourses#transfer" , user_types: [admin_user_type.id, employee_user_type.id])
                Permission.create(name: '[Lớp học] Xóa học viên khỏi khóa học', code: "Classes::Courses::StudentCourses#destroy" , user_types: [admin_user_type.id, employee_user_type.id])
                Permission.create(name: '[Lớp học] Chuyển học thử sang chính thức', code: "Classes::Courses::StudentCourses#transfer_free_trial" , user_types: [admin_user_type.id, employee_user_type.id])
                Permission.create(name: '[Lớp học] Xóa học viên khỏi khóa học thử', code: "Classes::Courses::StudentCourses#destroy_free_trial" , user_types: [admin_user_type.id, employee_user_type.id])


        # Potentials
        Permission.create(name: '[Tiềm năng] Danh sách tiềm năng', code: "Potentials#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Tiềm năng] Tạo tiềm năng', code: "Potentials#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Tiềm năng] Cập nhật thông tin tiềm năng', code: "Potentials#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Tiềm năng] Xóa tiềm năng', code: "Potentials#destroy" , user_types: [admin_user_type.id, employee_user_type.id])

        # Payment 
        Permission.create(name: '[Thanh toán] Vào trang thanh toán', code: "Payment#index" , user_types: [admin_user_type.id, employee_user_type.id])

        Permission.create(name: '[Thanh toán] Hiển thị danh sách khóa học', code: "Payment::Courses#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Thanh toán] Danh sách hóa đơn của khóa', code: "Payment::Courses::Receipts#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Thanh toán] Tạo thanh toán cho khóa', code: "Payment::Courses::Receipts#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Thanh toán] Hủy hóa đơn', code: "Payment::Courses::Receipts#destroy" , user_types: [admin_user_type.id, employee_user_type.id])

        Permission.create(name: '[Thanh toán] Hiển thị danh sách lớp học', code: "Payment::Classes#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Thanh toán] Danh sách hóa đơn của lớp học', code: "Payment::Classes::Receipts#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Thanh toán] Tạo thanh toán cho lớp', code: "Payment::Classes::Receipts#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: '[Thanh toán] Hủy hóa đơn của lớp', code: "Payment::Classes::Receipts#destroy" , user_types: [admin_user_type.id, employee_user_type.id])

        # Nạp tiền
        Permission.create(name: '[Nạp tiền] Nạp tiền cho học viên', code: "Payment::Students#deposit" , user_types: [admin_user_type.id, employee_user_type.id])

        Permission.create(name: '[Trang chủ] Thống kê học sinh đăng ký mới của chi nhánh theo tháng', code: "Statistic::Student::Branches#number_of_new_students_in_month_range" , user_types: [admin_user_type.id, employee_user_type.id])


        # # report
        # # incomes
        Permission.create(name: "[Báo cáo] Báo cáo tổng doanh thu chi nhánh", code: "Report::Income::Branches#class_grades", user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Báo cáo] Báo cáo tổng doanh thu trung tâm", code: "Report::Income::Generals#class_grades", user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Báo cáo] Báo cáo chi tiết doanh thu", code: "Report::Income::Details#class_grades", user_types: [admin_user_type.id, employee_user_type.id])
        # # discount
        Permission.create(name: "[Báo cáo] Báo cáo miễn giảm học phí", code: "Report::Discount::Branches#students", user_types: [admin_user_type.id, employee_user_type.id])
        # # debt
        Permission.create(name: "[Báo cáo] Báo cáo nợ phí chi nhánh", code: "Report::Debt::Branches#class_grades", user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Báo cáo] Báo cáo nợ phí trung tâm", code: "Report::Debt::Generals#class_grades", user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Báo cáo] Báo cáo chi tiết nợ phí", code: "Report::Debt::Details#class_grades", user_types: [admin_user_type.id, employee_user_type.id])
        # # student
        Permission.create(name: "[Báo cáo] Báo cáo học viên chi nhánh", code: "Report::Student::Branches#class_grades", user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Báo cáo] Báo cáo học viên trung tâm", code: "Report::Student::Generals#class_grades", user_types: [admin_user_type.id, employee_user_type.id])
        # # trial
        Permission.create(name: "[Báo cáo] Báo cáo học thử chi nhánh", code: "Report::Trial::Branches#class_grades", user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Báo cáo] Báo cáo học thử trung tâm", code: "Report::Trial::Generals#class_grades", user_types: [admin_user_type.id, employee_user_type.id])
        # Permission.create(name: "[Báo cáo] Báo cáo chi tiết", code: "Report::Student::Details#class_grades", user_types: [admin_user_type.id, employee_user_type.id])
        # # income and debt
        Permission.create(name: "[Báo cáo] Báo cáo tổng hợp doanh thu và nợ phí", code: "Report::IncomeAndDebt::Branches#students", user_types: [admin_user_type.id, employee_user_type.id])
        # HOME
        Permission.create(name: "[Common] Global search", code: "Home#global_search", user_types: [admin_user_type.id, employee_user_type.id])

        # Course
        Permission.create(name: "[Common] Global search", code: "Course#copy_student_course", user_types: [admin_user_type.id, employee_user_type.id])

        # Todos
        Permission.create(name: "[Việc cần làm] Đóng khóa học đã kết thúc", code: "Todos#courses", user_types: [admin_user_type.id, employee_user_type.id])

        # Gift
        Permission.create(name: "[Quản trị] Danh sách quà tặng", code: "Gifts#index" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Tạo quà tặng", code: "Gifts#create" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Cập nhật quà tặng", code: "Gifts#update" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Xóa quà tặng", code: "Gifts#destroy" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Khóa quà tặng", code: "Gifts#lock" , user_types: [admin_user_type.id, employee_user_type.id])
        Permission.create(name: "[Quản trị] Mở khóa quà tặng", code: "Gifts#unlock" , user_types: [admin_user_type.id, employee_user_type.id])
    end

    def generate_assign_group
        # create UserGroup
        admin_user_group = UserGroup.new( name: "Nhóm quyền admin", code: 'admin', description: "Chi nhánh admin", user_type: UserType.admin)
        admin_user_group.save(validate: false)
        # Assign user to UserGroup
        admin_users = User.where(user_type: UserType.admin)
        insert_users_into_user_group( admin_users, admin_user_group)
        # Assign permission to UserGroup
        permissions = Permission.all
        insert_permissions_into_user_group( permissions, admin_user_group)

        branches = Branch.all
        employee_permissions = Permission.where("? = ANY(user_types)", UserType.employee.id)
        
        teacher_permissions = Permission.where("? = ANY(user_types)", UserType.teacher.id)
        
        employee_user_type = UserType.employee
        teacher_user_type = UserType.teacher
        branches.each do |branch|
            # employee
            user_group = UserGroup.create(branch_id: branch.id, name: "Nhóm quyền nhân viên chi nhánh #{branch.name}", code: 'employee', description: "Chi nhánh #{branch.name}", user_type: employee_user_type)
            users = User.joins(:user_branches).where(user_branches: {branch_id: branch.id}).where(user_type: employee_user_type)
            insert_users_into_user_group( users, user_group)
            insert_permissions_into_user_group( employee_permissions, user_group)

            # teacher
            user_group = UserGroup.create(branch_id: branch.id, name: "Nhóm quyền giáo viên chi nhánh #{branch.name}", code: 'teacher', description: "Chi nhánh #{branch.name}", user_type: teacher_user_type)
            users = User.joins(:user_branches).where(user_branches: {branch_id: branch.id}).where(user_type: teacher_user_type)
            insert_users_into_user_group( users, user_group)
            insert_permissions_into_user_group( teacher_permissions, user_group)
        end
    end

    private
    def insert_permissions_into_user_group permissions, user_group
        permission_and_user_groups = []
        permissions.each do |permission|
            permission_and_user_groups << PermissionAndUserGroup.new(permission_id: permission.id, user_group_id: user_group.id)
        end
        PermissionAndUserGroup.import permission_and_user_groups
    end

    def insert_users_into_user_group users, user_group
        user_and_user_groups = []
        users.each do |user|
            user_and_user_groups << UserAndUserGroup.new(user_id: user.id, user_group_id: user_group.id)
        end
        UserAndUserGroup.import user_and_user_groups
    end

end