require 'csv'
data =  CSV.read("DANH_SACH_CAP_LOP_CHUYEN_NGANH_AV.csv")

module CurriculumData
  def generate levels
    curriculums = []
    data.each do |row|
      branch_name = row[1]
      level_name  = row[2]
      curriculum_name =  row[3]
      number_of_courses = row[4]
      fee_per_course = row[5]
      days_per_course = row[6]

      level_name = level_name.downcase.concat
      level = levels.detect{|level| level.name.downcase.concat == level_name}

      fee_per_course = fee_per_course:.to_f
      number_of_courses = number_of_courses.to_i
      days_per_course = days_per_course.to_i

      curriculums << Curriculum.new name: curriculum_name, 
                                    number_of_courses: number_of_courses, 
                                    days_per_course: days_per_course, 
                                    fee_per_course: fee_per_course, 
                                    level_id: level.id
    end
    Curriculum.import curriculums
    curriculums
  end
end
