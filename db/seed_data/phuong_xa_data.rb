require 'csv'
module PhuongXaData
  def generate_phuong_xa
    danh_sach_quan_huyen = District.pluck(:code, :id).to_h
    danh_sach_phuong_xa = []
    CSV.foreach(Rails.root + 'db/seed_data/phuong_xa.csv', headers: true) do |row|
      cap = 0
      if row['ten'].include?('Phường')
        cap = 2
      elsif row['ten'].include?('Xã')
        cap = 1
      end
      danh_sach_phuong_xa << Ward.new(code: row['ma'],
                                      name: row['ten'],
                                      level: cap,
                                      district_code: danh_sach_quan_huyen[row['quan_huyen']],
                                      description: 'Generate from system')
    end
    Ward.import danh_sach_phuong_xa
  end
end