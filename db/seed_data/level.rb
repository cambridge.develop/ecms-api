require 'csv'
data =  CSV.read("level.csv")

module LevelData
  def generate majors
    levels = []
    data.each do |row|
      branch_name = row[0]
      level_name = row[1]
      major_name = row[2]

      major_name = major_name.downcase.concat
      major = majors.detect{|major| major.name.downcase.concat == major_name}

      levels << Level.new name: level_name, major_id: major.id 
      
    end
    Level.import levels
    levels
  end
end
