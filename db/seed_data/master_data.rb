module MasterData
  def generate_dan_toc
    dan_tocs = []
    dan_tocs << Folk.new(code:'1', name: 'Kinh', other_name: 'Việt')
    dan_tocs << Folk.new(code:'2', name: 'Tày', other_name: 'Thổ, Ngạn, Phén, Thù lao, Pa Dí')
    dan_tocs << Folk.new(code:'3', name: 'Thái', other_name: 'Tày, Tày khao (Thái trắng), Tày Dăm (Thái Đen), Tày Mười Tây Thanh, Màn Thanh (Hang Ông (Tày Mường), Pi Thay, Thổ Đà Bắc')
    dan_tocs << Folk.new(code:'4', name: 'Hoa', other_name: 'Hán, Triều Châu, Phúc Kiến, Quảng Đông, Hải Nam, Hạ, Xạ Phạng')
    dan_tocs << Folk.new(code:'5', name: 'Khơ-me', other_name: 'Cur, Cul, Cu, Thổ, Việt gốc Miên, Khơ-me, Krôm')
    dan_tocs << Folk.new(code:'6', name: 'Mường', other_name: 'Mol, Mual, Mọi, Mọi Bi, Ao Tá, (Ậu Tá)')
    dan_tocs << Folk.new(code:'7', name: 'Nùng', other_name: 'Xuồng, Giang, Nùng An, Phàn Sinh, Nùng Cháo, Nùng Lòi, Quý Rim, Khèn Lài, ...')
    dan_tocs << Folk.new(code:'8', name: 'Hmông', other_name: 'Mèo, Mẹo, Hoa, Mèo Xanh, Mèo Đỏ, Mèo Đen, Ná Mẻo, Mán Trắng')
    dan_tocs << Folk.new(code:'9', name: 'Dao', other_name: 'Mán, Động, Trại, Xá, Dìu Miền, Kiềm, Miền, Quần Trắng, Dao Đỏ, Quần Chẹt, Lô Gang, Dao Tiền, Thanh Y, Lan Tẻn, Đại Bản,Tiểu Bản, Cóc Ngáng, Cóc Mùn, Sơn Đầu, ...')
    dan_tocs << Folk.new(code:'10', name: 'Gia-rai', other_name: 'Giơ-rai, Tơ-buăn, Chơ-rai, Hơ-bau, Hđrung, Chor, ...')
    dan_tocs << Folk.new(code:'11', name: 'Ngái', other_name: 'Xín, Lê, Đản, Khách Gia')
    dan_tocs << Folk.new(code:'12', name: 'Ê-đê', other_name: 'Ra-đê, Đê, Kpạ, A-đham, Krung, Ktul, Đliê Ruê, Blô, E-pan, Mđhur, Bih, ...')
    dan_tocs << Folk.new(code:'13', name: 'Ba-na', other_name: 'Gơ-lar, Tơ-lô, Giơ-lâng (Y-Lăng), Rơ-ngao, Krem, Roh, ConKđe, A-la Công, Kpăng Công, Bơ-nâm')
    dan_tocs << Folk.new(code:'14', name: 'Xơ-đăng', other_name: 'Xơ-teng, Hđang, Tơ-đra, Mơ-nâm, Ha-lăng, Ca-dông, Kmrâng, Con Lan, Bri-la, Tang')
    dan_tocs << Folk.new(code:'15', name: 'Sán Chay', other_name: 'Cao Lan-Sán chỉ, Cao Lan, Mán Cao Lan, Hờn Bạn, Sán Chỉ (Sơn Tử)')
    dan_tocs << Folk.new(code:'16', name: 'Cơ-ho', other_name: 'Xrê, Nốp (Tu Lốp), Cơ-don, Chil, Lat (Lach), Trinh')
    dan_tocs << Folk.new(code:'17', name: 'Chăm', other_name: 'Chàm, Chiêm Thành, Hroi')
    dan_tocs << Folk.new(code:'18', name: 'Sán Dìu', other_name: 'Sán Dẻo, Trại, Trại Đất, Mán Quần Cộc')
    dan_tocs << Folk.new(code:'19', name: 'Hrê', other_name: 'Chăm Rê, Chom, Krẹ Lũy')
    dan_tocs << Folk.new(code:'20', name: 'Mnông', other_name: 'Pnông, Nông, Pré, Bu-đâng, ĐiPri, Biat, Gar, Rơ-lam, Chil')
    dan_tocs << Folk.new(code:'21', name: 'Ra-glai', other_name: 'Ra-clây, Rai, Noang, La-oang')
    dan_tocs << Folk.new(code:'22', name: 'Xtiêng', other_name: 'Xa-điêng')
    dan_tocs << Folk.new(code:'23', name: 'Bru-Vân Kiều', other_name: 'Bru, Vân Kiều, Măng Coong, Tri Khùa')
    dan_tocs << Folk.new(code:'24', name: 'Thổ', other_name: 'Kẹo, Mọn, Cuối, Họ, Đan Lai, Ly Hà, Tày Pọng, Con Kha, Xá Lá Vàng')
    dan_tocs << Folk.new(code:'25', name: 'Giáy', other_name: 'Nhắng, Dẩng, Pầu Thìn Pu Nà, Cùi Chu , Xa')
    dan_tocs << Folk.new(code:'26', name: 'Cơ-tu', other_name: 'Ca-tu, Cao, Hạ, Phương, Ca-tang')
    dan_tocs << Folk.new(code:'27', name: 'Gié-Triêng', other_name: 'Đgiéh, Tareh, Giang Rẫy Pin, Triêng, Treng, Ta-riêng, Ve (Veh), La-ve, Ca-tang')
    dan_tocs << Folk.new(code:'28', name: 'Mạ', other_name: 'Châu Mạ, Mạ Ngăn, Mạ Xốp, Mạ Tô, Mạ Krung, ...')
    dan_tocs << Folk.new(code:'29', name: 'Khơ-mú', other_name: 'Xá Cẩu, Mứn Xen, Pu Thênh Tềnh, Tày Hay')
    dan_tocs << Folk.new(code:'30', name: 'Co', other_name: 'Cor, Col, Cùa, Trầu')
    dan_tocs << Folk.new(code:'31', name: 'Ta-ôi', other_name: 'Tôi-ôi, Pa-co, Pa-hi (Ba-hi)')
    dan_tocs << Folk.new(code:'32', name: 'Chơ-ro', other_name: 'Dơ-ro, Châu-ro')
    dan_tocs << Folk.new(code:'33', name: 'Kháng', other_name: 'Xá Khao, Xá Súa, Xá Dón, Xá Dẩng, Xá Hốc, Xá Ái, Xá Bung, Quảng Lâm')
    dan_tocs << Folk.new(code:'34', name: 'Xinh-mun', other_name: 'Puộc, Pụa')
    dan_tocs << Folk.new(code:'35', name: 'Hà Nhì', other_name: 'U Ni, Xá U Ni')
    dan_tocs << Folk.new(code:'36', name: 'Chu-ru', other_name: 'Chơ-ru, Chu')
    dan_tocs << Folk.new(code:'37', name: 'Lào', other_name: 'Lào Bốc, Lào Nọi')
    dan_tocs << Folk.new(code:'38', name: 'La Chi', other_name: 'Cù Tê, La Quả')
    dan_tocs << Folk.new(code:'39', name: 'La Ha', other_name: 'Xá Khao, Khlá Phlạo')
    dan_tocs << Folk.new(code:'40', name: 'Phù Lá', other_name: 'Bồ Khô Pạ, Mu Di, Pạ Xá, Phó, Phổ, VaXơ')
    dan_tocs << Folk.new(code:'41', name: 'La Hủ', other_name: 'Lao, Pu Đang,Khù Xung, Cò Xung, Khả Quy')
    dan_tocs << Folk.new(code:'42', name: 'Lự', other_name: 'Lừ, Nhuồn Duôn, Mun Di')
    dan_tocs << Folk.new(code:'43', name: 'Lô Lô', other_name: '')
    dan_tocs << Folk.new(code:'44', name: 'Chứt', other_name: 'Sách, May, Rục, Mã-liêng, A-rem, Tu vang, Pa-leng, Xơ-lang, Tơ-hung, Chà-củi, Tắc-củi, U-mo, Xá Lá Vàng')
    dan_tocs << Folk.new(code:'45', name: 'Mảng', other_name: 'Mảng Ư, Xá Lá Vàng')
    dan_tocs << Folk.new(code:'46', name: 'Pà Thẻn', other_name: 'Pà Hưng, Tống')
    dan_tocs << Folk.new(code:'47', name: 'Cơ Lao', other_name: '')
    dan_tocs << Folk.new(code:'48', name: 'Cống', other_name: 'Xắm Khống, Mấng Nhé, Xá Xeng')
    dan_tocs << Folk.new(code:'49', name: 'Bố Y', other_name: 'Chủng Chá, Trọng Gia, Tu Dí, Tu Dìn')
    dan_tocs << Folk.new(code:'50', name: 'Si La', other_name: 'Cù Dề Xừ, Khả pẻ')
    dan_tocs << Folk.new(code:'51', name: 'Pu Péo', other_name: 'Ka Bèo, Pen Ti Lô Lô')
    dan_tocs << Folk.new(code:'52', name: 'Brâu', other_name: 'Brao')
    dan_tocs << Folk.new(code:'53', name: 'Ơ Đu', other_name: 'Tày Hạt')
    dan_tocs << Folk.new(code:'54', name: 'Rơ-măm', other_name: '')
    dan_tocs << Folk.new(code:'55', name: 'Người nước ngoài', other_name: '')
    Folk.import dan_tocs
  end

  def generate_tinh
    tinhs = []
    tinhs << Province.new(code:'01', name: 'Thủ đô Hà Nội')
    tinhs << Province.new(code:'02', name: 'Hà Giang')
    tinhs << Province.new(code:'04', name: 'Cao Bằng')
    tinhs << Province.new(code:'06', name: 'Bắc Kạn')
    tinhs << Province.new(code:'08', name: 'Tuyên Quang')
    tinhs << Province.new(code:'10', name: 'Lào Cai')
    tinhs << Province.new(code:'11', name: 'Điện Biên')
    tinhs << Province.new(code:'12', name: 'Lai Châu')
    tinhs << Province.new(code:'14', name: 'Sơn La')
    tinhs << Province.new(code:'15', name: 'Yên Bái')
    tinhs << Province.new(code:'17', name: 'Hoà Bình')
    tinhs << Province.new(code:'19', name: 'Thái Nguyên')
    tinhs << Province.new(code:'20', name: 'Lạng Sơn')
    tinhs << Province.new(code:'22', name: 'Quảng Ninh')
    tinhs << Province.new(code:'24', name: 'Bắc Giang')
    tinhs << Province.new(code:'25', name: 'Phú Thọ')
    tinhs << Province.new(code:'26', name: 'Vĩnh Phúc')
    tinhs << Province.new(code:'27', name: 'Bắc Ninh')
    tinhs << Province.new(code:'30', name: 'Hải Dương')
    tinhs << Province.new(code:'31', name: 'TP.Hải Phòng')
    tinhs << Province.new(code:'33', name: 'Hưng Yên')
    tinhs << Province.new(code:'34', name: 'Thái Bình')
    tinhs << Province.new(code:'35', name: 'Hà Nam')
    tinhs << Province.new(code:'36', name: 'Nam Định')
    tinhs << Province.new(code:'37', name: 'Ninh Bình')
    tinhs << Province.new(code:'38', name: 'Thanh Hóa')
    tinhs << Province.new(code:'40', name: 'Nghệ An')
    tinhs << Province.new(code:'42', name: 'Hà Tĩnh')
    tinhs << Province.new(code:'44', name: 'Quảng Bình')
    tinhs << Province.new(code:'45', name: 'Quảng Trị')
    tinhs << Province.new(code:'46', name: 'Thừa Thiên Huế')
    tinhs << Province.new(code:'48', name: 'TP.Đà Nẵng')
    tinhs << Province.new(code:'49', name: 'Quảng Nam')
    tinhs << Province.new(code:'51', name: 'Quảng Ngãi')
    tinhs << Province.new(code:'52', name: 'Bình Định')
    tinhs << Province.new(code:'54', name: 'Phú Yên')
    tinhs << Province.new(code:'56', name: 'Khánh Hòa')
    tinhs << Province.new(code:'58', name: 'Ninh Thuận')
    tinhs << Province.new(code:'60', name: 'Bình Thuận')
    tinhs << Province.new(code:'62', name: 'Kon Tum')
    tinhs << Province.new(code:'64', name: 'Gia Lai')
    tinhs << Province.new(code:'66', name: 'Đắk Lắk')
    tinhs << Province.new(code:'67', name: 'Đắk Nông')
    tinhs << Province.new(code:'68', name: 'Lâm Đồng')
    tinhs << Province.new(code:'70', name: 'Bình Phước')
    tinhs << Province.new(code:'72', name: 'Tây Ninh')
    tinhs << Province.new(code:'74', name: 'Bình Dương')
    tinhs << Province.new(code:'75', name: 'Đồng Nai')
    tinhs << Province.new(code:'77', name: 'Bà Rịa - Vũng Tàu')
    tinhs << Province.new(code:'79', name: 'TP.Hồ Chí Minh')
    tinhs << Province.new(code:'80', name: 'Long An')
    tinhs << Province.new(code:'82', name: 'Tiền Giang')
    tinhs << Province.new(code:'83', name: 'Bến Tre')
    tinhs << Province.new(code:'84', name: 'Trà Vinh')
    tinhs << Province.new(code:'86', name: 'Vĩnh Long')
    tinhs << Province.new(code:'87', name: 'Đồng Tháp')
    tinhs << Province.new(code:'89', name: 'An Giang')
    tinhs << Province.new(code:'91', name: 'Kiên Giang')
    tinhs << Province.new(code:'92', name: 'TP.Cần Thơ')
    tinhs << Province.new(code:'93', name: 'Hậu Giang')
    tinhs << Province.new(code:'94', name: 'Sóc Trăng')
    tinhs << Province.new(code:'95', name: 'Bạc Liêu')
    tinhs << Province.new(code:'96', name: 'Cà Mau')
    tinhs << Province.new(code:'999', name: 'Khác')
    tinhs.each do |tinh|
      cap = 0
      if tinh.name.include?('TP.')
        cap = 1
      elsif tinh.name.include?('Thủ đô')
        cap = 2
      end
      tinh[:level] = cap
    end
    Province.import tinhs
  end

  def generate_quan_huyen
    danh_sach_quan_huyen = []
    danh_sach_quan_huyen << {tinh: '01', code:'001', name: 'Quận Ba Đình'}
    danh_sach_quan_huyen << {tinh: '01', code:'002', name: 'Quận Hoàn Kiếm'}
    danh_sach_quan_huyen << {tinh: '01', code:'003', name: 'Quận Tây Hồ'}
    danh_sach_quan_huyen << {tinh: '01', code:'004', name: 'Quận Long Biên'}
    danh_sach_quan_huyen << {tinh: '01', code:'005', name: 'Quận Cầu Giấy'}
    danh_sach_quan_huyen << {tinh: '01', code:'006', name: 'Quận Đống Đa'}
    danh_sach_quan_huyen << {tinh: '01', code:'007', name: 'Quận Hai Bà Trưng'}
    danh_sach_quan_huyen << {tinh: '01', code:'008', name: 'Quận Hoàng Mai'}
    danh_sach_quan_huyen << {tinh: '01', code:'009', name: 'Quận Thanh Xuân'}
    danh_sach_quan_huyen << {tinh: '01', code:'016', name: 'Huyện Sóc Sơn'}
    danh_sach_quan_huyen << {tinh: '01', code:'017', name: 'Huyện Đông Anh'}
    danh_sach_quan_huyen << {tinh: '01', code:'018', name: 'Huyện Gia Lâm'}
    danh_sach_quan_huyen << {tinh: '01', code:'019', name: 'Quận Nam Từ Liêm'}
    danh_sach_quan_huyen << {tinh: '01', code:'020', name: 'Huyện Thanh Trì'}
    danh_sach_quan_huyen << {tinh: '01', code:'021', name: 'Quận Bắc Từ Liêm'}
    danh_sach_quan_huyen << {tinh: '01', code:'250', name: 'Huyện Mê Linh'}
    danh_sach_quan_huyen << {tinh: '01', code:'268', name: 'Quận Hà Đông'}
    danh_sach_quan_huyen << {tinh: '01', code:'269', name: 'Thị xã Sơn Tây'}
    danh_sach_quan_huyen << {tinh: '01', code:'271', name: 'Huyện Ba Vì'}
    danh_sach_quan_huyen << {tinh: '01', code:'272', name: 'Huyện Phúc Thọ'}
    danh_sach_quan_huyen << {tinh: '01', code:'273', name: 'Huyện Đan Phượng'}
    danh_sach_quan_huyen << {tinh: '01', code:'274', name: 'Huyện Hoài Đức'}
    danh_sach_quan_huyen << {tinh: '01', code:'275', name: 'Huyện Quốc Oai'}
    danh_sach_quan_huyen << {tinh: '01', code:'276', name: 'Huyện Thạch Thất'}
    danh_sach_quan_huyen << {tinh: '01', code:'277', name: 'Huyện Chương Mỹ'}
    danh_sach_quan_huyen << {tinh: '01', code:'278', name: 'Huyện Thanh Oai'}
    danh_sach_quan_huyen << {tinh: '01', code:'279', name: 'Huyện Thường Tín'}
    danh_sach_quan_huyen << {tinh: '01', code:'280', name: 'Huyện Phú Xuyên'}
    danh_sach_quan_huyen << {tinh: '01', code:'281', name: 'Huyện Ứng Hòa'}
    danh_sach_quan_huyen << {tinh: '01', code:'282', name: 'Huyện Mỹ Đức'}
    danh_sach_quan_huyen << {tinh: '02', code:'024', name: 'Thành phố Hà Giang'}
    danh_sach_quan_huyen << {tinh: '02', code:'026', name: 'Huyện Đồng Văn'}
    danh_sach_quan_huyen << {tinh: '02', code:'027', name: 'Huyện Mèo Vạc'}
    danh_sach_quan_huyen << {tinh: '02', code:'028', name: 'Huyện Yên Minh'}
    danh_sach_quan_huyen << {tinh: '02', code:'029', name: 'Huyện Quản Bạ'}
    danh_sach_quan_huyen << {tinh: '02', code:'030', name: 'Huyện Vị Xuyên'}
    danh_sach_quan_huyen << {tinh: '02', code:'031', name: 'Huyện Bắc Mê'}
    danh_sach_quan_huyen << {tinh: '02', code:'032', name: 'Huyện Hoàng Su Phì'}
    danh_sach_quan_huyen << {tinh: '02', code:'033', name: 'Huyện Xín Mần'}
    danh_sach_quan_huyen << {tinh: '02', code:'034', name: 'Huyện Bắc Quang'}
    danh_sach_quan_huyen << {tinh: '02', code:'035', name: 'Huyện Quang Bình'}
    danh_sach_quan_huyen << {tinh: '04', code:'040', name: 'Thành phố Cao Bằng'}
    danh_sach_quan_huyen << {tinh: '04', code:'042', name: 'Huyện Bảo Lâm'}
    danh_sach_quan_huyen << {tinh: '04', code:'043', name: 'Huyện Bảo Lạc'}
    danh_sach_quan_huyen << {tinh: '04', code:'044', name: 'Huyện Thông Nông'}
    danh_sach_quan_huyen << {tinh: '04', code:'045', name: 'Huyện Hà Quảng'}
    danh_sach_quan_huyen << {tinh: '04', code:'046', name: 'Huyện Trà Lĩnh'}
    danh_sach_quan_huyen << {tinh: '04', code:'047', name: 'Huyện Trùng Khánh'}
    danh_sach_quan_huyen << {tinh: '04', code:'048', name: 'Huyện Hạ Lang'}
    danh_sach_quan_huyen << {tinh: '04', code:'049', name: 'Huyện Quảng Uyên'}
    danh_sach_quan_huyen << {tinh: '04', code:'050', name: 'Huyện Phục Hoà'}
    danh_sach_quan_huyen << {tinh: '04', code:'051', name: 'Huyện Hoà An'}
    danh_sach_quan_huyen << {tinh: '04', code:'052', name: 'Huyện Nguyên Bình'}
    danh_sach_quan_huyen << {tinh: '04', code:'053', name: 'Huyện Thạch An'}
    danh_sach_quan_huyen << {tinh: '06', code:'058', name: 'Thành Phố Bắc Kạn'}
    danh_sach_quan_huyen << {tinh: '06', code:'060', name: 'Huyện Pác Nặm'}
    danh_sach_quan_huyen << {tinh: '06', code:'061', name: 'Huyện Ba Bể'}
    danh_sach_quan_huyen << {tinh: '06', code:'062', name: 'Huyện Ngân Sơn'}
    danh_sach_quan_huyen << {tinh: '06', code:'063', name: 'Huyện Bạch Thông'}
    danh_sach_quan_huyen << {tinh: '06', code:'064', name: 'Huyện Chợ Đồn'}
    danh_sach_quan_huyen << {tinh: '06', code:'065', name: 'Huyện Chợ Mới'}
    danh_sach_quan_huyen << {tinh: '06', code:'066', name: 'Huyện Na Rì'}
    danh_sach_quan_huyen << {tinh: '08', code:'070', name: 'Thành phố Tuyên Quang'}
    danh_sach_quan_huyen << {tinh: '08', code:'071', name: 'Huyện Lâm Bình'}
    danh_sach_quan_huyen << {tinh: '08', code:'072', name: 'Huyện Na Hang'}
    danh_sach_quan_huyen << {tinh: '08', code:'073', name: 'Huyện Chiêm Hóa'}
    danh_sach_quan_huyen << {tinh: '08', code:'074', name: 'Huyện Hàm Yên'}
    danh_sach_quan_huyen << {tinh: '08', code:'075', name: 'Huyện Yên Sơn'}
    danh_sach_quan_huyen << {tinh: '08', code:'076', name: 'Huyện Sơn Dương'}
    danh_sach_quan_huyen << {tinh: '10', code:'080', name: 'Thành phố Lào Cai'}
    danh_sach_quan_huyen << {tinh: '10', code:'082', name: 'Huyện Bát Xát'}
    danh_sach_quan_huyen << {tinh: '10', code:'083', name: 'Huyện Mường Khương'}
    danh_sach_quan_huyen << {tinh: '10', code:'084', name: 'Huyện Si Ma Cai'}
    danh_sach_quan_huyen << {tinh: '10', code:'085', name: 'Huyện Bắc Hà'}
    danh_sach_quan_huyen << {tinh: '10', code:'086', name: 'Huyện Bảo Thắng'}
    danh_sach_quan_huyen << {tinh: '10', code:'087', name: 'Huyện Bảo Yên'}
    danh_sach_quan_huyen << {tinh: '10', code:'088', name: 'Huyện Sa Pa'}
    danh_sach_quan_huyen << {tinh: '10', code:'089', name: 'Huyện Văn Bàn'}
    danh_sach_quan_huyen << {tinh: '11', code:'094', name: 'Thành phố Điện Biên Phủ'}
    danh_sach_quan_huyen << {tinh: '11', code:'095', name: 'Thị Xã Mường Lay'}
    danh_sach_quan_huyen << {tinh: '11', code:'096', name: 'Huyện Mường Nhé'}
    danh_sach_quan_huyen << {tinh: '11', code:'097', name: 'Huyện Mường Chà'}
    danh_sach_quan_huyen << {tinh: '11', code:'098', name: 'Huyện Tủa Chùa'}
    danh_sach_quan_huyen << {tinh: '11', code:'099', name: 'Huyện Tuần Giáo'}
    danh_sach_quan_huyen << {tinh: '11', code:'100', name: 'Huyện Điện Biên'}
    danh_sach_quan_huyen << {tinh: '11', code:'101', name: 'Huyện Điện Biên Đông'}
    danh_sach_quan_huyen << {tinh: '11', code:'102', name: 'Huyện Mường Ảng'}
    danh_sach_quan_huyen << {tinh: '11', code:'103', name: 'Huyện Nậm Pồ'}
    danh_sach_quan_huyen << {tinh: '12', code:'105', name: 'Thành phố Lai Châu'}
    danh_sach_quan_huyen << {tinh: '12', code:'106', name: 'Huyện Tam Đường'}
    danh_sach_quan_huyen << {tinh: '12', code:'107', name: 'Huyện Mường Tè'}
    danh_sach_quan_huyen << {tinh: '12', code:'108', name: 'Huyện Sìn Hồ'}
    danh_sach_quan_huyen << {tinh: '12', code:'109', name: 'Huyện Phong Thổ'}
    danh_sach_quan_huyen << {tinh: '12', code:'110', name: 'Huyện Than Uyên'}
    danh_sach_quan_huyen << {tinh: '12', code:'111', name: 'Huyện Tân Uyên'}
    danh_sach_quan_huyen << {tinh: '12', code:'112', name: 'Huyện Nậm Nhùn'}
    danh_sach_quan_huyen << {tinh: '14', code:'116', name: 'Thành phố Sơn La'}
    danh_sach_quan_huyen << {tinh: '14', code:'118', name: 'Huyện Quỳnh Nhai'}
    danh_sach_quan_huyen << {tinh: '14', code:'119', name: 'Huyện Thuận Châu'}
    danh_sach_quan_huyen << {tinh: '14', code:'120', name: 'Huyện Mường La'}
    danh_sach_quan_huyen << {tinh: '14', code:'121', name: 'Huyện Bắc Yên'}
    danh_sach_quan_huyen << {tinh: '14', code:'122', name: 'Huyện Phù Yên'}
    danh_sach_quan_huyen << {tinh: '14', code:'123', name: 'Huyện Mộc Châu'}
    danh_sach_quan_huyen << {tinh: '14', code:'124', name: 'Huyện Yên Châu'}
    danh_sach_quan_huyen << {tinh: '14', code:'125', name: 'Huyện Mai Sơn'}
    danh_sach_quan_huyen << {tinh: '14', code:'126', name: 'Huyện Sông Mã'}
    danh_sach_quan_huyen << {tinh: '14', code:'127', name: 'Huyện Sốp Cộp'}
    danh_sach_quan_huyen << {tinh: '14', code:'128', name: 'Huyện Vân Hồ'}
    danh_sach_quan_huyen << {tinh: '15', code:'132', name: 'Thành phố Yên Bái'}
    danh_sach_quan_huyen << {tinh: '15', code:'133', name: 'Thị xã Nghĩa Lộ'}
    danh_sach_quan_huyen << {tinh: '15', code:'135', name: 'Huyện Lục Yên'}
    danh_sach_quan_huyen << {tinh: '15', code:'136', name: 'Huyện Văn Yên'}
    danh_sach_quan_huyen << {tinh: '15', code:'137', name: 'Huyện Mù Căng Chải'}
    danh_sach_quan_huyen << {tinh: '15', code:'138', name: 'Huyện Trấn Yên'}
    danh_sach_quan_huyen << {tinh: '15', code:'139', name: 'Huyện Trạm Tấu'}
    danh_sach_quan_huyen << {tinh: '15', code:'140', name: 'Huyện Văn Chấn'}
    danh_sach_quan_huyen << {tinh: '15', code:'141', name: 'Huyện Yên Bình'}
    danh_sach_quan_huyen << {tinh: '17', code:'148', name: 'Thành phố Hòa Bình'}
    danh_sach_quan_huyen << {tinh: '17', code:'150', name: 'Huyện Đà Bắc'}
    danh_sach_quan_huyen << {tinh: '17', code:'151', name: 'Huyện Kỳ Sơn'}
    danh_sach_quan_huyen << {tinh: '17', code:'152', name: 'Huyện Lương Sơn'}
    danh_sach_quan_huyen << {tinh: '17', code:'153', name: 'Huyện Kim Bôi'}
    danh_sach_quan_huyen << {tinh: '17', code:'154', name: 'Huyện Cao Phong'}
    danh_sach_quan_huyen << {tinh: '17', code:'155', name: 'Huyện Tân Lạc'}
    danh_sach_quan_huyen << {tinh: '17', code:'156', name: 'Huyện Mai Châu'}
    danh_sach_quan_huyen << {tinh: '17', code:'157', name: 'Huyện Lạc Sơn'}
    danh_sach_quan_huyen << {tinh: '17', code:'158', name: 'Huyện Yên Thủy'}
    danh_sach_quan_huyen << {tinh: '17', code:'159', name: 'Huyện Lạc Thủy'}
    danh_sach_quan_huyen << {tinh: '19', code:'164', name: 'Thành phố Thái Nguyên'}
    danh_sach_quan_huyen << {tinh: '19', code:'165', name: 'Thành phố Sông Công'}
    danh_sach_quan_huyen << {tinh: '19', code:'167', name: 'Huyện Định Hóa'}
    danh_sach_quan_huyen << {tinh: '19', code:'168', name: 'Huyện Phú Lương'}
    danh_sach_quan_huyen << {tinh: '19', code:'169', name: 'Huyện Đồng Hỷ'}
    danh_sach_quan_huyen << {tinh: '19', code:'170', name: 'Huyện Võ Nhai'}
    danh_sach_quan_huyen << {tinh: '19', code:'171', name: 'Huyện Đại Từ'}
    danh_sach_quan_huyen << {tinh: '19', code:'172', name: 'Thị xã Phổ Yên'}
    danh_sach_quan_huyen << {tinh: '19', code:'173', name: 'Huyện Phú Bình'}
    danh_sach_quan_huyen << {tinh: '20', code:'178', name: 'Thành phố Lạng Sơn'}
    danh_sach_quan_huyen << {tinh: '20', code:'180', name: 'Huyện Tràng Định'}
    danh_sach_quan_huyen << {tinh: '20', code:'181', name: 'Huyện Bình Gia'}
    danh_sach_quan_huyen << {tinh: '20', code:'182', name: 'Huyện Văn Lãng'}
    danh_sach_quan_huyen << {tinh: '20', code:'183', name: 'Huyện Cao Lộc'}
    danh_sach_quan_huyen << {tinh: '20', code:'184', name: 'Huyện Văn Quan'}
    danh_sach_quan_huyen << {tinh: '20', code:'185', name: 'Huyện Bắc Sơn'}
    danh_sach_quan_huyen << {tinh: '20', code:'186', name: 'Huyện Hữu Lũng'}
    danh_sach_quan_huyen << {tinh: '20', code:'187', name: 'Huyện Chi Lăng'}
    danh_sach_quan_huyen << {tinh: '20', code:'188', name: 'Huyện Lộc Bình'}
    danh_sach_quan_huyen << {tinh: '20', code:'189', name: 'Huyện Đình Lập'}
    danh_sach_quan_huyen << {tinh: '22', code:'193', name: 'Thành phố Hạ Long'}
    danh_sach_quan_huyen << {tinh: '22', code:'194', name: 'Thành phố Móng Cái'}
    danh_sach_quan_huyen << {tinh: '22', code:'195', name: 'Thành phố Cẩm Phả'}
    danh_sach_quan_huyen << {tinh: '22', code:'196', name: 'Thành phố Uông Bí'}
    danh_sach_quan_huyen << {tinh: '22', code:'198', name: 'Huyện Bình Liêu'}
    danh_sach_quan_huyen << {tinh: '22', code:'199', name: 'Huyện Tiên Yên'}
    danh_sach_quan_huyen << {tinh: '22', code:'200', name: 'Huyện Đầm Hà'}
    danh_sach_quan_huyen << {tinh: '22', code:'201', name: 'Huyện Hải Hà'}
    danh_sach_quan_huyen << {tinh: '22', code:'202', name: 'Huyện Ba Chẽ'}
    danh_sach_quan_huyen << {tinh: '22', code:'203', name: 'Huyện Vân Đồn'}
    danh_sach_quan_huyen << {tinh: '22', code:'204', name: 'Huyện Hoành Bồ'}
    danh_sach_quan_huyen << {tinh: '22', code:'205', name: 'Thị xã Đông Triều'}
    danh_sach_quan_huyen << {tinh: '22', code:'206', name: 'Thị xã Quảng Yên'}
    danh_sach_quan_huyen << {tinh: '22', code:'207', name: 'Huyện Cô Tô'}
    danh_sach_quan_huyen << {tinh: '24', code:'213', name: 'Thành phố Bắc Giang'}
    danh_sach_quan_huyen << {tinh: '24', code:'215', name: 'Huyện Yên Thế'}
    danh_sach_quan_huyen << {tinh: '24', code:'216', name: 'Huyện Tân Yên'}
    danh_sach_quan_huyen << {tinh: '24', code:'217', name: 'Huyện Lạng Giang'}
    danh_sach_quan_huyen << {tinh: '24', code:'218', name: 'Huyện Lục Nam'}
    danh_sach_quan_huyen << {tinh: '24', code:'219', name: 'Huyện Lục Ngạn'}
    danh_sach_quan_huyen << {tinh: '24', code:'220', name: 'Huyện Sơn Động'}
    danh_sach_quan_huyen << {tinh: '24', code:'221', name: 'Huyện Yên Dũng'}
    danh_sach_quan_huyen << {tinh: '24', code:'222', name: 'Huyện Việt Yên'}
    danh_sach_quan_huyen << {tinh: '24', code:'223', name: 'Huyện Hiệp Hòa'}
    danh_sach_quan_huyen << {tinh: '25', code:'227', name: 'Thành phố Việt Trì'}
    danh_sach_quan_huyen << {tinh: '25', code:'228', name: 'Thị xã Phú Thọ'}
    danh_sach_quan_huyen << {tinh: '25', code:'230', name: 'Huyện Đoan Hùng'}
    danh_sach_quan_huyen << {tinh: '25', code:'231', name: 'Huyện Hạ Hoà'}
    danh_sach_quan_huyen << {tinh: '25', code:'232', name: 'Huyện Thanh Ba'}
    danh_sach_quan_huyen << {tinh: '25', code:'233', name: 'Huyện Phù Ninh'}
    danh_sach_quan_huyen << {tinh: '25', code:'234', name: 'Huyện Yên Lập'}
    danh_sach_quan_huyen << {tinh: '25', code:'235', name: 'Huyện Cẩm Khê'}
    danh_sach_quan_huyen << {tinh: '25', code:'236', name: 'Huyện Tam Nông'}
    danh_sach_quan_huyen << {tinh: '25', code:'237', name: 'Huyện Lâm Thao'}
    danh_sach_quan_huyen << {tinh: '25', code:'238', name: 'Huyện Thanh Sơn'}
    danh_sach_quan_huyen << {tinh: '25', code:'239', name: 'Huyện Thanh Thuỷ'}
    danh_sach_quan_huyen << {tinh: '25', code:'240', name: 'Huyện Tân Sơn'}
    danh_sach_quan_huyen << {tinh: '26', code:'243', name: 'Thành phố Vĩnh Yên'}
    danh_sach_quan_huyen << {tinh: '26', code:'244', name: 'Thị xã Phúc Yên'}
    danh_sach_quan_huyen << {tinh: '26', code:'246', name: 'Huyện Lập Thạch'}
    danh_sach_quan_huyen << {tinh: '26', code:'247', name: 'Huyện Tam Dương'}
    danh_sach_quan_huyen << {tinh: '26', code:'248', name: 'Huyện Tam Đảo'}
    danh_sach_quan_huyen << {tinh: '26', code:'249', name: 'Huyện Bình Xuyên'}
    danh_sach_quan_huyen << {tinh: '26', code:'251', name: 'Huyện Yên Lạc'}
    danh_sach_quan_huyen << {tinh: '26', code:'252', name: 'Huyện Vĩnh Tường'}
    danh_sach_quan_huyen << {tinh: '26', code:'253', name: 'Huyện Sông Lô'}
    danh_sach_quan_huyen << {tinh: '27', code:'256', name: 'Thành phố Bắc Ninh'}
    danh_sach_quan_huyen << {tinh: '27', code:'258', name: 'Huyện Yên Phong'}
    danh_sach_quan_huyen << {tinh: '27', code:'259', name: 'Huyện Quế Võ'}
    danh_sach_quan_huyen << {tinh: '27', code:'260', name: 'Huyện Tiên Du'}
    danh_sach_quan_huyen << {tinh: '27', code:'261', name: 'Thị xã Từ Sơn'}
    danh_sach_quan_huyen << {tinh: '27', code:'262', name: 'Huyện Thuận Thành'}
    danh_sach_quan_huyen << {tinh: '27', code:'263', name: 'Huyện Gia Bình'}
    danh_sach_quan_huyen << {tinh: '27', code:'264', name: 'Huyện Lương Tài'}
    danh_sach_quan_huyen << {tinh: '30', code:'288', name: 'Thành phố Hải Dương'}
    danh_sach_quan_huyen << {tinh: '30', code:'290', name: 'Thị xã Chí Linh'}
    danh_sach_quan_huyen << {tinh: '30', code:'291', name: 'Huyện Nam Sách'}
    danh_sach_quan_huyen << {tinh: '30', code:'292', name: 'Huyện Kinh Môn'}
    danh_sach_quan_huyen << {tinh: '30', code:'293', name: 'Huyện Kim Thành'}
    danh_sach_quan_huyen << {tinh: '30', code:'294', name: 'Huyện Thanh Hà'}
    danh_sach_quan_huyen << {tinh: '30', code:'295', name: 'Huyện Cẩm Giàng'}
    danh_sach_quan_huyen << {tinh: '30', code:'296', name: 'Huyện Bình Giang'}
    danh_sach_quan_huyen << {tinh: '30', code:'297', name: 'Huyện Gia Lộc'}
    danh_sach_quan_huyen << {tinh: '30', code:'298', name: 'Huyện Tứ Kỳ'}
    danh_sach_quan_huyen << {tinh: '30', code:'299', name: 'Huyện Ninh Giang'}
    danh_sach_quan_huyen << {tinh: '30', code:'300', name: 'Huyện Thanh Miện'}
    danh_sach_quan_huyen << {tinh: '31', code:'303', name: 'Quận Hồng Bàng'}
    danh_sach_quan_huyen << {tinh: '31', code:'304', name: 'Quận Ngô Quyền'}
    danh_sach_quan_huyen << {tinh: '31', code:'305', name: 'Quận Lê Chân'}
    danh_sach_quan_huyen << {tinh: '31', code:'306', name: 'Quận Hải An'}
    danh_sach_quan_huyen << {tinh: '31', code:'307', name: 'Quận Kiến An'}
    danh_sach_quan_huyen << {tinh: '31', code:'308', name: 'Quận Đồ Sơn'}
    danh_sach_quan_huyen << {tinh: '31', code:'309', name: 'Quận Dương Kinh'}
    danh_sach_quan_huyen << {tinh: '31', code:'311', name: 'Huyện Thuỷ Nguyên'}
    danh_sach_quan_huyen << {tinh: '31', code:'312', name: 'Huyện An Dương'}
    danh_sach_quan_huyen << {tinh: '31', code:'313', name: 'Huyện An Lão'}
    danh_sach_quan_huyen << {tinh: '31', code:'314', name: 'Huyện Kiến Thuỵ'}
    danh_sach_quan_huyen << {tinh: '31', code:'315', name: 'Huyện Tiên Lãng'}
    danh_sach_quan_huyen << {tinh: '31', code:'316', name: 'Huyện Vĩnh Bảo'}
    danh_sach_quan_huyen << {tinh: '31', code:'317', name: 'Huyện Cát Hải'}
    danh_sach_quan_huyen << {tinh: '33', code:'323', name: 'Thành phố Hưng Yên'}
    danh_sach_quan_huyen << {tinh: '33', code:'325', name: 'Huyện Văn Lâm'}
    danh_sach_quan_huyen << {tinh: '33', code:'326', name: 'Huyện Văn Giang'}
    danh_sach_quan_huyen << {tinh: '33', code:'327', name: 'Huyện Yên Mỹ'}
    danh_sach_quan_huyen << {tinh: '33', code:'328', name: 'Huyện Mỹ Hào'}
    danh_sach_quan_huyen << {tinh: '33', code:'329', name: 'Huyện Ân Thi'}
    danh_sach_quan_huyen << {tinh: '33', code:'330', name: 'Huyện Khoái Châu'}
    danh_sach_quan_huyen << {tinh: '33', code:'331', name: 'Huyện Kim Động'}
    danh_sach_quan_huyen << {tinh: '33', code:'332', name: 'Huyện Tiên Lữ'}
    danh_sach_quan_huyen << {tinh: '33', code:'333', name: 'Huyện Phù Cừ'}
    danh_sach_quan_huyen << {tinh: '34', code:'336', name: 'Thành phố Thái Bình'}
    danh_sach_quan_huyen << {tinh: '34', code:'338', name: 'Huyện Quỳnh Phụ'}
    danh_sach_quan_huyen << {tinh: '34', code:'339', name: 'Huyện Hưng Hà'}
    danh_sach_quan_huyen << {tinh: '34', code:'340', name: 'Huyện Đông Hưng'}
    danh_sach_quan_huyen << {tinh: '34', code:'341', name: 'Huyện Thái Thụy'}
    danh_sach_quan_huyen << {tinh: '34', code:'342', name: 'Huyện Tiền Hải'}
    danh_sach_quan_huyen << {tinh: '34', code:'343', name: 'Huyện Kiến Xương'}
    danh_sach_quan_huyen << {tinh: '34', code:'344', name: 'Huyện Vũ Thư'}
    danh_sach_quan_huyen << {tinh: '35', code:'347', name: 'Thành phố Phủ Lý'}
    danh_sach_quan_huyen << {tinh: '35', code:'349', name: 'Huyện Duy Tiên'}
    danh_sach_quan_huyen << {tinh: '35', code:'350', name: 'Huyện Kim Bảng'}
    danh_sach_quan_huyen << {tinh: '35', code:'351', name: 'Huyện Thanh Liêm'}
    danh_sach_quan_huyen << {tinh: '35', code:'352', name: 'Huyện Bình Lục'}
    danh_sach_quan_huyen << {tinh: '35', code:'353', name: 'Huyện Lý Nhân'}
    danh_sach_quan_huyen << {tinh: '36', code:'356', name: 'Thành phố Nam Định'}
    danh_sach_quan_huyen << {tinh: '36', code:'358', name: 'Huyện Mỹ Lộc'}
    danh_sach_quan_huyen << {tinh: '36', code:'359', name: 'Huyện Vụ Bản'}
    danh_sach_quan_huyen << {tinh: '36', code:'360', name: 'Huyện Ý Yên'}
    danh_sach_quan_huyen << {tinh: '36', code:'361', name: 'Huyện Nghĩa Hưng'}
    danh_sach_quan_huyen << {tinh: '36', code:'362', name: 'Huyện Nam Trực'}
    danh_sach_quan_huyen << {tinh: '36', code:'363', name: 'Huyện Trực Ninh'}
    danh_sach_quan_huyen << {tinh: '36', code:'364', name: 'Huyện Xuân Trường'}
    danh_sach_quan_huyen << {tinh: '36', code:'365', name: 'Huyện Giao Thủy'}
    danh_sach_quan_huyen << {tinh: '36', code:'366', name: 'Huyện Hải Hậu'}
    danh_sach_quan_huyen << {tinh: '37', code:'369', name: 'Thành phố Ninh Bình'}
    danh_sach_quan_huyen << {tinh: '37', code:'370', name: 'Thành phố Tam Điệp'}
    danh_sach_quan_huyen << {tinh: '37', code:'372', name: 'Huyện Nho Quan'}
    danh_sach_quan_huyen << {tinh: '37', code:'373', name: 'Huyện Gia Viễn'}
    danh_sach_quan_huyen << {tinh: '37', code:'374', name: 'Huyện Hoa Lư'}
    danh_sach_quan_huyen << {tinh: '37', code:'375', name: 'Huyện Yên Khánh'}
    danh_sach_quan_huyen << {tinh: '37', code:'376', name: 'Huyện Kim Sơn'}
    danh_sach_quan_huyen << {tinh: '37', code:'377', name: 'Huyện Yên Mô'}
    danh_sach_quan_huyen << {tinh: '38', code:'380', name: 'Thành phố Thanh Hóa'}
    danh_sach_quan_huyen << {tinh: '38', code:'381', name: 'Thị xã Bỉm Sơn'}
    danh_sach_quan_huyen << {tinh: '38', code:'382', name: 'Thành phố Sầm Sơn'}
    danh_sach_quan_huyen << {tinh: '38', code:'384', name: 'Huyện Mường Lát'}
    danh_sach_quan_huyen << {tinh: '38', code:'385', name: 'Huyện Quan Hóa'}
    danh_sach_quan_huyen << {tinh: '38', code:'386', name: 'Huyện Bá Thước'}
    danh_sach_quan_huyen << {tinh: '38', code:'387', name: 'Huyện Quan Sơn'}
    danh_sach_quan_huyen << {tinh: '38', code:'388', name: 'Huyện Lang Chánh'}
    danh_sach_quan_huyen << {tinh: '38', code:'389', name: 'Huyện Ngọc Lặc'}
    danh_sach_quan_huyen << {tinh: '38', code:'390', name: 'Huyện Cẩm Thủy'}
    danh_sach_quan_huyen << {tinh: '38', code:'391', name: 'Huyện Thạch Thành'}
    danh_sach_quan_huyen << {tinh: '38', code:'392', name: 'Huyện Hà Trung'}
    danh_sach_quan_huyen << {tinh: '38', code:'393', name: 'Huyện Vĩnh Lộc'}
    danh_sach_quan_huyen << {tinh: '38', code:'394', name: 'Huyện Yên Định'}
    danh_sach_quan_huyen << {tinh: '38', code:'395', name: 'Huyện Thọ Xuân'}
    danh_sach_quan_huyen << {tinh: '38', code:'396', name: 'Huyện Thường Xuân'}
    danh_sach_quan_huyen << {tinh: '38', code:'397', name: 'Huyện Triệu Sơn'}
    danh_sach_quan_huyen << {tinh: '38', code:'398', name: 'Huyện Thiệu Hóa'}
    danh_sach_quan_huyen << {tinh: '38', code:'399', name: 'Huyện Hoằng Hóa'}
    danh_sach_quan_huyen << {tinh: '38', code:'400', name: 'Huyện Hậu Lộc'}
    danh_sach_quan_huyen << {tinh: '38', code:'401', name: 'Huyện Nga Sơn'}
    danh_sach_quan_huyen << {tinh: '38', code:'402', name: 'Huyện Như Xuân'}
    danh_sach_quan_huyen << {tinh: '38', code:'403', name: 'Huyện Như Thanh'}
    danh_sach_quan_huyen << {tinh: '38', code:'404', name: 'Huyện Nông Cống'}
    danh_sach_quan_huyen << {tinh: '38', code:'405', name: 'Huyện Đông Sơn'}
    danh_sach_quan_huyen << {tinh: '38', code:'406', name: 'Huyện Quảng Xương'}
    danh_sach_quan_huyen << {tinh: '38', code:'407', name: 'Huyện Tĩnh Gia'}
    danh_sach_quan_huyen << {tinh: '40', code:'412', name: 'Thành phố Vinh'}
    danh_sach_quan_huyen << {tinh: '40', code:'413', name: 'Thị xã Cửa Lò'}
    danh_sach_quan_huyen << {tinh: '40', code:'414', name: 'Thị xã Thái Hoà'}
    danh_sach_quan_huyen << {tinh: '40', code:'415', name: 'Huyện Quế Phong'}
    danh_sach_quan_huyen << {tinh: '40', code:'416', name: 'Huyện Quỳ Châu'}
    danh_sach_quan_huyen << {tinh: '40', code:'417', name: 'Huyện Kỳ Sơn'}
    danh_sach_quan_huyen << {tinh: '40', code:'418', name: 'Huyện Tương Dương'}
    danh_sach_quan_huyen << {tinh: '40', code:'419', name: 'Huyện Nghĩa Đàn'}
    danh_sach_quan_huyen << {tinh: '40', code:'420', name: 'Huyện Quỳ Hợp'}
    danh_sach_quan_huyen << {tinh: '40', code:'421', name: 'Huyện Quỳnh Lưu'}
    danh_sach_quan_huyen << {tinh: '40', code:'422', name: 'Huyện Con Cuông'}
    danh_sach_quan_huyen << {tinh: '40', code:'423', name: 'Huyện Tân Kỳ'}
    danh_sach_quan_huyen << {tinh: '40', code:'424', name: 'Huyện Anh Sơn'}
    danh_sach_quan_huyen << {tinh: '40', code:'425', name: 'Huyện Diễn Châu'}
    danh_sach_quan_huyen << {tinh: '40', code:'426', name: 'Huyện Yên Thành'}
    danh_sach_quan_huyen << {tinh: '40', code:'427', name: 'Huyện Đô Lương'}
    danh_sach_quan_huyen << {tinh: '40', code:'428', name: 'Huyện Thanh Chương'}
    danh_sach_quan_huyen << {tinh: '40', code:'429', name: 'Huyện Nghi Lộc'}
    danh_sach_quan_huyen << {tinh: '40', code:'430', name: 'Huyện Nam Đàn'}
    danh_sach_quan_huyen << {tinh: '40', code:'431', name: 'Huyện Hưng Nguyên'}
    danh_sach_quan_huyen << {tinh: '40', code:'432', name: 'Thị xã Hoàng Mai'}
    danh_sach_quan_huyen << {tinh: '42', code:'436', name: 'Thành phố Hà Tĩnh'}
    danh_sach_quan_huyen << {tinh: '42', code:'437', name: 'Thị xã Hồng Lĩnh'}
    danh_sach_quan_huyen << {tinh: '42', code:'439', name: 'Huyện Hương Sơn'}
    danh_sach_quan_huyen << {tinh: '42', code:'440', name: 'Huyện Đức Thọ'}
    danh_sach_quan_huyen << {tinh: '42', code:'441', name: 'Huyện Vũ Quang'}
    danh_sach_quan_huyen << {tinh: '42', code:'442', name: 'Huyện Nghi Xuân'}
    danh_sach_quan_huyen << {tinh: '42', code:'443', name: 'Huyện Can Lộc'}
    danh_sach_quan_huyen << {tinh: '42', code:'444', name: 'Huyện Hương Khê'}
    danh_sach_quan_huyen << {tinh: '42', code:'445', name: 'Huyện Thạch Hà'}
    danh_sach_quan_huyen << {tinh: '42', code:'446', name: 'Huyện Cẩm Xuyên'}
    danh_sach_quan_huyen << {tinh: '42', code:'447', name: 'Huyện Kỳ Anh'}
    danh_sach_quan_huyen << {tinh: '42', code:'448', name: 'Huyện Lộc Hà'}
    danh_sach_quan_huyen << {tinh: '42', code:'449', name: 'Thị xã Kỳ Anh'}
    danh_sach_quan_huyen << {tinh: '44', code:'450', name: 'Thành Phố Đồng Hới'}
    danh_sach_quan_huyen << {tinh: '44', code:'452', name: 'Huyện Minh Hóa'}
    danh_sach_quan_huyen << {tinh: '44', code:'453', name: 'Huyện Tuyên Hóa'}
    danh_sach_quan_huyen << {tinh: '44', code:'454', name: 'Huyện Quảng Trạch'}
    danh_sach_quan_huyen << {tinh: '44', code:'455', name: 'Huyện Bố Trạch'}
    danh_sach_quan_huyen << {tinh: '44', code:'456', name: 'Huyện Quảng Ninh'}
    danh_sach_quan_huyen << {tinh: '44', code:'457', name: 'Huyện Lệ Thủy'}
    danh_sach_quan_huyen << {tinh: '44', code:'458', name: 'Thị xã Ba Đồn'}
    danh_sach_quan_huyen << {tinh: '45', code:'461', name: 'Thành phố Đông Hà'}
    danh_sach_quan_huyen << {tinh: '45', code:'462', name: 'Thị xã Quảng Trị'}
    danh_sach_quan_huyen << {tinh: '45', code:'464', name: 'Huyện Vĩnh Linh'}
    danh_sach_quan_huyen << {tinh: '45', code:'465', name: 'Huyện Hướng Hóa'}
    danh_sach_quan_huyen << {tinh: '45', code:'466', name: 'Huyện Gio Linh'}
    danh_sach_quan_huyen << {tinh: '45', code:'467', name: 'Huyện Đa Krông'}
    danh_sach_quan_huyen << {tinh: '45', code:'468', name: 'Huyện Cam Lộ'}
    danh_sach_quan_huyen << {tinh: '45', code:'469', name: 'Huyện Triệu Phong'}
    danh_sach_quan_huyen << {tinh: '45', code:'470', name: 'Huyện Hải Lăng'}
    danh_sach_quan_huyen << {tinh: '46', code:'474', name: 'Thành phố Huế'}
    danh_sach_quan_huyen << {tinh: '46', code:'476', name: 'Huyện Phong Điền'}
    danh_sach_quan_huyen << {tinh: '46', code:'477', name: 'Huyện Quảng Điền'}
    danh_sach_quan_huyen << {tinh: '46', code:'478', name: 'Huyện Phú Vang'}
    danh_sach_quan_huyen << {tinh: '46', code:'479', name: 'Thị xã Hương Thủy'}
    danh_sach_quan_huyen << {tinh: '46', code:'480', name: 'Thị xã Hương Trà'}
    danh_sach_quan_huyen << {tinh: '46', code:'481', name: 'Huyện A Lưới'}
    danh_sach_quan_huyen << {tinh: '46', code:'482', name: 'Huyện Phú Lộc'}
    danh_sach_quan_huyen << {tinh: '46', code:'483', name: 'Huyện Nam Đông'}
    danh_sach_quan_huyen << {tinh: '48', code:'490', name: 'Quận Liên Chiểu'}
    danh_sach_quan_huyen << {tinh: '48', code:'491', name: 'Quận Thanh Khê'}
    danh_sach_quan_huyen << {tinh: '48', code:'492', name: 'Quận Hải Châu'}
    danh_sach_quan_huyen << {tinh: '48', code:'493', name: 'Quận Sơn Trà'}
    danh_sach_quan_huyen << {tinh: '48', code:'494', name: 'Quận Ngũ Hành Sơn'}
    danh_sach_quan_huyen << {tinh: '48', code:'495', name: 'Quận Cẩm Lệ'}
    danh_sach_quan_huyen << {tinh: '48', code:'497', name: 'Huyện Hòa Vang'}
    danh_sach_quan_huyen << {tinh: '49', code:'502', name: 'Thành phố Tam Kỳ'}
    danh_sach_quan_huyen << {tinh: '49', code:'503', name: 'Thành phố Hội An'}
    danh_sach_quan_huyen << {tinh: '49', code:'504', name: 'Huyện Tây Giang'}
    danh_sach_quan_huyen << {tinh: '49', code:'505', name: 'Huyện Đông Giang'}
    danh_sach_quan_huyen << {tinh: '49', code:'506', name: 'Huyện Đại Lộc'}
    danh_sach_quan_huyen << {tinh: '49', code:'507', name: 'Thị xã Điện Bàn'}
    danh_sach_quan_huyen << {tinh: '49', code:'508', name: 'Huyện Duy Xuyên'}
    danh_sach_quan_huyen << {tinh: '49', code:'509', name: 'Huyện Quế Sơn'}
    danh_sach_quan_huyen << {tinh: '49', code:'510', name: 'Huyện Nam Giang'}
    danh_sach_quan_huyen << {tinh: '49', code:'511', name: 'Huyện Phước Sơn'}
    danh_sach_quan_huyen << {tinh: '49', code:'512', name: 'Huyện Hiệp Đức'}
    danh_sach_quan_huyen << {tinh: '49', code:'513', name: 'Huyện Thăng Bình'}
    danh_sach_quan_huyen << {tinh: '49', code:'514', name: 'Huyện Tiên Phước'}
    danh_sach_quan_huyen << {tinh: '49', code:'515', name: 'Huyện Bắc Trà My'}
    danh_sach_quan_huyen << {tinh: '49', code:'516', name: 'Huyện Nam Trà My'}
    danh_sach_quan_huyen << {tinh: '49', code:'517', name: 'Huyện Núi Thành'}
    danh_sach_quan_huyen << {tinh: '49', code:'518', name: 'Huyện Phú Ninh'}
    danh_sach_quan_huyen << {tinh: '49', code:'519', name: 'Huyện Nông Sơn'}
    danh_sach_quan_huyen << {tinh: '51', code:'522', name: 'Thành phố Quảng Ngãi'}
    danh_sach_quan_huyen << {tinh: '51', code:'524', name: 'Huyện Bình Sơn'}
    danh_sach_quan_huyen << {tinh: '51', code:'525', name: 'Huyện Trà Bồng'}
    danh_sach_quan_huyen << {tinh: '51', code:'526', name: 'Huyện Tây Trà'}
    danh_sach_quan_huyen << {tinh: '51', code:'527', name: 'Huyện Sơn Tịnh'}
    danh_sach_quan_huyen << {tinh: '51', code:'528', name: 'Huyện Tư Nghĩa'}
    danh_sach_quan_huyen << {tinh: '51', code:'529', name: 'Huyện Sơn Hà'}
    danh_sach_quan_huyen << {tinh: '51', code:'530', name: 'Huyện Sơn Tây'}
    danh_sach_quan_huyen << {tinh: '51', code:'531', name: 'Huyện Minh Long'}
    danh_sach_quan_huyen << {tinh: '51', code:'532', name: 'Huyện Nghĩa Hành'}
    danh_sach_quan_huyen << {tinh: '51', code:'533', name: 'Huyện Mộ Đức'}
    danh_sach_quan_huyen << {tinh: '51', code:'534', name: 'Huyện Đức Phổ'}
    danh_sach_quan_huyen << {tinh: '51', code:'535', name: 'Huyện Ba Tơ'}
    danh_sach_quan_huyen << {tinh: '51', code:'536', name: 'Huyện Lý Sơn'}
    danh_sach_quan_huyen << {tinh: '52', code:'540', name: 'Thành phố Qui Nhơn'}
    danh_sach_quan_huyen << {tinh: '52', code:'542', name: 'Huyện An Lão'}
    danh_sach_quan_huyen << {tinh: '52', code:'543', name: 'Huyện Hoài Nhơn'}
    danh_sach_quan_huyen << {tinh: '52', code:'544', name: 'Huyện Hoài Ân'}
    danh_sach_quan_huyen << {tinh: '52', code:'545', name: 'Huyện Phù Mỹ'}
    danh_sach_quan_huyen << {tinh: '52', code:'546', name: 'Huyện Vĩnh Thạnh'}
    danh_sach_quan_huyen << {tinh: '52', code:'547', name: 'Huyện Tây Sơn'}
    danh_sach_quan_huyen << {tinh: '52', code:'548', name: 'Huyện Phù Cát'}
    danh_sach_quan_huyen << {tinh: '52', code:'549', name: 'Thị xã An Nhơn'}
    danh_sach_quan_huyen << {tinh: '52', code:'550', name: 'Huyện Tuy Phước'}
    danh_sach_quan_huyen << {tinh: '52', code:'551', name: 'Huyện Vân Canh'}
    danh_sach_quan_huyen << {tinh: '54', code:'555', name: 'Thành phố Tuy Hoà'}
    danh_sach_quan_huyen << {tinh: '54', code:'557', name: 'Thị xã Sông Cầu'}
    danh_sach_quan_huyen << {tinh: '54', code:'558', name: 'Huyện Đồng Xuân'}
    danh_sach_quan_huyen << {tinh: '54', code:'559', name: 'Huyện Tuy An'}
    danh_sach_quan_huyen << {tinh: '54', code:'560', name: 'Huyện Sơn Hòa'}
    danh_sach_quan_huyen << {tinh: '54', code:'561', name: 'Huyện Sông Hinh'}
    danh_sach_quan_huyen << {tinh: '54', code:'562', name: 'Huyện Tây Hoà'}
    danh_sach_quan_huyen << {tinh: '54', code:'563', name: 'Huyện Phú Hoà'}
    danh_sach_quan_huyen << {tinh: '54', code:'564', name: 'Huyện Đông Hòa'}
    danh_sach_quan_huyen << {tinh: '56', code:'568', name: 'Thành phố Nha Trang'}
    danh_sach_quan_huyen << {tinh: '56', code:'569', name: 'Thành phố Cam Ranh'}
    danh_sach_quan_huyen << {tinh: '56', code:'570', name: 'Huyện Cam Lâm'}
    danh_sach_quan_huyen << {tinh: '56', code:'571', name: 'Huyện Vạn Ninh'}
    danh_sach_quan_huyen << {tinh: '56', code:'572', name: 'Thị xã Ninh Hòa'}
    danh_sach_quan_huyen << {tinh: '56', code:'573', name: 'Huyện Khánh Vĩnh'}
    danh_sach_quan_huyen << {tinh: '56', code:'574', name: 'Huyện Diên Khánh'}
    danh_sach_quan_huyen << {tinh: '56', code:'575', name: 'Huyện Khánh Sơn'}
    danh_sach_quan_huyen << {tinh: '56', code:'576', name: 'Huyện Trường Sa'}
    danh_sach_quan_huyen << {tinh: '58', code:'582', name: 'Thành phố Phan Rang-Tháp Chàm'}
    danh_sach_quan_huyen << {tinh: '58', code:'584', name: 'Huyện Bác Ái'}
    danh_sach_quan_huyen << {tinh: '58', code:'585', name: 'Huyện Ninh Sơn'}
    danh_sach_quan_huyen << {tinh: '58', code:'586', name: 'Huyện Ninh Hải'}
    danh_sach_quan_huyen << {tinh: '58', code:'587', name: 'Huyện Ninh Phước'}
    danh_sach_quan_huyen << {tinh: '58', code:'588', name: 'Huyện Thuận Bắc'}
    danh_sach_quan_huyen << {tinh: '58', code:'589', name: 'Huyện Thuận Nam'}
    danh_sach_quan_huyen << {tinh: '60', code:'593', name: 'Thành phố Phan Thiết'}
    danh_sach_quan_huyen << {tinh: '60', code:'594', name: 'Thị xã La Gi'}
    danh_sach_quan_huyen << {tinh: '60', code:'595', name: 'Huyện Tuy Phong'}
    danh_sach_quan_huyen << {tinh: '60', code:'596', name: 'Huyện Bắc Bình'}
    danh_sach_quan_huyen << {tinh: '60', code:'597', name: 'Huyện Hàm Thuận Bắc'}
    danh_sach_quan_huyen << {tinh: '60', code:'598', name: 'Huyện Hàm Thuận Nam'}
    danh_sach_quan_huyen << {tinh: '60', code:'599', name: 'Huyện Tánh Linh'}
    danh_sach_quan_huyen << {tinh: '60', code:'600', name: 'Huyện Đức Linh'}
    danh_sach_quan_huyen << {tinh: '60', code:'601', name: 'Huyện Hàm Tân'}
    danh_sach_quan_huyen << {tinh: '60', code:'602', name: 'Huyện Phú Quí'}
    danh_sach_quan_huyen << {tinh: '62', code:'608', name: 'Thành phố Kon Tum'}
    danh_sach_quan_huyen << {tinh: '62', code:'610', name: 'Huyện Đắk Glei'}
    danh_sach_quan_huyen << {tinh: '62', code:'611', name: 'Huyện Ngọc Hồi'}
    danh_sach_quan_huyen << {tinh: '62', code:'612', name: 'Huyện Đắk Tô'}
    danh_sach_quan_huyen << {tinh: '62', code:'613', name: 'Huyện Kon Plông'}
    danh_sach_quan_huyen << {tinh: '62', code:'614', name: 'Huyện Kon Rẫy'}
    danh_sach_quan_huyen << {tinh: '62', code:'615', name: 'Huyện Đắk Hà'}
    danh_sach_quan_huyen << {tinh: '62', code:'616', name: 'Huyện Sa Thầy'}
    danh_sach_quan_huyen << {tinh: '62', code:'617', name: 'Huyện Tu Mơ Rông'}
    danh_sach_quan_huyen << {tinh: '62', code:'618', name: "Huyện Ia H' Drai"}
    danh_sach_quan_huyen << {tinh: '64', code:'622', name: 'Thành phố Pleiku'}
    danh_sach_quan_huyen << {tinh: '64', code:'623', name: 'Thị xã An Khê'}
    danh_sach_quan_huyen << {tinh: '64', code:'624', name: 'Thị xã Ayun Pa'}
    danh_sach_quan_huyen << {tinh: '64', code:'625', name: 'Huyện KBang'}
    danh_sach_quan_huyen << {tinh: '64', code:'626', name: 'Huyện Đăk Đoa'}
    danh_sach_quan_huyen << {tinh: '64', code:'627', name: 'Huyện Chư Păh'}
    danh_sach_quan_huyen << {tinh: '64', code:'628', name: 'Huyện Ia Grai'}
    danh_sach_quan_huyen << {tinh: '64', code:'629', name: 'Huyện Mang Yang'}
    danh_sach_quan_huyen << {tinh: '64', code:'630', name: 'Huyện Kông Chro'}
    danh_sach_quan_huyen << {tinh: '64', code:'631', name: 'Huyện Đức Cơ'}
    danh_sach_quan_huyen << {tinh: '64', code:'632', name: 'Huyện Chư Prông'}
    danh_sach_quan_huyen << {tinh: '64', code:'633', name: 'Huyện Chư Sê'}
    danh_sach_quan_huyen << {tinh: '64', code:'634', name: 'Huyện Đăk Pơ'}
    danh_sach_quan_huyen << {tinh: '64', code:'635', name: 'Huyện Ia Pa'}
    danh_sach_quan_huyen << {tinh: '64', code:'637', name: 'Huyện Krông Pa'}
    danh_sach_quan_huyen << {tinh: '64', code:'638', name: 'Huyện Phú Thiện'}
    danh_sach_quan_huyen << {tinh: '64', code:'639', name: 'Huyện Chư Pưh'}
    danh_sach_quan_huyen << {tinh: '66', code:'643', name: 'Thành phố Buôn Ma Thuột'}
    danh_sach_quan_huyen << {tinh: '66', code:'644', name: 'Thị Xã Buôn Hồ'}
    danh_sach_quan_huyen << {tinh: '66', code:'645', name: "Huyện Ea H'leo"}
    danh_sach_quan_huyen << {tinh: '66', code:'646', name: 'Huyện Ea Súp'}
    danh_sach_quan_huyen << {tinh: '66', code:'647', name: 'Huyện Buôn Đôn'}
    danh_sach_quan_huyen << {tinh: '66', code:'648', name: "Huyện Cư M'gar"}
    danh_sach_quan_huyen << {tinh: '66', code:'649', name: 'Huyện Krông Búk'}
    danh_sach_quan_huyen << {tinh: '66', code:'650', name: 'Huyện Krông Năng'}
    danh_sach_quan_huyen << {tinh: '66', code:'651', name: 'Huyện Ea Kar'}
    danh_sach_quan_huyen << {tinh: '66', code:'652', name: "Huyện M'Đrắk"}
    danh_sach_quan_huyen << {tinh: '66', code:'653', name: 'Huyện Krông Bông'}
    danh_sach_quan_huyen << {tinh: '66', code:'654', name: 'Huyện Krông Pắc'}
    danh_sach_quan_huyen << {tinh: '66', code:'655', name: 'Huyện Krông A Na'}
    danh_sach_quan_huyen << {tinh: '66', code:'656', name: 'Huyện Lắk'}
    danh_sach_quan_huyen << {tinh: '66', code:'657', name: 'Huyện Cư Kuin'}
    danh_sach_quan_huyen << {tinh: '67', code:'660', name: 'Thị xã Gia Nghĩa'}
    danh_sach_quan_huyen << {tinh: '67', code:'661', name: 'Huyện Đăk Glong'}
    danh_sach_quan_huyen << {tinh: '67', code:'662', name: 'Huyện Cư Jút'}
    danh_sach_quan_huyen << {tinh: '67', code:'663', name: 'Huyện Đắk Mil'}
    danh_sach_quan_huyen << {tinh: '67', code:'664', name: 'Huyện Krông Nô'}
    danh_sach_quan_huyen << {tinh: '67', code:'665', name: 'Huyện Đắk Song'}
    danh_sach_quan_huyen << {tinh: '67', code:'666', name: "Huyện Đắk R'Lấp"}
    danh_sach_quan_huyen << {tinh: '67', code:'667', name: 'Huyện Tuy Đức'}
    danh_sach_quan_huyen << {tinh: '68', code:'672', name: 'Thành phố Đà Lạt'}
    danh_sach_quan_huyen << {tinh: '68', code:'673', name: 'Thành phố Bảo Lộc'}
    danh_sach_quan_huyen << {tinh: '68', code:'674', name: 'Huyện Đam Rông'}
    danh_sach_quan_huyen << {tinh: '68', code:'675', name: 'Huyện Lạc Dương'}
    danh_sach_quan_huyen << {tinh: '68', code:'676', name: 'Huyện Lâm Hà'}
    danh_sach_quan_huyen << {tinh: '68', code:'677', name: 'Huyện Đơn Dương'}
    danh_sach_quan_huyen << {tinh: '68', code:'678', name: 'Huyện Đức Trọng'}
    danh_sach_quan_huyen << {tinh: '68', code:'679', name: 'Huyện Di Linh'}
    danh_sach_quan_huyen << {tinh: '68', code:'680', name: 'Huyện Bảo Lâm'}
    danh_sach_quan_huyen << {tinh: '68', code:'681', name: 'Huyện Đạ Huoai'}
    danh_sach_quan_huyen << {tinh: '68', code:'682', name: 'Huyện Đạ Tẻh'}
    danh_sach_quan_huyen << {tinh: '68', code:'683', name: 'Huyện Cát Tiên'}
    danh_sach_quan_huyen << {tinh: '70', code:'688', name: 'Thị xã Phước Long'}
    danh_sach_quan_huyen << {tinh: '70', code:'689', name: 'Thị xã Đồng Xoài'}
    danh_sach_quan_huyen << {tinh: '70', code:'690', name: 'Thị xã Bình Long'}
    danh_sach_quan_huyen << {tinh: '70', code:'691', name: 'Huyện Bù Gia Mập'}
    danh_sach_quan_huyen << {tinh: '70', code:'692', name: 'Huyện Lộc Ninh'}
    danh_sach_quan_huyen << {tinh: '70', code:'693', name: 'Huyện Bù Đốp'}
    danh_sach_quan_huyen << {tinh: '70', code:'694', name: 'Huyện Hớn Quản'}
    danh_sach_quan_huyen << {tinh: '70', code:'695', name: 'Huyện Đồng Phú'}
    danh_sach_quan_huyen << {tinh: '70', code:'696', name: 'Huyện Bù Đăng'}
    danh_sach_quan_huyen << {tinh: '70', code:'697', name: 'Huyện Chơn Thành'}
    danh_sach_quan_huyen << {tinh: '70', code:'698', name: 'Huyện Phú Riềng'}
    danh_sach_quan_huyen << {tinh: '72', code:'703', name: 'Thành phố Tây Ninh'}
    danh_sach_quan_huyen << {tinh: '72', code:'705', name: 'Huyện Tân Biên'}
    danh_sach_quan_huyen << {tinh: '72', code:'706', name: 'Huyện Tân Châu'}
    danh_sach_quan_huyen << {tinh: '72', code:'707', name: 'Huyện Dương Minh Châu'}
    danh_sach_quan_huyen << {tinh: '72', code:'708', name: 'Huyện Châu Thành'}
    danh_sach_quan_huyen << {tinh: '72', code:'709', name: 'Huyện Hòa Thành'}
    danh_sach_quan_huyen << {tinh: '72', code:'710', name: 'Huyện Gò Dầu'}
    danh_sach_quan_huyen << {tinh: '72', code:'711', name: 'Huyện Bến Cầu'}
    danh_sach_quan_huyen << {tinh: '72', code:'712', name: 'Huyện Trảng Bàng'}
    danh_sach_quan_huyen << {tinh: '74', code:'718', name: 'Thành phố Thủ Dầu Một'}
    danh_sach_quan_huyen << {tinh: '74', code:'719', name: 'Huyện Bàu Bàng'}
    danh_sach_quan_huyen << {tinh: '74', code:'720', name: 'Huyện Dầu Tiếng'}
    danh_sach_quan_huyen << {tinh: '74', code:'721', name: 'Thị xã Bến Cát'}
    danh_sach_quan_huyen << {tinh: '74', code:'722', name: 'Huyện Phú Giáo'}
    danh_sach_quan_huyen << {tinh: '74', code:'723', name: 'Thị xã Tân Uyên'}
    danh_sach_quan_huyen << {tinh: '74', code:'724', name: 'Thị xã Dĩ An'}
    danh_sach_quan_huyen << {tinh: '74', code:'725', name: 'Thị xã Thuận An'}
    danh_sach_quan_huyen << {tinh: '74', code:'726', name: 'Huyện Bắc Tân Uyên'}
    danh_sach_quan_huyen << {tinh: '75', code:'731', name: 'Thành phố Biên Hòa'}
    danh_sach_quan_huyen << {tinh: '75', code:'732', name: 'Thị xã Long Khánh'}
    danh_sach_quan_huyen << {tinh: '75', code:'734', name: 'Huyện Tân Phú'}
    danh_sach_quan_huyen << {tinh: '75', code:'735', name: 'Huyện Vĩnh Cửu'}
    danh_sach_quan_huyen << {tinh: '75', code:'736', name: 'Huyện Định Quán'}
    danh_sach_quan_huyen << {tinh: '75', code:'737', name: 'Huyện Trảng Bom'}
    danh_sach_quan_huyen << {tinh: '75', code:'738', name: 'Huyện Thống Nhất'}
    danh_sach_quan_huyen << {tinh: '75', code:'739', name: 'Huyện Cẩm Mỹ'}
    danh_sach_quan_huyen << {tinh: '75', code:'740', name: 'Huyện Long Thành'}
    danh_sach_quan_huyen << {tinh: '75', code:'741', name: 'Huyện Xuân Lộc'}
    danh_sach_quan_huyen << {tinh: '75', code:'742', name: 'Huyện Nhơn Trạch'}
    danh_sach_quan_huyen << {tinh: '77', code:'747', name: 'Thành phố Vũng Tàu'}
    danh_sach_quan_huyen << {tinh: '77', code:'748', name: 'Thành phố Bà Rịa'}
    danh_sach_quan_huyen << {tinh: '77', code:'750', name: 'Huyện Châu Đức'}
    danh_sach_quan_huyen << {tinh: '77', code:'751', name: 'Huyện Xuyên Mộc'}
    danh_sach_quan_huyen << {tinh: '77', code:'752', name: 'Huyện Long Điền'}
    danh_sach_quan_huyen << {tinh: '77', code:'753', name: 'Huyện Đất Đỏ'}
    danh_sach_quan_huyen << {tinh: '77', code:'754', name: 'Huyện Tân Thành'}
    danh_sach_quan_huyen << {tinh: '79', code:'760', name: 'Quận 1'}
    danh_sach_quan_huyen << {tinh: '79', code:'761', name: 'Quận 12'}
    danh_sach_quan_huyen << {tinh: '79', code:'762', name: 'Quận Thủ Đức'}
    danh_sach_quan_huyen << {tinh: '79', code:'763', name: 'Quận 9'}
    danh_sach_quan_huyen << {tinh: '79', code:'764', name: 'Quận Gò Vấp'}
    danh_sach_quan_huyen << {tinh: '79', code:'765', name: 'Quận Bình Thạnh'}
    danh_sach_quan_huyen << {tinh: '79', code:'766', name: 'Quận Tân Bình'}
    danh_sach_quan_huyen << {tinh: '79', code:'767', name: 'Quận Tân Phú'}
    danh_sach_quan_huyen << {tinh: '79', code:'768', name: 'Quận Phú Nhuận'}
    danh_sach_quan_huyen << {tinh: '79', code:'769', name: 'Quận 2'}
    danh_sach_quan_huyen << {tinh: '79', code:'770', name: 'Quận 3'}
    danh_sach_quan_huyen << {tinh: '79', code:'771', name: 'Quận 10'}
    danh_sach_quan_huyen << {tinh: '79', code:'772', name: 'Quận 11'}
    danh_sach_quan_huyen << {tinh: '79', code:'773', name: 'Quận 4'}
    danh_sach_quan_huyen << {tinh: '79', code:'774', name: 'Quận 5'}
    danh_sach_quan_huyen << {tinh: '79', code:'775', name: 'Quận 6'}
    danh_sach_quan_huyen << {tinh: '79', code:'776', name: 'Quận 8'}
    danh_sach_quan_huyen << {tinh: '79', code:'777', name: 'Quận Bình Tân'}
    danh_sach_quan_huyen << {tinh: '79', code:'778', name: 'Quận 7'}
    danh_sach_quan_huyen << {tinh: '79', code:'783', name: 'Huyện Củ Chi'}
    danh_sach_quan_huyen << {tinh: '79', code:'784', name: 'Huyện Hóc Môn'}
    danh_sach_quan_huyen << {tinh: '79', code:'785', name: 'Huyện Bình Chánh'}
    danh_sach_quan_huyen << {tinh: '79', code:'786', name: 'Huyện Nhà Bè'}
    danh_sach_quan_huyen << {tinh: '79', code:'787', name: 'Huyện Cần Giờ'}
    danh_sach_quan_huyen << {tinh: '80', code:'794', name: 'Thành phố Tân An'}
    danh_sach_quan_huyen << {tinh: '80', code:'795', name: 'Thị xã Kiến Tường'}
    danh_sach_quan_huyen << {tinh: '80', code:'796', name: 'Huyện Tân Hưng'}
    danh_sach_quan_huyen << {tinh: '80', code:'797', name: 'Huyện Vĩnh Hưng'}
    danh_sach_quan_huyen << {tinh: '80', code:'798', name: 'Huyện Mộc Hóa'}
    danh_sach_quan_huyen << {tinh: '80', code:'799', name: 'Huyện Tân Thạnh'}
    danh_sach_quan_huyen << {tinh: '80', code:'800', name: 'Huyện Thạnh Hóa'}
    danh_sach_quan_huyen << {tinh: '80', code:'801', name: 'Huyện Đức Huệ'}
    danh_sach_quan_huyen << {tinh: '80', code:'802', name: 'Huyện Đức Hòa'}
    danh_sach_quan_huyen << {tinh: '80', code:'803', name: 'Huyện Bến Lức'}
    danh_sach_quan_huyen << {tinh: '80', code:'804', name: 'Huyện Thủ Thừa'}
    danh_sach_quan_huyen << {tinh: '80', code:'805', name: 'Huyện Tân Trụ'}
    danh_sach_quan_huyen << {tinh: '80', code:'806', name: 'Huyện Cần Đước'}
    danh_sach_quan_huyen << {tinh: '80', code:'807', name: 'Huyện Cần Giuộc'}
    danh_sach_quan_huyen << {tinh: '80', code:'808', name: 'Huyện Châu Thành'}
    danh_sach_quan_huyen << {tinh: '82', code:'815', name: 'Thành phố Mỹ Tho'}
    danh_sach_quan_huyen << {tinh: '82', code:'816', name: 'Thị xã Gò Công'}
    danh_sach_quan_huyen << {tinh: '82', code:'817', name: 'Thị xã Cai Lậy'}
    danh_sach_quan_huyen << {tinh: '82', code:'818', name: 'Huyện Tân Phước'}
    danh_sach_quan_huyen << {tinh: '82', code:'819', name: 'Huyện Cái Bè'}
    danh_sach_quan_huyen << {tinh: '82', code:'820', name: 'Huyện Cai Lậy'}
    danh_sach_quan_huyen << {tinh: '82', code:'821', name: 'Huyện Châu Thành'}
    danh_sach_quan_huyen << {tinh: '82', code:'822', name: 'Huyện Chợ Gạo'}
    danh_sach_quan_huyen << {tinh: '82', code:'823', name: 'Huyện Gò Công Tây'}
    danh_sach_quan_huyen << {tinh: '82', code:'824', name: 'Huyện Gò Công Đông'}
    danh_sach_quan_huyen << {tinh: '82', code:'825', name: 'Huyện Tân Phú Đông'}
    danh_sach_quan_huyen << {tinh: '83', code:'829', name: 'Thành phố Bến Tre'}
    danh_sach_quan_huyen << {tinh: '83', code:'831', name: 'Huyện Châu Thành'}
    danh_sach_quan_huyen << {tinh: '83', code:'832', name: 'Huyện Chợ Lách'}
    danh_sach_quan_huyen << {tinh: '83', code:'833', name: 'Huyện Mỏ Cày Nam'}
    danh_sach_quan_huyen << {tinh: '83', code:'834', name: 'Huyện Giồng Trôm'}
    danh_sach_quan_huyen << {tinh: '83', code:'835', name: 'Huyện Bình Đại'}
    danh_sach_quan_huyen << {tinh: '83', code:'836', name: 'Huyện Ba Tri'}
    danh_sach_quan_huyen << {tinh: '83', code:'837', name: 'Huyện Thạnh Phú'}
    danh_sach_quan_huyen << {tinh: '83', code:'838', name: 'Huyện Mỏ Cày Bắc'}
    danh_sach_quan_huyen << {tinh: '84', code:'842', name: 'Thành phố Trà Vinh'}
    danh_sach_quan_huyen << {tinh: '84', code:'844', name: 'Huyện Càng Long'}
    danh_sach_quan_huyen << {tinh: '84', code:'845', name: 'Huyện Cầu Kè'}
    danh_sach_quan_huyen << {tinh: '84', code:'846', name: 'Huyện Tiểu Cần'}
    danh_sach_quan_huyen << {tinh: '84', code:'847', name: 'Huyện Châu Thành'}
    danh_sach_quan_huyen << {tinh: '84', code:'848', name: 'Huyện Cầu Ngang'}
    danh_sach_quan_huyen << {tinh: '84', code:'849', name: 'Huyện Trà Cú'}
    danh_sach_quan_huyen << {tinh: '84', code:'850', name: 'Huyện Duyên Hải'}
    danh_sach_quan_huyen << {tinh: '84', code:'851', name: 'Thị xã Duyên Hải'}
    danh_sach_quan_huyen << {tinh: '86', code:'855', name: 'Thành phố Vĩnh Long'}
    danh_sach_quan_huyen << {tinh: '86', code:'857', name: 'Huyện Long Hồ'}
    danh_sach_quan_huyen << {tinh: '86', code:'858', name: 'Huyện Mang Thít'}
    danh_sach_quan_huyen << {tinh: '86', code:'859', name: 'Huyện  Vũng Liêm'}
    danh_sach_quan_huyen << {tinh: '86', code:'860', name: 'Huyện Tam Bình'}
    danh_sach_quan_huyen << {tinh: '86', code:'861', name: 'Thị xã Bình Minh'}
    danh_sach_quan_huyen << {tinh: '86', code:'862', name: 'Huyện Trà Ôn'}
    danh_sach_quan_huyen << {tinh: '86', code:'863', name: 'Huyện Bình Tân'}
    danh_sach_quan_huyen << {tinh: '87', code:'866', name: 'Thành phố Cao Lãnh'}
    danh_sach_quan_huyen << {tinh: '87', code:'867', name: 'Thành phố Sa Đéc'}
    danh_sach_quan_huyen << {tinh: '87', code:'868', name: 'Thị xã Hồng Ngự'}
    danh_sach_quan_huyen << {tinh: '87', code:'869', name: 'Huyện Tân Hồng'}
    danh_sach_quan_huyen << {tinh: '87', code:'870', name: 'Huyện Hồng Ngự'}
    danh_sach_quan_huyen << {tinh: '87', code:'871', name: 'Huyện Tam Nông'}
    danh_sach_quan_huyen << {tinh: '87', code:'872', name: 'Huyện Tháp Mười'}
    danh_sach_quan_huyen << {tinh: '87', code:'873', name: 'Huyện Cao Lãnh'}
    danh_sach_quan_huyen << {tinh: '87', code:'874', name: 'Huyện Thanh Bình'}
    danh_sach_quan_huyen << {tinh: '87', code:'875', name: 'Huyện Lấp Vò'}
    danh_sach_quan_huyen << {tinh: '87', code:'876', name: 'Huyện Lai Vung'}
    danh_sach_quan_huyen << {tinh: '87', code:'877', name: 'Huyện Châu Thành'}
    danh_sach_quan_huyen << {tinh: '89', code:'883', name: 'Thành phố Long Xuyên'}
    danh_sach_quan_huyen << {tinh: '89', code:'884', name: 'Thành phố Châu Đốc'}
    danh_sach_quan_huyen << {tinh: '89', code:'886', name: 'Huyện An Phú'}
    danh_sach_quan_huyen << {tinh: '89', code:'887', name: 'Thị xã Tân Châu'}
    danh_sach_quan_huyen << {tinh: '89', code:'888', name: 'Huyện Phú Tân'}
    danh_sach_quan_huyen << {tinh: '89', code:'889', name: 'Huyện Châu Phú'}
    danh_sach_quan_huyen << {tinh: '89', code:'890', name: 'Huyện Tịnh Biên'}
    danh_sach_quan_huyen << {tinh: '89', code:'891', name: 'Huyện Tri Tôn'}
    danh_sach_quan_huyen << {tinh: '89', code:'892', name: 'Huyện Châu Thành'}
    danh_sach_quan_huyen << {tinh: '89', code:'893', name: 'Huyện Chợ Mới'}
    danh_sach_quan_huyen << {tinh: '89', code:'894', name: 'Huyện Thoại Sơn'}
    danh_sach_quan_huyen << {tinh: '91', code:'899', name: 'Thành phố Rạch Giá'}
    danh_sach_quan_huyen << {tinh: '91', code:'900', name: 'Thị xã Hà Tiên'}
    danh_sach_quan_huyen << {tinh: '91', code:'902', name: 'Huyện Kiên Lương'}
    danh_sach_quan_huyen << {tinh: '91', code:'903', name: 'Huyện Hòn Đất'}
    danh_sach_quan_huyen << {tinh: '91', code:'904', name: 'Huyện Tân Hiệp'}
    danh_sach_quan_huyen << {tinh: '91', code:'905', name: 'Huyện Châu Thành'}
    danh_sach_quan_huyen << {tinh: '91', code:'906', name: 'Huyện Giồng Riềng'}
    danh_sach_quan_huyen << {tinh: '91', code:'907', name: 'Huyện Gò Quao'}
    danh_sach_quan_huyen << {tinh: '91', code:'908', name: 'Huyện An Biên'}
    danh_sach_quan_huyen << {tinh: '91', code:'909', name: 'Huyện An Minh'}
    danh_sach_quan_huyen << {tinh: '91', code:'910', name: 'Huyện Vĩnh Thuận'}
    danh_sach_quan_huyen << {tinh: '91', code:'911', name: 'Huyện Phú Quốc'}
    danh_sach_quan_huyen << {tinh: '91', code:'912', name: 'Huyện Kiên Hải'}
    danh_sach_quan_huyen << {tinh: '91', code:'913', name: 'Huyện U Minh Thượng'}
    danh_sach_quan_huyen << {tinh: '91', code:'914', name: 'Huyện Giang Thành'}
    danh_sach_quan_huyen << {tinh: '92', code:'916', name: 'Quận Ninh Kiều'}
    danh_sach_quan_huyen << {tinh: '92', code:'917', name: 'Quận Ô Môn'}
    danh_sach_quan_huyen << {tinh: '92', code:'918', name: 'Quận Bình Thuỷ'}
    danh_sach_quan_huyen << {tinh: '92', code:'919', name: 'Quận Cái Răng'}
    danh_sach_quan_huyen << {tinh: '92', code:'923', name: 'Quận Thốt Nốt'}
    danh_sach_quan_huyen << {tinh: '92', code:'924', name: 'Huyện Vĩnh Thạnh'}
    danh_sach_quan_huyen << {tinh: '92', code:'925', name: 'Huyện Cờ Đỏ'}
    danh_sach_quan_huyen << {tinh: '92', code:'926', name: 'Huyện Phong Điền'}
    danh_sach_quan_huyen << {tinh: '92', code:'927', name: 'Huyện Thới Lai'}
    danh_sach_quan_huyen << {tinh: '93', code:'930', name: 'Thành phố Vị Thanh'}
    danh_sach_quan_huyen << {tinh: '93', code:'931', name: 'Thị xã Ngã Bảy'}
    danh_sach_quan_huyen << {tinh: '93', code:'932', name: 'Huyện Châu Thành A'}
    danh_sach_quan_huyen << {tinh: '93', code:'933', name: 'Huyện Châu Thành'}
    danh_sach_quan_huyen << {tinh: '93', code:'934', name: 'Huyện Phụng Hiệp'}
    danh_sach_quan_huyen << {tinh: '93', code:'935', name: 'Huyện Vị Thuỷ'}
    danh_sach_quan_huyen << {tinh: '93', code:'936', name: 'Huyện Long Mỹ'}
    danh_sach_quan_huyen << {tinh: '93', code:'937', name: 'Thị xã Long Mỹ'}
    danh_sach_quan_huyen << {tinh: '94', code:'941', name: 'Thành phố Sóc Trăng'}
    danh_sach_quan_huyen << {tinh: '94', code:'942', name: 'Huyện Châu Thành'}
    danh_sach_quan_huyen << {tinh: '94', code:'943', name: 'Huyện Kế Sách'}
    danh_sach_quan_huyen << {tinh: '94', code:'944', name: 'Huyện Mỹ Tú'}
    danh_sach_quan_huyen << {tinh: '94', code:'945', name: 'Huyện Cù Lao Dung'}
    danh_sach_quan_huyen << {tinh: '94', code:'946', name: 'Huyện Long Phú'}
    danh_sach_quan_huyen << {tinh: '94', code:'947', name: 'Huyện Mỹ Xuyên'}
    danh_sach_quan_huyen << {tinh: '94', code:'948', name: 'Thị xã Ngã Năm'}
    danh_sach_quan_huyen << {tinh: '94', code:'949', name: 'Huyện Thạnh Trị'}
    danh_sach_quan_huyen << {tinh: '94', code:'950', name: 'Thị xã Vĩnh Châu'}
    danh_sach_quan_huyen << {tinh: '94', code:'951', name: 'Huyện Trần Đề'}
    danh_sach_quan_huyen << {tinh: '95', code:'954', name: 'Thành phố Bạc Liêu'}
    danh_sach_quan_huyen << {tinh: '95', code:'956', name: 'Huyện Hồng Dân'}
    danh_sach_quan_huyen << {tinh: '95', code:'957', name: 'Huyện Phước Long'}
    danh_sach_quan_huyen << {tinh: '95', code:'958', name: 'Huyện Vĩnh Lợi'}
    danh_sach_quan_huyen << {tinh: '95', code:'959', name: 'Thị xã Giá Rai'}
    danh_sach_quan_huyen << {tinh: '95', code:'960', name: 'Huyện Đông Hải'}
    danh_sach_quan_huyen << {tinh: '95', code:'961', name: 'Huyện Hoà Bình'}
    danh_sach_quan_huyen << {tinh: '96', code:'964', name: 'Thành phố Cà Mau'}
    danh_sach_quan_huyen << {tinh: '96', code:'966', name: 'Huyện U Minh'}
    danh_sach_quan_huyen << {tinh: '96', code:'967', name: 'Huyện Thới Bình'}
    danh_sach_quan_huyen << {tinh: '96', code:'968', name: 'Huyện Trần Văn Thời'}
    danh_sach_quan_huyen << {tinh: '96', code:'969', name: 'Huyện Cái Nước'}
    danh_sach_quan_huyen << {tinh: '96', code:'970', name: 'Huyện Đầm Dơi'}
    danh_sach_quan_huyen << {tinh: '96', code:'971', name: 'Huyện Năm Căn'}
    danh_sach_quan_huyen << {tinh: '96', code:'972', name: 'Huyện Phú Tân'}
    danh_sach_quan_huyen << {tinh: '96', code:'973', name: 'Huyện Ngọc Hiển'}
    danh_sach_quan_huyen << {tinh: '999', code:'999', name: 'Khác'}

    danh_sach_ma_tinh = Province.all
    danh_sach_ma_tinh.each do |tinh|
      danh_sach_quan_huyen.each do |quan_huyen|
        quan_huyen_se_import = []
        if quan_huyen[:tinh] == tinh.code
          cap = 0
          if quan_huyen[:name].include?('Huyện')
            cap = 1
          elsif quan_huyen[:name].include?('Quận')
            cap = 2
          elsif quan_huyen[:name].include?('Thành phố')
            cap = 3
          end
          quan_huyen_se_import << District.new(code: quan_huyen[:code], name: quan_huyen[:name], province_code: tinh.code, level: cap )
          District.import quan_huyen_se_import
        end
      end
    end
  end

  def generate_gioi_tinh
    danh_sach_gioi_tinh = []
    danh_sach_gioi_tinh << Gender.new(code: 'nam', name: 'Nam', description: 'giới tính nam')
    danh_sach_gioi_tinh << Gender.new(code: 'nu', name: 'Nữ', description: 'giới tính nữ')
    danh_sach_gioi_tinh << Gender.new(code: 'khac', name: 'Khác', description: 'giới tính khác')
    Gender.import danh_sach_gioi_tinh
  end

  def generate_holiday_type
    holiday_types = []
    holiday_types << HolidayType.new(name: "Ngày lễ", code: 'normal_holiday')
    holiday_types << HolidayType.new(name: "Ngày tết", code: 'tet_holiday')
    HolidayType.import holiday_types
  end

  def generate_user_type
    user_types = []
    user_types << UserType.new(name: 'Quản trị', code: 'admin', description: 'Loại tài khoản quản trị', belong_to_branch: false)
    user_types << UserType.new(name: 'Cán bộ chi nhánh', code: 'employee', description: 'Cán bộ chi nhánh', belong_to_branch: true)
    user_types << UserType.new(name: 'Giáo viên', code: 'teacher', description: 'Giáo viên', belong_to_branch: true)
    user_types << UserType.new(name: 'Học sinh', code: 'student', description: 'Học sinh', belong_to_branch: true)
    user_types << UserType.new(name: 'Phụ huynh', code: 'parent', description: 'Phụ huynh', belong_to_branch: true)
    UserType.import user_types
  end
end