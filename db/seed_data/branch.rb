require 'csv'

module BranchData
  def generate     
    data =  CSV.read("db/seed_data/ds_chi_nhanh.csv")
    branches = []     
    user = User.first

    data.each_with_index do |row, index|
      if index == 0 then next end
      code     = row[0]
      name     = row[1]
      address  = row[2]
      phone    = row[3]
      fax      = row[4]
      manager  = row[5]
      branches << Branch.create(code: code, manager: user, name: name, address: address, phone: phone ? phone.gsub(/[\.\(\)\ \-]*/, "") : 0, email: "#{index}#{user.email}", description: '')
    end
    # Branch.import branches
    branches
  end
end
