# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_10_23_074047) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "attendance_courses", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "course_id"
    t.date "check_date"
    t.jsonb "attendance_list", default: {}
    t.uuid "creator_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["course_id"], name: "index_attendance_courses_on_course_id"
    t.index ["creator_id"], name: "index_attendance_courses_on_creator_id"
  end

  create_table "branches", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", default: "", null: false
    t.string "address", default: "", null: false
    t.string "phone", default: "", null: false
    t.string "email", default: "", null: false
    t.string "description", default: "", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "fax"
    t.uuid "manager_id"
  end

  create_table "certificates", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.string "description"
  end

  create_table "class_grades", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "description", default: "", null: false
    t.string "code"
    t.integer "number_of_courses", default: 0, null: false
    t.date "start_date"
    t.date "opening_date"
    t.date "end_date"
    t.boolean "is_end", default: false
    t.uuid "branch_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["branch_id"], name: "index_class_grades_on_branch_id"
  end

  create_table "course_schedules", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "course_id"
    t.time "monday_start"
    t.time "monday_end"
    t.time "tuesday_start"
    t.time "tuesday_end"
    t.time "wednesday_start"
    t.time "wednesday_end"
    t.time "thursday_start"
    t.time "thursday_end"
    t.time "friday_start"
    t.time "friday_end"
    t.time "saturday_start"
    t.time "saturday_end"
    t.time "sunday_start"
    t.time "sunday_end"
    t.index ["course_id"], name: "index_course_schedules_on_course_id"
  end

  create_table "courses", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "code", default: "", null: false
    t.string "name", default: "", null: false
    t.string "room", default: "", null: false
    t.integer "course_no", default: 0, null: false
    t.integer "number_of_students", default: 0, null: false
    t.date "start_date"
    t.date "opening_date"
    t.date "end_date"
    t.boolean "is_end", default: false
    t.uuid "curriculum_id"
    t.uuid "teacher_id"
    t.uuid "second_teacher_id"
    t.string "description", default: "", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.uuid "branch_id"
    t.float "tuition", default: 0.0, null: false
    t.integer "number_of_days", default: 0, null: false
    t.boolean "is_cancel", default: false
    t.uuid "class_grade_id"
    t.index ["code"], name: "index_courses_on_code", unique: true
    t.index ["curriculum_id"], name: "index_courses_on_curriculum_id"
    t.index ["second_teacher_id"], name: "index_courses_on_second_teacher_id"
    t.index ["teacher_id"], name: "index_courses_on_teacher_id"
  end

  create_table "curriculums", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "code", default: "", null: false
    t.string "name", default: "", null: false
    t.integer "number_of_courses", default: 0, null: false
    t.integer "days_per_course", default: 0, null: false
    t.float "fee_per_course", default: 0.0, null: false
    t.string "description", default: "", null: false
    t.uuid "level_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["code"], name: "index_curriculums_on_code", unique: true
    t.index ["level_id"], name: "index_curriculums_on_level_id"
  end

  create_table "districts", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "code", default: "", null: false
    t.string "name", default: "", null: false
    t.string "description", default: "", null: false
    t.integer "level", default: 0
    t.string "province_code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["province_code"], name: "index_districts_on_province_code"
  end

  create_table "folks", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "code", default: "", null: false
    t.string "name", default: "", null: false
    t.string "other_name", default: "", null: false
    t.string "desciption", default: "", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["code"], name: "index_folks_on_code", unique: true
  end

  create_table "genders", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "code", default: "", null: false
    t.string "name", default: "", null: false
    t.string "description", default: "", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["code"], name: "index_genders_on_code", unique: true
  end

  create_table "holiday_types", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "holidays", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.date "date_start"
    t.date "date_end"
    t.string "description", default: ""
    t.uuid "holiday_type_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["holiday_type_id"], name: "index_holidays_on_holiday_type_id"
  end

  create_table "levels", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "code", default: "", null: false
    t.string "name", default: "", null: false
    t.string "description", default: "", null: false
    t.uuid "major_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["code"], name: "index_levels_on_code", unique: true
    t.index ["major_id"], name: "index_levels_on_major_id"
  end

  create_table "literacies", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.string "description"
  end

  create_table "majors", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "code", default: "", null: false
    t.string "name", default: "", null: false
    t.string "description", default: "", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["code"], name: "index_majors_on_code", unique: true
  end

  create_table "objectives", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.string "description"
  end

  create_table "parents", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "full_name", default: "", null: false
    t.string "phone", default: "", null: false
    t.string "email", default: "", null: false
    t.string "address", default: "", null: false
    t.string "relation", default: "", null: false
    t.text "description", default: "", null: false
    t.datetime "birthday"
    t.string "current_job", default: "", null: false
    t.uuid "gender_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["gender_id"], name: "index_parents_on_gender_id"
  end

  create_table "permission_and_permission_groups", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "permission_id"
    t.uuid "permission_group_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["permission_group_id"], name: "index_permission_and_permission_groups_on_permission_group_id"
    t.index ["permission_id"], name: "index_permission_and_permission_groups_on_permission_id"
  end

  create_table "permission_and_user_groups", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "permission_id"
    t.uuid "user_group_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["permission_id"], name: "index_permission_and_user_groups_on_permission_id"
    t.index ["user_group_id"], name: "index_permission_and_user_groups_on_user_group_id"
  end

  create_table "permission_groups", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.uuid "user_type_id"
    t.index ["user_type_id"], name: "index_permission_groups_on_user_type_id"
  end

  create_table "permissions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.string "code", null: false
    t.text "description", default: "", null: false
    t.uuid "user_types", default: [], array: true
    t.index ["code"], name: "index_permissions_on_code", unique: true
    t.index ["user_types"], name: "index_permissions_on_user_types"
  end

  create_table "provinces", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "code", default: "", null: false
    t.string "name", default: "", null: false
    t.string "description", default: "", null: false
    t.integer "level", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["code"], name: "index_provinces_on_code", unique: true
  end

  create_table "receipts", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "student_course_id"
    t.string "paid_person_name"
    t.bigint "actual_paid_amount", default: 0
    t.string "money_in_words", default: ""
    t.string "description_receipt", default: ""
    t.string "description_internal", default: ""
    t.uuid "creator_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "gift", default: ""
    t.integer "discount_number", default: 0, null: false
    t.integer "discount_percentage", default: 0, null: false
    t.integer "credit_used", default: 0, null: false
    t.boolean "is_cancel", default: false, null: false
    t.index ["creator_id"], name: "index_receipts_on_creator_id"
    t.index ["student_course_id"], name: "index_receipts_on_student_course_id"
  end

  create_table "reference_sources", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.string "description"
  end

  create_table "schools", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.string "description"
    t.uuid "literacies_id"
    t.index ["literacies_id"], name: "index_schools_on_literacies_id"
  end

  create_table "student_certificates", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "student_id"
    t.uuid "certificate_id"
    t.index ["certificate_id"], name: "index_student_certificates_on_certificate_id"
    t.index ["student_id"], name: "index_student_certificates_on_student_id"
  end

  create_table "student_courses", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "student_id"
    t.uuid "course_id"
    t.date "start_date"
    t.uuid "transfer_from_course_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "is_transferred", default: false, null: false
    t.integer "days_late", default: 0, null: false
    t.float "credit", default: 0.0, null: false
    t.date "stopped_date"
    t.boolean "is_cancel", default: false
    t.index ["course_id"], name: "index_student_courses_on_course_id"
    t.index ["student_id"], name: "index_student_courses_on_student_id"
    t.index ["transfer_from_course_id"], name: "index_student_courses_on_transfer_from_course_id"
  end

  create_table "student_objectives", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "student_id"
    t.uuid "objective_id"
    t.index ["objective_id"], name: "index_student_objectives_on_objective_id"
    t.index ["student_id"], name: "index_student_objectives_on_student_id"
  end

  create_table "student_parents", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "student_id"
    t.uuid "parent_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["parent_id"], name: "index_student_parents_on_parent_id"
    t.index ["student_id"], name: "index_student_parents_on_student_id"
  end

  create_table "student_reference_sources", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "student_id"
    t.uuid "reference_source_id"
    t.index ["reference_source_id"], name: "index_student_reference_sources_on_reference_source_id"
    t.index ["student_id"], name: "index_student_reference_sources_on_student_id"
  end

  create_table "students", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "phone", default: "", null: false
    t.string "address", default: "", null: false
    t.text "description", default: "", null: false
    t.string "literacy", default: "", null: false
    t.date "birthday"
    t.string "study_purpose", default: "", null: false
    t.string "current_job", default: "", null: false
    t.string "referred_from", default: "", null: false
    t.integer "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.uuid "user_id"
    t.string "code", null: false
    t.integer "credit", default: 0, null: false
    t.string "class_name", default: "", null: false
    t.uuid "school_id"
    t.index ["code"], name: "index_students_on_code", unique: true
  end

  create_table "teacher_courses", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "course_id"
    t.uuid "teacher_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["course_id"], name: "index_teacher_courses_on_course_id"
    t.index ["teacher_id"], name: "index_teacher_courses_on_teacher_id"
  end

  create_table "teacher_statuses", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "code", default: "", null: false
    t.string "name", default: "", null: false
    t.string "description", default: "", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "teachers", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "specialized", default: "", null: false
    t.uuid "teacher_status_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.uuid "user_id"
    t.index ["teacher_status_id"], name: "index_teachers_on_teacher_status_id"
  end

  create_table "user_and_user_groups", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.uuid "user_group_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_group_id"], name: "index_user_and_user_groups_on_user_group_id"
    t.index ["user_id"], name: "index_user_and_user_groups_on_user_id"
  end

  create_table "user_branches", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.uuid "branch_id"
    t.boolean "is_approver", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["branch_id"], name: "index_user_branches_on_branch_id"
    t.index ["user_id"], name: "index_user_branches_on_user_id"
  end

  create_table "user_groups", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "branch_id"
    t.string "name", null: false
    t.text "description"
    t.string "code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.uuid "user_type_id"
    t.index ["branch_id"], name: "index_user_groups_on_branch_id"
    t.index ["user_type_id"], name: "index_user_groups_on_user_type_id"
  end

  create_table "user_types", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", default: "", null: false
    t.string "code", default: "", null: false
    t.string "description", default: "", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "belong_to_branch", default: false
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "username", default: "", null: false
    t.string "full_name", default: "", null: false
    t.string "phone", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.uuid "gender_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "must_change_password", default: false
    t.string "initial_password", default: ""
    t.uuid "user_type_id"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["gender_id"], name: "index_users_on_gender_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
    t.index ["user_type_id"], name: "index_users_on_user_type_id"
  end

  create_table "wards", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "code", default: "", null: false
    t.string "name", default: "", null: false
    t.string "description", default: "", null: false
    t.integer "level", default: 0
    t.string "district_code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["district_code"], name: "index_wards_on_district_code"
  end

  create_table "whitelisted_jwts", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "jti", null: false
    t.string "aud"
    t.datetime "exp", null: false
    t.uuid "user_id", null: false
    t.index ["jti"], name: "index_whitelisted_jwts_on_jti", unique: true
    t.index ["user_id"], name: "index_whitelisted_jwts_on_user_id"
  end

  add_foreign_key "attendance_courses", "courses"
  add_foreign_key "class_grades", "branches"
  add_foreign_key "courses", "curriculums"
  add_foreign_key "curriculums", "levels"
  add_foreign_key "holidays", "holiday_types"
  add_foreign_key "levels", "majors"
  add_foreign_key "parents", "genders"
  add_foreign_key "permission_and_permission_groups", "permission_groups"
  add_foreign_key "permission_and_permission_groups", "permissions"
  add_foreign_key "permission_and_user_groups", "permissions"
  add_foreign_key "permission_and_user_groups", "user_groups"
  add_foreign_key "receipts", "student_courses"
  add_foreign_key "schools", "literacies", column: "literacies_id"
  add_foreign_key "student_certificates", "certificates"
  add_foreign_key "student_certificates", "students"
  add_foreign_key "student_courses", "courses"
  add_foreign_key "student_courses", "students"
  add_foreign_key "student_objectives", "objectives"
  add_foreign_key "student_objectives", "students"
  add_foreign_key "student_parents", "parents"
  add_foreign_key "student_parents", "students"
  add_foreign_key "student_reference_sources", "reference_sources"
  add_foreign_key "student_reference_sources", "students"
  add_foreign_key "teacher_courses", "courses"
  add_foreign_key "teacher_courses", "teachers"
  add_foreign_key "teachers", "teacher_statuses"
  add_foreign_key "user_and_user_groups", "user_groups"
  add_foreign_key "user_and_user_groups", "users"
  add_foreign_key "user_branches", "branches"
  add_foreign_key "user_branches", "users"
  add_foreign_key "whitelisted_jwts", "users", on_delete: :cascade
end
