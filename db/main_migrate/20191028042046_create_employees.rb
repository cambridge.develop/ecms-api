class CreateEmployees < ActiveRecord::Migration[6.0]
  def change
    create_table :employees, id: :uuid do |t|
      t.date :birthday
      t.references :employee_statuses, type: :uuid, index: true, foreign_key: true
      t.references :users, type: :uuid, index: true, foreign_key: true
      t.timestamps
    end
  end
end