class UpdateBranchTable < ActiveRecord::Migration[6.0]
  def change
    add_column :branches, :fax, :integer, default: ''
    add_column :branches, :manager_id, :uuid, index: true, null: true
  end
end
