class CreateCourseReportMatView < ActiveRecord::Migration[6.0]
  def change
    execute <<-SQL
      SET datestyle = "ISO, DMY";
      CREATE MATERIALIZED VIEW course_reports_matview AS
        SELECT *
        FROM
        courses
        WHERE
          start_date >= DATE('#{Date.today.beginning_of_year.to_s}')
          AND start_date <= DATE('#{Date.today.end_of_year.to_s}')
    SQL
  end
end
