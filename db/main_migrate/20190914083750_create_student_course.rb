class CreateStudentCourse < ActiveRecord::Migration[6.0]
  def change
    create_table :student_courses, id: :uuid do |t|
      t.references :student, type: :uuid, index: true, foreign_key: true
      t.references :course, type: :uuid, index: true, foreign_key: true
      t.date :start_date
      t.uuid :transfer_from_course_id, index: true, foreign_key: true
      t.timestamps
    end
  end
end
