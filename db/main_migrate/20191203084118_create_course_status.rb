class CreateCourseStatus < ActiveRecord::Migration[6.0]
  def change
    create_table :course_statuses, id: :uuid do |t|
      t.string :code
      t.string :name
      t.string :description
    end
  end
end
