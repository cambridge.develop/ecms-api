class AddIsLockedToCurriculum < ActiveRecord::Migration[6.0]
  def change
    add_column :curriculums, :is_locked, :boolean, default: false
  end
end
