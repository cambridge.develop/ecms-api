class AddIsLockedToBranch < ActiveRecord::Migration[6.0]
  def change
    add_column :branches, :is_locked, :boolean, default: :false
  end
end
