class CreateProvince < ActiveRecord::Migration[6.0]
  def change
    create_table :provinces, id: :uuid do |t|
      t.string :code, null: false, default: '', unique: true
      t.string :name, null: false, default: ''
      t.string :description, null: false, default: ''
      t.integer :level, default: 0
      t.timestamps
    end
    add_index :provinces, :code, unique: true
  end
end
