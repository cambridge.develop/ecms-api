class UpdateStudentCode < ActiveRecord::Migration[6.0]
  def change
    add_column :students, :code, :string, null: false
    add_index :students, :code, unique: true
  end
end
