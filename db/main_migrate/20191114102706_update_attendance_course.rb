class UpdateAttendanceCourse < ActiveRecord::Migration[6.0]
  def change
    change_column_default :attendance_courses, :attendance_list, []
  end
end
