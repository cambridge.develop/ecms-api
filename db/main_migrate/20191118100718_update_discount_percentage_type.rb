class UpdateDiscountPercentageType < ActiveRecord::Migration[6.0]
  def change
    change_column :receipts, :discount_percentage, :float
  end
end
