class UpdateStudentCourse < ActiveRecord::Migration[6.0]
  def change
    add_column :student_courses, :is_transferred, :boolean, null: false, default: false
    add_column :student_courses, :days_late, :integer, null: false, default: 0
    add_column :student_courses, :debt, :float, null: false, default: 0
  end
end
