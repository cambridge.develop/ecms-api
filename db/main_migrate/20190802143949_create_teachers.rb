class CreateTeachers < ActiveRecord::Migration[6.0]
  def change
    create_table :teachers, id: :uuid do |t|
      t.string :code, null: false, default: ''
      t.string :full_name, null: false, default: ''
      t.string :specialized, null: false, default: ''
      t.string :address, null: false, default: ''
      t.string :email, null: false, default: ''
      t.string :phone, null: false, default: ''
      t.references :gender, type: :uuid, index: true, foreign_key: true
      t.references :teacher_status, type: :uuid, index: true, foreign_key: true
      t.timestamps

    end
  end
end
