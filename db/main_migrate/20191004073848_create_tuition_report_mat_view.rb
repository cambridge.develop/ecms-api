class CreateTuitionReportMatView < ActiveRecord::Migration[6.0]
  def change
    execute <<-SQL
    SET datestyle = "ISO, DMY";
    CREATE MATERIALIZED VIEW tuition_reports_matview AS
      SELECT 
        c.id,
        c.name,
        c.tuition,
        c.branch_id,
        c.number_of_days,
        (c.tuition / c.number_of_days) as cost_per_days,
        c.tuition * (SELECT COUNT (*) 
          FROM student_courses 
          WHERE course_id = c.id 
            AND is_transferred = false
        ) as income,
        c.start_date,
        c.end_date
      FROM courses c INNER JOIN student_courses s ON c.id = s.course_id
      WHERE
        c.start_date >= DATE('#{Date.today.beginning_of_year.to_s}')
        AND c.start_date <= DATE('#{Date.today.end_of_year.to_s}')
      GROUP BY
        c.id
    SQL
  end
end
