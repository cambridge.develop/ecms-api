class AddIsCancelToReceipt < ActiveRecord::Migration[6.0]
  def change
    add_column :receipts , :is_cancel, :boolean, null: false, default: false
  end
end
