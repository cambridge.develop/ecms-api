class CreateHolidays < ActiveRecord::Migration[6.0]
  def change
    create_table :holidays, id: :uuid do |t|
      t.string :name, null: false
      t.date :date_start
      t.date :date_end
      t.string :description, default: ''
      t.references :holiday_type, type: :uuid, index: true, foreign_key: true
      t.timestamps
    end
  end
end
