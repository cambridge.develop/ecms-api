class UpdateEmployee < ActiveRecord::Migration[6.0]
    def up
      remove_reference :employees, :users, index: true, foreign_key: true
      remove_reference :employees, :employee_statuses, index: true, foreign_key: true
      add_reference :employees, :user, index: true, foreign_key: true, type: :uuid
      add_reference :employees, :employee_status, index: true, foreign_key: true, type: :uuid
    end

    def down
      remove_reference :employees, :user, index: true, foreign_key: true
      remove_reference :employees, :employee_status, index: true, foreign_key: true
      add_reference :employees, :users, index: true, foreign_key: true, type: :uuid
      add_reference :employees, :employee_statuses, index: true, foreign_key: true, type: :uuid
    end
end
