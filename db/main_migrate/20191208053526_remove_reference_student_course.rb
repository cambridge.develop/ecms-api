class RemoveReferenceStudentCourse < ActiveRecord::Migration[6.0]
  def change
    remove_reference :student_courses, :student, index: true, foreign_key: true
    remove_reference :student_courses, :course, index: true, foreign_key: true
    add_column :student_courses, :student_id, :uuid, null:false
    add_column :student_courses, :course_id, :uuid, null:false
    add_index :student_courses, :student_id
    add_index :student_courses, :course_id
  end
end
