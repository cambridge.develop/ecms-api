class UpdateCodeForEmployeeAndTeacher < ActiveRecord::Migration[6.0]
  def change
    add_column :employees, :code, :string, default: "", null: false
    add_column :teachers, :code, :string, default: "", null: false
  end
end
