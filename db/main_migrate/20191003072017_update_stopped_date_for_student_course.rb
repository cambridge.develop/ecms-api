class UpdateStoppedDateForStudentCourse < ActiveRecord::Migration[6.0]
  def change
    add_column :student_courses , :stopped_date, :date
  end
end
