class CreatePotentials < ActiveRecord::Migration[6.0]
  def change
    create_table :potentials, id: :uuid do |t|
      t.references :curriculums, type: :uuid, index: true, foreign_key: true
      t.string :description, null: true, default: ''
      t.timestamps
    end
  end
end
