class CreateStudentCertificate < ActiveRecord::Migration[6.0]
  def change
    create_table :student_certificates, id: :uuid do |t|
      t.references :student, type: :uuid, index: true, foreign_key: true
      t.references :certificate, type: :uuid, index: true, foreign_key: true
    end
  end
end
