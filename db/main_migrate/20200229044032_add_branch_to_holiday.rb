class AddBranchToHoliday < ActiveRecord::Migration[6.0]
  def change
    add_column :holidays, :is_apply_all, :boolean, default: false
    add_column :holidays, :branch_id, :uuid, index: true
  end
end
