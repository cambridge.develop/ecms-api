class CreateGifts < ActiveRecord::Migration[6.0]
  def change
    create_table :gifts, id: :uuid do |t|
      t.string :code, null: false, default: '', unique: true
      t.string :name, null: false, default: ''
      t.string :description, null: false, default: ''
      t.boolean :is_locked, default: false
      t.timestamps
    end
    add_index :gifts, :code, unique: true

  end
end
