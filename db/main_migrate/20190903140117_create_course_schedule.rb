class CreateCourseSchedule < ActiveRecord::Migration[6.0]
  def change
    create_table :course_schedules, id: :uuid do |t|
      t.uuid :course_id, index: true, foreign_key: true
      t.time :monday_start, null:true
      t.time :monday_end, null:true
      t.time :tuesday_start, null:true
      t.time :tuesday_end, null:true
      t.time :wednesday_start, null:true
      t.time :wednesday_end, null:true
      t.time :thursday_start, null:true
      t.time :thursday_end, null:true
      t.time :friday_start, null:true
      t.time :friday_end, null:true
      t.time :saturday_start, null:true
      t.time :saturday_end, null:true
      t.time :sunday_start, null:true
      t.time :sunday_end, null:true
    end
  end
end
