class AddFreeTrialToStudentCourse < ActiveRecord::Migration[6.0]
  def change
    add_column :student_courses, :is_free_trial, :boolean, default: false
  end
end
