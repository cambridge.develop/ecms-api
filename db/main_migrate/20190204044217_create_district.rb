class CreateDistrict < ActiveRecord::Migration[6.0]
  def change
    create_table :districts, id: :uuid do |t|
      t.string :code, null: false, default: '', unique: true
      t.string :name, null: false, default: ''
      t.string :description, null: false, default: ''
      t.integer :level, default: 0
      t.string :province_code, index: true, foreign_key: true
      t.timestamps
    end
  end
end
