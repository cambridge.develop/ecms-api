class CreateReferenceSource < ActiveRecord::Migration[6.0]
  def change
    create_table :reference_sources, id: :uuid do |t|
      t.string :name
      t.string :code
    end
  end
end
