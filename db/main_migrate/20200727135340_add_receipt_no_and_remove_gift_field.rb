class AddReceiptNoAndRemoveGiftField < ActiveRecord::Migration[6.0]
  def change
    remove_column :receipts, :gift
    add_column :receipts, :receipt_no, :text, null: false, default: ''
    add_column :receipts, :is_bank, :boolean, null: false, default: false
  end
end
