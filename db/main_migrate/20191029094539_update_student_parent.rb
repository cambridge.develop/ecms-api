class UpdateStudentParent < ActiveRecord::Migration[6.0]
  def up
    add_column :student_parents, :relation, :string, null: false
    remove_column :parents, :relation
  end

  def down
    remove_column :student_parents, :relation
    add_column :parents, :relation, :string, null: false, default: ''
  end
end
