class CreateLevels < ActiveRecord::Migration[6.0]
  def change
    create_table :levels, id: :uuid do |t|
      t.string :code, null: false, default: '', unique: true
      t.string :name, null: false, default: ''
      t.string :description, null: false, default: ''
      t.references :major, type: :uuid, index: true, foreign_key: true
      t.timestamps
    end
    add_index :levels, :code, unique: true
  end
end
