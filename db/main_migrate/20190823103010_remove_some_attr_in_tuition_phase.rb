class RemoveSomeAttrInTuitionPhase < ActiveRecord::Migration[6.0]
  def up
    remove_column :tuition_phases, :fee
    remove_column :tuition_phases, :curriculum_id
  end

  def down
    add_column :tuition_phases, :fee, :money
    add_column :tuition_phases, :curriculum_id, :uuid
    add_index :tuition_phases, :curriculum_id, unique: true
  end

end
