class UpdateSchool < ActiveRecord::Migration[6.0]
  def up
    remove_reference :schools, :literacies, index: true, foreign_key: true
    add_reference :schools, :literacy, index: true, foreign_key: true, type: :uuid
  end

  def down
    remove_reference :schools, :literacy, index: true, foreign_key: true
    add_reference :schools, :literacies, index: true, foreign_key: true, type: :uuid
  end
end
