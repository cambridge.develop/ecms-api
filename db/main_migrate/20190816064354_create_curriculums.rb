class CreateCurriculums < ActiveRecord::Migration[6.0]
  def change
    create_table :curriculums, id: :uuid do |t|
      t.string :code, null: false, default: '', unique: true
      t.string :name, null: false, default: ''
      t.integer :number_of_courses, null: false, default: 0
      t.integer :days_per_course, null: false, default: 0
      t.float :fee_per_course, null: false, default: 0
      t.string :description, null: false, default: ''
      t.references :level, type: :uuid, index: true, foreign_key: true
      t.references :branch, type: :uuid, index: true, foreign_key: true
      t.timestamps
    end
    add_index :curriculums, :code, unique: true
  end
end
