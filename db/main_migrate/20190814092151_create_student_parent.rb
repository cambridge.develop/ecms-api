class CreateStudentParent < ActiveRecord::Migration[6.0]
  def change
    create_table :student_parents, id: :uuid do |t|
      t.references :student, type: :uuid, index: true, foreign_key: true
      t.references :parent, type: :uuid, index: true, foreign_key: true
      t.timestamps
    end
  end
end
