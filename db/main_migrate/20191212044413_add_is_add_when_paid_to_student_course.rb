class AddIsAddWhenPaidToStudentCourse < ActiveRecord::Migration[6.0]
  def change
    add_column :student_courses, :is_add_when_paid, :boolean, default: false
  end
end
