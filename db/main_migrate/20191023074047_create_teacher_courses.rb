class CreateTeacherCourses < ActiveRecord::Migration[6.0]
  def change
    create_table :teacher_courses, id: :uuid do |t|
      t.references :course, type: :uuid, index: true, foreign_key: true
      t.references :teacher, type: :uuid, index: true, foreign_key: true

      t.timestamps
    end
  end
end
