class UpadteCourseCurriculumn < ActiveRecord::Migration[6.0]
  def change
    execute <<-SQL
    DROP materialized VIEW if exists tuition_reports_matview;
    DROP materialized VIEW if exists course_reports_matview;
    SQL
    remove_reference :courses, :curriculum, index: true, foreign_key: true
    add_reference :class_grades, :curriculum, index: true, type: :uuid
  end
end
