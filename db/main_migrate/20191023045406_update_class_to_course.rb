class UpdateClassToCourse < ActiveRecord::Migration[6.0]
  def change
    add_column :courses, :class_grade_id, :uuid
  end
end
