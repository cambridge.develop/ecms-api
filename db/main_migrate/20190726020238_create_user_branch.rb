class CreateUserBranch < ActiveRecord::Migration[6.0]
  def change
    create_table :user_branches, id: :uuid do |t|
      # t.references :user, type: :uuid, index: true, foreign_key: true
      # t.references :branch, type: :uuid, index: true, foreign_key: true
      t.uuid :user_id, index: true
      t.uuid :branch_id, index: true
      t.boolean :is_approver, default: false
      t.timestamps
    end
  end
end
