class AddGiftToReceipt < ActiveRecord::Migration[6.0]
  def change
    add_column :receipts, :gift, :string, default: ''
  end
end
