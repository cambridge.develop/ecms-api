class UpdateCourse < ActiveRecord::Migration[6.0]
  def change
    change_column :courses, :start_date, :date
    change_column :courses, :end_date, :date
    change_column :courses, :opening_date, :date
    rename_column :courses, :curriculums_id, :curriculum_id
  end
end
