class CreateCourses < ActiveRecord::Migration[6.0]
  def change
    create_table :courses, id: :uuid do |t|
      t.string :code, null: false, default: '', unique: true
      t.string :name, null: false, default: ''
      t.string :room, null: false, default: ''
      t.integer :course_no, null: false, default: 0
      t.integer :number_of_students, null: false, default: 0
      t.datetime :start_date
      t.datetime :opening_date
      t.datetime :end_date
      t.boolean :is_end, default: false
      t.references :curriculums, type: :uuid, index: true, foreign_key: true
      t.uuid :teacher_id, index: true, foreign_key: true
      t.uuid :second_teacher_id, index: true, foreign_key: true
      t.jsonb :schedule, null:false, default: '{}'
      t.string :description, null: false, default: ''
      t.timestamps
    end
    add_index :courses, :code, unique: true
  end
end
