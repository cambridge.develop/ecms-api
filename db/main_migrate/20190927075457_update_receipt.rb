class UpdateReceipt < ActiveRecord::Migration[6.0]
  def change
    add_column :receipts, :discount_number, :integer, null: false, default: 0
    add_column :receipts, :discount_percentage, :integer, null: false, default: 0
  end
end
