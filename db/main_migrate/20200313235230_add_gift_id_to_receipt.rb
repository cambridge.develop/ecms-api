class AddGiftIdToReceipt < ActiveRecord::Migration[6.0]
  def change
    add_reference :receipts, :gift, foreign_key: true, index: true, type: :uuid
  end
end
