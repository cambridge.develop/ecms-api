class RemoveScheduleFromCourse < ActiveRecord::Migration[6.0]
  def up
    remove_column :courses, :schedule
  end
  def down
    add_column :courses,:schedule, :jsonb, null:false, default: '{}'
  end
end
