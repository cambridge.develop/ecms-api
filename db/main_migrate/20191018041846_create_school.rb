class CreateSchool < ActiveRecord::Migration[6.0]
  def change
    create_table :schools, id: :uuid do |t|
      t.string :name
      t.string :code
      t.string :description
      t.references :literacies, type: :uuid, index: true, foreign_key: true
    end
  end
end
