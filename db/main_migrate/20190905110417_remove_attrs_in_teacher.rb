class RemoveAttrsInTeacher < ActiveRecord::Migration[6.0]
  def up 
    remove_column :teachers, :gender_id
    remove_column :teachers, :address
    remove_column :teachers, :code
    remove_column :teachers, :email
    remove_column :teachers, :full_name
    remove_column :teachers, :phone

  end

  def down
    add_column :teachers, :gender_id, :uuid, index: true
    add_column :teachers, :address, :string, index: true
    add_column :teachers, :code, :string, index: true
    add_column :teachers, :email, :string, index: true
    add_column :teachers, :full_name, :string, index: true
    add_column :teachers, :phone, :string, index: true
  end

end
