class RemoveAttrInStudent < ActiveRecord::Migration[6.0]
  def up 
    remove_column :students, :code
    remove_column :students, :email
    remove_column :students, :full_name
    remove_column :students, :gender_id
    add_column  :students, :user_id, :uuid, index: true
  end

  def down
    add_column  :students, :code, :string, index: true
    add_column  :students, :email, :string, index: true
    add_column  :students, :full_name, :string, index: true
    add_column  :students, :gender_id, :uuid, index: true
    remove_column :students, :user_id
  end
end
