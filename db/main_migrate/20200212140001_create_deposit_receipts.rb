class CreateDepositReceipts < ActiveRecord::Migration[6.0]
  def change
    create_table :deposit_receipts, id: :uuid do |t|
      t.string :deposit_person_name, default: ""
      t.uuid :creator_id, index: true, null: false
      t.uuid :student_id, index: true, null: false
      t.uuid :branch_id, index: true, null: false
      t.money :amount, default: 0
      t.string :receipt_description, default: ''
      t.string :internal_description, default: ''
      t.timestamps
    
    end
  end
end
