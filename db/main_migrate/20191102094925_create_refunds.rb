class CreateRefunds < ActiveRecord::Migration[6.0]
  def change
    create_table :refunds, id: :uuid do |t|
      t.references :student, type: :uuid, index: true, foreign_key: true
      t.string :description
      t.bigint :withdrawal_amount
      t.boolean :is_cancel, default: false
      t.string :withdrawal_person_name
      t.uuid :creator_id, index: true, foreign_key: true
      t.timestamps
    end
  end
end
