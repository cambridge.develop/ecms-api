class CreateUserAndUserGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :user_and_user_groups, id: :uuid do |t|
      # t.references :user, type: :uuid, index: true, foreign_key: true
      # t.references :user_group, type: :uuid, index: true, foreign_key: true
      t.uuid :user_id, index: true
      t.uuid :user_group_id, index: true
      t.timestamps
    end
  end
end
