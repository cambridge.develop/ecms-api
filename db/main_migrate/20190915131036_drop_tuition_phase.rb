class DropTuitionPhase < ActiveRecord::Migration[6.0]
  def change
    drop_table :tuition_phases
  end
end
