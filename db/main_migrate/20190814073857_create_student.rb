class CreateStudent < ActiveRecord::Migration[6.0]
  def change
    create_table :students, id: :uuid do |t|
      t.string :code, null: false, default: ''
      t.string :full_name, null: false, default: ''
      t.string :phone, null: false, default: ''
      t.string :email, null: false, default: ''
      t.string :address, null: false, default: ''
      t.text :description, null: false, default: ''
      t.string :literacy, null: false, default: ''
      t.date :birthday
      t.string :study_purpose, null: false, default: ''
      t.string :current_job, null: false, default: ''
      t.string :referred_from, null: false, default: ''
      t.integer :status

      t.references :gender, type: :uuid, index: true, foreign_key: true
      t.timestamps
    end
  end
end
