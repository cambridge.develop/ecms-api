class UpdateCourseForTuition < ActiveRecord::Migration[6.0]
  def change
    add_column :courses,:tuition, :float, null:false, default: 0
    add_column :courses,:number_of_days, :integer, null:false, default: 0

  end
end
