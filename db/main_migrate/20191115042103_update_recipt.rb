class UpdateRecipt < ActiveRecord::Migration[6.0]
  def change
    add_column :receipts, :is_paid_for_class, :boolean, default: false
  end
end
