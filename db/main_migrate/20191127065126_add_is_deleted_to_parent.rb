class AddIsDeletedToParent < ActiveRecord::Migration[6.0]
  def change
    add_column :parents, :is_deleted, :boolean, default: false
  end
end
