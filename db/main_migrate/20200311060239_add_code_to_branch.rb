class AddCodeToBranch < ActiveRecord::Migration[6.0]
  def change
    add_column :branches, :code,:string, null: false, default: '', unique: true
  end

end
