class AddTestResultToStudent < ActiveRecord::Migration[6.0]
  def change
    add_column :students, :test_result, :string, default: ''
  end
end
