class CreateUserType < ActiveRecord::Migration[6.0]
  def change
    create_table :user_types, id: :uuid do |t|
      t.string :name, null: false, default: ''
      t.string :code, null: false, default: ''
      t.string :description, null: false, default: ''
      t.timestamps
    end
  end
end
