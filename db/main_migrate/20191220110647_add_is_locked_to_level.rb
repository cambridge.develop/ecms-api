class AddIsLockedToLevel < ActiveRecord::Migration[6.0]
  def change
    add_column :levels, :is_locked, :boolean, default: false
  end
end
