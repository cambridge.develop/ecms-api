class CreateLiteracy < ActiveRecord::Migration[6.0]
  def change
    create_table :literacies, id: :uuid do |t|
      t.string :name
      t.string :code
      t.string :description
    end
  end
end
