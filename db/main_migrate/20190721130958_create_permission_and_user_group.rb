class CreatePermissionAndUserGroup < ActiveRecord::Migration[6.0]
  def change
    create_table :permission_and_user_groups, id: :uuid do |t|
      t.references :permission, type: :uuid, index: true, foreign_key: true
      t.references :user_group, type: :uuid, index: true, foreign_key: true
      t.timestamps
    end
  end
end
