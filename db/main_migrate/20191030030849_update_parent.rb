class UpdateParent < ActiveRecord::Migration[6.0]
  def up
    remove_column :parents, :email
    remove_column :parents, :phone
    remove_column :parents, :full_name
    remove_reference :parents, :gender, index: true, foreign_key: true
    add_reference :parents, :user, index: true, foreign_key: true, type: :uuid
  end

  def down
    add_column :parents, :email, :string, null:false, default: ''
    add_column :parents, :phone, :string, null: false, default: ''
    add_column :parents, :full_name, :string, null: false, default: ''
    add_reference :parents, :gender, index: true, foreign_key: true, type: :uuid
    remove_reference :parents, :user, index: true, foreign_key: true, type: :uuid
  end
end
