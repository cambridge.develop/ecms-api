class CreateReceipt < ActiveRecord::Migration[6.0]
  def change
    create_table :receipts, id: :uuid do |t|
      t.references :student_course, type: :uuid, index: true, foreign_key: true
      t.string :paid_person_name
      t.bigint :actual_paid_amount, default: 0
      t.string :money_in_words, default: ''
      t.string :description_receipt, default: ''
      t.string :description_internal, default: ''
      t.uuid :creator_id, index: true, foreign_key: true
      t.timestamps
    end
  end
end
