class CreatePolicies < ActiveRecord::Migration[6.0]
  def change
    create_table :policies, id: :uuid do |t|
      t.string :name, null:false
      t.string :code, null:false
      t.float :percent_discount
      t.float :amount_discount
      t.string :description
      t.timestamps
    end
  end
end
