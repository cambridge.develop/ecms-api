class AddDescriptionToReferenceSource < ActiveRecord::Migration[6.0]
  def change
    add_column :reference_sources, :description, :string
  end
end
