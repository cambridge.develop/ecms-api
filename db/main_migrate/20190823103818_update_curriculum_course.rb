class UpdateCurriculumCourse < ActiveRecord::Migration[6.0]
  def up
    add_column :courses, :branch_id, :uuid, index: true, null: true
    remove_column :curriculums, :branch_id
  end
  def down
    remove_column :courses, :branch_id
    add_column :curriculums, :branch_id, :uuid, index: true, null: true
  end
end
