class CreateTeacherStatus < ActiveRecord::Migration[6.0]
  def change
    create_table :teacher_statuses, id: :uuid do |t|
      t.string :code, null: false, default: '', unique: true
      t.string :name, null: false, default: ''
      t.string :description, null: false, default: ''
      t.timestamps
    end
  end
end
