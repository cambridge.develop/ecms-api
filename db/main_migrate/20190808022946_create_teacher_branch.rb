class CreateTeacherBranch < ActiveRecord::Migration[6.0]
  def change
    create_table :teacher_branches, id: :uuid do |t|
      t.references :teacher, type: :uuid, index: true, foreign_key: true
      t.references :branch, type: :uuid, index: true, foreign_key: true
      t.timestamps
    end
  end
end
