class UpdateStudent < ActiveRecord::Migration[6.0]
  def up
    remove_columns :students, :literacy, :current_job, :referred_from, :study_purpose
  end

  def down
    add_column :students, :study_purpose, null: false, default: ''
    add_column :students, :current_job, null: false, default: ''
    add_column :students, :referred_from, null: false, default: ''
    add_column :students, :literacy, null: false, default: ''
  end
end
