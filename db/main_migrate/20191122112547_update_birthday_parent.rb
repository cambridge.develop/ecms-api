class UpdateBirthdayParent < ActiveRecord::Migration[6.0]
  def change
    change_column :parents, :birthday, :date
  end
end
