class CreateParent < ActiveRecord::Migration[6.0]
  def change
    create_table :parents, id: :uuid do |t|
      t.string :full_name, null: false, default: ''
      t.string :phone, null: false, default: ''
      t.string :email, null: false, default: ''
      t.string :address, null: false, default: ''
      t.string :relation, null: false, default: ''
      t.text :description, null: false, default: ''
      t.datetime :birthday
      t.string :current_job, null: false, default: ''

      t.references :gender, type: :uuid, index: true, foreign_key: true
      t.timestamps
    end
  end
end
