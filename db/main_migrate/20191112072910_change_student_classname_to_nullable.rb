class ChangeStudentClassnameToNullable < ActiveRecord::Migration[6.0]
  def change
    change_column_null :students, :class_name, true
  end
end
