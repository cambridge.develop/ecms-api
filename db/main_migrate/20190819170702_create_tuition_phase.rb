class CreateTuitionPhase < ActiveRecord::Migration[6.0]
  def change
    create_table :tuition_phases, id: :uuid do |t|
      t.string :code, unique: true, null: false
      t.date :date_start
      t.date :date_end
      t.integer :course_number
      t.money :fee
      t.string :content
      t.references :branch, type: :uuid, null: false, index: true
      t.uuid :curriculum_id, null: false, index: true
      t.uuid :course_id, null: false, index: true
      t.boolean :is_closed, default: false
      t.timestamps
    end
  end
end
