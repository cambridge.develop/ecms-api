class CreateAttendanceCourses < ActiveRecord::Migration[6.0]
  def change
    create_table :attendance_courses, id: :uuid do |t|
      t.references :course, type: :uuid, index: true, foreign_key: true
      t.date :check_date
      t.jsonb :attendance_list, default: {}
      t.uuid :creator_id, index: true, foreign_key: true
      t.timestamps
    end
  end
end
