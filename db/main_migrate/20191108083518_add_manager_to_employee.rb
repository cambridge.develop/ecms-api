class AddManagerToEmployee < ActiveRecord::Migration[6.0]
  def change
    add_column :employees, :is_manager, :boolean, default: false
  end
end
