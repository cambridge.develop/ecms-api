class UpdateStudentPotentials < ActiveRecord::Migration[6.0]
  def change
    add_column :student_potentials, :name,:string, null: false, default: ''
    add_column :student_potentials, :status,:integer, null: false, default: 0
    add_reference :student_potentials, :branch, index: true, foreign_key: true, type: :uuid
    add_reference :student_potentials, :curriculum, index: true, foreign_key: true, type: :uuid
  end
end
