class CreatePermissionAndPermissionGroup < ActiveRecord::Migration[6.0]
  def change
    create_table :permission_and_permission_groups, id: :uuid do |t|
      t.references :permission, type: :uuid, index: true, foreign_key: true
      t.references :permission_group, type: :uuid, index: true, foreign_key: true
      t.timestamps
    end
  end
end
