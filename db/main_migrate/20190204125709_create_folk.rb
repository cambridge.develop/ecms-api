class CreateFolk < ActiveRecord::Migration[6.0]
  def change
    create_table :folks, id: :uuid do |t|
      t.string :code, null: false, default: '', unique: true
      t.string :name, null: false, default: ''
      t.string :other_name, null: false, default: ''
      t.string :desciption, null: false, default: ''
      t.timestamps
    end
    add_index :folks, :code, unique: true
  end
end
