class RemovePotentials < ActiveRecord::Migration[6.0]
  def up
    remove_reference :student_potentials, :potential, index: true, foreign_key: true
    drop_table :potentials
  end

  def down
    create_table :potentials, id: :uuid do |t|
      t.references :curriculums, type: :uuid, index: true, foreign_key: true
      t.string :description, null: true, default: ''
      t.timestamps
    end
    add_reference :student_potentials, :potential, index: true, foreign_key: true, type: :uuid
  end
end
