class AddIsCancelToStudentCourse < ActiveRecord::Migration[6.0]
  def change
    add_column :student_courses, :is_cancel, :boolean, index: true, default: false
  end
end
