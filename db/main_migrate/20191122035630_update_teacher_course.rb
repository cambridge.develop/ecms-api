class UpdateTeacherCourse < ActiveRecord::Migration[6.0]
  def change
    add_column :teacher_courses, :is_main, :boolean, default: false
    add_column :teacher_courses, :start_date, :date
    add_column :teacher_courses, :end_date, :date
  end
end
