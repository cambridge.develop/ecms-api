class CreateGender < ActiveRecord::Migration[6.0]
  def change
    create_table :genders, id: :uuid do |t|
      t.string :code, null: false, default: '', unique: true
      t.string :name, null: false, default: ''
      t.string :description, null: false, default: ''
      t.timestamps
    end
    add_index :genders, :code, unique: true
  end
end
