class AddPaidAmountToReceipt < ActiveRecord::Migration[6.0]
  def change
    add_column :receipts, :paid_amount, :integer, default: 0
  end
end
