class AddUserTypeIntoUserGroupAndPermission < ActiveRecord::Migration[6.0]
  def change
    add_column :permissions, :user_types, :uuid, array: true, index: true, default: []
    add_reference :user_groups, :user_type, type: :uuid, index: true, null: true
    add_reference :permission_groups, :user_type, type: :uuid, index: true, null: true
    
    add_index :permissions, :user_types
    # , using: 'gin'
  end
end
