class UpdateClassGrade < ActiveRecord::Migration[6.0]
  def up
    add_column :class_grades, :number_of_students, :integer
    remove_column :courses, :teacher_id
    remove_column :courses, :second_teacher_id
    remove_column :courses, :branch_id
  end

  def down
    remove_column :class_grades, :number_of_courses
    add_column :courses,:teacher_id, index: true, foreign_key: true
    add_column :courses,:second_teacher_id, index: true, foreign_key: true
    add_column :courses,:branch_id, index: true, foreign_key: true
  end
end
