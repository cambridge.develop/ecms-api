class AddDiscountAmountToStudentCourse < ActiveRecord::Migration[6.0]
  def change
    add_column :student_courses, :discount_amount, :bigint, :default => 0
  end
end
