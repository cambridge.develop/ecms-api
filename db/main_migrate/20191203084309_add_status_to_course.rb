class AddStatusToCourse < ActiveRecord::Migration[6.0]
  def up
    remove_column :courses, :is_cancel
    remove_column :courses, :is_end
    add_reference :courses, :course_status, foreign_key: true, index: true, type: :uuid
  end
  def down
    add_column :courses, :is_cancel, :boolean
    add_column :courses, :is_end, :boolean
    add_reference :courses, :course_status, foreign_key: true, index: true, type: :uuid
  end
end
