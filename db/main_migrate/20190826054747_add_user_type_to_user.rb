class AddUserTypeToUser < ActiveRecord::Migration[6.0]
  def change
    # add_reference :users, :user_type , type: :uuid, index: true, null: true
    add_column :users, :user_type_id, :uuid, null: false, default: '', index: true
  end
end
