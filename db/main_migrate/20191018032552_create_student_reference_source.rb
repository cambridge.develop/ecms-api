class CreateStudentReferenceSource < ActiveRecord::Migration[6.0]
  def change
    create_table :student_reference_sources, id: :uuid do |t|
      t.references :student, type: :uuid, index: true, foreign_key: true
      t.references :reference_source, type: :uuid, index: true, foreign_key: true
    end
  end
end
