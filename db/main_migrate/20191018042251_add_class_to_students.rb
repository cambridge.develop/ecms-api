class AddClassToStudents < ActiveRecord::Migration[6.0]
  def change
    add_column :students, :class_name, :string, null: false, default: ''
  end
end
