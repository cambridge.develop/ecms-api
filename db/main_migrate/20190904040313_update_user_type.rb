class UpdateUserType < ActiveRecord::Migration[6.0]
  def change
    add_column :user_types, :belong_to_branch, :boolean, default: false
  end
end
