class AddCreditToReceipt < ActiveRecord::Migration[6.0]
  def change
    add_column :receipts , :credit_used, :integer, null: false, default: 0
  end
end
