class UpdateCertificate < ActiveRecord::Migration[6.0]
  def change
    add_column :student_certificates, :issue_organization, :string
    add_column :student_certificates, :issue_date, :date
    add_column :student_certificates, :expiration_date, :date
    add_column :student_certificates, :license_number, :string
    add_column :student_certificates, :point, :string
  end
end
