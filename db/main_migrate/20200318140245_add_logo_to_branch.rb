class AddLogoToBranch < ActiveRecord::Migration[6.0]
  def change
    add_column :branches, :logo_base64, :text, null: false, default: ''
    add_column :branches, :logo_name, :text, null: false, default: ''
  end
end
