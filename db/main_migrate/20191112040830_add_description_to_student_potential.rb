class AddDescriptionToStudentPotential < ActiveRecord::Migration[6.0]
  def change
    add_column :student_potentials, :description, :string,null: true, default: ''
  end
end
