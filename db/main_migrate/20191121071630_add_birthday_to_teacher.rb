class AddBirthdayToTeacher < ActiveRecord::Migration[6.0]
  def change
    add_column :teachers, :birthday, :date
  end
end
