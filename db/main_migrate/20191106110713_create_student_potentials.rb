class CreateStudentPotentials < ActiveRecord::Migration[6.0]
  def change
    create_table :student_potentials, id: :uuid do |t|
      t.references :student, type: :uuid, index: true, foreign_key: true
      t.references :potential, type: :uuid, index: true, foreign_key: true
      t.timestamps
    end
  end
end
