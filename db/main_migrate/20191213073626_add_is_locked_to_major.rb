class AddIsLockedToMajor < ActiveRecord::Migration[6.0]
  def change
    add_column :majors, :is_locked, :boolean, default: false
  end
end
