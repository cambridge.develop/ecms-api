class CreatePermission < ActiveRecord::Migration[6.0]
  def change
    create_table :permissions, id: :uuid do |t|
      t.string :name, null: false
      t.string :code, null: false
      t.text :description, null: false, default: ''
    end
    add_index :permissions, :code, unique: true
  end
end
