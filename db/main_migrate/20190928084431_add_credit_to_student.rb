class AddCreditToStudent < ActiveRecord::Migration[6.0]
  def change
    add_column :students, :credit, :integer, null: false, default: 0
  end
end
