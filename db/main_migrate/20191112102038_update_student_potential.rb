class UpdateStudentPotential < ActiveRecord::Migration[6.0]
  def up
    remove_reference :student_potentials, :student, index: true, foreign_key: true
    add_column :student_potentials, :student_id, :uuid
  end

  def down
    add_reference :student_potentials, :student, index: true, foreign_key: true, type: :uuid
    remove_column :student_potentials, :student_id
  end
end
