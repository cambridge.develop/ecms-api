class CreateUserGroup < ActiveRecord::Migration[6.0]
  def change
    create_table :user_groups, id: :uuid do |t|
      t.uuid :branch_id, index: true
      t.string :name, null: false
      t.text :description
      t.string :code
      t.timestamps
    end
  end
end
