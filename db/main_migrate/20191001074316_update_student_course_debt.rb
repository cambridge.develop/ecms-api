class UpdateStudentCourseDebt < ActiveRecord::Migration[6.0]
  def change
    rename_column :student_courses, :debt, :credit
  end
end
