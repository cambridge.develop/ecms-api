class CreateClassGrades < ActiveRecord::Migration[6.0]
  def change
    create_table :class_grades, id: :uuid do |t|
      t.string :name
      t.string :description, null: false, default: ''
      t.string :code
      t.integer :number_of_courses, null: false, default: 0
      t.date :start_date
      t.date :opening_date
      t.date :end_date
      t.boolean :is_end, default: false
      t.references :branch, foreign_key: true, index: true, type: :uuid
      t.timestamps
    end
  end
end
