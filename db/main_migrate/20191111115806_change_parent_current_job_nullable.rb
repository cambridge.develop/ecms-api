class ChangeParentCurrentJobNullable < ActiveRecord::Migration[6.0]
  def change
    change_column_null :parents, :current_job, true
  end
end
