Rails.application.routes.draw do
  devise_for :users,
             path: '',
             path_names: {
                 sign_in: 'login',
                 sign_out: 'logout'
             },
             controllers: {
                 sessions: 'sessions',
                 registrations: 'registrations',
                 confirmations: 'confirmations',
                 passwords: 'passwords'
             }

  namespace :api, as: '' do
    namespace :v1, as: '' do
      namespace :payment, as: '' do
        resources :students do
          member do
            get :courses
            get :classes
            get :paid_classes
            get :credit
            get :deposit_receipts
            put :deposit
          end
        end
        resources :receipts do
          put :cancel, on: :member
          collection do
            post 'create_with_class'
            post 'create_with_course'
            put 'cancel_with_class'
          end
        end
        resources :student_courses do
          get :receipts, on: :member
        end
        resources :refunds do

        end
      end
      namespace :payment_v2, as: '' do
        resources :student_courses do
          collection do
            get :get_student_course
          end
        end
        resources :receipts do
          collection do
            put :cancel_by_receipt_no
          end
        end
        resources :students do
          member do
            get :class_grade
            get :receipts
          end
        end
      end
      namespace :attendance, as: '' do
        resources :class_grades, path: 'classes' do
          get :courses, on: :member
        end
        resources :courses do
          get :student_courses, on: :member
          get :attendance_courses, on: :member
          get :teachers, on: :member
        end
        resources :attendance_courses do
          get :students, on: :member
        end
      end

      namespace :authorization, as: '' do
        resources :permissions
      end

      namespace :master, as: '', path: '' do

        resources :certificates do
          get :students, on: :member
        end

        resources :literacies do
          get :students, on: :member
          get :schools, on: :member
        end

        resources :objectives do
          get :students, on: :member
        end

        resources :reference_sources do
          get :students, on: :member
        end

        resources :schools do
          get :students, on: :member
        end

        resources :teacher_statuses do
          get :teachers, on: :member
        end

        resources :holidays

        resources :holiday_types

        resources :genders

        resources :policies
      end

      resources :majors do
        get :levels, on: :member
        patch :lock, on: :member
        patch :unlock, on: :member
      end

      resources :gifts do
        patch :lock, on: :member
        patch :unlock, on: :member
      end

      resources :levels do
        get :curriculums, on: :member
        patch :lock, on: :member
        patch :unlock, on: :member
      end

      resources :curriculums do
        get :classes, on: :member
        patch :lock, on: :member
        patch :unlock, on: :member
      end

      resources :courses do
        get :students, on: :member
        get :student_courses, on: :member
        get :student_courses_history, on: :member
        get :student_courses_free_trial, on: :member
        get :close, on: :member
        get :cancel, on: :member
        post :copy_student_course, on: :member
      end

      resources :class_grades, path: 'classes' do
        get :courses, on: :member
        post :cancel, on: :member
        get :close
      end

      resources :teachers do
        get :courses, on: :member
        get :classes, on: :member
        get :teacher_courses, on: :member
        get :statistics, on: :member
        put :lock, on: :member
      end

      resources :parents do
        get :students, on: :member
        get :student_parents, on: :member
        put :update_relation, on: :member
        delete :delete, on: :member
      end

      resources :students do
        member do
          get :student_certificates
          # get :courses
          get :student_courses
          get :student_logs
          get :parents
          get :student_parents
          get :student_reference_sources
          get :student_objectives
          get :student_potentials
          get :class_grades
          get :statistics
          put :update_test_result
        end
      end

      resources :branches do
        member do
          get :users
          get :courses
          get :students
          get :teachers
          get :employees
          put :add_user_to_branch
          put :remove_user_in_branch
          get :unadded_users
          patch :lock
          patch :unlock
        end
      end

      resources :student_parents do

      end

      resources :student_certificates do

      end

      resources :student_objectives do

      end

      resources :student_potentials do

      end

      resources :student_reference_sources do

      end

      resources :student_courses do
        post :transfer, on: :member
        delete :cancel_trial, on: :member
        put :convert_trial_to_official, on: :member
      end

      resources :teacher_courses do

      end


      resources :employees do
        # get :permissions, on: :member
      end

      resources :users do
        collection do
          patch 'update_password'
        end
        patch :lock, on: :member
        patch :unlock, on: :member
      end
      resources :permission_groups do
        get :permissions, on: :member
        get :not_in_group_permissions, on: :member
      end

      resources :user_groups do
        member do
          get :users
          get :permissions
          get :not_in_group_permissions
          get :not_in_group_permission_groups
          get :not_in_group_users
        end
      end

      resources :user_and_user_groups do
        collection do
          put :delete_users
        end
      end

      resources :permission_and_permission_groups do
        collection do
          put :delete_permissions
        end
      end

      resources :permission_and_user_groups do
        collection do
          put :delete_permissions
        end
      end
      # put 'auth/change_password', as: 'auth#change_password', as: 'auth_change_password'
      # post 'auth/:id/request_confirm_account', to: 'auth#request_account_confirmation', as: 'auth_request_confirm_account'
      # get 'auth/:id/confirm_account/:token', to: 'auth#confirm_account', as: 'auth_confirm_account'
      post 'auth/reset_password', to: 'auth#reset_password', as: 'auth_reset_password'
      patch 'auth/new_password/:token', to: 'auth#new_password', as: 'auth_new_password'

      resources :select_datas do
        collection do
          get :classes
          get :class_generals
          get :courses
          get :majors
          get :gifts
          get :curriculums
          get :levels
          get :managers
          get :teachers
          get :user_types
          get :student_potential_statuses
          get :students
          get :literacies
          get :holiday_types
          get :genders
          get :branches
          get :employee_statuses
          get :teacher_statuses
          get :course_statuses
        end
      end

      resources :selected_branches do

      end

      namespace :statistics, as: '' do
        namespace :student do
          resource :branches do
            post :day
            post :month
            post :last_period
            get :objectives
            get :reference_sources
            get :literacies
            get :number_of_new_students_in_month_range
          end
          resource :generals do
            post :day
            post :month
            post :last_period
            get :objectives
            get :reference_sources
            get :literacies
          end
        end
      end

      namespace :report, as: '' do
        namespace :income_and_debt do
          resource :branches do
            get :class_grades
          end
        end
        namespace :income do
          resource :branches do
            post :day
            post :month
            post :curriculums
            post :class_grades
          end
          resource :generals do
            post :day
            post :month
            post :curriculums
            post :class_grades
          end
          resource :details do
            post :class_grades
          end
        end
        namespace :discount do
          resource :branches do
            get :students
          end
        end
        namespace :student do
          resource :branches do
            post :day
            post :month
          end
          resource :generals do
            post :day
            post :month
          end
        end
        namespace :debt do
          resource :branches do
            post :class_grades
          end
          resource :generals do
            post :class_grades
          end
          resource :details do
            get :search_student, path: 'students'
            post :students
            post :class_grades
          end
        end
        namespace :trial do
          resource :branches do
            post :class_grades
          end
          resource :generals do
            post :class_grades
          end
          resource :details do
            post :class_grades
          end
        end
      end

      resources :home do
        collection do
          get :global_search
        end
      end

      resources :todos do
        collection do
          get :courses
        end
      end
    end
  end
end
